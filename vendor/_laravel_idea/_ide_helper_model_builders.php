<?php
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
/** @noinspection PhpUnusedAliasInspection */

namespace LaravelIdea\Helper {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Database\ConnectionInterface;
    use Illuminate\Database\Eloquent\Builder;
    use Illuminate\Database\Query\Expression;
    
    /**
     * @see \Illuminate\Database\Query\Builder::select
     * @method $this select(array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::addSelect
     * @method $this addSelect(array $column)
     * @see \Illuminate\Database\Concerns\BuildsQueries::when
     * @method $this when($value, callable $callback, callable|null $default = null)
     * @see \Illuminate\Database\Query\Builder::whereIn
     * @method $this whereIn(string $column, $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::whereJsonLength
     * @method $this whereJsonLength(string $column, $operator, $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereExists
     * @method $this orWhereExists(\Closure $callback, bool $not = false)
     * @see \Illuminate\Database\Query\Builder::orWhereNotIn
     * @method $this orWhereNotIn(string $column, $values)
     * @see \Illuminate\Database\Query\Builder::truncate
     * @method $this truncate()
     * @see \Illuminate\Database\Query\Builder::selectRaw
     * @method $this selectRaw(string $expression, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::insertOrIgnore
     * @method int insertOrIgnore(array $values)
     * @see \Illuminate\Database\Query\Builder::lock
     * @method $this lock(bool|string $value = true)
     * @see \Illuminate\Database\Query\Builder::join
     * @method $this join(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @see \Illuminate\Database\Query\Builder::unionAll
     * @method $this unionAll(\Closure|\Illuminate\Database\Query\Builder $query)
     * @see \Illuminate\Database\Query\Builder::whereMonth
     * @method $this whereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::having
     * @method $this having(string $column, null|string $operator = null, null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereNull
     * @method $this orWhereNull(string $column)
     * @see \Illuminate\Database\Query\Builder::whereNested
     * @method $this whereNested(\Closure $callback, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::joinWhere
     * @method $this joinWhere(string $table, \Closure|string $first, string $operator, string $second, string $type = 'inner')
     * @see \Illuminate\Database\Query\Builder::orWhereJsonContains
     * @method $this orWhereJsonContains(string $column, $value)
     * @see \Illuminate\Database\Query\Builder::orderBy
     * @method $this orderBy(string $column, string $direction = 'asc')
     * @see \Illuminate\Database\Query\Builder::raw
     * @method Expression raw($value)
     * @see \Illuminate\Database\Query\Builder::orWhereRowValues
     * @method $this orWhereRowValues(array $columns, string $operator, array $values)
     * @see \Illuminate\Database\Concerns\BuildsQueries::each
     * @method $this each(callable $callback, int $count = 1000)
     * @see \Illuminate\Database\Query\Builder::setBindings
     * @method $this setBindings(array $bindings, string $type = 'where')
     * @see \Illuminate\Database\Query\Builder::orWhereJsonLength
     * @method $this orWhereJsonLength(string $column, $operator, $value = null)
     * @see \Illuminate\Database\Query\Builder::whereRowValues
     * @method $this whereRowValues(array $columns, string $operator, array $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::useWritePdo
     * @method $this useWritePdo()
     * @see \Illuminate\Database\Query\Builder::orWhereNotExists
     * @method $this orWhereNotExists(\Closure $callback)
     * @see \Illuminate\Database\Query\Builder::orWhereIn
     * @method $this orWhereIn(string $column, $values)
     * @see \Illuminate\Database\Query\Builder::newQuery
     * @method $this newQuery()
     * @see \Illuminate\Database\Query\Builder::rightJoinSub
     * @method $this rightJoinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::crossJoin
     * @method $this crossJoin(string $table, \Closure|null|string $first = null, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::orderByDesc
     * @method $this orderByDesc(string $column)
     * @see \Illuminate\Database\Query\Builder::average
     * @method mixed average(string $column)
     * @see \Illuminate\Database\Query\Builder::orWhereNotNull
     * @method $this orWhereNotNull(string $column)
     * @see \Illuminate\Database\Query\Builder::getProcessor
     * @method $this getProcessor()
     * @see \Illuminate\Database\Query\Builder::skip
     * @method $this skip(int $value)
     * @see \Illuminate\Database\Query\Builder::sum
     * @method int sum(string $column)
     * @see \Illuminate\Database\Query\Builder::havingRaw
     * @method $this havingRaw(string $sql, array $bindings = [], string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::leftJoinWhere
     * @method $this leftJoinWhere(string $table, \Closure|string $first, string $operator, string $second)
     * @see \Illuminate\Database\Query\Builder::getRawBindings
     * @method $this getRawBindings()
     * @see \Illuminate\Database\Query\Builder::orWhereColumn
     * @method $this orWhereColumn(array|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::min
     * @method mixed min(string $column)
     * @see \Illuminate\Support\Traits\Macroable::hasMacro
     * @method $this hasMacro(string $name)
     * @see \Illuminate\Database\Query\Builder::whereNotExists
     * @method $this whereNotExists(\Closure $callback, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereIntegerInRaw
     * @method $this whereIntegerInRaw(string $column, array|Arrayable $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Concerns\BuildsQueries::unless
     * @method $this unless($value, callable $callback, callable|null $default = null)
     * @see \Illuminate\Database\Query\Builder::whereDay
     * @method $this whereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereNotIn
     * @method $this whereNotIn(string $column, $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereTime
     * @method $this whereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::forNestedWhere
     * @method $this forNestedWhere()
     * @see \Illuminate\Database\Query\Builder::insertUsing
     * @method bool insertUsing(array $columns, \Closure|\Illuminate\Database\Query\Builder|string $query)
     * @see \Illuminate\Database\Query\Builder::rightJoinWhere
     * @method $this rightJoinWhere(string $table, \Closure|string $first, string $operator, string $second)
     * @see \Illuminate\Database\Query\Builder::max
     * @method mixed max(string $column)
     * @see \Illuminate\Database\Query\Builder::whereExists
     * @method $this whereExists(\Closure $callback, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::inRandomOrder
     * @method $this inRandomOrder(string $seed = '')
     * @see \Illuminate\Database\Query\Builder::havingBetween
     * @method $this havingBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::union
     * @method $this union(\Closure|\Illuminate\Database\Query\Builder $query, bool $all = false)
     * @see \Illuminate\Database\Query\Builder::groupBy
     * @method $this groupBy(...$groups)
     * @see \Illuminate\Database\Query\Builder::orWhereYear
     * @method $this orWhereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::orWhereDay
     * @method $this orWhereDay(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::joinSub
     * @method $this joinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null, string $type = 'inner', bool $where = false)
     * @see \Illuminate\Database\Query\Builder::whereDate
     * @method $this whereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::whereJsonDoesntContain
     * @method $this whereJsonDoesntContain(string $column, $value, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::forPageAfterId
     * @method $this forPageAfterId(int $perPage = 15, int|null $lastId = 0, string $column = 'id')
     * @see \Illuminate\Database\Query\Builder::forPage
     * @method $this forPage(int $page, int $perPage = 15)
     * @see \Illuminate\Database\Query\Builder::exists
     * @method bool exists()
     * @see \Illuminate\Support\Traits\Macroable::macroCall
     * @method $this macroCall(string $method, array $parameters)
     * @see \Illuminate\Database\Query\Builder::selectSub
     * @method $this selectSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as)
     * @see \Illuminate\Database\Concerns\BuildsQueries::first
     * @method $this first(array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::dd
     * @method void dd()
     * @see \Illuminate\Database\Query\Builder::whereColumn
     * @method $this whereColumn(array|string $first, null|string $operator = null, null|string $second = null, null|string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::numericAggregate
     * @method $this numericAggregate(string $function, array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::whereNull
     * @method $this whereNull(array|string $columns, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::prepareValueAndOperator
     * @method $this prepareValueAndOperator(string $value, string $operator, bool $useDefault = false)
     * @see \Illuminate\Database\Query\Builder::whereNotBetween
     * @method $this whereNotBetween(string $column, array $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::getConnection
     * @method ConnectionInterface getConnection()
     * @see \Illuminate\Database\Query\Builder::mergeBindings
     * @method $this mergeBindings(\Illuminate\Database\Query\Builder $query)
     * @see \Illuminate\Database\Query\Builder::whereIntegerNotInRaw
     * @method $this whereIntegerNotInRaw(string $column, array|Arrayable $values, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::orWhereJsonDoesntContain
     * @method $this orWhereJsonDoesntContain(string $column, $value)
     * @see \Illuminate\Database\Query\Builder::orWhereRaw
     * @method $this orWhereRaw(string $sql, $bindings = [])
     * @see \Illuminate\Database\Query\Builder::leftJoinSub
     * @method $this leftJoinSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::limit
     * @method $this limit(int $value)
     * @see \Illuminate\Database\Query\Builder::whereJsonContains
     * @method $this whereJsonContains(string $column, $value, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::from
     * @method $this from(string $table)
     * @see \Illuminate\Database\Query\Builder::insertGetId
     * @method int insertGetId(array $values, null|string $sequence = null)
     * @see \Illuminate\Database\Query\Builder::whereBetween
     * @method $this whereBetween(string $column, array $values, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Database\Query\Builder::mergeWheres
     * @method $this mergeWheres(array $wheres, array $bindings)
     * @see \Illuminate\Database\Query\Builder::sharedLock
     * @method $this sharedLock()
     * @see \Illuminate\Database\Query\Builder::orderByRaw
     * @method $this orderByRaw(string $sql, array $bindings = [])
     * @see \Illuminate\Database\Concerns\BuildsQueries::tap
     * @method $this tap(callable $callback)
     * @see \Illuminate\Database\Query\Builder::doesntExist
     * @method bool doesntExist()
     * @see \Illuminate\Database\Query\Builder::offset
     * @method $this offset(int $value)
     * @see \Illuminate\Database\Query\Builder::orWhereMonth
     * @method $this orWhereMonth(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::whereNotNull
     * @method $this whereNotNull(string $column, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::count
     * @method int count(string $columns = '*')
     * @see \Illuminate\Database\Query\Builder::orWhereNotBetween
     * @method $this orWhereNotBetween(string $column, array $values)
     * @see \Illuminate\Database\Query\Builder::fromRaw
     * @method $this fromRaw(string $expression, $bindings = [])
     * @see \Illuminate\Support\Traits\Macroable::mixin
     * @method $this mixin(object $mixin, bool $replace = true)
     * @see \Illuminate\Database\Query\Builder::take
     * @method $this take(int $value)
     * @see \Illuminate\Database\Query\Builder::updateOrInsert
     * @method $this updateOrInsert(array $attributes, array $values = [])
     * @see \Illuminate\Database\Query\Builder::addNestedWhereQuery
     * @method $this addNestedWhereQuery(\Illuminate\Database\Query\Builder $query, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::cloneWithout
     * @method $this cloneWithout(array $properties)
     * @see \Illuminate\Database\Query\Builder::rightJoin
     * @method $this rightJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::fromSub
     * @method $this fromSub(\Closure|\Illuminate\Database\Query\Builder|string $query, string $as)
     * @see \Illuminate\Database\Query\Builder::leftJoin
     * @method $this leftJoin(string $table, \Closure|string $first, null|string $operator = null, null|string $second = null)
     * @see \Illuminate\Database\Query\Builder::insert
     * @method bool insert(array $values)
     * @see \Illuminate\Database\Query\Builder::distinct
     * @method $this distinct()
     * @see \Illuminate\Database\Concerns\BuildsQueries::chunk
     * @method $this chunk(int $count, callable $callback)
     * @see \Illuminate\Database\Query\Builder::whereYear
     * @method $this whereYear(string $column, string $operator, \DateTimeInterface|int|null|string $value = null, string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::getCountForPagination
     * @method $this getCountForPagination(array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::aggregate
     * @method $this aggregate(string $function, array $columns = ['*'])
     * @see \Illuminate\Database\Query\Builder::orWhereDate
     * @method $this orWhereDate(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::avg
     * @method mixed avg(string $column)
     * @see \Illuminate\Database\Query\Builder::addBinding
     * @method $this addBinding($value, string $type = 'where')
     * @see \Illuminate\Database\Query\Builder::getGrammar
     * @method $this getGrammar()
     * @see \Illuminate\Database\Query\Builder::lockForUpdate
     * @method $this lockForUpdate()
     * @see \Illuminate\Database\Query\Builder::dump
     * @method void dump()
     * @see \Illuminate\Database\Query\Builder::implode
     * @method $this implode(string $column, string $glue = '')
     * @see \Illuminate\Database\Query\Builder::cloneWithoutBindings
     * @method $this cloneWithoutBindings(array $except)
     * @see \Illuminate\Database\Query\Builder::addWhereExistsQuery
     * @method $this addWhereExistsQuery(\Illuminate\Database\Query\Builder $query, string $boolean = 'and', bool $not = false)
     * @see \Illuminate\Support\Traits\Macroable::macro
     * @method $this macro(string $name, callable|object $macro)
     * @see \Illuminate\Database\Query\Builder::whereRaw
     * @method $this whereRaw(string $sql, $bindings = [], string $boolean = 'and')
     * @see \Illuminate\Database\Query\Builder::toSql
     * @method string toSql()
     * @see \Illuminate\Database\Query\Builder::orHaving
     * @method $this orHaving(string $column, null|string $operator = null, null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::orHavingRaw
     * @method $this orHavingRaw(string $sql, array $bindings = [])
     * @see \Illuminate\Database\Query\Builder::getBindings
     * @method array getBindings()
     * @see \Illuminate\Database\Query\Builder::forPageBeforeId
     * @method $this forPageBeforeId(int $perPage = 15, int|null $lastId = 0, string $column = 'id')
     * @see \Illuminate\Database\Query\Builder::orWhereTime
     * @method $this orWhereTime(string $column, string $operator, \DateTimeInterface|null|string $value = null)
     * @see \Illuminate\Database\Query\Builder::orWhereBetween
     * @method $this orWhereBetween(string $column, array $values)
     * @see \Illuminate\Database\Query\Builder::dynamicWhere
     * @method $this dynamicWhere(string $method, array $parameters)
     */
    class _BaseBuilder extends Builder {}
    
    /**
     * @method \Illuminate\Support\Collection mapSpread(callable $callback)
     * @method \Illuminate\Support\Collection mapWithKeys(callable $callback)
     * @method \Illuminate\Support\Collection zip(array $items)
     * @method \Illuminate\Support\Collection partition(callable|string $key, $operator = null, $value = null)
     * @method \Illuminate\Support\Collection mapInto(string $class)
     * @method \Illuminate\Support\Collection mapToGroups(callable $callback)
     * @method \Illuminate\Support\Collection map(callable $callback)
     * @method \Illuminate\Support\Collection groupBy(array|callable|string $groupBy, bool $preserveKeys = false)
     * @method \Illuminate\Support\Collection pluck(string $value, null|string $key = null)
     * @method \Illuminate\Support\Collection pad(int $size, $value)
     * @method \Illuminate\Support\Collection split(int $numberOfGroups)
     * @method \Illuminate\Support\Collection combine($values)
     * @method \Illuminate\Support\Collection countBy(callable|null $callback = null)
     * @method \Illuminate\Support\Collection mapToDictionary(callable $callback)
     * @method \Illuminate\Support\Collection keys()
     * @method \Illuminate\Support\Collection transform(callable $callback)
     * @method \Illuminate\Support\Collection flatMap(callable $callback)
     * @method \Illuminate\Support\Collection collapse()
     */
    class _BaseCollection extends \Illuminate\Database\Eloquent\Collection {}
}

namespace LaravelIdea\Helper\App {

    use App\Ads;
    use App\AdvertisementStatus;
    use App\Categories;
    use App\Category;
    use App\City;
    use App\Complaints;
    use App\Contact_us;
    use App\Contract_course;
    use App\Country;
    use App\Course;
    use App\Currency;
    use App\District;
    use App\Favorite_places;
    use App\InforamtionString;
    use App\Licensed_courses;
    use App\MeetingRoom;
    use App\Ministry;
    use App\Notification;
    use App\Order;
    use App\questionnaire;
    use App\Rate;
    use App\Request;
    use App\Setting;
    use App\Slider;
    use App\Sponsored_advertisements;
    use App\Trainer;
    use App\User;
    use App\User_lastpasswords;
    use App\User_notification;
    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    
    /**
     * @method Ads shift()
     * @method Ads pop()
     * @method Ads get($key, $default = null)
     * @method Ads pull($key, $default = null)
     * @method Ads first(callable $callback = null, $default = null)
     * @method Ads firstWhere(string $key, $operator = null, $value = null)
     * @method Ads find($key, $default = null)
     * @method Ads[] all()
     * @method Ads last(callable $callback = null, $default = null)
     */
    class _IH_Ads_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Ads[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Ads_QB whereId($value)
     * @method _IH_Ads_QB whereImage($value)
     * @method _IH_Ads_QB whereLink($value)
     * @method _IH_Ads_QB whereCreatedAt($value)
     * @method _IH_Ads_QB whereUpdatedAt($value)
     * @method Ads create(array $attributes = [])
     * @method _IH_Ads_C|Ads[] cursor()
     * @method Ads|null find($id, array $columns = ['*'])
     * @method _IH_Ads_C|Ads[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Ads findOrFail($id, array $columns = ['*'])
     * @method _IH_Ads_C|Ads[] findOrNew($id, array $columns = ['*'])
     * @method Ads first(array $columns = ['*'])
     * @method Ads firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Ads firstOrCreate(array $attributes, array $values = [])
     * @method Ads firstOrFail(array $columns = ['*'])
     * @method Ads firstOrNew(array $attributes, array $values = [])
     * @method Ads forceCreate(array $attributes)
     * @method _IH_Ads_C|Ads[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Ads_C|Ads[] get(array $columns = ['*'])
     * @method Ads getModel()
     * @method Ads[] getModels(array $columns = ['*'])
     * @method _IH_Ads_C|Ads[] hydrate(array $items)
     * @method Ads make(array $attributes = [])
     * @method Ads newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Ads[]|_IH_Ads_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Ads[]|_IH_Ads_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Ads updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Ads_QB extends _BaseBuilder {}
    
    /**
     * @method AdvertisementStatus shift()
     * @method AdvertisementStatus pop()
     * @method AdvertisementStatus get($key, $default = null)
     * @method AdvertisementStatus pull($key, $default = null)
     * @method AdvertisementStatus first(callable $callback = null, $default = null)
     * @method AdvertisementStatus firstWhere(string $key, $operator = null, $value = null)
     * @method AdvertisementStatus find($key, $default = null)
     * @method AdvertisementStatus[] all()
     * @method AdvertisementStatus last(callable $callback = null, $default = null)
     */
    class _IH_AdvertisementStatus_C extends _BaseCollection {
        /**
         * @param int $size
         * @return AdvertisementStatus[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method AdvertisementStatus create(array $attributes = [])
     * @method _IH_AdvertisementStatus_C|AdvertisementStatus[] cursor()
     * @method AdvertisementStatus|null find($id, array $columns = ['*'])
     * @method _IH_AdvertisementStatus_C|AdvertisementStatus[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method AdvertisementStatus findOrFail($id, array $columns = ['*'])
     * @method _IH_AdvertisementStatus_C|AdvertisementStatus[] findOrNew($id, array $columns = ['*'])
     * @method AdvertisementStatus first(array $columns = ['*'])
     * @method AdvertisementStatus firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method AdvertisementStatus firstOrCreate(array $attributes, array $values = [])
     * @method AdvertisementStatus firstOrFail(array $columns = ['*'])
     * @method AdvertisementStatus firstOrNew(array $attributes, array $values = [])
     * @method AdvertisementStatus forceCreate(array $attributes)
     * @method _IH_AdvertisementStatus_C|AdvertisementStatus[] fromQuery(string $query, array $bindings = [])
     * @method _IH_AdvertisementStatus_C|AdvertisementStatus[] get(array $columns = ['*'])
     * @method AdvertisementStatus getModel()
     * @method AdvertisementStatus[] getModels(array $columns = ['*'])
     * @method _IH_AdvertisementStatus_C|AdvertisementStatus[] hydrate(array $items)
     * @method AdvertisementStatus make(array $attributes = [])
     * @method AdvertisementStatus newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|AdvertisementStatus[]|_IH_AdvertisementStatus_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|AdvertisementStatus[]|_IH_AdvertisementStatus_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method AdvertisementStatus updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_AdvertisementStatus_QB extends _BaseBuilder {}
    
    /**
     * @method Categories shift()
     * @method Categories pop()
     * @method Categories get($key, $default = null)
     * @method Categories pull($key, $default = null)
     * @method Categories first(callable $callback = null, $default = null)
     * @method Categories firstWhere(string $key, $operator = null, $value = null)
     * @method Categories find($key, $default = null)
     * @method Categories[] all()
     * @method Categories last(callable $callback = null, $default = null)
     */
    class _IH_Categories_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Categories[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Categories_QB whereId($value)
     * @method _IH_Categories_QB whereName($value)
     * @method _IH_Categories_QB whereCreatedAt($value)
     * @method _IH_Categories_QB whereUpdatedAt($value)
     * @method Categories create(array $attributes = [])
     * @method _IH_Categories_C|Categories[] cursor()
     * @method Categories|null find($id, array $columns = ['*'])
     * @method _IH_Categories_C|Categories[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Categories findOrFail($id, array $columns = ['*'])
     * @method _IH_Categories_C|Categories[] findOrNew($id, array $columns = ['*'])
     * @method Categories first(array $columns = ['*'])
     * @method Categories firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Categories firstOrCreate(array $attributes, array $values = [])
     * @method Categories firstOrFail(array $columns = ['*'])
     * @method Categories firstOrNew(array $attributes, array $values = [])
     * @method Categories forceCreate(array $attributes)
     * @method _IH_Categories_C|Categories[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Categories_C|Categories[] get(array $columns = ['*'])
     * @method Categories getModel()
     * @method Categories[] getModels(array $columns = ['*'])
     * @method _IH_Categories_C|Categories[] hydrate(array $items)
     * @method Categories make(array $attributes = [])
     * @method Categories newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Categories[]|_IH_Categories_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Categories[]|_IH_Categories_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Categories updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Categories_QB extends _BaseBuilder {}
    
    /**
     * @method Category shift()
     * @method Category pop()
     * @method Category get($key, $default = null)
     * @method Category pull($key, $default = null)
     * @method Category first(callable $callback = null, $default = null)
     * @method Category firstWhere(string $key, $operator = null, $value = null)
     * @method Category find($key, $default = null)
     * @method Category[] all()
     * @method Category last(callable $callback = null, $default = null)
     */
    class _IH_Category_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Category[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Category_QB whereId($value)
     * @method _IH_Category_QB whereName($value)
     * @method _IH_Category_QB whereCreatedAt($value)
     * @method _IH_Category_QB whereUpdatedAt($value)
     * @method Category create(array $attributes = [])
     * @method _IH_Category_C|Category[] cursor()
     * @method Category|null find($id, array $columns = ['*'])
     * @method _IH_Category_C|Category[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Category findOrFail($id, array $columns = ['*'])
     * @method _IH_Category_C|Category[] findOrNew($id, array $columns = ['*'])
     * @method Category first(array $columns = ['*'])
     * @method Category firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Category firstOrCreate(array $attributes, array $values = [])
     * @method Category firstOrFail(array $columns = ['*'])
     * @method Category firstOrNew(array $attributes, array $values = [])
     * @method Category forceCreate(array $attributes)
     * @method _IH_Category_C|Category[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Category_C|Category[] get(array $columns = ['*'])
     * @method Category getModel()
     * @method Category[] getModels(array $columns = ['*'])
     * @method _IH_Category_C|Category[] hydrate(array $items)
     * @method Category make(array $attributes = [])
     * @method Category newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Category[]|_IH_Category_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Category[]|_IH_Category_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Category updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Category_QB extends _BaseBuilder {}
    
    /**
     * @method City shift()
     * @method City pop()
     * @method City get($key, $default = null)
     * @method City pull($key, $default = null)
     * @method City first(callable $callback = null, $default = null)
     * @method City firstWhere(string $key, $operator = null, $value = null)
     * @method City find($key, $default = null)
     * @method City[] all()
     * @method City last(callable $callback = null, $default = null)
     */
    class _IH_City_C extends _BaseCollection {
        /**
         * @param int $size
         * @return City[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_City_QB whereId($value)
     * @method _IH_City_QB whereNameAr($value)
     * @method _IH_City_QB whereNameEn($value)
     * @method _IH_City_QB whereCountryId($value)
     * @method _IH_City_QB whereImage($value)
     * @method _IH_City_QB whereCreatedAt($value)
     * @method _IH_City_QB whereUpdatedAt($value)
     * @method City create(array $attributes = [])
     * @method _IH_City_C|City[] cursor()
     * @method City|null find($id, array $columns = ['*'])
     * @method _IH_City_C|City[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method City findOrFail($id, array $columns = ['*'])
     * @method _IH_City_C|City[] findOrNew($id, array $columns = ['*'])
     * @method City first(array $columns = ['*'])
     * @method City firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method City firstOrCreate(array $attributes, array $values = [])
     * @method City firstOrFail(array $columns = ['*'])
     * @method City firstOrNew(array $attributes, array $values = [])
     * @method City forceCreate(array $attributes)
     * @method _IH_City_C|City[] fromQuery(string $query, array $bindings = [])
     * @method _IH_City_C|City[] get(array $columns = ['*'])
     * @method City getModel()
     * @method City[] getModels(array $columns = ['*'])
     * @method _IH_City_C|City[] hydrate(array $items)
     * @method City make(array $attributes = [])
     * @method City newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|City[]|_IH_City_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|City[]|_IH_City_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method City updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_City_QB extends _BaseBuilder {}
    
    /**
     * @method Complaints shift()
     * @method Complaints pop()
     * @method Complaints get($key, $default = null)
     * @method Complaints pull($key, $default = null)
     * @method Complaints first(callable $callback = null, $default = null)
     * @method Complaints firstWhere(string $key, $operator = null, $value = null)
     * @method Complaints find($key, $default = null)
     * @method Complaints[] all()
     * @method Complaints last(callable $callback = null, $default = null)
     */
    class _IH_Complaints_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Complaints[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Complaints create(array $attributes = [])
     * @method _IH_Complaints_C|Complaints[] cursor()
     * @method Complaints|null find($id, array $columns = ['*'])
     * @method _IH_Complaints_C|Complaints[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Complaints findOrFail($id, array $columns = ['*'])
     * @method _IH_Complaints_C|Complaints[] findOrNew($id, array $columns = ['*'])
     * @method Complaints first(array $columns = ['*'])
     * @method Complaints firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Complaints firstOrCreate(array $attributes, array $values = [])
     * @method Complaints firstOrFail(array $columns = ['*'])
     * @method Complaints firstOrNew(array $attributes, array $values = [])
     * @method Complaints forceCreate(array $attributes)
     * @method _IH_Complaints_C|Complaints[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Complaints_C|Complaints[] get(array $columns = ['*'])
     * @method Complaints getModel()
     * @method Complaints[] getModels(array $columns = ['*'])
     * @method _IH_Complaints_C|Complaints[] hydrate(array $items)
     * @method Complaints make(array $attributes = [])
     * @method Complaints newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Complaints[]|_IH_Complaints_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Complaints[]|_IH_Complaints_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Complaints updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Complaints_QB extends _BaseBuilder {}
    
    /**
     * @method Contact_us shift()
     * @method Contact_us pop()
     * @method Contact_us get($key, $default = null)
     * @method Contact_us pull($key, $default = null)
     * @method Contact_us first(callable $callback = null, $default = null)
     * @method Contact_us firstWhere(string $key, $operator = null, $value = null)
     * @method Contact_us find($key, $default = null)
     * @method Contact_us[] all()
     * @method Contact_us last(callable $callback = null, $default = null)
     */
    class _IH_Contact_us_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Contact_us[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Contact_us create(array $attributes = [])
     * @method _IH_Contact_us_C|Contact_us[] cursor()
     * @method Contact_us|null find($id, array $columns = ['*'])
     * @method _IH_Contact_us_C|Contact_us[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Contact_us findOrFail($id, array $columns = ['*'])
     * @method _IH_Contact_us_C|Contact_us[] findOrNew($id, array $columns = ['*'])
     * @method Contact_us first(array $columns = ['*'])
     * @method Contact_us firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Contact_us firstOrCreate(array $attributes, array $values = [])
     * @method Contact_us firstOrFail(array $columns = ['*'])
     * @method Contact_us firstOrNew(array $attributes, array $values = [])
     * @method Contact_us forceCreate(array $attributes)
     * @method _IH_Contact_us_C|Contact_us[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Contact_us_C|Contact_us[] get(array $columns = ['*'])
     * @method Contact_us getModel()
     * @method Contact_us[] getModels(array $columns = ['*'])
     * @method _IH_Contact_us_C|Contact_us[] hydrate(array $items)
     * @method Contact_us make(array $attributes = [])
     * @method Contact_us newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Contact_us[]|_IH_Contact_us_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Contact_us[]|_IH_Contact_us_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Contact_us updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Contact_us_QB extends _BaseBuilder {}
    
    /**
     * @method Contract_course shift()
     * @method Contract_course pop()
     * @method Contract_course get($key, $default = null)
     * @method Contract_course pull($key, $default = null)
     * @method Contract_course first(callable $callback = null, $default = null)
     * @method Contract_course firstWhere(string $key, $operator = null, $value = null)
     * @method Contract_course find($key, $default = null)
     * @method Contract_course[] all()
     * @method Contract_course last(callable $callback = null, $default = null)
     */
    class _IH_Contract_course_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Contract_course[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Contract_course_QB whereId($value)
     * @method _IH_Contract_course_QB whereName($value)
     * @method _IH_Contract_course_QB whereEmail($value)
     * @method _IH_Contract_course_QB whereMobile($value)
     * @method _IH_Contract_course_QB whereEntity($value)
     * @method _IH_Contract_course_QB whereCity($value)
     * @method _IH_Contract_course_QB whereCourse($value)
     * @method _IH_Contract_course_QB whereCreatedAt($value)
     * @method _IH_Contract_course_QB whereUpdatedAt($value)
     * @method Contract_course create(array $attributes = [])
     * @method _IH_Contract_course_C|Contract_course[] cursor()
     * @method Contract_course|null find($id, array $columns = ['*'])
     * @method _IH_Contract_course_C|Contract_course[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Contract_course findOrFail($id, array $columns = ['*'])
     * @method _IH_Contract_course_C|Contract_course[] findOrNew($id, array $columns = ['*'])
     * @method Contract_course first(array $columns = ['*'])
     * @method Contract_course firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Contract_course firstOrCreate(array $attributes, array $values = [])
     * @method Contract_course firstOrFail(array $columns = ['*'])
     * @method Contract_course firstOrNew(array $attributes, array $values = [])
     * @method Contract_course forceCreate(array $attributes)
     * @method _IH_Contract_course_C|Contract_course[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Contract_course_C|Contract_course[] get(array $columns = ['*'])
     * @method Contract_course getModel()
     * @method Contract_course[] getModels(array $columns = ['*'])
     * @method _IH_Contract_course_C|Contract_course[] hydrate(array $items)
     * @method Contract_course make(array $attributes = [])
     * @method Contract_course newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Contract_course[]|_IH_Contract_course_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Contract_course[]|_IH_Contract_course_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Contract_course updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Contract_course_QB extends _BaseBuilder {}
    
    /**
     * @method Country shift()
     * @method Country pop()
     * @method Country get($key, $default = null)
     * @method Country pull($key, $default = null)
     * @method Country first(callable $callback = null, $default = null)
     * @method Country firstWhere(string $key, $operator = null, $value = null)
     * @method Country find($key, $default = null)
     * @method Country[] all()
     * @method Country last(callable $callback = null, $default = null)
     */
    class _IH_Country_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Country[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Country_QB whereId($value)
     * @method _IH_Country_QB whereImage($value)
     * @method _IH_Country_QB whereNameAr($value)
     * @method _IH_Country_QB whereNameEn($value)
     * @method _IH_Country_QB whereCurrencyId($value)
     * @method _IH_Country_QB whereCode($value)
     * @method _IH_Country_QB whereActive($value)
     * @method _IH_Country_QB whereStartWith($value)
     * @method _IH_Country_QB whereNumberCount($value)
     * @method _IH_Country_QB whereIdentityCount($value)
     * @method _IH_Country_QB whereIdentityStartwith($value)
     * @method _IH_Country_QB whereCreatedAt($value)
     * @method _IH_Country_QB whereUpdatedAt($value)
     * @method Country create(array $attributes = [])
     * @method _IH_Country_C|Country[] cursor()
     * @method Country|null find($id, array $columns = ['*'])
     * @method _IH_Country_C|Country[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Country findOrFail($id, array $columns = ['*'])
     * @method _IH_Country_C|Country[] findOrNew($id, array $columns = ['*'])
     * @method Country first(array $columns = ['*'])
     * @method Country firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Country firstOrCreate(array $attributes, array $values = [])
     * @method Country firstOrFail(array $columns = ['*'])
     * @method Country firstOrNew(array $attributes, array $values = [])
     * @method Country forceCreate(array $attributes)
     * @method _IH_Country_C|Country[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Country_C|Country[] get(array $columns = ['*'])
     * @method Country getModel()
     * @method Country[] getModels(array $columns = ['*'])
     * @method _IH_Country_C|Country[] hydrate(array $items)
     * @method Country make(array $attributes = [])
     * @method Country newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Country[]|_IH_Country_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Country[]|_IH_Country_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Country updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Country_QB extends _BaseBuilder {}
    
    /**
     * @method Course shift()
     * @method Course pop()
     * @method Course get($key, $default = null)
     * @method Course pull($key, $default = null)
     * @method Course first(callable $callback = null, $default = null)
     * @method Course firstWhere(string $key, $operator = null, $value = null)
     * @method Course find($key, $default = null)
     * @method Course[] all()
     * @method Course last(callable $callback = null, $default = null)
     */
    class _IH_Course_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Course[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Course_QB whereId($value)
     * @method _IH_Course_QB whereTrainerId($value)
     * @method _IH_Course_QB whereImage($value)
     * @method _IH_Course_QB whereName($value)
     * @method _IH_Course_QB wherePrice($value)
     * @method _IH_Course_QB whereDate($value)
     * @method _IH_Course_QB whereDuration($value)
     * @method _IH_Course_QB whereDescription($value)
     * @method _IH_Course_QB whereBreif($value)
     * @method _IH_Course_QB whereFeatures($value)
     * @method _IH_Course_QB whereLink($value)
     * @method _IH_Course_QB whereIsSpecial($value)
     * @method _IH_Course_QB whereCreatedAt($value)
     * @method _IH_Course_QB whereUpdatedAt($value)
     * @method Course create(array $attributes = [])
     * @method _IH_Course_C|Course[] cursor()
     * @method Course|null find($id, array $columns = ['*'])
     * @method _IH_Course_C|Course[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Course findOrFail($id, array $columns = ['*'])
     * @method _IH_Course_C|Course[] findOrNew($id, array $columns = ['*'])
     * @method Course first(array $columns = ['*'])
     * @method Course firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Course firstOrCreate(array $attributes, array $values = [])
     * @method Course firstOrFail(array $columns = ['*'])
     * @method Course firstOrNew(array $attributes, array $values = [])
     * @method Course forceCreate(array $attributes)
     * @method _IH_Course_C|Course[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Course_C|Course[] get(array $columns = ['*'])
     * @method Course getModel()
     * @method Course[] getModels(array $columns = ['*'])
     * @method _IH_Course_C|Course[] hydrate(array $items)
     * @method Course make(array $attributes = [])
     * @method Course newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Course[]|_IH_Course_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Course[]|_IH_Course_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Course updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Course_QB extends _BaseBuilder {}
    
    /**
     * @method Currency shift()
     * @method Currency pop()
     * @method Currency get($key, $default = null)
     * @method Currency pull($key, $default = null)
     * @method Currency first(callable $callback = null, $default = null)
     * @method Currency firstWhere(string $key, $operator = null, $value = null)
     * @method Currency find($key, $default = null)
     * @method Currency[] all()
     * @method Currency last(callable $callback = null, $default = null)
     */
    class _IH_Currency_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Currency[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Currency create(array $attributes = [])
     * @method _IH_Currency_C|Currency[] cursor()
     * @method Currency|null find($id, array $columns = ['*'])
     * @method _IH_Currency_C|Currency[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Currency findOrFail($id, array $columns = ['*'])
     * @method _IH_Currency_C|Currency[] findOrNew($id, array $columns = ['*'])
     * @method Currency first(array $columns = ['*'])
     * @method Currency firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Currency firstOrCreate(array $attributes, array $values = [])
     * @method Currency firstOrFail(array $columns = ['*'])
     * @method Currency firstOrNew(array $attributes, array $values = [])
     * @method Currency forceCreate(array $attributes)
     * @method _IH_Currency_C|Currency[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Currency_C|Currency[] get(array $columns = ['*'])
     * @method Currency getModel()
     * @method Currency[] getModels(array $columns = ['*'])
     * @method _IH_Currency_C|Currency[] hydrate(array $items)
     * @method Currency make(array $attributes = [])
     * @method Currency newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Currency[]|_IH_Currency_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Currency[]|_IH_Currency_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Currency updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Currency_QB extends _BaseBuilder {}
    
    /**
     * @method District shift()
     * @method District pop()
     * @method District get($key, $default = null)
     * @method District pull($key, $default = null)
     * @method District first(callable $callback = null, $default = null)
     * @method District firstWhere(string $key, $operator = null, $value = null)
     * @method District find($key, $default = null)
     * @method District[] all()
     * @method District last(callable $callback = null, $default = null)
     */
    class _IH_District_C extends _BaseCollection {
        /**
         * @param int $size
         * @return District[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method District create(array $attributes = [])
     * @method _IH_District_C|District[] cursor()
     * @method District|null find($id, array $columns = ['*'])
     * @method _IH_District_C|District[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method District findOrFail($id, array $columns = ['*'])
     * @method _IH_District_C|District[] findOrNew($id, array $columns = ['*'])
     * @method District first(array $columns = ['*'])
     * @method District firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method District firstOrCreate(array $attributes, array $values = [])
     * @method District firstOrFail(array $columns = ['*'])
     * @method District firstOrNew(array $attributes, array $values = [])
     * @method District forceCreate(array $attributes)
     * @method _IH_District_C|District[] fromQuery(string $query, array $bindings = [])
     * @method _IH_District_C|District[] get(array $columns = ['*'])
     * @method District getModel()
     * @method District[] getModels(array $columns = ['*'])
     * @method _IH_District_C|District[] hydrate(array $items)
     * @method District make(array $attributes = [])
     * @method District newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|District[]|_IH_District_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|District[]|_IH_District_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method District updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_District_QB extends _BaseBuilder {}
    
    /**
     * @method Favorite_places shift()
     * @method Favorite_places pop()
     * @method Favorite_places get($key, $default = null)
     * @method Favorite_places pull($key, $default = null)
     * @method Favorite_places first(callable $callback = null, $default = null)
     * @method Favorite_places firstWhere(string $key, $operator = null, $value = null)
     * @method Favorite_places find($key, $default = null)
     * @method Favorite_places[] all()
     * @method Favorite_places last(callable $callback = null, $default = null)
     */
    class _IH_Favorite_places_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Favorite_places[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Favorite_places create(array $attributes = [])
     * @method _IH_Favorite_places_C|Favorite_places[] cursor()
     * @method Favorite_places|null find($id, array $columns = ['*'])
     * @method _IH_Favorite_places_C|Favorite_places[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Favorite_places findOrFail($id, array $columns = ['*'])
     * @method _IH_Favorite_places_C|Favorite_places[] findOrNew($id, array $columns = ['*'])
     * @method Favorite_places first(array $columns = ['*'])
     * @method Favorite_places firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Favorite_places firstOrCreate(array $attributes, array $values = [])
     * @method Favorite_places firstOrFail(array $columns = ['*'])
     * @method Favorite_places firstOrNew(array $attributes, array $values = [])
     * @method Favorite_places forceCreate(array $attributes)
     * @method _IH_Favorite_places_C|Favorite_places[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Favorite_places_C|Favorite_places[] get(array $columns = ['*'])
     * @method Favorite_places getModel()
     * @method Favorite_places[] getModels(array $columns = ['*'])
     * @method _IH_Favorite_places_C|Favorite_places[] hydrate(array $items)
     * @method Favorite_places make(array $attributes = [])
     * @method Favorite_places newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Favorite_places[]|_IH_Favorite_places_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Favorite_places[]|_IH_Favorite_places_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Favorite_places updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Favorite_places_QB extends _BaseBuilder {}
    
    /**
     * @method InforamtionString shift()
     * @method InforamtionString pop()
     * @method InforamtionString get($key, $default = null)
     * @method InforamtionString pull($key, $default = null)
     * @method InforamtionString first(callable $callback = null, $default = null)
     * @method InforamtionString firstWhere(string $key, $operator = null, $value = null)
     * @method InforamtionString find($key, $default = null)
     * @method InforamtionString[] all()
     * @method InforamtionString last(callable $callback = null, $default = null)
     */
    class _IH_InforamtionString_C extends _BaseCollection {
        /**
         * @param int $size
         * @return InforamtionString[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method InforamtionString create(array $attributes = [])
     * @method _IH_InforamtionString_C|InforamtionString[] cursor()
     * @method InforamtionString|null find($id, array $columns = ['*'])
     * @method _IH_InforamtionString_C|InforamtionString[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method InforamtionString findOrFail($id, array $columns = ['*'])
     * @method _IH_InforamtionString_C|InforamtionString[] findOrNew($id, array $columns = ['*'])
     * @method InforamtionString first(array $columns = ['*'])
     * @method InforamtionString firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method InforamtionString firstOrCreate(array $attributes, array $values = [])
     * @method InforamtionString firstOrFail(array $columns = ['*'])
     * @method InforamtionString firstOrNew(array $attributes, array $values = [])
     * @method InforamtionString forceCreate(array $attributes)
     * @method _IH_InforamtionString_C|InforamtionString[] fromQuery(string $query, array $bindings = [])
     * @method _IH_InforamtionString_C|InforamtionString[] get(array $columns = ['*'])
     * @method InforamtionString getModel()
     * @method InforamtionString[] getModels(array $columns = ['*'])
     * @method _IH_InforamtionString_C|InforamtionString[] hydrate(array $items)
     * @method InforamtionString make(array $attributes = [])
     * @method InforamtionString newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|InforamtionString[]|_IH_InforamtionString_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|InforamtionString[]|_IH_InforamtionString_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method InforamtionString updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_InforamtionString_QB extends _BaseBuilder {}
    
    /**
     * @method Licensed_courses shift()
     * @method Licensed_courses pop()
     * @method Licensed_courses get($key, $default = null)
     * @method Licensed_courses pull($key, $default = null)
     * @method Licensed_courses first(callable $callback = null, $default = null)
     * @method Licensed_courses firstWhere(string $key, $operator = null, $value = null)
     * @method Licensed_courses find($key, $default = null)
     * @method Licensed_courses[] all()
     * @method Licensed_courses last(callable $callback = null, $default = null)
     */
    class _IH_Licensed_courses_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Licensed_courses[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Licensed_courses_QB whereId($value)
     * @method _IH_Licensed_courses_QB whereName($value)
     * @method _IH_Licensed_courses_QB whereNoAccreditation($value)
     * @method _IH_Licensed_courses_QB whereCreatedAt($value)
     * @method _IH_Licensed_courses_QB whereUpdatedAt($value)
     * @method Licensed_courses create(array $attributes = [])
     * @method _IH_Licensed_courses_C|Licensed_courses[] cursor()
     * @method Licensed_courses|null find($id, array $columns = ['*'])
     * @method _IH_Licensed_courses_C|Licensed_courses[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Licensed_courses findOrFail($id, array $columns = ['*'])
     * @method _IH_Licensed_courses_C|Licensed_courses[] findOrNew($id, array $columns = ['*'])
     * @method Licensed_courses first(array $columns = ['*'])
     * @method Licensed_courses firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Licensed_courses firstOrCreate(array $attributes, array $values = [])
     * @method Licensed_courses firstOrFail(array $columns = ['*'])
     * @method Licensed_courses firstOrNew(array $attributes, array $values = [])
     * @method Licensed_courses forceCreate(array $attributes)
     * @method _IH_Licensed_courses_C|Licensed_courses[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Licensed_courses_C|Licensed_courses[] get(array $columns = ['*'])
     * @method Licensed_courses getModel()
     * @method Licensed_courses[] getModels(array $columns = ['*'])
     * @method _IH_Licensed_courses_C|Licensed_courses[] hydrate(array $items)
     * @method Licensed_courses make(array $attributes = [])
     * @method Licensed_courses newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Licensed_courses[]|_IH_Licensed_courses_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Licensed_courses[]|_IH_Licensed_courses_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Licensed_courses updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Licensed_courses_QB extends _BaseBuilder {}
    
    /**
     * @method MeetingRoom shift()
     * @method MeetingRoom pop()
     * @method MeetingRoom get($key, $default = null)
     * @method MeetingRoom pull($key, $default = null)
     * @method MeetingRoom first(callable $callback = null, $default = null)
     * @method MeetingRoom firstWhere(string $key, $operator = null, $value = null)
     * @method MeetingRoom find($key, $default = null)
     * @method MeetingRoom[] all()
     * @method MeetingRoom last(callable $callback = null, $default = null)
     */
    class _IH_MeetingRoom_C extends _BaseCollection {
        /**
         * @param int $size
         * @return MeetingRoom[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_MeetingRoom_QB whereId($value)
     * @method _IH_MeetingRoom_QB whereName($value)
     * @method _IH_MeetingRoom_QB whereHostId($value)
     * @method _IH_MeetingRoom_QB whereJwt($value)
     * @method _IH_MeetingRoom_QB whereApiKey($value)
     * @method _IH_MeetingRoom_QB whereApiSecret($value)
     * @method _IH_MeetingRoom_QB whereCreatedAt($value)
     * @method _IH_MeetingRoom_QB whereUpdatedAt($value)
     * @method MeetingRoom create(array $attributes = [])
     * @method _IH_MeetingRoom_C|MeetingRoom[] cursor()
     * @method MeetingRoom|null find($id, array $columns = ['*'])
     * @method _IH_MeetingRoom_C|MeetingRoom[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method MeetingRoom findOrFail($id, array $columns = ['*'])
     * @method _IH_MeetingRoom_C|MeetingRoom[] findOrNew($id, array $columns = ['*'])
     * @method MeetingRoom first(array $columns = ['*'])
     * @method MeetingRoom firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method MeetingRoom firstOrCreate(array $attributes, array $values = [])
     * @method MeetingRoom firstOrFail(array $columns = ['*'])
     * @method MeetingRoom firstOrNew(array $attributes, array $values = [])
     * @method MeetingRoom forceCreate(array $attributes)
     * @method _IH_MeetingRoom_C|MeetingRoom[] fromQuery(string $query, array $bindings = [])
     * @method _IH_MeetingRoom_C|MeetingRoom[] get(array $columns = ['*'])
     * @method MeetingRoom getModel()
     * @method MeetingRoom[] getModels(array $columns = ['*'])
     * @method _IH_MeetingRoom_C|MeetingRoom[] hydrate(array $items)
     * @method MeetingRoom make(array $attributes = [])
     * @method MeetingRoom newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|MeetingRoom[]|_IH_MeetingRoom_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|MeetingRoom[]|_IH_MeetingRoom_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method MeetingRoom updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_MeetingRoom_QB extends _BaseBuilder {}
    
    /**
     * @method Ministry shift()
     * @method Ministry pop()
     * @method Ministry get($key, $default = null)
     * @method Ministry pull($key, $default = null)
     * @method Ministry first(callable $callback = null, $default = null)
     * @method Ministry firstWhere(string $key, $operator = null, $value = null)
     * @method Ministry find($key, $default = null)
     * @method Ministry[] all()
     * @method Ministry last(callable $callback = null, $default = null)
     */
    class _IH_Ministry_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Ministry[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Ministry create(array $attributes = [])
     * @method _IH_Ministry_C|Ministry[] cursor()
     * @method Ministry|null find($id, array $columns = ['*'])
     * @method _IH_Ministry_C|Ministry[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Ministry findOrFail($id, array $columns = ['*'])
     * @method _IH_Ministry_C|Ministry[] findOrNew($id, array $columns = ['*'])
     * @method Ministry first(array $columns = ['*'])
     * @method Ministry firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Ministry firstOrCreate(array $attributes, array $values = [])
     * @method Ministry firstOrFail(array $columns = ['*'])
     * @method Ministry firstOrNew(array $attributes, array $values = [])
     * @method Ministry forceCreate(array $attributes)
     * @method _IH_Ministry_C|Ministry[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Ministry_C|Ministry[] get(array $columns = ['*'])
     * @method Ministry getModel()
     * @method Ministry[] getModels(array $columns = ['*'])
     * @method _IH_Ministry_C|Ministry[] hydrate(array $items)
     * @method Ministry make(array $attributes = [])
     * @method Ministry newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Ministry[]|_IH_Ministry_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Ministry[]|_IH_Ministry_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Ministry updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Ministry_QB extends _BaseBuilder {}
    
    /**
     * @method Notification shift()
     * @method Notification pop()
     * @method Notification get($key, $default = null)
     * @method Notification pull($key, $default = null)
     * @method Notification first(callable $callback = null, $default = null)
     * @method Notification firstWhere(string $key, $operator = null, $value = null)
     * @method Notification find($key, $default = null)
     * @method Notification[] all()
     * @method Notification last(callable $callback = null, $default = null)
     */
    class _IH_Notification_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Notification[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Notification_QB whereId($value)
     * @method _IH_Notification_QB whereTitle($value)
     * @method _IH_Notification_QB whereBody($value)
     * @method _IH_Notification_QB whereCreatedAt($value)
     * @method _IH_Notification_QB whereUpdatedAt($value)
     * @method Notification create(array $attributes = [])
     * @method _IH_Notification_C|Notification[] cursor()
     * @method Notification|null find($id, array $columns = ['*'])
     * @method _IH_Notification_C|Notification[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Notification findOrFail($id, array $columns = ['*'])
     * @method _IH_Notification_C|Notification[] findOrNew($id, array $columns = ['*'])
     * @method Notification first(array $columns = ['*'])
     * @method Notification firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Notification firstOrCreate(array $attributes, array $values = [])
     * @method Notification firstOrFail(array $columns = ['*'])
     * @method Notification firstOrNew(array $attributes, array $values = [])
     * @method Notification forceCreate(array $attributes)
     * @method _IH_Notification_C|Notification[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Notification_C|Notification[] get(array $columns = ['*'])
     * @method Notification getModel()
     * @method Notification[] getModels(array $columns = ['*'])
     * @method _IH_Notification_C|Notification[] hydrate(array $items)
     * @method Notification make(array $attributes = [])
     * @method Notification newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Notification[]|_IH_Notification_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Notification[]|_IH_Notification_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Notification updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Notification_QB extends _BaseBuilder {}
    
    /**
     * @method Order shift()
     * @method Order pop()
     * @method Order get($key, $default = null)
     * @method Order pull($key, $default = null)
     * @method Order first(callable $callback = null, $default = null)
     * @method Order firstWhere(string $key, $operator = null, $value = null)
     * @method Order find($key, $default = null)
     * @method Order[] all()
     * @method Order last(callable $callback = null, $default = null)
     */
    class _IH_Order_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Order[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Order_QB whereId($value)
     * @method _IH_Order_QB whereUserId($value)
     * @method _IH_Order_QB whereCourseId($value)
     * @method _IH_Order_QB whereName($value)
     * @method _IH_Order_QB whereMobile($value)
     * @method _IH_Order_QB whereEmail($value)
     * @method _IH_Order_QB whereIdentity($value)
     * @method _IH_Order_QB whereCreatedAt($value)
     * @method _IH_Order_QB whereUpdatedAt($value)
     * @method Order create(array $attributes = [])
     * @method _IH_Order_C|Order[] cursor()
     * @method Order|null find($id, array $columns = ['*'])
     * @method _IH_Order_C|Order[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Order findOrFail($id, array $columns = ['*'])
     * @method _IH_Order_C|Order[] findOrNew($id, array $columns = ['*'])
     * @method Order first(array $columns = ['*'])
     * @method Order firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Order firstOrCreate(array $attributes, array $values = [])
     * @method Order firstOrFail(array $columns = ['*'])
     * @method Order firstOrNew(array $attributes, array $values = [])
     * @method Order forceCreate(array $attributes)
     * @method _IH_Order_C|Order[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Order_C|Order[] get(array $columns = ['*'])
     * @method Order getModel()
     * @method Order[] getModels(array $columns = ['*'])
     * @method _IH_Order_C|Order[] hydrate(array $items)
     * @method Order make(array $attributes = [])
     * @method Order newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Order[]|_IH_Order_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Order[]|_IH_Order_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Order updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Order_QB extends _BaseBuilder {}
    
    /**
     * @method Rate shift()
     * @method Rate pop()
     * @method Rate get($key, $default = null)
     * @method Rate pull($key, $default = null)
     * @method Rate first(callable $callback = null, $default = null)
     * @method Rate firstWhere(string $key, $operator = null, $value = null)
     * @method Rate find($key, $default = null)
     * @method Rate[] all()
     * @method Rate last(callable $callback = null, $default = null)
     */
    class _IH_Rate_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Rate[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Rate create(array $attributes = [])
     * @method _IH_Rate_C|Rate[] cursor()
     * @method Rate|null find($id, array $columns = ['*'])
     * @method _IH_Rate_C|Rate[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Rate findOrFail($id, array $columns = ['*'])
     * @method _IH_Rate_C|Rate[] findOrNew($id, array $columns = ['*'])
     * @method Rate first(array $columns = ['*'])
     * @method Rate firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Rate firstOrCreate(array $attributes, array $values = [])
     * @method Rate firstOrFail(array $columns = ['*'])
     * @method Rate firstOrNew(array $attributes, array $values = [])
     * @method Rate forceCreate(array $attributes)
     * @method _IH_Rate_C|Rate[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Rate_C|Rate[] get(array $columns = ['*'])
     * @method Rate getModel()
     * @method Rate[] getModels(array $columns = ['*'])
     * @method _IH_Rate_C|Rate[] hydrate(array $items)
     * @method Rate make(array $attributes = [])
     * @method Rate newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Rate[]|_IH_Rate_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Rate[]|_IH_Rate_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Rate updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Rate_QB extends _BaseBuilder {}
    
    /**
     * @method Request shift()
     * @method Request pop()
     * @method Request get($key, $default = null)
     * @method Request pull($key, $default = null)
     * @method Request first(callable $callback = null, $default = null)
     * @method Request firstWhere(string $key, $operator = null, $value = null)
     * @method Request find($key, $default = null)
     * @method Request[] all()
     * @method Request last(callable $callback = null, $default = null)
     */
    class _IH_Request_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Request[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Request_QB whereId($value)
     * @method _IH_Request_QB whereCourseId($value)
     * @method _IH_Request_QB whereName($value)
     * @method _IH_Request_QB whereEmail($value)
     * @method _IH_Request_QB whereMobile($value)
     * @method _IH_Request_QB whereCreatedAt($value)
     * @method _IH_Request_QB whereUpdatedAt($value)
     * @method Request create(array $attributes = [])
     * @method _IH_Request_C|Request[] cursor()
     * @method Request|null find($id, array $columns = ['*'])
     * @method _IH_Request_C|Request[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Request findOrFail($id, array $columns = ['*'])
     * @method _IH_Request_C|Request[] findOrNew($id, array $columns = ['*'])
     * @method Request first(array $columns = ['*'])
     * @method Request firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Request firstOrCreate(array $attributes, array $values = [])
     * @method Request firstOrFail(array $columns = ['*'])
     * @method Request firstOrNew(array $attributes, array $values = [])
     * @method Request forceCreate(array $attributes)
     * @method _IH_Request_C|Request[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Request_C|Request[] get(array $columns = ['*'])
     * @method Request getModel()
     * @method Request[] getModels(array $columns = ['*'])
     * @method _IH_Request_C|Request[] hydrate(array $items)
     * @method Request make(array $attributes = [])
     * @method Request newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Request[]|_IH_Request_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Request[]|_IH_Request_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Request updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Request_QB extends _BaseBuilder {}
    
    /**
     * @method Setting shift()
     * @method Setting pop()
     * @method Setting get($key, $default = null)
     * @method Setting pull($key, $default = null)
     * @method Setting first(callable $callback = null, $default = null)
     * @method Setting firstWhere(string $key, $operator = null, $value = null)
     * @method Setting find($key, $default = null)
     * @method Setting[] all()
     * @method Setting last(callable $callback = null, $default = null)
     */
    class _IH_Setting_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Setting[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Setting_QB whereId($value)
     * @method _IH_Setting_QB whereDeliveryCost($value)
     * @method _IH_Setting_QB whereTax($value)
     * @method _IH_Setting_QB whereLogo($value)
     * @method _IH_Setting_QB whereNameAr($value)
     * @method _IH_Setting_QB whereNameEn($value)
     * @method _IH_Setting_QB whereAboutAr($value)
     * @method _IH_Setting_QB whereAboutEn($value)
     * @method _IH_Setting_QB whereTextAr($value)
     * @method _IH_Setting_QB whereTextEn($value)
     * @method _IH_Setting_QB whereTermsAr($value)
     * @method _IH_Setting_QB whereTermsEn($value)
     * @method _IH_Setting_QB whereCreatedAt($value)
     * @method _IH_Setting_QB whereUpdatedAt($value)
     * @method Setting create(array $attributes = [])
     * @method _IH_Setting_C|Setting[] cursor()
     * @method Setting|null find($id, array $columns = ['*'])
     * @method _IH_Setting_C|Setting[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Setting findOrFail($id, array $columns = ['*'])
     * @method _IH_Setting_C|Setting[] findOrNew($id, array $columns = ['*'])
     * @method Setting first(array $columns = ['*'])
     * @method Setting firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Setting firstOrCreate(array $attributes, array $values = [])
     * @method Setting firstOrFail(array $columns = ['*'])
     * @method Setting firstOrNew(array $attributes, array $values = [])
     * @method Setting forceCreate(array $attributes)
     * @method _IH_Setting_C|Setting[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Setting_C|Setting[] get(array $columns = ['*'])
     * @method Setting getModel()
     * @method Setting[] getModels(array $columns = ['*'])
     * @method _IH_Setting_C|Setting[] hydrate(array $items)
     * @method Setting make(array $attributes = [])
     * @method Setting newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Setting[]|_IH_Setting_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Setting[]|_IH_Setting_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Setting updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Setting_QB extends _BaseBuilder {}
    
    /**
     * @method Slider shift()
     * @method Slider pop()
     * @method Slider get($key, $default = null)
     * @method Slider pull($key, $default = null)
     * @method Slider first(callable $callback = null, $default = null)
     * @method Slider firstWhere(string $key, $operator = null, $value = null)
     * @method Slider find($key, $default = null)
     * @method Slider[] all()
     * @method Slider last(callable $callback = null, $default = null)
     */
    class _IH_Slider_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Slider[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Slider_QB whereId($value)
     * @method _IH_Slider_QB whereImage($value)
     * @method _IH_Slider_QB whereNameAr($value)
     * @method _IH_Slider_QB whereNameEn($value)
     * @method _IH_Slider_QB whereCreatedAt($value)
     * @method _IH_Slider_QB whereUpdatedAt($value)
     * @method Slider create(array $attributes = [])
     * @method _IH_Slider_C|Slider[] cursor()
     * @method Slider|null find($id, array $columns = ['*'])
     * @method _IH_Slider_C|Slider[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Slider findOrFail($id, array $columns = ['*'])
     * @method _IH_Slider_C|Slider[] findOrNew($id, array $columns = ['*'])
     * @method Slider first(array $columns = ['*'])
     * @method Slider firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Slider firstOrCreate(array $attributes, array $values = [])
     * @method Slider firstOrFail(array $columns = ['*'])
     * @method Slider firstOrNew(array $attributes, array $values = [])
     * @method Slider forceCreate(array $attributes)
     * @method _IH_Slider_C|Slider[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Slider_C|Slider[] get(array $columns = ['*'])
     * @method Slider getModel()
     * @method Slider[] getModels(array $columns = ['*'])
     * @method _IH_Slider_C|Slider[] hydrate(array $items)
     * @method Slider make(array $attributes = [])
     * @method Slider newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Slider[]|_IH_Slider_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Slider[]|_IH_Slider_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Slider updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Slider_QB extends _BaseBuilder {}
    
    /**
     * @method Sponsored_advertisements shift()
     * @method Sponsored_advertisements pop()
     * @method Sponsored_advertisements get($key, $default = null)
     * @method Sponsored_advertisements pull($key, $default = null)
     * @method Sponsored_advertisements first(callable $callback = null, $default = null)
     * @method Sponsored_advertisements firstWhere(string $key, $operator = null, $value = null)
     * @method Sponsored_advertisements find($key, $default = null)
     * @method Sponsored_advertisements[] all()
     * @method Sponsored_advertisements last(callable $callback = null, $default = null)
     */
    class _IH_Sponsored_advertisements_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Sponsored_advertisements[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Sponsored_advertisements_QB whereId($value)
     * @method _IH_Sponsored_advertisements_QB whereUserId($value)
     * @method _IH_Sponsored_advertisements_QB whereCourseId($value)
     * @method _IH_Sponsored_advertisements_QB whereName($value)
     * @method _IH_Sponsored_advertisements_QB whereMobile($value)
     * @method _IH_Sponsored_advertisements_QB whereEmail($value)
     * @method _IH_Sponsored_advertisements_QB whereCreatedAt($value)
     * @method _IH_Sponsored_advertisements_QB whereUpdatedAt($value)
     * @method Sponsored_advertisements create(array $attributes = [])
     * @method _IH_Sponsored_advertisements_C|Sponsored_advertisements[] cursor()
     * @method Sponsored_advertisements|null find($id, array $columns = ['*'])
     * @method _IH_Sponsored_advertisements_C|Sponsored_advertisements[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Sponsored_advertisements findOrFail($id, array $columns = ['*'])
     * @method _IH_Sponsored_advertisements_C|Sponsored_advertisements[] findOrNew($id, array $columns = ['*'])
     * @method Sponsored_advertisements first(array $columns = ['*'])
     * @method Sponsored_advertisements firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Sponsored_advertisements firstOrCreate(array $attributes, array $values = [])
     * @method Sponsored_advertisements firstOrFail(array $columns = ['*'])
     * @method Sponsored_advertisements firstOrNew(array $attributes, array $values = [])
     * @method Sponsored_advertisements forceCreate(array $attributes)
     * @method _IH_Sponsored_advertisements_C|Sponsored_advertisements[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Sponsored_advertisements_C|Sponsored_advertisements[] get(array $columns = ['*'])
     * @method Sponsored_advertisements getModel()
     * @method Sponsored_advertisements[] getModels(array $columns = ['*'])
     * @method _IH_Sponsored_advertisements_C|Sponsored_advertisements[] hydrate(array $items)
     * @method Sponsored_advertisements make(array $attributes = [])
     * @method Sponsored_advertisements newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Sponsored_advertisements[]|_IH_Sponsored_advertisements_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Sponsored_advertisements[]|_IH_Sponsored_advertisements_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Sponsored_advertisements updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Sponsored_advertisements_QB extends _BaseBuilder {}
    
    /**
     * @method Trainer shift()
     * @method Trainer pop()
     * @method Trainer get($key, $default = null)
     * @method Trainer pull($key, $default = null)
     * @method Trainer first(callable $callback = null, $default = null)
     * @method Trainer firstWhere(string $key, $operator = null, $value = null)
     * @method Trainer find($key, $default = null)
     * @method Trainer[] all()
     * @method Trainer last(callable $callback = null, $default = null)
     */
    class _IH_Trainer_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Trainer[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_Trainer_QB whereId($value)
     * @method _IH_Trainer_QB whereCurrentPosition($value)
     * @method _IH_Trainer_QB whereUserId($value)
     * @method _IH_Trainer_QB whereAbout($value)
     * @method _IH_Trainer_QB whereExperience($value)
     * @method _IH_Trainer_QB whereQualification($value)
     * @method _IH_Trainer_QB whereAchievements($value)
     * @method _IH_Trainer_QB whereCreatedAt($value)
     * @method _IH_Trainer_QB whereUpdatedAt($value)
     * @method Trainer create(array $attributes = [])
     * @method _IH_Trainer_C|Trainer[] cursor()
     * @method Trainer|null find($id, array $columns = ['*'])
     * @method _IH_Trainer_C|Trainer[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Trainer findOrFail($id, array $columns = ['*'])
     * @method _IH_Trainer_C|Trainer[] findOrNew($id, array $columns = ['*'])
     * @method Trainer first(array $columns = ['*'])
     * @method Trainer firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Trainer firstOrCreate(array $attributes, array $values = [])
     * @method Trainer firstOrFail(array $columns = ['*'])
     * @method Trainer firstOrNew(array $attributes, array $values = [])
     * @method Trainer forceCreate(array $attributes)
     * @method _IH_Trainer_C|Trainer[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Trainer_C|Trainer[] get(array $columns = ['*'])
     * @method Trainer getModel()
     * @method Trainer[] getModels(array $columns = ['*'])
     * @method _IH_Trainer_C|Trainer[] hydrate(array $items)
     * @method Trainer make(array $attributes = [])
     * @method Trainer newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Trainer[]|_IH_Trainer_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Trainer[]|_IH_Trainer_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Trainer updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Trainer_QB extends _BaseBuilder {}
    
    /**
     * @method User shift()
     * @method User pop()
     * @method User get($key, $default = null)
     * @method User pull($key, $default = null)
     * @method User first(callable $callback = null, $default = null)
     * @method User firstWhere(string $key, $operator = null, $value = null)
     * @method User find($key, $default = null)
     * @method User[] all()
     * @method User last(callable $callback = null, $default = null)
     */
    class _IH_User_C extends _BaseCollection {
        /**
         * @param int $size
         * @return User[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_User_QB whereId($value)
     * @method _IH_User_QB whereSocialId($value)
     * @method _IH_User_QB whereUserName($value)
     * @method _IH_User_QB whereUserTypeId($value)
     * @method _IH_User_QB whereName($value)
     * @method _IH_User_QB whereDescription($value)
     * @method _IH_User_QB whereImage($value)
     * @method _IH_User_QB whereCountryId($value)
     * @method _IH_User_QB whereCity($value)
     * @method _IH_User_QB whereLat($value)
     * @method _IH_User_QB whereLng($value)
     * @method _IH_User_QB whereMobile($value)
     * @method _IH_User_QB whereMobileVerified($value)
     * @method _IH_User_QB whereEmail($value)
     * @method _IH_User_QB whereEmailVerifiedAt($value)
     * @method _IH_User_QB whereEmailVerified($value)
     * @method _IH_User_QB wherePassword($value)
     * @method _IH_User_QB whereFireBaseToken($value)
     * @method _IH_User_QB wherePasscode($value)
     * @method _IH_User_QB whereUserverify($value)
     * @method _IH_User_QB whereNeedResetPassword($value)
     * @method _IH_User_QB whereRememberToken($value)
     * @method _IH_User_QB whereCreatedAt($value)
     * @method _IH_User_QB whereUpdatedAt($value)
     * @method User create(array $attributes = [])
     * @method _IH_User_C|User[] cursor()
     * @method User|null find($id, array $columns = ['*'])
     * @method _IH_User_C|User[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method User findOrFail($id, array $columns = ['*'])
     * @method _IH_User_C|User[] findOrNew($id, array $columns = ['*'])
     * @method User first(array $columns = ['*'])
     * @method User firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method User firstOrCreate(array $attributes, array $values = [])
     * @method User firstOrFail(array $columns = ['*'])
     * @method User firstOrNew(array $attributes, array $values = [])
     * @method User forceCreate(array $attributes)
     * @method _IH_User_C|User[] fromQuery(string $query, array $bindings = [])
     * @method _IH_User_C|User[] get(array $columns = ['*'])
     * @method User getModel()
     * @method User[] getModels(array $columns = ['*'])
     * @method _IH_User_C|User[] hydrate(array $items)
     * @method User make(array $attributes = [])
     * @method User newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|User[]|_IH_User_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|User[]|_IH_User_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method User updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_User_QB extends _BaseBuilder {}
    
    /**
     * @method User_lastpasswords shift()
     * @method User_lastpasswords pop()
     * @method User_lastpasswords get($key, $default = null)
     * @method User_lastpasswords pull($key, $default = null)
     * @method User_lastpasswords first(callable $callback = null, $default = null)
     * @method User_lastpasswords firstWhere(string $key, $operator = null, $value = null)
     * @method User_lastpasswords find($key, $default = null)
     * @method User_lastpasswords[] all()
     * @method User_lastpasswords last(callable $callback = null, $default = null)
     */
    class _IH_User_lastpasswords_C extends _BaseCollection {
        /**
         * @param int $size
         * @return User_lastpasswords[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_User_lastpasswords_QB whereId($value)
     * @method _IH_User_lastpasswords_QB whereUserId($value)
     * @method _IH_User_lastpasswords_QB wherePasscode($value)
     * @method _IH_User_lastpasswords_QB whereExpiredAt($value)
     * @method _IH_User_lastpasswords_QB whereCreatedAt($value)
     * @method _IH_User_lastpasswords_QB whereUpdatedAt($value)
     * @method User_lastpasswords create(array $attributes = [])
     * @method _IH_User_lastpasswords_C|User_lastpasswords[] cursor()
     * @method User_lastpasswords|null find($id, array $columns = ['*'])
     * @method _IH_User_lastpasswords_C|User_lastpasswords[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method User_lastpasswords findOrFail($id, array $columns = ['*'])
     * @method _IH_User_lastpasswords_C|User_lastpasswords[] findOrNew($id, array $columns = ['*'])
     * @method User_lastpasswords first(array $columns = ['*'])
     * @method User_lastpasswords firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method User_lastpasswords firstOrCreate(array $attributes, array $values = [])
     * @method User_lastpasswords firstOrFail(array $columns = ['*'])
     * @method User_lastpasswords firstOrNew(array $attributes, array $values = [])
     * @method User_lastpasswords forceCreate(array $attributes)
     * @method _IH_User_lastpasswords_C|User_lastpasswords[] fromQuery(string $query, array $bindings = [])
     * @method _IH_User_lastpasswords_C|User_lastpasswords[] get(array $columns = ['*'])
     * @method User_lastpasswords getModel()
     * @method User_lastpasswords[] getModels(array $columns = ['*'])
     * @method _IH_User_lastpasswords_C|User_lastpasswords[] hydrate(array $items)
     * @method User_lastpasswords make(array $attributes = [])
     * @method User_lastpasswords newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|User_lastpasswords[]|_IH_User_lastpasswords_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|User_lastpasswords[]|_IH_User_lastpasswords_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method User_lastpasswords updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_User_lastpasswords_QB extends _BaseBuilder {}
    
    /**
     * @method User_notification shift()
     * @method User_notification pop()
     * @method User_notification get($key, $default = null)
     * @method User_notification pull($key, $default = null)
     * @method User_notification first(callable $callback = null, $default = null)
     * @method User_notification firstWhere(string $key, $operator = null, $value = null)
     * @method User_notification find($key, $default = null)
     * @method User_notification[] all()
     * @method User_notification last(callable $callback = null, $default = null)
     */
    class _IH_User_notification_C extends _BaseCollection {
        /**
         * @param int $size
         * @return User_notification[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_User_notification_QB whereId($value)
     * @method _IH_User_notification_QB whereUserId($value)
     * @method _IH_User_notification_QB whereNotificationId($value)
     * @method _IH_User_notification_QB whereViewed($value)
     * @method User_notification create(array $attributes = [])
     * @method _IH_User_notification_C|User_notification[] cursor()
     * @method User_notification|null find($id, array $columns = ['*'])
     * @method _IH_User_notification_C|User_notification[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method User_notification findOrFail($id, array $columns = ['*'])
     * @method _IH_User_notification_C|User_notification[] findOrNew($id, array $columns = ['*'])
     * @method User_notification first(array $columns = ['*'])
     * @method User_notification firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method User_notification firstOrCreate(array $attributes, array $values = [])
     * @method User_notification firstOrFail(array $columns = ['*'])
     * @method User_notification firstOrNew(array $attributes, array $values = [])
     * @method User_notification forceCreate(array $attributes)
     * @method _IH_User_notification_C|User_notification[] fromQuery(string $query, array $bindings = [])
     * @method _IH_User_notification_C|User_notification[] get(array $columns = ['*'])
     * @method User_notification getModel()
     * @method User_notification[] getModels(array $columns = ['*'])
     * @method _IH_User_notification_C|User_notification[] hydrate(array $items)
     * @method User_notification make(array $attributes = [])
     * @method User_notification newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|User_notification[]|_IH_User_notification_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|User_notification[]|_IH_User_notification_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method User_notification updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_User_notification_QB extends _BaseBuilder {}
    
    /**
     * @method questionnaire shift()
     * @method questionnaire pop()
     * @method questionnaire get($key, $default = null)
     * @method questionnaire pull($key, $default = null)
     * @method questionnaire first(callable $callback = null, $default = null)
     * @method questionnaire firstWhere(string $key, $operator = null, $value = null)
     * @method questionnaire find($key, $default = null)
     * @method questionnaire[] all()
     * @method questionnaire last(callable $callback = null, $default = null)
     */
    class _IH_questionnaire_C extends _BaseCollection {
        /**
         * @param int $size
         * @return questionnaire[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method questionnaire create(array $attributes = [])
     * @method _IH_questionnaire_C|questionnaire[] cursor()
     * @method questionnaire|null find($id, array $columns = ['*'])
     * @method _IH_questionnaire_C|questionnaire[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method questionnaire findOrFail($id, array $columns = ['*'])
     * @method _IH_questionnaire_C|questionnaire[] findOrNew($id, array $columns = ['*'])
     * @method questionnaire first(array $columns = ['*'])
     * @method questionnaire firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method questionnaire firstOrCreate(array $attributes, array $values = [])
     * @method questionnaire firstOrFail(array $columns = ['*'])
     * @method questionnaire firstOrNew(array $attributes, array $values = [])
     * @method questionnaire forceCreate(array $attributes)
     * @method _IH_questionnaire_C|questionnaire[] fromQuery(string $query, array $bindings = [])
     * @method _IH_questionnaire_C|questionnaire[] get(array $columns = ['*'])
     * @method questionnaire getModel()
     * @method questionnaire[] getModels(array $columns = ['*'])
     * @method _IH_questionnaire_C|questionnaire[] hydrate(array $items)
     * @method questionnaire make(array $attributes = [])
     * @method questionnaire newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|questionnaire[]|_IH_questionnaire_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|questionnaire[]|_IH_questionnaire_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method questionnaire updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_questionnaire_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Illuminate\Notifications {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Notifications\DatabaseNotification;
    use Illuminate\Notifications\DatabaseNotificationCollection;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    
    /**
     * @method DatabaseNotification shift()
     * @method DatabaseNotification pop()
     * @method DatabaseNotification get($key, $default = null)
     * @method DatabaseNotification pull($key, $default = null)
     * @method DatabaseNotification first(callable $callback = null, $default = null)
     * @method DatabaseNotification firstWhere(string $key, $operator = null, $value = null)
     * @method DatabaseNotification find($key, $default = null)
     * @method DatabaseNotification[] all()
     * @method DatabaseNotification last(callable $callback = null, $default = null)
     * @mixin DatabaseNotificationCollection
     */
    class _IH_DatabaseNotification_C extends _BaseCollection {
        /**
         * @param int $size
         * @return DatabaseNotification[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method _IH_DatabaseNotification_QB whereId($value)
     * @method _IH_DatabaseNotification_QB whereTitle($value)
     * @method _IH_DatabaseNotification_QB whereBody($value)
     * @method _IH_DatabaseNotification_QB whereCreatedAt($value)
     * @method _IH_DatabaseNotification_QB whereUpdatedAt($value)
     * @method DatabaseNotification create(array $attributes = [])
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] cursor()
     * @method DatabaseNotification|null find($id, array $columns = ['*'])
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method DatabaseNotification findOrFail($id, array $columns = ['*'])
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] findOrNew($id, array $columns = ['*'])
     * @method DatabaseNotification first(array $columns = ['*'])
     * @method DatabaseNotification firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method DatabaseNotification firstOrCreate(array $attributes, array $values = [])
     * @method DatabaseNotification firstOrFail(array $columns = ['*'])
     * @method DatabaseNotification firstOrNew(array $attributes, array $values = [])
     * @method DatabaseNotification forceCreate(array $attributes)
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] fromQuery(string $query, array $bindings = [])
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] get(array $columns = ['*'])
     * @method DatabaseNotification getModel()
     * @method DatabaseNotification[] getModels(array $columns = ['*'])
     * @method _IH_DatabaseNotification_C|DatabaseNotification[] hydrate(array $items)
     * @method DatabaseNotification make(array $attributes = [])
     * @method DatabaseNotification newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|DatabaseNotification[]|_IH_DatabaseNotification_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|DatabaseNotification[]|_IH_DatabaseNotification_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method DatabaseNotification updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_DatabaseNotification_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Laratrust\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use Laratrust\Models\LaratrustPermission;
    use Laratrust\Models\LaratrustRole;
    use Laratrust\Models\LaratrustTeam;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    
    /**
     * @method LaratrustPermission shift()
     * @method LaratrustPermission pop()
     * @method LaratrustPermission get($key, $default = null)
     * @method LaratrustPermission pull($key, $default = null)
     * @method LaratrustPermission first(callable $callback = null, $default = null)
     * @method LaratrustPermission firstWhere(string $key, $operator = null, $value = null)
     * @method LaratrustPermission find($key, $default = null)
     * @method LaratrustPermission[] all()
     * @method LaratrustPermission last(callable $callback = null, $default = null)
     */
    class _IH_LaratrustPermission_C extends _BaseCollection {
        /**
         * @param int $size
         * @return LaratrustPermission[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method LaratrustPermission create(array $attributes = [])
     * @method _IH_LaratrustPermission_C|LaratrustPermission[] cursor()
     * @method LaratrustPermission|null find($id, array $columns = ['*'])
     * @method _IH_LaratrustPermission_C|LaratrustPermission[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method LaratrustPermission findOrFail($id, array $columns = ['*'])
     * @method _IH_LaratrustPermission_C|LaratrustPermission[] findOrNew($id, array $columns = ['*'])
     * @method LaratrustPermission first(array $columns = ['*'])
     * @method LaratrustPermission firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method LaratrustPermission firstOrCreate(array $attributes, array $values = [])
     * @method LaratrustPermission firstOrFail(array $columns = ['*'])
     * @method LaratrustPermission firstOrNew(array $attributes, array $values = [])
     * @method LaratrustPermission forceCreate(array $attributes)
     * @method _IH_LaratrustPermission_C|LaratrustPermission[] fromQuery(string $query, array $bindings = [])
     * @method _IH_LaratrustPermission_C|LaratrustPermission[] get(array $columns = ['*'])
     * @method LaratrustPermission getModel()
     * @method LaratrustPermission[] getModels(array $columns = ['*'])
     * @method _IH_LaratrustPermission_C|LaratrustPermission[] hydrate(array $items)
     * @method LaratrustPermission make(array $attributes = [])
     * @method LaratrustPermission newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|LaratrustPermission[]|_IH_LaratrustPermission_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|LaratrustPermission[]|_IH_LaratrustPermission_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method LaratrustPermission updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_LaratrustPermission_QB extends _BaseBuilder {}
    
    /**
     * @method LaratrustRole shift()
     * @method LaratrustRole pop()
     * @method LaratrustRole get($key, $default = null)
     * @method LaratrustRole pull($key, $default = null)
     * @method LaratrustRole first(callable $callback = null, $default = null)
     * @method LaratrustRole firstWhere(string $key, $operator = null, $value = null)
     * @method LaratrustRole find($key, $default = null)
     * @method LaratrustRole[] all()
     * @method LaratrustRole last(callable $callback = null, $default = null)
     */
    class _IH_LaratrustRole_C extends _BaseCollection {
        /**
         * @param int $size
         * @return LaratrustRole[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method LaratrustRole create(array $attributes = [])
     * @method _IH_LaratrustRole_C|LaratrustRole[] cursor()
     * @method LaratrustRole|null find($id, array $columns = ['*'])
     * @method _IH_LaratrustRole_C|LaratrustRole[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method LaratrustRole findOrFail($id, array $columns = ['*'])
     * @method _IH_LaratrustRole_C|LaratrustRole[] findOrNew($id, array $columns = ['*'])
     * @method LaratrustRole first(array $columns = ['*'])
     * @method LaratrustRole firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method LaratrustRole firstOrCreate(array $attributes, array $values = [])
     * @method LaratrustRole firstOrFail(array $columns = ['*'])
     * @method LaratrustRole firstOrNew(array $attributes, array $values = [])
     * @method LaratrustRole forceCreate(array $attributes)
     * @method _IH_LaratrustRole_C|LaratrustRole[] fromQuery(string $query, array $bindings = [])
     * @method _IH_LaratrustRole_C|LaratrustRole[] get(array $columns = ['*'])
     * @method LaratrustRole getModel()
     * @method LaratrustRole[] getModels(array $columns = ['*'])
     * @method _IH_LaratrustRole_C|LaratrustRole[] hydrate(array $items)
     * @method LaratrustRole make(array $attributes = [])
     * @method LaratrustRole newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|LaratrustRole[]|_IH_LaratrustRole_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|LaratrustRole[]|_IH_LaratrustRole_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method LaratrustRole updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_LaratrustRole_QB extends _BaseBuilder {}
    
    /**
     * @method LaratrustTeam shift()
     * @method LaratrustTeam pop()
     * @method LaratrustTeam get($key, $default = null)
     * @method LaratrustTeam pull($key, $default = null)
     * @method LaratrustTeam first(callable $callback = null, $default = null)
     * @method LaratrustTeam firstWhere(string $key, $operator = null, $value = null)
     * @method LaratrustTeam find($key, $default = null)
     * @method LaratrustTeam[] all()
     * @method LaratrustTeam last(callable $callback = null, $default = null)
     */
    class _IH_LaratrustTeam_C extends _BaseCollection {
        /**
         * @param int $size
         * @return LaratrustTeam[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method LaratrustTeam create(array $attributes = [])
     * @method _IH_LaratrustTeam_C|LaratrustTeam[] cursor()
     * @method LaratrustTeam|null find($id, array $columns = ['*'])
     * @method _IH_LaratrustTeam_C|LaratrustTeam[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method LaratrustTeam findOrFail($id, array $columns = ['*'])
     * @method _IH_LaratrustTeam_C|LaratrustTeam[] findOrNew($id, array $columns = ['*'])
     * @method LaratrustTeam first(array $columns = ['*'])
     * @method LaratrustTeam firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method LaratrustTeam firstOrCreate(array $attributes, array $values = [])
     * @method LaratrustTeam firstOrFail(array $columns = ['*'])
     * @method LaratrustTeam firstOrNew(array $attributes, array $values = [])
     * @method LaratrustTeam forceCreate(array $attributes)
     * @method _IH_LaratrustTeam_C|LaratrustTeam[] fromQuery(string $query, array $bindings = [])
     * @method _IH_LaratrustTeam_C|LaratrustTeam[] get(array $columns = ['*'])
     * @method LaratrustTeam getModel()
     * @method LaratrustTeam[] getModels(array $columns = ['*'])
     * @method _IH_LaratrustTeam_C|LaratrustTeam[] hydrate(array $items)
     * @method LaratrustTeam make(array $attributes = [])
     * @method LaratrustTeam newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|LaratrustTeam[]|_IH_LaratrustTeam_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|LaratrustTeam[]|_IH_LaratrustTeam_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method LaratrustTeam updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_LaratrustTeam_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Laravel\Passport {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use Laravel\Passport\AuthCode;
    use Laravel\Passport\Client;
    use Laravel\Passport\PersonalAccessClient;
    use Laravel\Passport\Token;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    
    /**
     * @method AuthCode shift()
     * @method AuthCode pop()
     * @method AuthCode get($key, $default = null)
     * @method AuthCode pull($key, $default = null)
     * @method AuthCode first(callable $callback = null, $default = null)
     * @method AuthCode firstWhere(string $key, $operator = null, $value = null)
     * @method AuthCode find($key, $default = null)
     * @method AuthCode[] all()
     * @method AuthCode last(callable $callback = null, $default = null)
     */
    class _IH_AuthCode_C extends _BaseCollection {
        /**
         * @param int $size
         * @return AuthCode[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method AuthCode create(array $attributes = [])
     * @method _IH_AuthCode_C|AuthCode[] cursor()
     * @method AuthCode|null find($id, array $columns = ['*'])
     * @method _IH_AuthCode_C|AuthCode[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method AuthCode findOrFail($id, array $columns = ['*'])
     * @method _IH_AuthCode_C|AuthCode[] findOrNew($id, array $columns = ['*'])
     * @method AuthCode first(array $columns = ['*'])
     * @method AuthCode firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method AuthCode firstOrCreate(array $attributes, array $values = [])
     * @method AuthCode firstOrFail(array $columns = ['*'])
     * @method AuthCode firstOrNew(array $attributes, array $values = [])
     * @method AuthCode forceCreate(array $attributes)
     * @method _IH_AuthCode_C|AuthCode[] fromQuery(string $query, array $bindings = [])
     * @method _IH_AuthCode_C|AuthCode[] get(array $columns = ['*'])
     * @method AuthCode getModel()
     * @method AuthCode[] getModels(array $columns = ['*'])
     * @method _IH_AuthCode_C|AuthCode[] hydrate(array $items)
     * @method AuthCode make(array $attributes = [])
     * @method AuthCode newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|AuthCode[]|_IH_AuthCode_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|AuthCode[]|_IH_AuthCode_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method AuthCode updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_AuthCode_QB extends _BaseBuilder {}
    
    /**
     * @method Client shift()
     * @method Client pop()
     * @method Client get($key, $default = null)
     * @method Client pull($key, $default = null)
     * @method Client first(callable $callback = null, $default = null)
     * @method Client firstWhere(string $key, $operator = null, $value = null)
     * @method Client find($key, $default = null)
     * @method Client[] all()
     * @method Client last(callable $callback = null, $default = null)
     */
    class _IH_Client_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Client[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Client create(array $attributes = [])
     * @method _IH_Client_C|Client[] cursor()
     * @method Client|null find($id, array $columns = ['*'])
     * @method _IH_Client_C|Client[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Client findOrFail($id, array $columns = ['*'])
     * @method _IH_Client_C|Client[] findOrNew($id, array $columns = ['*'])
     * @method Client first(array $columns = ['*'])
     * @method Client firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Client firstOrCreate(array $attributes, array $values = [])
     * @method Client firstOrFail(array $columns = ['*'])
     * @method Client firstOrNew(array $attributes, array $values = [])
     * @method Client forceCreate(array $attributes)
     * @method _IH_Client_C|Client[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Client_C|Client[] get(array $columns = ['*'])
     * @method Client getModel()
     * @method Client[] getModels(array $columns = ['*'])
     * @method _IH_Client_C|Client[] hydrate(array $items)
     * @method Client make(array $attributes = [])
     * @method Client newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Client[]|_IH_Client_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Client[]|_IH_Client_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Client updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Client_QB extends _BaseBuilder {}
    
    /**
     * @method PersonalAccessClient shift()
     * @method PersonalAccessClient pop()
     * @method PersonalAccessClient get($key, $default = null)
     * @method PersonalAccessClient pull($key, $default = null)
     * @method PersonalAccessClient first(callable $callback = null, $default = null)
     * @method PersonalAccessClient firstWhere(string $key, $operator = null, $value = null)
     * @method PersonalAccessClient find($key, $default = null)
     * @method PersonalAccessClient[] all()
     * @method PersonalAccessClient last(callable $callback = null, $default = null)
     */
    class _IH_PersonalAccessClient_C extends _BaseCollection {
        /**
         * @param int $size
         * @return PersonalAccessClient[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method PersonalAccessClient create(array $attributes = [])
     * @method _IH_PersonalAccessClient_C|PersonalAccessClient[] cursor()
     * @method PersonalAccessClient|null find($id, array $columns = ['*'])
     * @method _IH_PersonalAccessClient_C|PersonalAccessClient[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method PersonalAccessClient findOrFail($id, array $columns = ['*'])
     * @method _IH_PersonalAccessClient_C|PersonalAccessClient[] findOrNew($id, array $columns = ['*'])
     * @method PersonalAccessClient first(array $columns = ['*'])
     * @method PersonalAccessClient firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method PersonalAccessClient firstOrCreate(array $attributes, array $values = [])
     * @method PersonalAccessClient firstOrFail(array $columns = ['*'])
     * @method PersonalAccessClient firstOrNew(array $attributes, array $values = [])
     * @method PersonalAccessClient forceCreate(array $attributes)
     * @method _IH_PersonalAccessClient_C|PersonalAccessClient[] fromQuery(string $query, array $bindings = [])
     * @method _IH_PersonalAccessClient_C|PersonalAccessClient[] get(array $columns = ['*'])
     * @method PersonalAccessClient getModel()
     * @method PersonalAccessClient[] getModels(array $columns = ['*'])
     * @method _IH_PersonalAccessClient_C|PersonalAccessClient[] hydrate(array $items)
     * @method PersonalAccessClient make(array $attributes = [])
     * @method PersonalAccessClient newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|PersonalAccessClient[]|_IH_PersonalAccessClient_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|PersonalAccessClient[]|_IH_PersonalAccessClient_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method PersonalAccessClient updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_PersonalAccessClient_QB extends _BaseBuilder {}
    
    /**
     * @method Token shift()
     * @method Token pop()
     * @method Token get($key, $default = null)
     * @method Token pull($key, $default = null)
     * @method Token first(callable $callback = null, $default = null)
     * @method Token firstWhere(string $key, $operator = null, $value = null)
     * @method Token find($key, $default = null)
     * @method Token[] all()
     * @method Token last(callable $callback = null, $default = null)
     */
    class _IH_Token_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Token[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Token create(array $attributes = [])
     * @method _IH_Token_C|Token[] cursor()
     * @method Token|null find($id, array $columns = ['*'])
     * @method _IH_Token_C|Token[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Token findOrFail($id, array $columns = ['*'])
     * @method _IH_Token_C|Token[] findOrNew($id, array $columns = ['*'])
     * @method Token first(array $columns = ['*'])
     * @method Token firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Token firstOrCreate(array $attributes, array $values = [])
     * @method Token firstOrFail(array $columns = ['*'])
     * @method Token firstOrNew(array $attributes, array $values = [])
     * @method Token forceCreate(array $attributes)
     * @method _IH_Token_C|Token[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Token_C|Token[] get(array $columns = ['*'])
     * @method Token getModel()
     * @method Token[] getModels(array $columns = ['*'])
     * @method _IH_Token_C|Token[] hydrate(array $items)
     * @method Token make(array $attributes = [])
     * @method Token newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Token[]|_IH_Token_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Token[]|_IH_Token_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Token updateOrCreate(array $attributes, array $values = [])
     */
    class _IH_Token_QB extends _BaseBuilder {}
}

namespace LaravelIdea\Helper\Spatie\Permission\Models {

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Pagination\LengthAwarePaginator;
    use Illuminate\Pagination\Paginator;
    use Illuminate\Support\Collection;
    use LaravelIdea\Helper\_BaseBuilder;
    use LaravelIdea\Helper\_BaseCollection;
    use Spatie\Permission\Contracts\Permission as Permission1;
    use Spatie\Permission\Contracts\Role;
    use Spatie\Permission\Models\Permission;
    use Spatie\Permission\Models\Role as Role1;
    
    /**
     * @method Permission shift()
     * @method Permission pop()
     * @method Permission get($key, $default = null)
     * @method Permission pull($key, $default = null)
     * @method Permission first(callable $callback = null, $default = null)
     * @method Permission firstWhere(string $key, $operator = null, $value = null)
     * @method Permission find($key, $default = null)
     * @method Permission[] all()
     * @method Permission last(callable $callback = null, $default = null)
     */
    class _IH_Permission_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Permission[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Permission create(array $attributes = [])
     * @method _IH_Permission_C|Permission[] cursor()
     * @method Permission|null find($id, array $columns = ['*'])
     * @method _IH_Permission_C|Permission[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Permission findOrFail($id, array $columns = ['*'])
     * @method _IH_Permission_C|Permission[] findOrNew($id, array $columns = ['*'])
     * @method Permission first(array $columns = ['*'])
     * @method Permission firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Permission firstOrCreate(array $attributes, array $values = [])
     * @method Permission firstOrFail(array $columns = ['*'])
     * @method Permission firstOrNew(array $attributes, array $values = [])
     * @method Permission forceCreate(array $attributes)
     * @method _IH_Permission_C|Permission[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Permission_C|Permission[] get(array $columns = ['*'])
     * @method Permission getModel()
     * @method Permission[] getModels(array $columns = ['*'])
     * @method _IH_Permission_C|Permission[] hydrate(array $items)
     * @method Permission make(array $attributes = [])
     * @method Permission newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Permission[]|_IH_Permission_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Permission[]|_IH_Permission_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Permission updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Permission_QB permission(array|Collection|Permission1|string $permissions)
     * @method _IH_Permission_QB role(array|Collection|Role|string $roles, string $guard = null)
     */
    class _IH_Permission_QB extends _BaseBuilder {}
    
    /**
     * @method Role1 shift()
     * @method Role1 pop()
     * @method Role1 get($key, $default = null)
     * @method Role1 pull($key, $default = null)
     * @method Role1 first(callable $callback = null, $default = null)
     * @method Role1 firstWhere(string $key, $operator = null, $value = null)
     * @method Role1 find($key, $default = null)
     * @method Role1[] all()
     * @method Role1 last(callable $callback = null, $default = null)
     */
    class _IH_Role_C extends _BaseCollection {
        /**
         * @param int $size
         * @return Role1[][]
         */
        public function chunk($size)
        {
            return [];
        }
    }
    
    /**
     * @method Role1 create(array $attributes = [])
     * @method _IH_Role_C|Role1[] cursor()
     * @method Role1|null find($id, array $columns = ['*'])
     * @method _IH_Role_C|Role1[] findMany(array|Arrayable $ids, array $columns = ['*'])
     * @method Role1 findOrFail($id, array $columns = ['*'])
     * @method _IH_Role_C|Role1[] findOrNew($id, array $columns = ['*'])
     * @method Role1 first(array $columns = ['*'])
     * @method Role1 firstOr(array|\Closure $columns = ['*'], \Closure $callback = null)
     * @method Role1 firstOrCreate(array $attributes, array $values = [])
     * @method Role1 firstOrFail(array $columns = ['*'])
     * @method Role1 firstOrNew(array $attributes, array $values = [])
     * @method Role1 forceCreate(array $attributes)
     * @method _IH_Role_C|Role1[] fromQuery(string $query, array $bindings = [])
     * @method _IH_Role_C|Role1[] get(array $columns = ['*'])
     * @method Role1 getModel()
     * @method Role1[] getModels(array $columns = ['*'])
     * @method _IH_Role_C|Role1[] hydrate(array $items)
     * @method Role1 make(array $attributes = [])
     * @method Role1 newModelInstance(array $attributes = [])
     * @method LengthAwarePaginator|Role1[]|_IH_Role_C paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Paginator|Role1[]|_IH_Role_C simplePaginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int|null $page = null)
     * @method Role1 updateOrCreate(array $attributes, array $values = [])
     * @method _IH_Role_QB permission(array|Collection|Permission1|string $permissions)
     */
    class _IH_Role_QB extends _BaseBuilder {}
}