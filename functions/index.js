const functions = require('firebase-functions');

firebase = require('firebase');
const geofire = require('geofire');
url = require('url');

var config = {
    apiKey: "AIzaSyAZ5tptMsrZLjp6XdfFSXNZS-sTcs69mKg",
    authDomain: "lahzah-d2743.firebaseapp.com",
    databaseURL: "https://lahzah-d2743.firebaseio.com",
    projectId: "lahzah-d2743",
    storageBucket: "lahzah-d2743.appspot.com",
    messagingSenderId: "8140903320",
    appId: "1:8140903320:web:18f4fff928349d482851ed",
    measurementId: "G-XZL779S2BX"
};

firebase.initializeApp(config);

exports.getDrivers = functions.https.onRequest((request, response) => {

    drivers = [];
    functions.logger.info("Hello logs!", request.query.radius);
    radius = parseFloat(request.query.radius);
    latCenter = parseFloat(request.query.latCenter);
    lngCenter = parseFloat(request.query.lngCenter);
    serviceTypeId = parseFloat(request.query.serviceTypeId);
    sectionTypeId = parseFloat(request.query.sectionTypeId);

    functions.logger.info("Hello logs!", {structuredData: true});

    firebaseRef = firebase.database().ref("Providers/"+ serviceTypeId +"/Available/" + sectionTypeId );
    // firebaseRef = firebase.database().ref("Providers/");

    const geoFireInstance = new geofire.GeoFire(firebaseRef);

    var geoQuery = geoFireInstance.query({
        center: [latCenter, lngCenter],
        radius: radius
    });

    var onKeyEnteredRegistration = geoQuery.on("key_entered", function (key, location, distance) {
        console.log(key + " is available now ");
        driverData = {driver_id: key};
        drivers.push(driverData);
    });

    var onReadyRegistration = geoQuery.on("ready", function (key, location, distance) {
        onKeyEnteredRegistration.cancel();
    });


    response.setHeader('Content-Type', 'application/json');
    response.send(JSON.stringify({drivers}));
});
