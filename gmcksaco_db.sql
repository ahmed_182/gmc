-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 14, 2021 at 01:03 PM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gmcksaco_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ads_clicks`
--

CREATE TABLE `ads_clicks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ad_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `advertisement_statuses`
--

CREATE TABLE `advertisement_statuses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `advertisement_statuses`
--

INSERT INTO `advertisement_statuses` (`id`, `name_ar`, `name_en`, `created_at`, `updated_at`) VALUES
(1, 'جاري التسجيل', NULL, NULL, NULL),
(2, 'تم التسجيل', NULL, NULL, NULL),
(3, 'السعر غير مناسب', NULL, NULL, NULL),
(4, ' الوقت غير مناسب ', NULL, NULL, NULL),
(5, 'غير مهتم ', NULL, NULL, NULL),
(6, 'مجاني', NULL, NULL, NULL),
(7, 'طلب خصم', NULL, NULL, NULL),
(8, 'انتهى ', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(4, 'الحاسب الالي', '2021-05-22 22:46:04', '2021-05-22 22:46:04'),
(5, 'التخطيط الاستراتيجي', '2021-05-22 22:52:04', '2021-05-22 22:52:04'),
(6, 'تطوير الذات وتنمية المهارات', '2021-05-22 22:52:39', '2021-05-22 22:52:39'),
(7, 'القيادات والادارة العليا', '2021-05-22 22:52:58', '2021-05-22 22:53:20'),
(8, 'الامن والسلامة والصحة المهنية', '2021-05-22 22:53:56', '2021-05-22 22:53:56'),
(9, 'التسويق والمبيعات', '2021-05-22 22:54:28', '2021-05-22 22:54:28'),
(10, 'القطاع المصرفي والتمويل والاستثمار', '2021-05-22 22:55:01', '2021-05-22 22:55:01'),
(11, 'العلاقات العامة والاعلام', '2021-05-22 22:55:23', '2021-05-22 22:55:41'),
(12, 'مديري المكاتب والسكرتارية', '2021-05-22 22:56:16', '2021-05-22 22:56:16'),
(13, 'الموارد البشرية وشؤون الموظفين', '2021-05-22 22:56:50', '2021-05-22 22:56:50'),
(14, 'المعلمين والمشرفين التربويين', '2021-05-22 22:57:26', '2021-05-22 22:57:26'),
(15, 'الشؤون القانونية', '2021-05-22 22:58:02', '2021-05-22 22:58:02'),
(16, 'برامج الجودة', '2021-05-22 22:58:22', '2021-05-22 22:58:22');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `complaints`
--

INSERT INTO `complaints` (`id`, `name`, `email`, `title`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Zorita Burks', 'jywo@mailinator.com', NULL, 'Exercitation nihil u', '2021-09-07 22:04:36', '2021-09-07 22:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `contact_uses`
--

CREATE TABLE `contact_uses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_uses`
--

INSERT INTO `contact_uses` (`id`, `name`, `email`, `mobile`, `country_id`, `title`, `message`, `created_at`, `updated_at`) VALUES
(2, 'محمد عماد', 'dina_mohammedd@outlook.com', '55555555555', NULL, NULL, 'يليبا', '2021-02-14 14:52:22', '2021-02-14 14:52:22'),
(3, 'محمد عماد', 'dina_mohammedd@outlook.com', '966566677782', NULL, NULL, 'fxghd', '2021-02-14 14:55:42', '2021-02-14 14:55:42'),
(4, 'OJHdFWbKIGhnElpC', 'sortenjorif@gmail.com', '3565110556', NULL, NULL, 'LZjaYhEoJqHI', '2021-05-04 11:11:29', '2021-05-04 11:11:29'),
(5, 'vtXQKBrcJ', 'sortenjorif@gmail.com', '4908205244', NULL, NULL, 'WBPOfTVo', '2021-05-04 11:11:32', '2021-05-04 11:11:32'),
(6, 'UbsJWtyDoQZOR', 'suesnoteddq@gmail.com', '2771353673', NULL, NULL, 'ZCPbFnAita', '2021-05-21 12:42:56', '2021-05-21 12:42:56'),
(7, 'JmdbwNfTIBlYiS', 'suesnoteddq@gmail.com', '7136968979', NULL, NULL, 'xLwZJeRTof', '2021-05-21 12:43:02', '2021-05-21 12:43:02'),
(8, 'محمد تجربة', 'info@gmc-sa.net', '0591393045', NULL, NULL, 'هل يعمل هذا الصندوق بشكل جيد.', '2021-08-17 23:47:54', '2021-08-17 23:47:54'),
(9, 'محمد فرج', 'gmsdesigners01@gmail.com', '0533392936', NULL, NULL, 'سيلافحاىتةقىتلاهختىسقف', '2021-08-18 19:39:57', '2021-08-18 19:39:57'),
(10, 'محمد البنا', 'admin@admin.com', '0533392936', NULL, NULL, 'هخبلىاتيلاتهتىا', '2021-08-18 22:49:33', '2021-08-18 22:49:33'),
(11, 'Al3ttar', 'ahmedemadmohamed95@gmail.com', '5123188000', NULL, NULL, 'test', '2021-08-24 02:28:43', '2021-08-24 02:28:43'),
(12, 'LKL', 'amrgmc@gmail.com', '0532252226', NULL, NULL, '21', '2021-08-25 22:56:28', '2021-08-25 22:56:28');

-- --------------------------------------------------------

--
-- Table structure for table `contract_courses`
--

CREATE TABLE `contract_courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contract_courses`
--

INSERT INTO `contract_courses` (`id`, `name`, `email`, `mobile`, `entity`, `city`, `course`, `created_at`, `updated_at`) VALUES
(1, 'محمد عماد', 'admin@admin.com', '0507667757', 'جهة هامة', 'cairo', 'تصميم', '2021-02-14 17:32:14', '2021-02-14 17:32:14'),
(2, 'AHMED ABDELHAMID', 'info@gmc-sa.net', '0591393045', 'مركز صناعة العبقرية', 'الرياض', 'تجربة', '2021-08-17 23:52:36', '2021-08-17 23:52:36');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` longtext COLLATE utf8mb4_unicode_ci,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `symbol` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `image`, `name`, `currency_id`, `code`, `active`, `symbol`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Saudi Arabia', NULL, '966', '1', 'SA', NULL, NULL),
(4, NULL, 'Algeria', NULL, '213', '1', 'DZ', NULL, NULL),
(18, NULL, 'Bahrain', NULL, '973', '1', 'BH', NULL, NULL),
(66, NULL, 'Egypt', NULL, '20', '1', 'EG', NULL, NULL),
(105, NULL, 'Iran, Islamic Republic of', NULL, '98', '1', 'IR', NULL, NULL),
(121, NULL, 'Kuwait', NULL, '965', '1', 'KW', NULL, NULL),
(125, NULL, 'Lebanon', NULL, '961', '1', 'LB', NULL, NULL),
(169, NULL, 'Oman', NULL, '968', '1', 'OM', NULL, NULL),
(214, NULL, 'Sudan', NULL, '249', '1', 'SD', NULL, NULL),
(220, NULL, 'Syrian Arab Republic', NULL, '963', '1', 'SY', NULL, NULL),
(230, NULL, 'Tunisia', NULL, '216', '1', 'TN', NULL, NULL),
(237, NULL, 'United Arab Emirates', NULL, '971', '1', 'AE', NULL, NULL),
(250, NULL, 'Yemen', NULL, '967', '1', 'YE', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trainer_id` int(11) DEFAULT NULL,
  `coordinator_id` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(199) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `offer` longtext COLLATE utf8mb4_unicode_ci,
  `breif` longtext COLLATE utf8mb4_unicode_ci,
  `features` longtext COLLATE utf8mb4_unicode_ci,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `is_special` int(11) NOT NULL DEFAULT '0',
  `confirm` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `trainer_id`, `coordinator_id`, `image`, `name`, `price`, `date`, `end_date`, `duration`, `description`, `offer`, `breif`, `features`, `link`, `link_youtube`, `category_id`, `is_special`, `confirm`, `created_at`, `updated_at`) VALUES
(14, 29, 23, 'https://gmcksa.com/storage/app/public/images/Fkwuv6MafW7hSFFj3lhCmxIQ6keT9yaVKMqqimf8.png', 'إدارة سلسة الإمداد والمشتريات واللوجستيات SCM', '499', '2021-05-26', '2021-05-28', 'يومين', NULL, NULL, 'أصبحت إدارة سلاسل الإمداد أمراً لا يمكن الاستغناء عنه بسبب الابتكار المستمر في التكنولوجيا وتوقعات العملاء المتغيرة بسرعة', '<ul><li style=\"border: 0px; font-size: 29px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">مفاهيم سلاسل الإمداد العالمية وتطوراتها.</li><li style=\"border: 0px; font-size: 29px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">عناصر ومكونات سلاسل الإمداد.</li><li style=\"border: 0px; font-size: 29px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">الطرق الاحترافية للتعاقد مع الموردين.</li><li style=\"border: 0px; font-size: 29px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">تطوير عمليات المشتريات والتموين.</li><li style=\"border: 0px; font-size: 29px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">الإدارة الاستراتيجية للوجستيات الحديثة.</li><li style=\"border: 0px; font-size: 29px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">مخاطر سلاسل الإمداد العالمية.</li><li style=\"border: 0px; font-size: 29px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">دور سلاسل الإمداد في التجارة الالكترونية.</li></ul>', 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 16, 0, 0, '2021-05-25 00:28:44', '2021-05-27 20:09:16'),
(15, 11, 22, 'https://gmcksa.com/storage/app/public/images/KkorTZa8awc6EFR8W0g6w7AeVQUDg1tAq4DpLdt4.png', 'مهارات الطريق إلى الحكمة (الوهاب)', '799', '2021-05-28', '2021-05-30', 'يومين', '<ul><li>ماهو الفرق العميق بين الرسالة الروحية والشغف والموهبة؟&nbsp;</li><li>من هو النجم وهل الموهبة بوابة النجومية؟&nbsp;</li><li>هل كل الناس لهم مواهب أم أن الموهبة نادرة مثل ما تعتقد؟&nbsp;</li><li>كيف تنشط اسم الله الوهاب في نفسك وحياتك وترى تجليات موهبتك و أرزاقها العظيمة؟&nbsp;</li><li>لماذا كثير من الموهبين يعيشون اليسر والسعة في الرزق ويستحقون ( كل ميسر لما خلق له )؟&nbsp;</li><li>هل الأسماء الحسنى التي تعمل معك بسهولة علامة انه موهبتك؟&nbsp;</li><li>ماهي العلاقة العظيمة بين موهبتك ونجاح استخلافك في الأرض؟&nbsp;</li><li>(هل تفهم سر قد أفلح من زكاها )وعلاقة تزكية النفس بخروج أفضل ما فيها وأعمقه ( الموهبة)؟</li></ul><div><br></div>', NULL, 'كيف نربط بين البصيرة ومعرفتك بذاتك وبين الموهبة؟ ماهو الفرق العميق بين الرسالة الروحية والشغف والموهبة؟ من هو النجم وهل الموهبة بوابة النجومية؟ هل كل الناس لهم مواهب أم أن الموهبة نادرة مثل ما تعتقد؟', '<p><br></p>', 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 6, 0, 0, '2021-05-25 00:48:11', '2021-05-27 20:08:14'),
(16, 22, 23, 'https://gmcksa.com/storage/app/public/images/6o804fEiqhBVW6uRGIWlenDf1QlS9KYfFI95IZ3P.jpg', 'تطوير مهارات مدقق/كبير مدققين نظام إدارة الجودة الأيزو9001 2015', '2950', '2021-05-29', '2021-06-02', '5 أيام', '<ul><li><li>&nbsp;المقدمة والمفاهيم الحديثة وأساسيات نظام إدارة الجودة لسلسلة 9000 ISO</li><li>&nbsp;عرض متطلبات المواصفة القياسية الدولية لنظام إدارة الجودة 9001:2015 ISO&nbsp;</li><li>مبادئ وأساسيات عمليات التدقيق وفقاً للمواصفة الدولية 19011:2018 ISO&nbsp;</li><li>جدولة وتخطيط عمليات التدقيق الداخلية والخارجية وفقا لمتطلبات المواصفة القياسية الدولية 9001:2015 ISO&nbsp;</li><li>مسؤوليات المدقق ، وسمات وآليات اختيار المدققين&nbsp;</li><li>الأدوات والتقنيات الفعالة للتدقيق وفقاً للمواصفة القياسية الدولية لنظام إدارة الجودة 9001:2015 ISO&nbsp;</li><li>إعداد التقارير الفعالة والمتابعة للتدقيق على نظام إدارة الجودة وفق المواصفة القياسية الدولية 9001:2015 ISO&nbsp;</li><li>إجراءات اعتماد وتسجيل المدققين والاختبار النهائي للمشاركين</li></li></ul>', NULL, 'يهدف البرنامج إلى اكتساب المتدربين لمجموعة من المهارات والمعارف والقناعات الخاصة باحتراف المراجعة (التدقيق) على نظام إدارة الجودة بشكل عام والمراجعات لصالح جهات منح شهادات 9001 ISO بشكل خاص', NULL, 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 16, 0, 0, '2021-05-25 01:50:04', '2021-05-27 20:07:32'),
(17, 26, 19, 'https://gmcksa.com/storage/app/public/images/eUI0sZFEtRFBItrdMTmHffZwH3n8WFWNOUKjpxfr.png', 'حماية الذات (الصحة النفسية)', '650', '2021-05-29', '2021-05-30', '2', '<ul><li><li>اكتساب مهارات للتخلص من التوتر والقلق</li><li>تحقيق الاتزان النفسي والوجداني في شخصية الانسان</li><li>التخلص من الذكريات السلبية وتراكم المشاعر السلبية</li><li>تشخيص القلق المرضي وأنواع الاكتئاب والتعرف على 21 عرض لها.</li><li>مسارات علاج الاضطرابات النفسية:</li><li>«الايماني – تقدير الذات – الغذائي.. الخ – الافكار العقلاني – والآلية»</li><li>تعلم طرق العلاج السلوكي للقلق المرضي والاكتئاب</li><li>تدريبات عملية على الاسترخاء للتخلص من القلق والتوتر</li><li>بناء نمط جديد وأسلوب حياة يشكل وقاية من الاضطرابات النفسية</li><li>ويجعل الفرد مستمتعاً بحياته.</li></li></ul>', NULL, 'الصحة النفسية عبارة عن حالة من العافية يمكن فيها للفرد تكريس قدراته او قدراتها الخاصة والتكيّف مع أنواع الإجهاد العادية والعمل بتفان وفعالية والإسهام في مجتمعه', NULL, 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 6, 0, 0, '2021-05-25 01:59:32', '2021-05-25 01:59:32'),
(18, 11, 22, 'https://gmcksa.com/storage/app/public/images/IXSL7FWApuFaG768UBA2Bf0TqmOd9PKltUt2rUNR.png', 'اكتشاف وتفعيل نقاط القوة ( الشخصية القوية )', '399', '2021-05-30', '2021-05-30', 'يوم 1', '<ul><li><li>كيفية بناء الثقة بالنفس.</li><li>القوة الداخلية وأثرها على بناء الفرص.</li><li>كيفية التعامل بذكاء مع الصراعات ومناطق التوتر؟</li><li>أسرار التعامل مع الشخصيات الصعبة.</li><li>قانون السقف، وأثره العظيم على علاقتي.</li><li>قوة الشخصية وإدارة المسافة مع الآخرين «هندسة المسافات».</li><li>الأثر العظيم للتوكيد وتقدير الذات على الحياة والنجاح.</li><li>كيفية إدارة ضغوطات الحياة بنجاح «إدارة الضغوطات».</li></li></ul>', NULL, 'القوة لا تأتي من الإمكانات الجسدية بل من عزيمة الإرادة', NULL, 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 6, 0, 0, '2021-05-25 02:05:26', '2021-05-27 20:06:36'),
(19, 31, 19, 'https://gmcksa.com/storage/app/public/images/yIrUn4yQMHxlsOCwcHidmFRcPLoTAv9EH72ZJFp3.jpg', 'فن الاتيكيت', '300', '2021-05-31', '2021-05-31', 'يوم 1', '<ul><li><li>مفهوم الاتيكيت والبرتوكول والبرستيج.</li><li>اتيكيت الاستقبال والتحية والمصافحة.</li><li>اتيكيت التعريف وتبادل بطاقات التعارف.</li><li>اتيكيت اختيار العطور بحسب المناسبات.</li><li>اتيكيت الحفلات والضيافة.</li></li></ul>', NULL, 'الإتيكيت أحد آداب السلوكيات الاجتماعية، سواء كانت المُعاشرة، أو الخصال الحميدة، أو التصرف بشكل لبق، والمجاملة، ويمتلك الإتيكيت العديد من القواعد التي تتحكم في تعاملات وسلوكيات الإنسان مع الناس.', NULL, 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 'https://www.youtube.com/embed/QXBUPCa4WhU\" title=\"YouTube video player', 6, 0, 0, '2021-05-25 02:14:10', '2021-05-25 02:22:40'),
(20, 16, 19, 'https://gmcksa.com/storage/app/public/images/x2N0kIzhZc6syAOy9JgBPoCsk0Bbii1MB2D4pJJZ.jpg', 'منصة التغير ( قوة التجاوز)', '299', '2021-05-31', '2021-05-31', 'يوم 1', '<ul><li><li>صيغ تفكير مفيدة لتجاوز المشاعر المؤذية.</li><li>تمرين التوقف عن اللوم والتحسر.</li><li>كيف تمتلك مقومات التجاوز.</li><li>تمرين تغيير الصورة الذهنية القديمة.</li><li>أفكار تهيئة النفس لمرحلة التسليم.</li></li></ul>', NULL, 'من الطبيعي أن يمر الإنسان في حياته ببعــض المواقـف الصعبة والأزمــات التـي تجعله يشعـــر بالاستسلام والتعب النفسي ولكن من غير الطبيعي أن يستسلم الإنسان لهذه الظروف وأن يضعف لدرجة يؤثر فيهــا علـى نجـــاح حيـــاته واستمـراها', NULL, 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 'https://www.youtube.com/embed/QXBUPCa4WhU\" title=\"YouTube video player', 6, 0, 0, '2021-05-25 02:51:50', '2021-05-25 02:51:50'),
(21, 16, 19, 'https://gmcksa.com/storage/app/public/images/Cp50r3gO6uwgy5OT6xpdGYKPIa20ghfqB0TBGvHR.png', 'حماية الذات ( فك التعلق والوقاية منه)', '299', '2021-06-01', '2021-06-01', 'يوم 1', '<ul><li><li>فك التعلق والوقاية منه.</li><li>فهـم التعلــق بشكل أعمق.</li><li>الوقاية من التعلق عن طريق رفع تقدير الذات.</li><li>صيـغ تفكــير فـك التعلـق.</li><li>مجموعـة تمــارين لفـك التعلـق.</li></li></ul>', NULL, 'التعلق حالة تأسرك وتصنع حاجزاً نفسياً وهمياً؛ وهو أنك لا تستطيع أن تعيش في الحياة من دون أفراد معينين، وهو ما يعني أنك أصبحت تشعر بأنك لا تستطيع أن تعمل شيئاً دون وجود هؤلاء المقربين منك', NULL, 'https://gmc-sa.net', 'https://youtu.be/QXBUPCa4WhU', 6, 0, 0, '2021-05-25 02:57:14', '2021-05-25 21:56:57'),
(23, 5, 23, 'https://gmcksa.com/storage/app/public/images/euM19fynXzWiwNmqh8rf9gySqS4QOf8FX123au4l.png', 'منصة التغير ( الخجل الاجتماعي)', '115', '2021-06-06', '2021-06-06', 'يوم 1', '<ul><li><li>الخجل الاجتماعي كيف يعمل في ذهن الإنسان ومتى يتحول لرهاب اجتماعي.</li><li>منشأ الخجل الاجتماعي وعلاقته بضعف الثقة بالنفس وعلاقته بالطفولة.</li><li>مشكلات التواصل مع الآخرين، وسلوكيات تحافظ على بقاء المشكلة.</li><li>الخوف من الرفض والتقييم وطرق التعامل معه.</li><li>القيود الذهنية والافتراضات الخاطئة المتعلقة بالخجل الاجتماعي، وكيفية التعامل معها.</li></li></ul>', NULL, 'مشاعر الخجل أو عدم الراحة في بعض المواقف لا تعكس بالضرورة علامات اضطراب القلق الاجتماعي فتختلف مستويات الراحة في المواقف الاجتماعية', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-05-25 21:52:43', '2021-05-27 19:47:47'),
(24, 8, 19, 'https://gmcksa.com/storage/app/public/images/j6Vw0LxRafL9SbLIXiOT0nYfD8RfPS1vgoGdJIip.png', 'ادارة المشاريع الاحترافية', '1250', '2021-06-13', '2021-06-17', '5 أيام', '<ul><li><li>إنشاء فريق عمل ذو أداء عالي</li><li>البدء في المشروع</li><li>القيام بالعمل</li><li>التأكد من بقاء الفريق على المسار</li><li>وضع الأعمال في الاعتبار</li><li>برنامج متكامل للتعرف على المنهجية الجديدة لإدارة المشاريع</li></li></ul>', NULL, 'هو تخصّص يتعلّق بتنظيم وإدارة الموارد، مثل الموارد البشريّة، بالطريقة التي تمكّن إنجاز المشروع باحترام مضمونه المحدد وبمراعاة عوامل الجودة والتوقيت والتكلفة', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-05-25 23:46:05', '2021-05-25 23:47:30'),
(25, 9, 23, 'https://gmcksa.com/storage/app/public/images/4UJ7CsM8u2DXO3PpdKv9OLfxkfjoH7jiJeMzMYA9.png', 'فن الحوار', '115', '2021-06-14', '2021-06-14', 'يوم 1', '<ul><li><li>لماذا نفشل في كثير من حواراتنا وكيف نتجاوز ذلك؟</li><li>امتلك القدرة على جذب الآخرين في حديثك وحوارك</li><li>الحوار بين الرجل والمرأة والاقناع لكل منهما</li><li>نماذج التأثير الإنساني وتطبيقها&nbsp;</li><li>قوانين الاقناع والتأثير في الآخرين</li><li>حالة التعصب في الرأي وطريقة التعامل معها</li></li></ul>', NULL, 'الحوار هو فن الاتصال والتواصل مع الآخرين بطريقة حضاريّة وراقية بعيداً عن العصبيّة والتشدد والمغالاة في التمسك بالرأي، ويعتبر فن الحوار أحد أهم الفنون التي يُتقنها الناجحون', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-05-25 23:58:48', '2021-05-27 19:46:30'),
(26, 9, 23, 'https://gmcksa.com/storage/app/public/images/J7xdphGlwEFodJN8BRTGU6rduGV4fUvqMyjrjcNA.png', 'مهارات الالقاء والعرض', '230', '2021-06-15', '2021-06-16', 'يومين', '<ul><li><li>التعرف على وسائل التخلص من الخوف والقلق.</li><li>التعرف على أسس بنـاء عملية الـعرض والتحضير للمادة المعروضة.</li><li>التعرف على قواعد الإلقاء الجيد وسبل إيصال المعلومة.</li><li>اكتساب مهارة الإقناع أثناء العرض.</li><li>التعرف على أفضل وسائل الإلقاء والصوت الجيد.</li><li>التحذير من أخطاء الملقين وبيان طرق التخلص منها.</li></li></ul>', NULL, 'الإلقاء هو مجموعةٌ من الكلمات المنثورة، التي يخاطب بها المتكلم جمعاً من المستمعين؛ بهدف إقناعهم والتأثير فيهم', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-05-26 00:04:41', '2021-05-27 19:46:09'),
(27, 35, 57, 'https://gmcksa.com/storage/app/public/images/soZaFhy0rZsIJPTlUgUOn3jb9M2SMY8OdY2TZdgy.jpg', 'الحزام الأصفر', '300', '2021-06-06', '2021-06-06', 'يوم 1', '<ul><li><li>المفاهيم والمصطلحات الرئيسية المرتبطة بمنهجية “السته سيقما و اللين”</li><li>مرحلة تعريف المشروع و كيفية استخدام الأدوات الخاصة بها&nbsp;</li><li>(SIPOC, CTQ, VOC, TPC &amp; Project Charter)</li><li>مرحلة قياس المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Introduction to statistic, Data collection plan &amp; Sigma Level)</li><li>مرحلة تحليل المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Brainstorming, Fishbone, CI Matrix, Pareto, Value Stream Mapping, Touch Time and Lag Time &amp; 8 Wastes)</li><li>مرحلة تحسين المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Prioritization Matrix, Action Plan, Effort and Impact Matrix, 5S, Visual Management, SOP, OPL, ESCAP &amp; Poka Yoke)</li><li>مرحلة التحكم بالمشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Sustain Plan, Control Plan &amp; Lesson Learned)</li></li></ul>', NULL, 'منهجية لين ستة سيجما هي منهجية إدارية لتحسين أداء الأعمال، حيث تمزج مابين منهجية اللين (منهجية تقليل الهدر) وستة سيجما. يوفر تدريب منهجية اللين الستة سيجما -الحزام الأصفر نظرة عامة على تقنيات ستة سيجما ومقاييسها ومنهجيات التحسين الأساسية.', NULL, NULL, 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 16, 0, 0, '2021-05-26 20:19:03', '2021-05-27 19:45:31'),
(28, 35, 57, 'https://gmcksa.com/storage/app/public/images/KGVhHHmbsf8eVAzijtc16vJx4zRufVvUjrWMrx04.png', 'الحزام الأخضر', '700', '2021-06-07', '2021-06-10', '4 أيام', '<ul><li><li>المفاهيم والمصطلحات الرئيسية المرتبطة بمنهجية “السته سيقما و اللين”</li><li>مرحلة تعريف المشروع و كيفية استخدام الأدوات الخاصة بها&nbsp;</li><li>(SIPOC, CTQ, VOC, TPC &amp; Project Charter)</li><li>مرحلة قياس المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Introduction to statistic, Data collection plan &amp; Sigma Level)</li><li>مرحلة تحليل المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Brainstorming, Fishbone, CI Matrix, Pareto, Value Stream Mapping, Touch Time and Lag Time &amp; 8 Wastes)</li><li>مرحلة تحسين المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Prioritization Matrix, Action Plan, Effort and Impact Matrix, 5S, Visual Management, SOP, OPL, ESCAP &amp; Poka Yoke)</li><li>مرحلة التحكم بالمشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Sustain Plan, Control Plan &amp; Lesson Learned)</li></li></ul>', NULL, 'منهجية لين ستة سيجما هي منهجية إدارية لتحسين أداء الأعمال، حيث تمزج مابين منهجية اللين (منهجية تقليل الهدر) وستة سيجما. يوفر تدريب منهجية اللين الستة سيجما -الحزام الأصفر نظرة عامة على تقنيات ستة سيجما ومقاييسها ومنهجيات التحسين الأساسية.', NULL, NULL, 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 16, 0, 0, '2021-05-26 20:45:47', '2021-05-27 19:45:09'),
(29, 35, 57, 'https://gmcksa.com/storage/app/public/images/hXmEyBV5auQlXwV4JFScNYD7URwAs8yv6EEqFSPz.png', 'الحزام الأسود', '700', '2021-06-13', '2021-06-15', '3 أيام', '<ul><li><li>تحليل نظام القياس MSA (Cpk &amp; Ppk)</li><li>التحكم الإحصائي بالعميات SPC</li><li>خرائط التحكم للمتغيرات الكمية</li><li>(Variable Data)</li><li>خرائط التحكم للمتغيرات النوعية</li><li>(Attribute Data)</li><li>اختبار الفرضيات لمعرفة الأسباب</li><li>الجذرية للمشكلة</li><li>العلاقة بين منهجية الستة سيقما</li><li>ومفهوم إعادة هندسة العمليات</li><li>العلاقة بين إدارة المشاريع ومنهجية</li><li>الستة سيقما&nbsp;</li><li>(Change, Risk &amp; Communication Plans)</li></li></ul>', NULL, 'منهجية لين ستة سيجما هي منهجية إدارية لتحسين أداء الأعمال، حيث تمزج مابين منهجية اللين (منهجية تقليل الهدر) وستة سيجما. يوفر تدريب منهجية اللين الستة سيجما -الحزام الأصفر نظرة عامة على تقنيات ستة سيجما ومقاييسها ومنهجيات التحسين الأساسية.', NULL, NULL, 'https://www.youtube.com/watch?v=QXBUPCa4WhU', 16, 0, 0, '2021-05-26 20:53:12', '2021-05-27 19:44:47'),
(30, 10, 23, 'https://gmcksa.com/storage/app/public/images/KXcRGlA23lNJnR88E32wHtVQIj9PANB79yGNyIxX.png', 'الابداع في العلاقات الانسانية (المرأة اللغز)', '450', '2021-06-10', '2021-06-11', 'يومين', '<ul><li><li>7 أسرار في الرجل تجهلها النساء.</li><li>13 خطأ قاتل ترتكبها معظم النساء!</li><li>الحاجات الـ9 الأساسية التي يريدها الرجل.</li><li>مفهوم الدائرة العامة والخاصة عند الرجل.</li><li>8 ممارسات تقوم بها الزوجة لقتل علاقتها الزوجية دون أن تدري.</li><li>الألغاز الـ 9 لكي تكون الزوجة لغز يعشقه الرجل.</li></li></ul>', NULL, '7 أسرار في الرجل تجهلها النساء 13 خطأ قاتل ترتكبها معظم النساء الحاجات الـ9 الأساسية التي يريدها الرجل مفهوم الدائرة العامة والخاصة عند الرجل', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-05-26 21:03:40', '2021-05-27 19:43:46'),
(31, 28, 19, 'https://gmcksa.com/storage/app/public/images/KFPZ2i7c1jOPdD2uAAWjoTn3G4b4bTrrtMdvMyQb.png', 'مهارات تصميم الشعارات وبناء الهوية البصرية', '350', '2021-06-11', '2021-06-12', 'يومين', '<div><br></div><ul><li>التعرف على واجهة برنامج Adobe Illustrator.</li><li>التعرف على أنواع الشعارات.</li><li>تعلم أساسيات رسم الشعارات.</li><li>توليد الأفكار والعصف الذهني.</li><li>تعلم تصميم تطبيقات الهوية البصرية.</li><li>تعلم تنفيذ المخطوطات.</li><li>تعلم تصميم الأيقونات.</li><li>تعلم تجويد المشروع.</li><li>تعلم عرض الهوية على العملاء.</li><li>إخراج المشروع.</li></ul>', NULL, 'الشعار هو عبارةٌ عن صورةٍ أو رسمةٍ بصريةٍ إيضاحيةٍ، وهو الوجه المحدد الذي يتمّ من خلاله التعرف على شخصٍ ما أو مؤسسةٍ أو شركة أو منتج محدد', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 4, 0, 0, '2021-05-27 21:23:07', '2021-05-27 21:25:06'),
(32, 26, 19, 'https://gmcksa.com/storage/app/public/images/09jFpBi9qoprlior8N9k0gAoEsHDq9MoEx16ulez.png', 'استراتيجيات حل المشاكل (التعامل مع المشكلات والتحديات الحياتية )', '299', '2021-07-06', '2021-07-06', 'يومين', '<ul><li><li>منبع المشكلات الثلاث</li><li>قوة الاختيار واتخاذ القرار</li><li>قوة المسؤولية وراحة البال&nbsp;</li><li>أنماط التفكير خارج المألوف</li></li></ul>', NULL, 'المشاكل أكثر ما يقلق ويوتر الأشخاص، وتراكم المشاكل فوق بعضها البعض دون وجود الحلول لهذه المشكلة قد يزيد من تفاقم المشكلات', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-06-12 22:55:22', '2021-06-12 22:57:23'),
(33, 26, 19, 'https://gmcksa.com/storage/app/public/images/xKm0GdGmHZCyzYsAM1Il6P0W6fQ7lX2xb8WTuL4z.png', 'مهارات تعديل سلوك الاطفال  (الصحة النفسية للطفل)', '299', '2021-07-05', '2021-07-05', 'يوم 1', '<ul><li><li>مستويات الصحة النفسية الأربعة للطفل</li><li>الصحة النفسية والحاجات الأساسية</li><li>كيف نقوي الصلابة النفسية للطفل</li><li>أخطاء المربين المدمرة للصحة النفسية</li><li>كيف نحقق الغاية الكبرى للصحة النفسية</li></li></ul>', NULL, 'تعتبر خدمات الإرشاد النفسي أداة تربوية نفسية شاملة تساعد على إشباع احتياجات أبنائنا وتقوية حوافزهم وإثراء خبراتهم , وهي تسهم بشكل كبير في تحقيق النمو السوي لديهم وفقاً لميولهم وقدراتهم واستعداداتهم', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-06-12 23:04:43', '2021-06-12 23:04:43'),
(34, 11, 19, 'https://gmcksa.com/storage/app/public/images/QaBUoANV1p2rc3gVIMW4IcgdS3FTe7iwUmNjMl21.png', 'سيكولوجية المال (الذكاء المالي)', '349', '2021-07-03', '2021-07-03', 'يوم 1', '<ul><li><li>كيف يكفي الراتب إلى نهاية الشهر.</li><li>عشر أفكار ذهبية لتنمية الذكاء المالي.</li><li>لماذا يصبح الفقراء أكثر فقراً والأغنياء أكثر غنى.</li><li>أسرار الذكاء المالي في كتابة الوصية والعقود.</li><li>أربع خطوات عملية للتخلص من الديون.</li><li>عشر أسباب لفشل المشاريع التجارية.</li></li></ul>', NULL, 'جمع المال من أجل جمع المال فحسب لن يضمن لك السعادة؛ فالهدف من جمع المال هو إنفاقه على أهدافك، لأن السعادة تنبع من استخدام هذا المال على تلك الأهداف.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-06-16 00:47:05', '2021-06-16 00:47:05'),
(35, 11, 19, 'https://gmcksa.com/storage/app/public/images/NrsM8zXPFChdth2RgOnggioSW9NbvUJJmkb05ovd.png', 'أطلق قدراتك (الحظ العظيم)', '699', '2021-07-01', '2021-07-02', 'يومين', '<ul><li><li>ما هو المعنى العميق لقانون العدل الإلهي ؟</li><li>ما هو معنى الحظ العظيم وعلاقته بالكارما الذهبية؟</li><li>كيف تدمر كارما الفطر وكارما الظلم الأأرزاق مهما كانت كثيرة؟</li><li>لماذا البعض لديه قدرات ومواهب لكنه متعطل في الحياة «كارما التعطيل»؟</li><li>هل تحلق كارما الأجداد بنا وتؤثر على حياتنا؟</li><li>كيف افهم رسالة العلاقات المرتبكه والصرعات والكيد وعلاقته بكارما التعاملات؟</li><li>هل بعض الأمراض ممكن أن تكون رسالة لكارما سلبية لي؟</li><li>كيف يتم تنظيف الكارما السلبية «السيئات» تطبيقات الاستغفار الحقيقي؟</li><li>سر الكارما الذهبية واخراجك من عن الزجاجة بشكل مذهل «تذويب المشكلات»؟</li><li>كيف استحق بالكارما الذهبية ارزاق اللطف التي لا تحتاج سعي «يرزقك من حيث لا تحتسب»؟</li><li>علاقة الكارما الذهبية باستحقاق النقلة في حياتك والفتوحات.</li><li>ما هي الأعمال التي تبني لي كارما ذهبية لي وقد تمتد لأحفادي من بعدي؟</li><li>علاقة الكارما الذهبية بالتسخير والتيسير العالي في الأرض.</li></li></ul>', NULL, 'علاقة الكارما الذهبية باستحقاق النقلة في حياتك والفتوحات', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-06-16 02:20:16', '2021-06-16 02:21:26'),
(36, 29, 19, 'https://gmcksa.com/storage/app/public/images/9qr4FDNmRIH2Mm2l5anGcorh5o63qWTqR2Ioxxr7.png', 'إدارة سلسلة الامداد والمشتريات واللوجيستيات SCM', '399', '2021-06-29', '2021-06-30', 'يومين', '<ul><li><li>مفاهيم سلاسل الإمداد العالمية وتطوراتها.</li><li>عناصر ومكونات سلاسل الإمداد.</li><li>الطرق الاحترافية للتعاقد مع الموردين.</li><li>تطوير عمليات المشتريات والتموين.</li><li>الإدارة الاستراتيجية للوجستيات الحديثة.</li><li>مخاطر سلاسل الإمداد العالمية.</li><li>دور سلاسل الإمداد في التجارة الالكترونية.</li></li></ul>', NULL, 'أصبحت إدارة سلاسل الإمداد أمراً لا يمكن الاستغناء عنه بسبب الابتكار المستمر في التكنولوجيا وتوقعات العملاء المتغيرة بسرعة', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 9, 0, 0, '2021-06-16 02:33:58', '2021-06-16 02:33:58'),
(37, 10, 19, 'https://gmcksa.com/storage/app/public/images/VsWDuWKXEoD4kbqRRfhq0G9kDSdTJ9dNpyXfWxPM.png', 'بوصلة التفكير ( مقياس هيرمان)', '399', '2021-06-29', '2021-06-30', 'يومين', '<ul><li><li>فهم ذاتك وفهم تصرفاتك.</li><li>معرفة مكانك وقدراتك في العمل</li><li>فهم الآخرين وفهم تصرفاتهم</li><li>القدرة على تحويل الشخصيات إلى شخصيات إيجابية</li><li>التعرُّف على ردود فعل الآخرين</li><li>معرفة القوة والضعف في العلاقات الأسرية والإجتماعية</li><li>معرفة نسبة التوافق بين الأزواج وقوة العلاقة</li><li>القدرة على تحليل شخصية الموظفين</li><li>القدرة على تدوير الموظفين حسب كفائتهم وإبداعاتهم</li></li></ul>', NULL, 'رفع مستوى فاعلية التفاهم والتخاطب بشكل إيجابي عن طريق فهم الاخرين زملاء العمل و الموظفين و الابناء', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-06-16 23:56:15', '2021-06-16 23:56:15'),
(38, 8, 19, 'https://gmcksa.com/storage/app/public/images/NBcWt2s3qtypAR84egB9p9bvUpjw87Ok9E4eIZEv.png', 'إدارة المشاريع الاحترافية PMP', '999', '2021-07-10', '2021-07-14', '5 أيام', '<ul><li><li>مدخل إلى إدارة المشاريع</li><li>إدارة الموارد البشرية للمشروع</li><li>إدارة الاتصالات في المشروع</li><li>نظام ودورة حياة المشروع</li><li>إدارة التعاقدات والتوريدات</li><li>نظرة على دليل إدارة المشروعات «PMBOK»</li></li></ul>', NULL, 'هو تخصّص يتعلّق بتنظيم وإدارة الموارد، مثل الموارد البشريّة، بالطريقة التي تمكّن إنجاز المشروع باحترام مضمونه المحدد وبمراعاة عوامل الجودة والتوقيت والتكلفة', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-06-17 00:02:18', '2021-06-17 00:02:18'),
(39, 28, 19, 'https://gmcksa.com/storage/app/public/images/jGaJnHBTEQMmxY3bZRDO1ZMpjfr2A2UrajXfUmfD.png', 'مهارات تصميم الإنفوجرافيك', '399', '2021-06-28', '2021-06-30', '3 أيام', '<ul><li><li>التعرف على واجهة برنامج Adobe Illustrator.</li><li>تعلم الأدوات الرئيسية.</li><li>تعلم رسم الأيقونات.</li><li>توليد الأفكار لصناعة الانفوجرافيك.</li><li>تنظيم المحتوى.</li></li></ul>', NULL, 'تعلم رسم الأيقونات وتوليد الأفكار لصناعة بداية من تنظيم المحتوى وإخراجه ثم نشره', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 4, 0, 0, '2021-06-20 20:10:46', '2021-06-20 20:10:46'),
(40, 28, 19, 'https://gmcksa.com/storage/app/public/images/ssY97LMUauF1BHeC2lhMNqXhPsrUOYRRaJP2jTBr.png', 'دورة مهارات تصميم الانفوجرافيك', '399', '2021-07-11', '2021-07-13', '3 أيام', '<ul><li><li>التعرف على واجهة برنامج Adobe Illustrator.</li><li>تعلم الأدوات الرئيسية.</li><li>تعلم رسم الأيقونات.</li><li>توليد الأفكار لصناعة الانفوجرافيك.</li><li>تنظيم المحتوى.</li></li></ul>', NULL, 'تعلم رسم الأيقونات وتوليد الأفكار لصناعة بداية من تنظيم المحتوى وإخراجه ثم نشره', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 4, 0, 0, '2021-07-09 00:37:14', '2021-07-09 00:37:14'),
(41, 5, 19, 'https://gmcksa.com/storage/app/public/images/DVPPmdYl0ArI04MzvzZowaSu2s9H3mIHYBOiUfMr.png', 'دورة إدارة الضغوط والانفعالات “إدارة الغضب”', '115', '2021-07-15', '2021-07-15', 'يوم 1', '<ul><li><li>ماهو تفسير الغضب في حياتنا؟</li><li>لماذا البعض أكثر عصبية من البعض الآخر؟</li><li>ماهي التقنيات العملية للتعامل مع الغضب؟</li><li>العقل الغاضب كيف يعمل من الداخل؟</li><li>كيف ننزع فتيل الغضب قبل أن يبدأ “البحث عن المحفز”؟</li></li></ul>', NULL, 'الغضبُ قوة محايدة قد تستخدمها بشكل غير صحيح فتخسر نفسك، وتفقد محبة من حولك، أو تستخدمها دفاعًا عن الخير ودفعا للشر.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-07-10 23:03:05', '2021-07-10 23:03:05'),
(42, 9, 19, 'https://gmcksa.com/storage/app/public/images/W7Tpg2yM4cJA6Vm7VogAmIf8NnT8oP7bPBCta8Tf.png', 'دورة مهارات الإلقاء والعرض “تحدث بثقة”', '230', '2021-07-17', '2021-07-18', 'يومين', '<ul><li><li>التعرف على وسائل التخلص من الخوف والقلق.</li><li>التعرف على أسس بنـاء عملية الـعرض والتحضير للمادة المعروضة.</li><li>التعرف على قواعد الإلقاء الجيد وسبل إيصال المعلومة.</li><li>اكتساب مهارة الإقناع أثناء العرض.</li><li>التعرف على أفضل وسائل الإلقاء والصوت الجيد.</li><li>التحذير من أخطاء الملقين وبيان طرق التخلص منها.</li></li></ul>', NULL, 'الإلقاء هو مجموعةٌ من الكلمات المنثورة، التي يخاطب بها المتكلم جمعاً من المستمعين؛ بهدف إقناعهم والتأثير فيهم', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-07-10 23:09:45', '2021-07-10 23:09:45'),
(43, 29, 19, 'https://gmcksa.com/storage/app/public/images/8fWgV6sHjmxuktXcE4kbAGN5zhlF7AWfW0dR5CYO.png', 'دورة إدارة سلسلة الامداد والمشتريات واللوجيستيات SCM', '299', '2021-07-27', '2021-07-28', 'يومين', '<ul><li><li>مفاهيم سلاسل الإمداد العالمية وتطوراتها.</li><li>عناصر ومكونات سلاسل الإمداد.</li><li>الطرق الاحترافية للتعاقد مع الموردين.</li><li>تطوير عمليات المشتريات والتموين.</li><li>الإدارة الاستراتيجية للوجستيات الحديثة.</li><li>مخاطر سلاسل الإمداد العالمية.</li><li>دور سلاسل الإمداد في التجارة الالكترونية</li></li></ul>', NULL, 'أصبحت إدارة سلاسل الإمداد أمراً لا يمكن الاستغناء عنه بسبب الابتكار المستمر في التكنولوجيا وتوقعات العملاء المتغيرة بسرعة', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 16, 0, 0, '2021-07-10 23:12:03', '2021-07-10 23:12:03'),
(44, 29, 19, 'https://gmcksa.com/storage/app/public/images/yesmWk7epSWrrD7s9b84po8Pds6E25yqvThkJeo4.png', 'دورة الإدارة الحديثة للمستودعات الحكومية والخدمية', '299', '2021-07-29', '2021-07-30', 'يومين', '<ul><li><li>مفاهيم إدارة المستودعات واهدافها.</li><li>ادارة مخاطر المستودعات.&nbsp;</li><li>عوامل نجاح ادارة المستودعات.</li><li>انواع المستودعات وطريقة اعمالها.</li><li>طريقة تصنيف المواد داخل المستودعات.&nbsp;</li><li>الاساليب المخزنية للمستودعات.</li><li>مهام وادوات الرقابة على المخزون.</li><li>اجراءات وعمليات الجرد ونماذجها المستخدمة.&nbsp;</li><li>تخطيط المخزون وتحديد مستوياته المُثلى.</li><li>المهام الاحترافية للعاملين في المستودعات</li></li></ul>', NULL, 'معرفة التنظيم الداخلي لجهاز المستودعات. إدراك أهمية المخزون والتخزين وتكاليفه ومبررات الاحتفاظ به. المحافظة على سلامة وأمن العاملين والمخزونات بالمستودعات.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 4, 0, 0, '2021-07-10 23:14:38', '2021-07-10 23:14:38'),
(45, 36, 19, 'https://gmcksa.com/storage/app/public/images/6nJywcqk5nKW2QeFFYmNWuBQtTiSFivsBuCbE9HX.png', 'منهجية لين 6 سيحما “الحزام الأصفر”', '300', '2021-08-01', '2021-08-01', 'يوم 1', '<ul><li><li>المفاهيم والمصطلحات الرئيسية المرتبطة بمنهجية “السته سيقما و اللين”</li><li>مرحلة تعريف المشروع و كيفية استخدام الأدوات الخاصة بها&nbsp;</li><li>(SIPOC, CTQ, VOC, TPC &amp; Project Charter)</li><li>مرحلة قياس المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Introduction to statistic, Data collection plan &amp; Sigma Level)</li><li>مرحلة تحليل المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Brainstorming, Fishbone, CI Matrix, Pareto, Value Stream Mapping, Touch Time and Lag Time &amp; 8 Wastes)</li><li>مرحلة تحسين المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Prioritization Matrix, Action Plan, Effort and Impact Matrix, 5S, Visual Management, SOP, OPL, ESCAP &amp; Poka Yoke)</li><li>مرحلة التحكم بالمشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Sustain Plan, Control Plan &amp; Lesson Learned)</li></li></ul>', NULL, 'منهجية لين ستة سيجما هي منهجية إدارية لتحسين أداء الأعمال، حيث تمزج مابين منهجية اللين (منهجية تقليل الهدر) وستة سيجما. يوفر تدريب منهجية اللين الستة سيجما -الحزام الأصفر نظرة عامة على تقنيات ستة سيجما ومقاييسها ومنهجيات التحسين الأساسية.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-07-10 23:18:07', '2021-07-10 23:18:07'),
(46, 38, 19, 'https://gmc-sa.net/storage/app/public/images/ATIriNOSmqSolhwOtvPpMb5NptCVQGDdoLyWNDrR.png', 'منهجية لين 6 سيحما “الحزام الأخضر”', '700', '2021-08-31', '2021-09-01', 'يومين', '<ul><li><li>المفاهيم والمصطلحات الرئيسية المرتبطة بمنهجية “السته سيقما و اللين”</li><li>مرحلة تعريف المشروع و كيفية استخدام الأدوات الخاصة بها&nbsp;</li><li>(SIPOC, CTQ, VOC, TPC &amp; Project Charter)</li><li>مرحلة قياس المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Introduction to statistic, Data collection plan &amp; Sigma Level)</li><li>مرحلة تحليل المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Brainstorming, Fishbone, CI Matrix, Pareto, Value Stream Mapping, Touch Time and Lag Time &amp; 8 Wastes)</li><li>مرحلة تحسين المشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Prioritization Matrix, Action Plan, Effort and Impact Matrix, 5S, Visual Management, SOP, OPL, ESCAP &amp; Poka Yoke)</li><li>مرحلة التحكم بالمشروع و كيفية استخدام الأدوات الخاصة بها</li><li>(Sustain Plan, Control Plan &amp; Lesson Learned)</li></li></ul>', NULL, 'منهجية لين ستة سيجما هي منهجية إدارية لتحسين أداء الأعمال، حيث تمزج مابين منهجية اللين (منهجية تقليل الهدر) وستة سيجما. يوفر تدريب منهجية اللين الستة سيجما -الحزام الأصفر نظرة عامة على تقنيات ستة سيجما ومقاييسها ومنهجيات التحسين الأساسية.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-07-10 23:23:12', '2021-08-17 23:01:36'),
(47, 37, 19, 'https://gmcksa.com/storage/app/public/images/5qLVoIOHocfzNggYmMlx5frVOWE4uZfA7NbEwWqP.png', 'منهجية لين 6 سيحما “الحزام الأسود”', '700', '2021-08-05', '2021-08-05', '3 أيام', '<ul><li><li>تحليل نظام القياس MSA (Cpk &amp; Ppk)</li><li>التحكم الإحصائي بالعميات SPC</li><li>خرائط التحكم للمتغيرات الكمية</li><li>(Variable Data)</li><li>خرائط التحكم للمتغيرات النوعية</li><li>(Attribute Data)</li><li>اختبار الفرضيات لمعرفة الأسباب</li><li>الجذرية للمشكلة</li><li>العلاقة بين منهجية الستة سيقما</li><li>ومفهوم إعادة هندسة العمليات</li><li>العلاقة بين إدارة المشاريع ومنهجية</li><li>الستة سيقما&nbsp;</li><li>(Change, Risk &amp; Communication Plans)</li></li></ul>', NULL, 'منهجية لين ستة سيجما هي منهجية إدارية لتحسين أداء الأعمال، حيث تمزج مابين منهجية اللين (منهجية تقليل الهدر) وستة سيجما. يوفر تدريب منهجية اللين الستة سيجما -الحزام الأصفر نظرة عامة على تقنيات ستة سيجما ومقاييسها ومنهجيات التحسين الأساسية.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-07-10 23:25:05', '2021-07-10 23:25:05'),
(48, 19, 19, 'https://gmcksa.com/storage/app/public/images/7iczn0dXkdafIX3jcHaFP6d1gC2NXka4mgni1zP5.png', 'عرض دورتي (التخطيط الاستراتيجي باستخدام مؤشرات قياس الأداء & مهارات قياس مؤشرات الأداء)', '499', '2021-08-02', '2021-08-04', '3 أيام', NULL, NULL, '.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 16, 0, 0, '2021-07-11 19:53:07', '2021-07-11 19:55:34'),
(49, 39, 19, 'https://gmcksa.com/storage/app/public/images/6U6x1RODqP01TapcpKlxMj6aYu71Ij5gbCGMdCr5.png', 'تدريب المدربين', '599', '2021-08-01', '2021-08-05', '5 أيام', '<ul><li><li>ماهي الأسرار التي تجعل المدرب يأسر المتدربين ويبهرهم.</li><li>كيف تصل لتحقيق أهداف البرنامج بدقة عالية.</li><li>إزالة التوتر والتحدث بطلاقة.</li><li>كيف تحفـز المتدربين بطرق فعّالة ومبتكرة.</li><li>أساليب عملية لتصميم التمارين والانشطة التدريبية.</li></li></ul>', NULL, 'إذا كنت تحلم بأن تكون مدرباً محترفاً في أي مجال, أو تكون قادراً على الإلقاء و التأثير في الآخرين أو تسعى لتطوير مهاراتك القيادية, فلا بد لك من التمكن من المعلومات والمهارات و القناعات و السلوكيات التي تقدمها لك دورة تدريب المدربين', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-07-11 20:01:51', '2021-07-11 20:02:21'),
(50, 31, 19, 'https://gmcksa.com/storage/app/public/images/C1xqTTL6X4iOnn8TffQvUtWqEvv44wv9LoZkNvsP.png', 'دورة فن الإتيكيت “مفهوم الإتيكيت والبروتوكول والبرستيج”', '230', '2021-07-14', '2021-07-14', 'يوم 1', '<ul><li><li>مفهوم الاتيكيت والبرتوكول والبرستيج.</li><li>اتيكيت الاستقبال والتحية والمصافحة.</li><li>اتيكيت التعريف وتبادل بطاقات التعارف.</li><li>اتيكيت اختيار العطور بحسب المناسبات.</li><li>اتيكيت الحفلات والضيافة.</li></li></ul>', NULL, 'الإتيكيت أحد آداب السلوكيات الاجتماعية، سواء كانت المُعاشرة، أو الخصال الحميدة، أو التصرف بشكل لبق، والمجاملة، ويمتلك الإتيكيت العديد من القواعد التي تتحكم في تعاملات وسلوكيات الإنسان مع الناس', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-07-12 19:14:37', '2021-07-12 19:14:37'),
(51, 26, 19, 'https://gmcksa.com/storage/app/public/images/BvMUK5rEP9ufLdebYsouDpesO4pD3jvpk2mg0fwV.jpg', 'دورة مهارات التغلب على القلق والتوتر والاكتئاب', '299', '2021-07-29', '2021-07-29', 'يوم 1', '<ul><li><li>هل تعاني من اضطراب في النوم؟هل تعاني من التعب والإرهاق الجسدي؟</li><li>هل تعاني من الإمساك؟هل تعاني من التشويش الفكري وضعف التركيز؟</li><li>هل تعاني من التوتر وسرعة الإنفعال؟</li><li>هل تعاني من فقدان أو زيادة الشهية في الأكل؟</li></li></ul>', NULL, 'من الطبيعي أن يشعر الإنسان بالقلق أو بالفزع من حين إلى آخر أما إذا كان الإحساس بالقلق يتكرر في أحيان متقاربة دون أي سبب حقيقي، إلى درجة أنه يعيق مجرى الحياة اليومي الطبيعي فالمرجح أن هذا الإنسان يعاني من اضطراب القلق', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-07-12 19:18:44', '2021-07-12 19:18:44'),
(52, 26, 19, 'https://gmcksa.com/storage/app/public/images/0fY3iOu0fxxRulrSKxvWYXacxFbvjSapdCi5LVAG.jpg', 'دورة *حماية الذات* كيف تحمي طفلك من الإيذاء والتنمر؟', '299', '2021-07-30', '2021-07-30', 'يوم 1', '<ul><li><li>تعريف التحرش الجنسي.</li><li>كيف تعرف أن ابنك تعرض للتحرش؟</li><li>آثار التعرض للتحرش.</li><li>كيف تقى إبنك من التحرش؟</li><li>دليل شامل للوالدين للتعامل مع أنواع الإيذاء.</li><li>العوامل الأساسية في التنشئة الصحية التي تبعدك عن مخاوف الأمور.</li><li>تشخيص ومساعدة.</li></li></ul>', NULL, 'إن عدم الوعي الكافي للأطفال قد يجعلهم يتقبلون مثل هذه الأفعال، لأنهم حتى وإن استنكروها في أنفسهم، فإن اليد العليا في مثل هذه المواقف عادة ما تكون للشخص المتحرش', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-07-12 19:22:32', '2021-07-12 19:22:32'),
(53, 8, 19, 'https://gmcksa.com/storage/app/public/images/1PzH036LeqQK6CPmWq57y4POIGGuU3h7rhr2dddO.jpg', 'دورة إدارة المشاريع الاحترافية PMP', '899', '2021-08-07', '2021-08-11', '5 أيام', '<ul><li><li>مدخل إلى إدارة المشاريع</li><li>إدارة الموارد البشرية للمشروع</li><li>إدارة الاتصالات في المشروع</li><li>نظام ودورة حياة المشروع</li><li>إدارة التعاقدات والتوريدات</li><li>نظرة على دليل إدارة المشروعات «PMBOK»</li></li></ul>', NULL, 'هو تخصّص يتعلّق بتنظيم وإدارة الموارد، مثل الموارد البشريّة، بالطريقة التي تمكّن إنجاز المشروع باحترام مضمونه المحدد وبمراعاة عوامل الجودة والتوقيت والتكلفة', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-07-24 23:51:40', '2021-07-24 23:51:40'),
(54, 4, 23, 'https://gmcksa.com/storage/app/public/images/RtQoP2p9T71Ncjzdh6goPPgn8cJBM2OQiKa4amD3.png', 'bfdfgdsf', '200', '2021-08-11', '2021-08-16', '5', 'dsgfffffff', NULL, 'dhdfgdsf', '<p>gdgswfrwd</p>', 'https://ezar-fashion.com/women-shop/', 'https://www.youtube.com/watch?v=RSCyJdn67Sg', 4, 0, 0, '2021-08-12 00:53:23', '2021-08-12 00:53:23'),
(55, 28, 19, 'https://gmcksa.com/storage/app/public/images/9wRjGjEcZWU80Pph08BaZ42G5FqxXbsn7qDmPoah.png', 'دورة الموشن جرافيك', '399', '2021-08-18', '2021-08-20', '3 أيام', '<ul><li><li>التعرف على واجهة برنامج ADOBE AFTER EFFECTS.</li><li>تعلم أساسيات التحريك.التعلم على الانفوجرافيكس.</li><li>التعامل مع الطبقات ثلاثية الأبعاد.</li><li>التعرف على أساسيات الرسوم المتحركة.</li><li>كتابة الأكواد المساعدة لتسهيل عمل مشاهد المؤثرات البصرية.</li><li>التعامل مع ملفات الصوتية والاستفادة منها.</li><li>تدريبات عملية مكثفة.</li></li></ul>', NULL, 'موشن جرافيك معناها الرسوم التوضيحية المتحركة، وهي عبارة عن عرض متحرك للرسوم بهدف توضيح وتبسيط المعلومات وكلمة جرافيك معناها الصور أو الرسوم البيانية أو التخطيطية أو التوضيحية، وكلمة موشن تعني الحركة.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 4, 0, 0, '2021-08-17 21:37:50', '2021-08-17 21:37:50'),
(56, 10, 19, 'https://gmcksa.com/storage/app/public/images/uaSfJkVVrFW9DJnod4AUHMqLZpurXZUDaaWblBrl.png', 'دورة المرأة اللغز', '399', '2021-08-19', '2021-08-20', 'يومين', '<ul><li><li>7 أسرار في الرجل تجهلها النساء.</li><li>13 خطأ قاتل ترتكبها معظم النساء!</li><li>الحاجات الـ9 الأساسية التي يريدها الرجل.</li><li>مفهوم الدائرة العامة والخاصة عند الرجل.</li><li>8 ممارسات تقوم بها الزوجة لقتل علاقتها الزوجية دون أن تدري.</li><li>الألغاز الـ 9 لكي تكون الزوجة لغز يعشقه الرجل.</li></li></ul>', NULL, '7 أسرار في الرجل تجهلها النساء.\r\n13 خطأ قاتل ترتكبها معظم النساء.\r\nالحاجات الـ9 الأساسية التي يريدها الرجل.\r\nمفهوم الدائرة العامة والخاصة عند الرجل.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-08-17 21:42:52', '2021-08-17 21:42:52'),
(57, 10, 19, 'https://gmc-sa.net/storage/app/public/images/mJhWkAAjFcxsJryfEywVIBpMCQp9iAzrITqK2pul.png', 'دورة التطهير من المشاعر المسمومة', '199', '2021-08-21', '2021-08-21', 'يوم 1', '<ul><li><li>مفهوم دائرة الألم والاختيار</li><li>كيف يتكون السم العطافي في الجهاز العاطفي</li><li>مهارات التعامل مع الموافق المؤلمة</li><li>كيف نحمي أنفسنا من الأشخاص السامين عاطفيا</li><li>كيف نتخلص من الإطار السلبي الذي يتحكم بمشاعرنا</li><li>مهارات الحائط السيكولوجي لتعزيز الصورة الإيجابية لدينا</li></li></ul>', NULL, 'تهدف المشاعر الإيجابية إلى تعزيز متعة التجربة حتى نسعى إليها مرة أخرى، تُنشّط المشاعر الإيجابية نظام المكافأة في أدمغتنا والذي يجعلنا نشعر بالأمان، أما المشاعر السلبية فهي تحذرنا من المواقف الخطيرة وترفع من غرائز البقاء داخلنا حتى نصبح أكثر وعياً.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-08-17 21:44:08', '2021-08-17 22:10:51'),
(58, 8, 19, 'https://gmc-sa.net/storage/app/public/images/qJ5FMSbMa6xzvbco3aIVpveSvRvOCmN9oOA66Pj1.jpg', 'دورة إدارة المشاريع الاحترافية PMP', '899', '2021-08-21', '2021-08-25', '5 أيام', '<ul><li><li>مـــــدخــــل إلـــــى إدارة المشـــــــاريع.</li><li>إدارة المــوارد البشرية للــمشروع.</li><li>إدارة الاتصـالات فــي المشـــروع.</li><li>نظـــام ودورة حيـــــاة الـــمشـــروع.</li><li>إدارة التعــــاقـــدات والتوريـــــدات.</li><li>نظرة على دليل إدارة المشروعات «PMBOK»</li></li></ul>', NULL, 'هو تخصّص يتعلّق بتنظيم وإدارة الموارد، مثل الموارد البشريّة، بالطريقة التي تمكّن إنجاز المشروع باحترام مضمونه المحدد وبمراعاة عوامل الجودة والتوقيت والتكلفة.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 12, 0, 0, '2021-08-17 22:13:41', '2021-08-17 22:13:41'),
(59, 16, 19, 'https://gmc-sa.net/storage/app/public/images/rphwhaUxuCZR27rayjVTFpJ9Sj63ClnXnn2iDexJ.jpg', 'دورة اطلق قدراتك مفاتيح الاستحقاق', '199', '2021-08-24', '2021-08-24', 'يوم 1', '<ul><li><li>الخطوة الأولى: تجاوز معيقات الاستحقاق.</li><li>الاستحقاق في العلاقات كيف يظهر الناس في حياتك؟</li><li>المنظومة الرباعية للاستحقاق.</li><li>كيف ترفع استحقاقك مقابل واقعك.</li><li>كيف يتحقق التقبل مع وجود سقف عالي؟</li><li>كيف ترفع سقف توقعاتك دون أن تتعرض&nbsp;</li><li>للخذلان والإحباط؟</li></li></ul>', NULL, 'قد يعيش الناس حياتهم بأكملها من دون أن يعرفوا ما هي قدراتهم وإمكانياتهم الحقيقية والتي قد توفر لهم ولمن حولهم حياةً أفضل بكثيرٍ من تلك التي يعيشونها.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-08-17 22:51:20', '2021-08-17 22:52:27'),
(60, 4, 19, 'https://gmc-sa.net/storage/app/public/images/IjzoYy9GC5exLVPQc22WEyj4En5U9whO3brI2kXD.jpg', 'دورة قوة التجاوز', 'عرض خاص %33 خصم على الدورة بـ199', '2021-08-25', '2021-08-25', 'يوم 1', '<ul><li><li>صيغ تفكير مفيدة لتجاوز المشاعر المؤذية.</li><li>تمرين التوقف عن اللوم والتحسر.</li><li>كيف تمتلك مقومات التجاوز.</li><li>تمرين تغيير الصورة الذهنية القديمة.</li><li>أفكار تهيئة النفس لمرحلة التسليم.</li></li></ul>', NULL, 'من الطبيعي أن يمر الإنسان في حياته ببعـض المواقـف الصعبة والأزمــات التــــي تجعله يشعــــر بالاستسلام والتعب النفسي ولكن من غير الطبيعي أن يستسلم الإنسان لهذه الظروف وأن يضعف لدرجة يؤثر فيها علـى نجـاح حيـــاته واستمـراها.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-08-17 22:56:17', '2021-08-23 23:25:39'),
(61, 22, 19, 'https://gmc-sa.net/storage/app/public/images/mKsKA1ucbUSv0dw4FPwIerJAqZR1ZGgjdMH5CO0y.jpg', 'دورة تطوير مهارات استشاري الجودة', '850', '2021-08-29', '2021-08-29', '5 أيام', '<ul><li><li>الأساسيات والمبادى الحديثة لنظام في إدارة الجودة.</li><li>التخطيط الاستراتيجي لنظام إدارة الجودة.</li><li>الهيكلة الموحدة المواصفات القياسية الدولية الصادرة عن منظمة ISO.</li><li>تطبيقات متطلبات المواصفة القياسية لنظام إدارة الجودة 9001:2015 ISO.</li><li>منهجية التدقيق الداخلي والتقييم الذاتي لنظام إدارة الجودة.</li><li>منهجية العمل الإستشاري لتأهيل المنظمات وفق متطلبات المواصفات القياسية الدولية.</li></li></ul>', NULL, '.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 16, 0, 0, '2021-08-17 22:58:32', '2021-08-17 23:02:12'),
(62, 35, 19, 'https://gmc-sa.net/storage/app/public/images/8I5i3kDr1rFRLZ86viJ1JiRCLU1ycw41wTzc2qAl.png', 'دورة سيجما الحزام الأصفر', '300', '2021-08-30', '2021-08-30', 'يوم 1', '<ul><li><li>تعريف منهجية \"السته سيقما و اللين\" .</li><li>تاريخ منهجية \"السته سيقما و اللين\".</li><li>أحزمة منهجية \"السته سيقما\".</li><li>فوائد منهجية \"السته سيقما و اللين\".</li><li>متطلبات منهجية \"السته سيقما و اللين\".</li><li>كيفية بناء منهجية \"السته سيقما و اللين\" في المنظمات.</li></li></ul>', NULL, '.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 16, 0, 0, '2021-08-17 23:04:55', '2021-08-17 23:04:55'),
(63, 37, 19, 'https://gmc-sa.net/storage/app/public/images/tNzTTHlzsoqYzet0X49h2qd5xxELrAJEtHh4VLle.png', 'دورة سيجما الحزام الأسود', '700', '2021-09-02', '2021-09-04', '3 أيام', '<ul><li><li>تحليل نظام القياس MSA (Cpk &amp; Ppk)</li><li>التحكم الإحصائي بالعميات SPC</li><li>خرائط التحكم للمتغيرات الكمية</li><li>(Variable Data)</li><li>خرائط التحكم للمتغيرات النوعية</li><li>(Attribute Data)</li><li>اختبار الفرضيات لمعرفة الأسباب</li><li>الجذرية للمشكلة</li><li>العلاقة بين منهجية الستة سيقما</li><li>ومفهوم إعادة هندسة العمليات</li><li>العلاقة بين إدارة المشاريع ومنهجية</li><li>الستة سيقما&nbsp;</li><li>(Change, Risk &amp; Communication Plans)</li></li></ul>', NULL, '.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 16, 0, 0, '2021-08-17 23:09:35', '2021-08-17 23:09:35'),
(64, 24, 19, 'https://gmc-sa.net/storage/app/public/images/Ft45rPkEyUNcy9lXhBtHw4mhZvrg79Pn6RZq2v7p.png', 'دورة إدارة وتخطيط الموارد البشرية', '499', '2021-09-06', '2021-09-08', '3 أيام', '<ul><li><li>المفهوم الحديث لإدارة الموارد البشرية وأثرها على التخطيط الاستراتيجي للمنطمة.</li><li>مهام إدارة الموارد البشرية.</li><li>التخطيط للموارد البشرية.</li><li>إستقطاب وتعيين الموظفين.</li><li>إعداد الهياكل التنظيمية.</li><li>تحيد المسار والتوصيف الوظيفي.</li><li>تقييم أداء الموظفين.</li><li>خطط التدريب وتنمية الموارد البشرية.</li><li>الرواتب والمزايا والحوافز.</li><li>الدور القيادي في إدارة الموارد البشرية.</li><li>تطبيقات عملية لأنشطة الموارد البشرية.</li></li></ul>', NULL, 'من الطبيعي أن يمر الإنسان في حياته ببعـض المواقـف الصعبة والأزمــات التــــي تجعله يشعــــر بالاستسلام والتعب النفسي ولكن من غير الطبيعي أن يستسلم الإنسان لهذه الظروف وأن يضعف لدرجة يؤثر فيها علـى نجـاح حيـــاته واستمـراها.', '<p>يحصل المتدرب على:</p><p><br></p><p>- شهادة معتمدة من المؤسسة العامة.</p><p>- حقيبة تدريبية في شكل PDF بمحتوى الدورة.</p><p>- محاضرات مسجلة بعد البث..</p>', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-08-17 23:23:19', '2021-08-31 21:33:03'),
(65, 19, 19, 'https://gmc-sa.net/storage/app/public/images/9gtmFgkFuecs7QJlC7JGYCcyffzfaI9WQYCr83OI.jpg', 'دورة القيادة الاستراتيجية التنفيذية ومهارات المدير التنفيذي', '499', '2021-09-06', '2021-09-08', '3 أيام', '<p>.</p>', '<p>دورتين بسعر دورة واحدة بخصم 25%</p>', '.', '<p>يحصل المتدرب على:</p><p><br></p><p>- شهادتين معتمدة من المؤسسة العامة.</p><p>- حقيبة تدريبية في شكل PDF بمحتوى البرنامج.</p><p>- محاضرات مسجلة بعد البث..</p>', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-08-17 23:26:53', '2021-08-31 23:06:04'),
(66, 26, 19, 'https://gmc-sa.net/storage/app/public/images/YcDDGrqTSnJCZlPDVg0NA7TDF17EU6FtIwx3tUXK.jpg', 'دورة التربية من الضعف إلى القوة', '299', '2021-09-06', '2021-09-06', 'يوم 1', '<ul><li><li>كيف نبني الثقة الشاملة في حياة أبنائنا؟</li><li>ماهي مظاهر ضعف الشخصية لدى أبنائنا:</li><li>الخوف، الخجل، الحساسية المفرطة، سرعة الغضب.</li><li>تعرف على أعراض ضعف الشخصية ومؤشراتها الـ6</li><li>ماهي الأسباب العشرة والأخطاء التربوية وراء ضعف شخصية أبنائنا.</li><li>خطوات العلاج والتخلص من ضعف الشخصية وأعراضها&nbsp; (برامج عملي وخطوات تربوية).</li></li></ul>', NULL, 'يعد التنظيم ضروريا في حياة الطفل ولا سيما سنواته الأولى, إن الطفل يحترم ويقدر الوالد والوالدة اللذين يرسمان الحدود.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-08-17 23:35:40', '2021-08-17 23:35:40'),
(67, 26, 19, 'https://gmc-sa.net/storage/app/public/images/TFaZbmaVMohQN0hI11cwmUjgXyODM9QH4gCmDmfA.jpg', 'دورة التحرر الوجداني', '299', '2021-09-07', '2021-09-07', 'يوم 1', '<ul><li><span style=\"font-size: 0.875rem;\">كيف تحمي نفسك من الابتزاز الوجداني؟</span><br></li><li>تعرف على الشخصيات الخمسة: (النرجسي - الضحية - المتحكم - المنتقد - المشتت)</li><li>كيف تحمي نفسك من تأثيرهم السلبي؟</li><li>وكيف تتعامل معهم؟</li></ul>', NULL, 'هي فئات واسعة من سمات الشخصية. في حين أن هناك مجموعة كبيرة من المؤلفات التي تدعم نموذج الشخصية المكون من خمسة عوامل ، لا يتفق الباحثون دائمًا على العلامات الدقيقة لكل', '<ol><li>يحصل المتدرب على <span style=\"background-color: rgb(255, 255, 0);\">شهادة معتمدة</span> من المؤسسة العامة.</li><li>يحصل المتدرب على <span style=\"background-color: rgb(255, 255, 0);\">حقيبة تدريب</span> في شكل PDF بمحتوى الدورة.</li><li><span style=\"background-color: rgb(255, 255, 0);\">محاضرات مسجلة</span> بعد البث.</li></ol>', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-08-17 23:39:06', '2021-08-24 01:56:41'),
(68, 11, 19, 'https://gmc-sa.net/storage/app/public/images/20gsSFyFm231AaXRyACgvHRzFxPUm1l654lITgWt.jpg', 'دورة سر التجلي', '799', '2021-09-09', '2021-09-10', 'يومين', '<ul><li><li>ماهو التجلي .. وأين تكون أهداف مختفية.</li><li>لماذا قال الله «وأن سعيه سوف يُرى»؟</li><li>لماذا مشاعري بوابة التجلي الذهبية لي.</li><li>تطبيقات اختبار الإختيار (وهم لا يفتنون).</li><li>أسرار متقدمة في عالم النوايا.</li><li>كيف تفهم أنه لا يوجد نوايا سيئة بل أفعال سيئة.</li><li>ماهي مؤشرات أم ما أريد بدأ يظهر.</li><li>ماهي علاقة الفرح بالتجلي (فضحكت فبشرناها).</li><li>الفرق المهم بين التجلي البطئ والسريع.</li><li>علاقة التركيز والانتباه بالتجلي.</li><li>سر الوضوح والبيان في سرعة التجلي.</li><li>تطبيقات تجلي الحرية والسعة في حياتك.</li><li>سر بسيط لكنه يعرقل الكثير من التجليات ويدمرها!!</li></li></ul>', '<p>عرض الدورتين 1030 ريال&nbsp;</p>', 'كيف تفهم أنه لا يوجد نوايا سيئة بل أفعال سيئة.\r\nماهي علاقة الفرح بالتجلي (فضحكت فبشرناها).\r\nالفرق المهم بين التجلي البطئ والسريع.', '<p>يحصل المتدرب على:</p><p><br></p><p>- شهادة معتمدة من المؤسسة العامة.</p><p>- حقيبة تدريبية في شكل PDF بمحتوى الدورة.</p><p>- محاضرات مسجلة بعد البث..</p>', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 1, 0, '2021-08-17 23:42:09', '2021-09-05 23:46:12'),
(69, 11, 19, 'https://gmc-sa.net/storage/app/public/images/c5kAQOvz0lHhLvbLaalApJTr10mA6HwX0E4vEqPu.jpg', 'دورة الحق المبين', '350', '2021-09-11', '2021-09-11', 'يوم 1', '<ul><li><li>قوانين الاستحقاق وتحقيق الرغبات.</li><li>ماهو الاستحقاق الحقيقي «الحق المبين»؟</li><li>لماذا نخسر النعم ويكون هذا استحقاقنا؟</li><li>ماهو وهم الاستحقاق الذي يعيشه الناس؟</li><li>كيف انتقل من الاستحقاق الذي يعيشه الناس؟</li><li>كيف استحق الاستحقاق الذهبي «فسنيسره لليسرى»؟</li><li>وتمارين وتطبيقات رفع الاستحقاق.</li><li>علاقة الاستحقاق بالابتلاء السلبي.</li><li>ولماذا يستحق البعض العسر الخالص؟</li><li>ولماذا يستحق البعض اليسر الخالص؟</li><li>وماهي مؤشرات ضعف الاستحقاق؟</li><li>ولماذا يدمر الناس أهدافهم أموالهم علاقاتهم وحياتهم؟</li></li></ul>', NULL, 'لماذا نخسر النعم ويكون هذا استحقاقنا؟\r\nماهو وهم الاستحقاق الذي يعيشه الناس؟\r\nكيف انتقل من الاستحقاق الذي يعيشه الناس؟', '<p>يحصل المتدرب على:</p><p><br></p><p>- شهادة معتمدة من المؤسسة العامة.</p><p>- حقيبة تدريبية في شكل PDF بمحتوى الدورة.</p><p>- محاضرات مسجلة بعد البث..</p>', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-08-17 23:47:30', '2021-08-31 22:17:45'),
(70, 14, 19, 'https://gmc-sa.net/storage/app/public/images/fF1sMvkoaff1C6qHm6hup6jEoaePS0uW5URFQtHq.jpg', 'دورة تطوير مهارات أخصائي الموارد البشرية', NULL, '2021-09-13', '2021-09-16', '4 أيام', '<ul><li><li>تحليل وتوصيف وتصنيف وتقويم الوظائف.</li><li>إعداد سلم الرواتب.</li><li>تخطيط القوى العامة.</li><li>وضع الحوافز والمزايا.</li><li>وضع خطة المسار الوظيفي والتعاقب الوظيفي.</li><li>الاستقطاب والاختيار والتعيين.</li><li>إجاراء مقابلات وظيفية منهجية.</li><li>التدريب والتنمية.</li><li>تقييم الأداء الوظيفي.</li></li></ul>', NULL, 'إن الموارد البشرية تعد العامل الرئيسي للانتاج الاقتصادي ، أما الميكنة و المال فإنها ليست سوى اشياء من صنع الإنسان و من نتاج براعته .', '<p>يحصل المتدرب على:</p><p><br></p><p>- شهادة معتمدة من المؤسسة العامة.</p><p>- حقيبة تدريبية في شكل PDF بمحتوى الدورة.</p><p>- محاضرات مسجلة بعد البث..</p>', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-08-18 00:09:43', '2021-09-09 02:28:04');
INSERT INTO `courses` (`id`, `trainer_id`, `coordinator_id`, `image`, `name`, `price`, `date`, `end_date`, `duration`, `description`, `offer`, `breif`, `features`, `link`, `link_youtube`, `category_id`, `is_special`, `confirm`, `created_at`, `updated_at`) VALUES
(71, 24, 19, 'https://gmc-sa.net/storage/app/public/images/hAHloxSXAGttJCkrXj5llwftRvBl4JpjsZq7NRxu.png', 'دورة مهارات إدارية', '299', '2021-09-19', '2021-09-20', 'يومين', '<ul><li><li>أنواع الإتصالات الإدارية ومميزاتها.عناصر وأجزاء وأشكال التقارير الإدارية والرسائل المذكرات.</li><li>خطوات إعداد تقرير إداري وكتابة رسالة ومذكرة إدارية.</li><li>المهارات الفنية في إعداد وتصميم كتابة التقارير والمراسلات.</li><li>المهارات والقواعد اللغوية في كتابة المراسلات المختلفة.</li><li>تدريبات ومشاريع تطبيقية.</li></li></ul>', NULL, 'استطاعت الإدارة الحديثة أن تواجه الواقع من خلال نجاحها في التعرف على العالم وإدراكها لما يدور فيه، وأدى ذلك إلى نجاحها في التعامل مع حقائق الحياة دون انتقائية أو أحلام يقظة', '<p>يحصل المتدرب على:</p><p>- شهادة معتمدة من المؤسسة العامة.<br>- حقيبة تدريبية في شكل PDF بمحتوى الدورة.</p><p>- محاضرات مسجلة بعد البث.</p>', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-08-18 01:07:58', '2021-08-28 22:54:18'),
(72, 29, 19, 'https://gmc-sa.net/storage/app/public/images/QQIuhkh2eAUhVTpofd3knSdYGcJ0ciJUOTsVO7KV.jpg', 'دورة سلسلة الإمداد والمشتريات واللوجستيات', '299', '2021-09-27', '2021-09-28', 'يومين', '<ul><li><li>مفاهيم سلاسل الإمداد العالمية وتطوراتها&nbsp;</li><li>عناصر ومكونات سلاسل الإمداد&nbsp;</li><li>الطرق الاحترافية للتعاقد مع الموردين</li><li>تطوير عمليات المشتريات والتموين</li><li>الإدارة الاستراتيجية للوجستيات الحديثة&nbsp;</li><li>مخاطر سلاسل الإمداد العالمية</li><li>دور سلاسل الإمداد في التجارة الالكترونية</li></li></ul>', NULL, 'أصبحت إدارة سلاسل الإمداد أمراً لا يمكن الاستغناء عنه بسبب الابتكار المستمر في التكنولوجيا وتوقعات العملاء المتغيرة بسرعة.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 1, 0, '2021-08-18 01:10:59', '2021-09-13 00:17:54'),
(73, 29, 19, 'https://gmc-sa.net/storage/app/public/images/8SCd6CEDFNsStAxn4mnEFWU20RmgzXUVIJqIKN01.jpg', 'دورة الإدارة الحديثة للمستودعات الحكومية والخدمية', '299', '2021-09-29', '2021-09-30', 'يومين', '<ul><li><li>مفاهيم إدارة المستودعات واهدافها.</li><li>ادارة مخاطر المستودعات.&nbsp;</li><li>عوامل نجاح ادارة المستودعات.</li><li>انواع المستودعات وطريقة اعمالها.</li><li>طريقة تصنيف المواد داخل المستودعات.&nbsp;</li><li>الاساليب المخزنية للمستودعات.</li><li>مهام وادوات الرقابة على المخزون.</li><li>اجراءات وعمليات الجرد ونماذجها المستخدمة.&nbsp;</li><li>تخطيط المخزون وتحديد مستوياته المُثلى.</li><li>المهام الاحترافية للعاملين في المستودعات</li></li></ul>', NULL, 'تخطيط وتنظيم المواد والمستودعات بأسلوب فعال.\r\nإعداد الإستراتيجيات الحديثة في إدارة المخازن والمستودعات.\r\nالطرق والنظم الحديثة في مراقبة المخزون.\r\nالأساليب الحديثة في عملية مراقبة المخزون.\r\nتطبيق الممارسات المتقدمة في تخطيط وتنظيم ومراقبة المواد والمستودعات وإدارة التخزين.', NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 0, 0, '2021-08-18 01:14:14', '2021-08-18 01:14:14'),
(75, 36, 19, 'https://gmc-sa.net/storage/app/public/images/5CX9frGH9ww6N73UhwkI2oLhuE0q3o6W993BRKGP.jpg', 'البرنامج التدريبي الشامل للثلاث مستويات', 'الحزام الأصفر:300 ريال   الحزام الأخضر:700 ريال    الحزام الأسود: 700 ريال', '2021-08-30', '2021-09-04', '6 أيام', NULL, '<p>عرض الثلاث احزمة 1400 ريال&nbsp;</p>', '.', '<ul><li>يحصل المتدرب على&nbsp;&nbsp;<span style=\"background-color: rgb(239, 239, 239);\">شهادة معتمدة&nbsp;&nbsp;</span>من المؤسسة العامة لكل حزام&nbsp;</li><li>يحصل المتدرب على<span style=\"background-color: rgb(255, 255, 255);\">&nbsp;&nbsp;</span><span style=\"background-color: rgb(239, 239, 239);\">حقيبة تدريبية</span>&nbsp; في شكل PDF&nbsp;</li><li><span style=\"background-color: rgb(239, 239, 239);\">محاضرات مسجلة&nbsp;&nbsp;</span>(بعد البث)&nbsp;</li><li><font color=\"#e76363\">احصل على الحزام الأصفر مجانا عند الاشتراك&nbsp;في&nbsp; عرض الثلاث أحزمة&nbsp;</font></li></ul>', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 13, 1, 0, '2021-08-21 22:04:31', '2021-08-22 20:28:04'),
(76, 28, 19, 'https://gmc-sa.net/storage/app/public/images/OEjQWh2GbkRdRwMp7njTxLmNr1ZK2izSZRtlJHCF.jpg', 'مهارات تصميم الشعارات وبناء الهوية البصرية', '299', '2021-09-06', '2021-09-07', 'يومين', NULL, '<ul><li><li>التعرف على واجهة برنامج Adobe Illustrator.</li><li>التعرف على أنواع الشعارات.</li><li>تعلم أساسيات رسم الشعارات.</li><li>توليد الأفكار والعصف الذهني.</li><li>تعلم تصميم تطبيقات الهوية البصرية.</li><li>تعلم تنفيذ المخطوطات.</li><li>تعلم تصميم الأيقونات.</li><li>تعلم تجويد المشروع.</li><li>تعلم عرض الهوية على العملاء.</li><li>إخراج المشروع.</li></li></ul>', 'الشعار هو عبارةٌ عن صورةٍ أو رسمةٍ بصريةٍ إيضاحيةٍ، وهو الوجه المحدد الذي يتمّ من خلاله التعرف على شخصٍ ما أو مؤسسةٍ أو شركة أو منتج محدد.', '<p>يحصل المتدرب على:</p><ul><li>شهادة معتمدة من المؤسسة العامة.</li><li>حقيبة تدريبية في شكل PDF بمحتوى الدورة.</li><li>تمارين عملية مع المدرب.\"من الصفر وحتى الاتقان\".</li><li>محاضرات مسجلة بعد البث.</li></ul>', 'https://www.youtube.com/embed/QXBUPCa4WhU', 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-08-28 22:31:41', '2021-08-28 22:56:51'),
(77, 16, 19, 'https://gmc-sa.net/storage/app/public/images/AvN5UEwfKVKjT2kHFwhdfzLTXeQewoMA1AEo4YYw.png', 'موازين الحياة. (الاكتفاء المتوازن لتحرير المشاعر)', '199', '2021-09-01', '2021-09-01', 'يوم 1', '<ul><li><li>التعامــل مـــع وهــم الاحتيـــاج.</li><li>تمارين لتحريـــــر المشاعــــــر السلــبية.</li><li>تجاوز التــــدميــر الـذاتـي.</li><li>التحـــرر مــــن الأفكـــار السلــبية.</li><li>تــــمريـــــن التحـــرر مــــــــن التعــلق.</li></li></ul>', NULL, 'التذمر حول ما حدث في الماضي هو تمرين عقلي سلبي لا جدوى منه، وبدلاً من أن تصرف طاقتك في التذمر والتفكير بما حدث كرس هذه الطاقة في ما ينفعك  ويطورك', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-08-28 22:46:32', '2021-08-28 22:48:27'),
(78, 16, 19, 'https://gmc-sa.net/storage/app/public/images/pFuDxDzeS4OZYjeWGKOCWj52cOLVDJNyVveuimKx.jpg', 'صناعة الإرادة (روح مطمئنة)', '199', '2021-08-31', '2021-08-31', 'يوم 1', '<ul><li><li>إدراك أسرار الاطمئنان من خلال: التقبل، التسليم، التصالح</li><li>قوى التجاوز: تجاوز الأنا، تجاوز الماضي</li><li>استراتيجية تنظيف الداخل</li><li>تقنية فصل الأجزاء \"لإدارة المشاعر\"</li><li>كيف تركز على الرسالة من الحدث وليس الألم</li><li>موازنة: السعادة.. والاحتياج</li></li></ul>', NULL, 'الإرادة هي ما يعبر عن مثابرة المرء واندفاعه للقيام بعمل معين بصرف النظر عن العوائق والمصاعب التي سوف يواجهها في طريق إنجازه لذلك العمل', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 'https://www.youtube.com/embed/QXBUPCa4WhU', 6, 0, 0, '2021-08-28 22:53:00', '2021-08-28 22:53:00'),
(79, 9, 19, 'https://gmc-sa.net/storage/app/public/images/Sc5hlHPxFtikPAGzLw36KQhLasZA1spNZoL0aFz5.jpg', 'تدريب المدربين', '499', '2021-09-19', '2021-09-23', '5 أيام', '<ul><li><li>ماهي الأسرار التي تجعل المدرب يأسر المتدربين ويبهرهم؟</li><li>كيف تصل لتحقيق أهداف البرنامج بدقة عالية؟</li><li>إزالة التوتر والتحدث بطلاقة</li><li>كيف تحفـز المتدربين بطرق فعّالة ومبتكرة؟</li><li>أساليب عملية لتصميم التمارين والانشطة التدريبية</li><li>تصميم وتنظيم البرنامج من خلال نظام مكارثي ( 4mat )</li></li></ul>', NULL, 'إذا كنت تحلم بأن تكون مدرباً محترفاً في أي مجال.\r\n- أن تكون قادراً على الإلقاء و التأثير في الآخرين أو تسعى لتطوير مهاراتك القيادية.', NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 'https://www.youtube.com/embed/QXBUPCa4WhU', 7, 0, 0, '2021-09-01 01:03:01', '2021-09-01 01:03:01'),
(80, 4, 19, 'https://gmc-sa.net/storage/app/public/images/X3YltYFHu8w7qHpUbt2yGXqAZHd4B1WnDvnoMfPg.jpg', 'السلامة والصحة المهنية وفقا لماعيير OSHA', '950', '2021-09-05', '2021-09-09', '5 أيام', NULL, NULL, NULL, NULL, 'https://www.youtube.com/embed/QXBUPCa4WhU', 'https://www.youtube.com/embed/QXBUPCa4WhU', 8, 0, 0, '2021-09-01 18:27:55', '2021-09-01 18:27:55');

-- --------------------------------------------------------

--
-- Table structure for table `inforamtion_strings`
--

CREATE TABLE `inforamtion_strings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `terms` longtext COLLATE utf8mb4_unicode_ci,
  `privacy` longtext COLLATE utf8mb4_unicode_ci,
  `integrity` longtext COLLATE utf8mb4_unicode_ci,
  `property_rights` longtext COLLATE utf8mb4_unicode_ci,
  `e_learning` longtext COLLATE utf8mb4_unicode_ci,
  `cheat` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `inforamtion_strings`
--

INSERT INTO `inforamtion_strings` (`id`, `terms`, `privacy`, `integrity`, `property_rights`, `e_learning`, `cheat`, `created_at`, `updated_at`) VALUES
(1, NULL, '<p class=\"MsoNormal\" dir=\"RTL\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;line-height:\r\n107%;font-family:&quot;Arial&quot;,sans-serif;mso-ascii-font-family:Calibri;mso-ascii-theme-font:\r\nminor-latin;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;\r\nmso-bidi-theme-font:minor-bidi\"></span></p><p class=\"MsoNormal\" align=\"center\" dir=\"RTL\" style=\"text-align:center\"><span dir=\"RTL\"></span></p><p class=\"MsoNormal\" align=\"center\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: center; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"AR-SA\" style=\"font-size: 18pt; font-family: Arial, sans-serif;\">سياسة الخصوصية</span></b><span dir=\"LTR\" style=\"font-size: 18pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" align=\"center\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: center; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\">&nbsp;</span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√&nbsp;يتعهد موقع صناعة\r\nالعبقرية باستخدام أنظمة تعليم إلكتروني تدعم مختلف أنواع الأجهزة باختلاف أنظمة\r\nتشغيلها ومتوافقة مع جميع شاشات الأجهزة بما فيها الأجهزة اللوحية والهواتف الذكية\r\n.<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ ويقر المركز بأن محتوى\r\nالموقع الإلكتروني ، ملتزم بالقوانين والسياسات الوطنية ، وقد تم التأكد من سلامته\r\nالفكرية ، وغير منسوخ من موقع آخر .<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#385623\">√&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">ويقر بعدم مخالفته للأنظمة والقرارات السارية في\r\nالمملكة العربية السعودية .<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#385623\">√&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">كما يقر بمتابعة حداثة المحتوى الرقمي ، وبعدم\r\nمخالفته للقوانين .</span><span style=\"font-family: Arial, sans-serif; font-size: 16pt;\">&nbsp;</span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#385623\">√&nbsp;&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">يتعهد الموقع كذلك بحفظ خصوصية المشترك في\r\nمعلوماته الشخصية ، مثل اسم المتدرب ، وأرقام تواصله ،والبريد الإلكتروني وغيره ،</span><span style=\"font-family: Arial, sans-serif; font-size: 16pt;\">&nbsp;ويتعهد كذلك بالحفاظ على سرية المعلومات\r\nالشخصية الدقيقة للمتدرب ، مثل (بطاقة الائتمان ) وغيرها ، ولا يمكن الاطلاع عليها\r\nإلاّ من قِبل الموظف المصرح له من إدارة المركز.</span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#385623\">√&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">ويُمنح المتدرب حق تصفح الموقع ، و الاطلاع على\r\nإعلانات الدورات قبل التسجيل في الموقع .<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#1F4E79\">وبذلك تعتبر سياسة\r\nالخصوصية المتبعة في الموقع الإلكتروني لمركز صناعة العبقرية ، والخاصة به ،</span></b><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"> </span><b><span lang=\"AR-SA\" style=\"font-size:\r\n16.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\ncolor:#1F4E79\">بمثابة الخطوط الإرشادية للمتدرب ، لذا يرجى قراءة قوانين هذه\r\nالسياسة قبل التسجيل في الموقع ، وبتسجيل المتدرب يكون قد وافق على الالتزام بجميع\r\nهذه القوانين .</span></b><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span><span dir=\"LTR\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span>&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span><span dir=\"LTR\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span>&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align: center; \">\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" dir=\"RTL\"><span dir=\"LTR\" style=\"font-size:16.0pt;line-height:\r\n107%\"><o:p>&nbsp;</o:p></span></p>', '<p class=\"MsoListParagraphCxSpFirst\" dir=\"RTL\" style=\"text-align: justify; \"><span style=\"font-family: Arial, sans-serif; text-align: center; text-indent: 6px; font-size: 14pt;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span><b style=\"text-align: center; font-size: 0.875rem;\"><span lang=\"AR-SA\" style=\"font-size: 18pt; font-family: Arial, sans-serif;\">سياسة النزاهة الأكاديمية</span></b></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span dir=\"RTL\"></span><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 14pt; font-family: Arial, sans-serif;\"><span dir=\"RTL\"></span><span dir=\"RTL\"></span>&nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\r\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ يلتزم موقع صناعة العبقرية على اتباع\r\nإرشادات سياسة النزاهة الأكاديمية ، حيث أن أي انتهاك من قبل المتدرب لسياسة و\r\nقوانين المركز وخاصة سياسة الأمانة الأكاديمية ، سيعرضه للإجراء الجزائي من قبل\r\nإدارة المركز.</span><span lang=\"AR-SA\" style=\"font-size: 10pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 10pt; font-family: Arial, sans-serif;\">&nbsp;<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ جميع المحتوى التعليمي المتوفر في الموقع\r\nالإلكتروني من مذكرات ، أو ملفات أو محاضرات أو غيرها ، هي للاستفادة الشخصية\r\nللمتدرب ، حيث أن الحقوق محفوظة ، وانتهاك هذه الحقوق قد يعرض المتدرب إلى إنهاء\r\nمشاركته والاستفادة من الموقع والدورات .</span><span lang=\"AR-SA\" style=\"font-size: 10pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 10pt; font-family: Arial, sans-serif;\">&nbsp;<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ يلزم على المتدرب تقديم متطلبات الدورة من\r\nواجبات ، وبحوث أو إنجاز مهام ، أو تقييم ، حيث ينجزه بشكل شخصي دون المشاركة مع\r\nطرف آخر، إلاّ بطلب أو إذن المدرب أو من ينوب عنه .</span><span lang=\"AR-SA\" style=\"font-size: 10pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 10pt; font-family: Arial, sans-serif;\">&nbsp;<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ لا يسمح للمتدرب باستخدام محتويات الموقع\r\nكالعلامة التجارية للموقع ، وغيرها لأغراض أخرى .</span><span lang=\"AR-SA\" style=\"font-size: 10pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoListParagraphCxSpFirst\" dir=\"RTL\" style=\"text-align: justify; \"><span style=\"font-size: 14pt;\">&nbsp;</span><span style=\"font-family: Arial, sans-serif; text-align: center; text-indent: 6px; font-size: 14pt;\">&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span><span style=\"font-family: Arial, sans-serif; font-size: 14pt; text-align: right;\">&nbsp;<br></span></p>', '<div><p class=\"MsoNormal\" dir=\"RTL\"><b><span lang=\"AR-SA\" dir=\"RTL\" style=\"font-size:16.0pt;\r\nline-height:107%;font-family:&quot;Arial&quot;,sans-serif;mso-ascii-font-family:Calibri;\r\nmso-ascii-theme-font:minor-latin;mso-fareast-font-family:PMingLiU;mso-fareast-theme-font:\r\nminor-fareast;mso-hansi-font-family:Calibri;mso-hansi-theme-font:minor-latin;\r\nmso-ansi-language:EN-US;mso-fareast-language:ZH-TW;mso-bidi-language:AR-SA\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span></b></p>\r\n\r\n<p class=\"MsoNormal\" align=\"center\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: center; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"AR-SA\" style=\"font-size: 18pt; font-family: Arial, sans-serif;\">&nbsp;سياسة الالتزام بحماية الحقوق الفكرية وحقوق\r\nالنشر</span></b><span dir=\"LTR\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#1F4E79\">أولاً : حقوق الملكية\r\nالفكرية :</span></b><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#385723\">√&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">يتعهد مركز صناعة العبقرية للتدريب ، باحترام حقوق\r\nالملكية الفكرية عند إعداد وصناعة المحتوى التدريبي ، والبرامج التدريبية .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ كما يتعهد باحترام حقوق\r\nالملكية الفكرية &nbsp;عند رفع جميع الدورات والمقالات الرقمية ، وأي محتوى رقمي\r\nعلى أنظمته الإلكترونية ، سوآء ما ينشر على الموقع الإلكتروني أو في حسابات\r\nالتواصل الاجتماعي التابعة للمركز .<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">&nbsp;كذلك</span><span lang=\"AR-SA\" style=\"font-size:\r\n16.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\ncolor:#385723\">&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">يلتزم المركز بحقوق الملكية الفكرية عند تطوير البرامج التدريبية\r\nوالمحتوى العلمي ، ورفعه على نظام التعلم الإلكتروني ، وتحويله إلى مقررات&nbsp;\r\nإلكترونية بالشراكة مع المدربين .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#385723\">√</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">&nbsp;التنوع في أساليب أسئلة الاختبار بحيث يكون\r\nالتركيز على المشاريع والاختبارات الشفوية .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#1F4E79\">ثانيًا : حقوق النشر:</span></b><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ يتعهد مركز صناعة\r\nالعبقرية في الحفاظ على حقوق النشر ، ويحق للمركز حق النشر للمحتوى المرئي في\r\nأنظمته الإلكترونية الخاصة به مع توضيح مصدر هذه المواد التربوية .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#385723\">√</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">&nbsp; كما يلتزم المركز بحفظ المحتوى الإلكتروني\r\nمن تسجيلات للبرامج التدريبية في مصادر التخزين المعتمدة لديه ، ورفع المحتوى\r\nالإلكتروني على أنظمة التعليم الإلكتروني .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#385723\">√&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">كما يتعهد بعدم مخالفة المحتوى المقدم لحقوق النشر\r\nولا يتعدى على حقوق لطرف أو جهة .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#385723\">√</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">&nbsp; كما يلزم المدرب الذي تم التعاقد معه\r\nباحترام حقوق الملكية الفكرية وحقوق النشر ، حيث يمنع لأي متدرب نشر أو تصوير أي\r\nمحتوى للمدرب دون إذن المدرب .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-fareast-font-family:&quot;Times New Roman&quot;;color:#385723\">√ و</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">يحق لإدارة الموقع مراجعة منشورات المتدربين ، وفي\r\nحال تم نشر محتوى مخالف لقوانين المركز يحق لإدارة المركز التصرف بالتعديل أو\r\nالإزالة .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">ومن أمثلة المحتوى\r\nالمخالف للقوانين :</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">٭&nbsp; نشر محتوى مخل\r\nبالآداب الإسلامية العامة .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">٭&nbsp; نشر محتوى سياسي\r\n.</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">٭&nbsp; نشر محتوى فيه\r\nانتهاك للملكية الفكرية أو حقوق نشر لفرد أو منشأة .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">٭&nbsp;&nbsp;تشهير أو\r\nإساءة لأي فرد أو جهة معينة .</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 22pt; font-family: Arial, sans-serif;\">&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span dir=\"LTR\" style=\"font-size:14.0pt;line-height:107%\"><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span dir=\"LTR\" style=\"font-size:14.0pt;line-height:107%\"><o:p>&nbsp;</o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\">\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 22pt; font-family: Arial, sans-serif;\">&nbsp;<o:p></o:p></span></p></div><ul><o:p></o:p></ul>', '<p class=\"MsoNormal\" align=\"center\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: center; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span dir=\"RTL\"></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><br></p><p class=\"MsoNormal\" align=\"center\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: center; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"AR-SA\" style=\"font-family: Arial, sans-serif;\">&nbsp;</span></b><b><span lang=\"AR-SA\" style=\"font-size: 18pt; font-family: Arial, sans-serif;\">سياسة الحضور ( التعليم الإلكتروني)</span></b><span dir=\"LTR\" style=\"font-size: 8pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" align=\"center\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: center; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span dir=\"LTR\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;color:#385723\">مرحبًا بكم في موقع صناعة العبقرية\r\nللتدريب&nbsp;&nbsp;<a href=\"https://gmc-sa.net/\"><span lang=\"EN-US\" dir=\"LTR\" style=\"color:#385723;letter-spacing:-.2pt\">https://gmc-sa.net</span></a></span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">&nbsp; يعد التعليم الإلكتروني ثورة حديثة في\r\nأساليب وطرق التعليم والتدريب ، حيث يعده بعض الباحثين أنّ التعليم الإلكتروني\r\nأكثر فاعلية من التعليم الاعتيادي ، ويمكن وصفه بأنه افتراضي بوسائله ، وواقعي\r\nبنتائجه ، وبذلك يعادل الحضور الإلكتروني للحضور الاعتيادي .<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">وذلك لعدة مزايا منها :<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ قدرته على تحسين أداء المتدرب بأقل وقت ، وأقل\r\nجهد و تكلفة .<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ تسهيل عملية التدريب للمتدرب ، حيث يمكنه التعلم\r\nمن أي مكان ، وفي أي وقت دون قيود المكان والزمان .<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ ساعد التعليم الإلكتروني المتدرب في تحقيق\r\nالتعلم الذاتي .<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ يعتبر أكثر مراعاة للفروق الفردية .<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">√ تنوع وسائله التعليمية بين صوت وصورة وفيديو\r\nوملفات وغيرها ، ونظراً لهذه الأهمية ،،<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;color:#203864\">يتعهد مركز صناعة العبقرية بتطبيق سياسة الحضور\r\nالإلكتروني بجميع صوره ، وأساليبه منه :</span></b><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">*&nbsp;الحضور الإلكتروني المتزامن : وهو التعليم\r\nالذي يجتمع فيه المدرب والمتدرب في وقت واحد بالصوت والصورة .<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">*&nbsp; الحضور الإلكتروني غير المتزامن : حيث يضع\r\nالمدرب محتواه التدريبي من ملفات ، وصوت وصورة على المنصة الإلكترونية ، ثم يرجع\r\nإليه المتدرب في أي وقت يشاء حسب سياسة المركز .<o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;color:#203864\">كما يلتزم المركز بالحد الأدنى من تقديم ساعات\r\nالحضور المتزامن وهو مالا يقل عن (25%) ،حيث يسمح بزيادة (25%) في القاعة .</span></b><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:\r\n&quot;Times New Roman&quot;;color:#203864\">&nbsp;&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:16.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p><p class=\"MsoNormal\" align=\"center\" dir=\"RTL\" style=\"margin-bottom: 0in; text-align: center; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span dir=\"RTL\"></span>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</p><p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span dir=\"LTR\" style=\"font-size:14.0pt;line-height:107%\"><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\"><span dir=\"LTR\" style=\"font-size:18.0pt;line-height:\r\n107%\"><o:p>&nbsp;</o:p></span></p>', '<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in -4.5pt 0in 0in; text-align: justify; text-indent: 4.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><br></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 0.5in 0in 0in; text-align: justify; text-indent: -31.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><b><span lang=\"AR-SA\" style=\"font-size: 14pt; font-family: Arial, sans-serif;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span></b><b><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">سياسة آلية فحص المتدربين ومنع الغش وانتحال\r\nالهوية</span></b><span dir=\"LTR\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 0.5in 0in 0in; text-align: justify; text-indent: -31.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 10.5pt; font-family: Arial, sans-serif;\"><br>\r\n<!--[if !supportLineBreakNewLine]--><br>\r\n<!--[endif]--><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 0.5in 0in 0in; text-align: justify; text-indent: -31.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 12pt; font-family: Arial, sans-serif;\">&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 14pt; font-family: Arial, sans-serif;\">&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">يتعهد مركز صناعة العبقرية بعرض قوانين وأنظمة\r\nالاختبارات ،من أنظمة عقوبات الغش ، وانتحال الهوية للمتدرب قبل البدء بالاختبار .</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 0.5in 0in 0in; text-align: justify; text-indent: -31.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">← كما يتعهد بضبط أي سلوكيات مخالفة ، لضمان جودة السياسة التقييمية\r\nللمركز.</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 0.5in 0in 0in; text-align: justify; text-indent: -27pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">←&nbsp; حيث يَلزم المتدرب الاطلاع على هذه الأنظمة قبل شروعه في\r\nالاختبار و من ثم تطبيقها .</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 9pt 0in 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">←&nbsp;\r\nويلتزم المركز بتنظيم حملات توعوية، وابتكار حلول تقنية ، لضبط حالات الغش وانتحال\r\nالهوية.<br>\r\n←&nbsp; ويلتزم المركز بتطبيق عقوبات على المتدرب ، في حالة ارتكابه أي مخالفة\r\nلقواعد الاختبارات ،&nbsp; ومن هذه المخالفات :</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 13.5pt 0in 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span><span dir=\"LTR\" style=\"font-size:16.0pt;\r\nfont-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\ncolor:#385723\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span>֍</span><span dir=\"RTL\"></span><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><span dir=\"RTL\"></span><span dir=\"RTL\"></span>&nbsp; انتحال شخصية أخرى في الاختبار</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 13.5pt 0in 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span><span dir=\"LTR\" style=\"font-size:16.0pt;\r\nfont-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\ncolor:#385723\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span>֍</span><span dir=\"RTL\"></span><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><span dir=\"RTL\"></span><span dir=\"RTL\"></span>&nbsp; ارتكاب سلوك سيئ مع احد المدربين أو المشرفين</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 13.5pt 0in 0in; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span><span dir=\"LTR\" style=\"font-size:16.0pt;\r\nfont-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\ncolor:#385723\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span>֍</span><span dir=\"RTL\"></span><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\"><span dir=\"RTL\"></span><span dir=\"RTL\"></span>&nbsp; محاولة غش ومساعدة شخص بغرض الغش.</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 22.5pt 0in 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">في حال ارتكب المتدرب إحدى هذه المخالفات يحق للمدرب برفع المخالفة\r\nلإدارة المركز ، حيث يقوم المركز بتطبيق المخالفة على المتدرب وتكون العقوبات\r\nكالتالي:</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 22.5pt 0in 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">←&nbsp;توجيه إنذار للمتدرب وحرمانه من الاختبار.</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 22.5pt 0in 0in; text-align: justify; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; font-family: Arial, sans-serif;\">←&nbsp;الحرمان من دخول الاختبار.</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin: 0in 0.5in 0in 0in; text-align: justify; text-indent: -31.5pt; line-height: normal; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span lang=\"AR-SA\" style=\"font-size: 22pt; font-family: Arial, sans-serif;\">&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 20pt; font-family: Arial, sans-serif;\">&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 9pt; font-family: Arial, sans-serif;\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"text-align:justify;text-justify:inter-ideograph\"><span dir=\"LTR\"><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-right:22.5pt;text-align:justify;\r\ntext-justify:inter-ideograph;tab-stops:283.2pt\"><span lang=\"AR-SA\" style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Arial&quot;,sans-serif;\r\nmso-ascii-font-family:Calibri;mso-ascii-theme-font:minor-latin;mso-hansi-font-family:\r\nCalibri;mso-hansi-theme-font:minor-latin;mso-bidi-theme-font:minor-bidi\">&nbsp;</span></p>', NULL, '2021-09-09 02:46:40');

-- --------------------------------------------------------

--
-- Table structure for table `licensed_courses`
--

CREATE TABLE `licensed_courses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_accreditation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `licensed_courses`
--

INSERT INTO `licensed_courses` (`id`, `name`, `no_accreditation`, `created_at`, `updated_at`) VALUES
(1, 'دوره رائعة', '1234', '2021-03-11 12:53:35', '2021-04-10 20:57:58');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2018_10_07_184333_create_notifications_table', 1),
(9, '2018_10_07_184351_create_user_notifications_table', 1),
(10, '2019_09_10_094330_create_user_lastpasswords_table', 1),
(11, '2019_09_18_195814_create_countries_table', 1),
(12, '2019_09_18_200241_create_cities_table', 1),
(13, '2020_05_10_155006_create_user_types_table', 1),
(14, '2020_08_20_151602_create_ads_clicks_table', 1),
(15, '2020_08_20_151719_create_ads_table', 1),
(16, '2020_10_14_132605_create_settings_table', 1),
(17, '2020_10_15_072153_create_payment_types_table', 1),
(18, '2021_02_05_180054_create_trainers_table', 2),
(19, '2021_02_05_201627_create_sliders_table', 3),
(20, '2020_12_22_173800_create_contact_uses_table', 4),
(21, '2021_02_05_210253_create_courses_table', 5),
(22, '2021_02_08_134417_create_orders_table', 6),
(23, '2021_02_14_183708_create_requests_table', 7),
(24, '2021_02_14_184230_create_contract_courses_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci,
  `body` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, ' العبقرية للتدريب', ' المدير نشط حسابك', '2021-02-06 14:56:48', '2021-02-06 14:56:48'),
(2, ' العبقرية للتدريب', ' المدير عطل حسابك', '2021-02-06 15:15:11', '2021-02-06 15:15:11'),
(3, ' العبقرية للتدريب', ' المدير عطل حسابك', '2021-03-02 08:19:29', '2021-03-02 08:19:29'),
(4, ' العبقرية للتدريب', ' المدير نشط حسابك', '2021-03-02 08:19:34', '2021-03-02 08:19:34'),
(5, ' العبقرية للتدريب', ' المدير عطل حسابك', '2021-04-10 20:07:50', '2021-04-10 20:07:50'),
(6, ' العبقرية للتدريب', ' المدير نشط حسابك', '2021-04-10 20:07:55', '2021-04-10 20:07:55'),
(7, ' العبقرية للتدريب', ' المدير عطل حسابك', '2021-05-19 18:40:07', '2021-05-19 18:40:07'),
(8, ' العبقرية للتدريب', ' المدير نشط حسابك', '2021-05-19 18:40:15', '2021-05-19 18:40:15');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qualification_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `course_id`, `name`, `mobile`, `country_id`, `email`, `identity`, `qualification_id`, `other`, `paid`, `created_at`, `updated_at`) VALUES
(114, 15, 16, 'ahmed abdelhamidk', '506888677', 1, 'gmctsa@gmail.com', '222121452', 'بكالوريوس', NULL, 0, '2021-05-25 01:59:49', '2021-05-25 02:07:42'),
(115, 15, 16, 'علي ابو وردة', '٥٥٥٥٥٥٥٥', 1, 'gmctsa@gmail.com', '٨٤٧٣٧٣٦٣٧٣٨', 'دكتوراه', NULL, 1, '2021-05-25 02:06:28', '2021-05-25 02:06:28'),
(116, 20, 39, 'احمد عبد الحميد محمود سعد', '595149449', 1, 'ahmed0595149449@gmail.com', '2281773651', 'بكالوريوس', NULL, 1, '2021-06-25 00:34:50', '2021-06-25 00:34:50'),
(117, 20, 39, 'إبراهيم سعد ناصر بن جريد', '555211332', 1, 'isj0555211332@gmail.com', '1052121355', 'بكالوريوس', 'جامعي', 1, '2021-06-25 00:37:34', '2021-06-25 00:37:34'),
(118, 20, 39, 'بارك الله فيكم', '532252228', 1, 'smz.gmc@gmail.com', '1010101010', 'بكالوريوس', 'بكالوريوس', 1, '2021-06-25 00:38:13', '2021-06-25 00:38:13'),
(119, 1, 73, 'محمد فرج', '533392936', 1, 'gmsdesigners01@gmail.com', 'ewaghtrh', 'بكالوريوس', 'fd[h,ly', 1, '2021-08-18 19:47:30', '2021-08-18 19:47:30'),
(120, 1, 65, 'Al3ttar', '1028822983', 1, 'ahmedemadmohamed95@gmail.com', '1321689812', 'بكالوريوس', NULL, 1, '2021-08-24 03:44:26', '2021-08-24 03:44:26');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_types`
--

CREATE TABLE `payment_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questionnaires`
--

CREATE TABLE `questionnaires` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty11` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty12` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty13` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questionnaires`
--

INSERT INTO `questionnaires` (`id`, `name`, `email`, `qty1`, `qty2`, `qty3`, `qty4`, `qty5`, `qty6`, `qty7`, `qty8`, `qty9`, `qty10`, `qty11`, `qty12`, `qty13`, `created_at`, `updated_at`) VALUES
(4, 'محمد عماد', 'miti@mailinator.com', 'محايد', 'محايد', 'محايد', 'أوافق', 'محايد', 'محايد', 'محايد', 'لا أوافق', 'محايد', 'لا أوافق', 'محايد', 'أوافق', 'أوافق', '2021-09-07 21:29:52', '2021-09-07 21:29:52');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `course_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `course_id`, `country_id`, `name`, `email`, `mobile`, `created_at`, `updated_at`) VALUES
(11, 12, 1, 'أحمد عماد', 'ahmedemadmohamed95@gmail.com', '595149449', '2021-03-28 21:53:49', '2021-03-28 21:53:49'),
(12, 12, 1, 'ALI ABU WARDEH', 'aaliabuwardeh@gmail.com', '508279970', '2021-03-30 02:29:50', '2021-03-30 02:29:50'),
(13, 69, 1, 'IBRAHIM S BIN JURAYED', 'info@gmc-sa.net', '591393045', '2021-08-17 23:51:00', '2021-08-17 23:51:00'),
(14, 73, 1, 'محمد فرج', 'gmsdesigners01@gmail.com', '533392936', '2021-08-18 19:43:16', '2021-08-18 19:43:16'),
(15, 65, 1, 'حسن يحي محمد الشريف', 'hassan6267@gmail.com', '554566267', '2021-08-20 20:39:41', '2021-08-20 20:39:41'),
(16, 71, 1, 'حسن يحي محمد الشريف', 'hassan6267@gmail.com', '554566267', '2021-08-20 20:42:10', '2021-08-20 20:42:10'),
(17, 57, 1, 'ذكرى مرضي العنزي', 'n3ooma_87@hotmail.com', '553860650', '2021-08-22 05:30:58', '2021-08-22 05:30:58'),
(18, 75, 1, 'Al3ttar', 'ahmedemadmohamed95@gmail.com', '1028822983', '2021-08-24 03:32:10', '2021-08-24 03:32:10'),
(19, 75, 1, 'Al3ttar', 'ahmedemadmohamed95@gmail.com', '1028822983', '2021-08-24 03:38:03', '2021-08-24 03:38:03'),
(20, 75, 1, 'Al3ttar', 'ahmedemadmohamed95@gmail.com', '1028822983', '2021-08-24 03:40:17', '2021-08-24 03:40:17'),
(21, 75, 1, 'Al3ttar', 'ahmedemadmohamed95@gmail.com', '5123188000', '2021-08-24 03:40:37', '2021-08-24 03:40:37'),
(22, 75, 1, 'Al3ttar', 'ahmedemadmohamed95@gmail.com', '532252226', '2021-08-24 03:41:33', '2021-08-24 03:41:33'),
(23, 75, 1, 'Al3ttar', 'ahmedemadmohamed95@gmail.com', '532252226', '2021-08-24 03:41:59', '2021-08-24 03:41:59'),
(24, 59, 1, 'ندى الدخيل', 'nada1993al@gmail.com', '548103782', '2021-08-25 00:02:26', '2021-08-25 00:02:26'),
(25, 66, 1, 'منى العقيل', 'zezokha9@gmail.com', '593366615', '2021-08-25 10:54:22', '2021-08-25 10:54:22'),
(26, 75, 1, 'مم', 'amrgmc@gmail.com', '', '2021-08-25 22:49:50', '2021-08-25 22:49:50'),
(27, 73, 1, 'Al3ttar', 'ahmedemadmohamed95@gmail.com', '1028822983', '2021-08-25 22:50:43', '2021-08-25 22:50:43'),
(28, 75, 1, '0.', 'amrgmcsa@gmail.com', '564545', '2021-08-25 22:50:53', '2021-08-25 22:50:53'),
(29, 72, 1, 'منال سعود الأحمدي', 'Mannal_r@hotmail.com', '594669984', '2021-08-26 23:31:19', '2021-08-26 23:31:19'),
(30, 69, 1, 'هلا سعود', 'hallloo23@live.com', '548666691', '2021-08-28 19:15:37', '2021-08-28 19:15:37'),
(31, 69, 1, 'هلا سعود', 'hallloo23@live.com', '548666691', '2021-08-28 19:15:41', '2021-08-28 19:15:41'),
(32, 69, 1, 'هلا سعود', 'hallloo23@live.com', '548666691', '2021-08-28 19:15:42', '2021-08-28 19:15:42'),
(33, 61, 1, 'خالد علي يحيى الاعجم', 'k.d.0566400021@gmail.com', '566400021', '2021-08-28 22:44:40', '2021-08-28 22:44:40'),
(34, 61, 1, 'خالد علي يحيى الاعجم', 'k.d.0566400021@gmail.com', '566400021', '2021-08-28 22:44:42', '2021-08-28 22:44:42'),
(35, 75, 1, 'خالد علي يحيى الاعجم', 'k.d.0566400021@gmail.com', '566400021', '2021-08-28 23:01:42', '2021-08-28 23:01:42'),
(36, 72, 1, 'بناء المرزوقي', 'wx33@live.com', '554775117', '2021-09-01 00:36:37', '2021-09-01 00:36:37'),
(37, 72, 1, 'بناء المرزوقي', 'wx33@live.com', '554775117', '2021-09-01 00:36:37', '2021-09-01 00:36:37'),
(38, 65, 1, 'ربيع صالح عيسى الخالدي', 'alkhaldi2019@hotmail.com', '563929728', '2021-09-01 06:40:23', '2021-09-01 06:40:23'),
(39, 67, 1, 'ديما حمد المشيطي', 'ideema90m@hotmail.com', '556166611', '2021-09-02 07:28:35', '2021-09-02 07:28:35'),
(40, 67, 1, 'ديما حمد المشيطي', 'ideema90m@hotmail.com', '556166611', '2021-09-02 07:28:37', '2021-09-02 07:28:37'),
(41, 67, 1, 'ديما حمد المشيطي', 'ideema90m@hotmail.com', '556166611', '2021-09-02 07:28:40', '2021-09-02 07:28:40'),
(42, 67, 1, 'ديما حمد المشيطي', 'ideema90m@hotmail.com', '556166611', '2021-09-02 07:28:42', '2021-09-02 07:28:42'),
(43, 67, 1, 'ديما حمد المشيطي', 'ideema90m@hotmail.com', '556166611', '2021-09-02 07:28:44', '2021-09-02 07:28:44'),
(44, 65, 1, 'ديما حمد المشيطي', 'ideema90m@hotmail.com', '556166611', '2021-09-02 07:30:55', '2021-09-02 07:30:55'),
(45, 76, 1, 'غدير حسن باصريح', 'ghadeer.basarih8@gmail.com', '542669993', '2021-09-03 02:48:52', '2021-09-03 02:48:52'),
(46, 69, 1, 'فاطمة عبدالله المرشد', 'sara769400@cloud.com', '555079378', '2021-09-04 22:53:32', '2021-09-04 22:53:32'),
(47, 69, 1, 'فاطمة عبدالله المرشد', 'sara769400@cloud.com', '555079378', '2021-09-04 22:53:34', '2021-09-04 22:53:34'),
(48, 64, 1, 'فهد محمد صبار الشمري', 'fhkh123123@gmail.com', '535888765', '2021-09-05 03:30:49', '2021-09-05 03:30:49'),
(49, 72, 1, 'ابتسام حسن الشهري', 'ialshehri@live.com', '558331775', '2021-09-05 06:59:12', '2021-09-05 06:59:12'),
(50, 72, 1, 'ابتسام حسن الشهري', 'ialshehri@live.com', '558331775', '2021-09-05 06:59:16', '2021-09-05 06:59:16'),
(51, 71, 1, 'آلاء محمد أمين الجندي', 'allamameen1401@gmail.com', '+966562835101', '2021-09-05 20:43:01', '2021-09-05 20:43:01'),
(52, 80, 1, 'ياسر عبدالرحمن أحمد تكروني', 'Yasser.x1406@gmail.com', '545010154', '2021-09-06 17:18:56', '2021-09-06 17:18:56'),
(53, 80, 1, 'عبدالمجيد مبارك الحربي', 'sbwrsm@outlook.com', '570051013', '2021-09-08 18:19:48', '2021-09-08 18:19:48'),
(54, 69, 1, 'Muhammad Usman', 'altamimmii@gmail.com', '555481715', '2021-09-08 18:39:04', '2021-09-08 18:39:04'),
(55, 79, 1, 'أمل سعود عويد الحربي', 'amal.sl1443@gmail.com', '540906640', '2021-09-09 02:37:15', '2021-09-09 02:37:15'),
(56, 79, 1, 'دلال سعود عويد العوفي', 'D.sl29@hotmail.com', '598710079', '2021-09-09 03:16:12', '2021-09-09 03:16:12'),
(57, 80, 1, 'عبدالمجيد مبارك مبروك الحربي', 'sbwrsm@outlook.com', '570051013', '2021-09-09 16:29:23', '2021-09-09 16:29:23'),
(58, 70, 1, 'نايل ملفي المطيري', 'Nail.m.almutairi@gmail.com', '550414945', '2021-09-09 16:52:09', '2021-09-09 16:52:09'),
(59, 70, 1, 'نايل ملفي المطيري', 'Nail.m.almutairi@gmail.com', '550414945', '2021-09-09 16:52:11', '2021-09-09 16:52:11'),
(60, 70, 1, 'نايل ملفي المطيري', 'Nail.m.almutairi@gmail.com', '550414945', '2021-09-09 16:52:17', '2021-09-09 16:52:17'),
(61, 70, 1, 'نايل ملفي المطيري', 'Nail.m.almutairi@gmail.com', '550414945', '2021-09-09 16:52:21', '2021-09-09 16:52:21'),
(62, 70, 1, 'نايل ملفي المطيري', 'Nail.m.almutairi@gmail.com', '550414945', '2021-09-09 16:52:21', '2021-09-09 16:52:21'),
(63, 70, 1, 'سلطان بن ثاني بن دوشان العنزي', 'shlat2014@gmail.com', '530333730', '2021-09-09 22:02:01', '2021-09-09 22:02:01'),
(64, 70, 1, 'مشاري محمد الشهيل', 'mishari.alshohail@hotmail.com', '583423338', '2021-09-10 09:12:49', '2021-09-10 09:12:49'),
(65, 69, 1, 'Layla', 'layla.it@hotmail.com', '530601477', '2021-09-11 17:32:55', '2021-09-11 17:32:55'),
(66, 70, 1, 'لمى عبدالله المطيري', 'lalmutiri06@gmail.com', '566786490', '2021-09-12 07:06:04', '2021-09-12 07:06:04'),
(67, 70, 1, 'سعود عبدالله المطيري', 's00ood1422@gmail.com', '568057856', '2021-09-12 07:07:17', '2021-09-12 07:07:17'),
(68, 73, 1, 'حسن محمد صالح الصيعري', 'alsaiari215@gmail.com', '505904059', '2021-09-12 20:40:43', '2021-09-12 20:40:43'),
(69, 70, 1, 'زمزم عبدالله العلوي', 'Zezecute97@gmail.com', '561345625', '2021-09-12 23:53:00', '2021-09-12 23:53:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `delivery_cost` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `tax` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` longtext COLLATE utf8mb4_unicode_ci,
  `facebook` longtext COLLATE utf8mb4_unicode_ci,
  `twitter` longtext COLLATE utf8mb4_unicode_ci,
  `instagram` longtext COLLATE utf8mb4_unicode_ci,
  `linkedin` longtext COLLATE utf8mb4_unicode_ci,
  `whatsapp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `terms_en` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `delivery_cost`, `tax`, `logo`, `address`, `mobile`, `email`, `facebook`, `twitter`, `instagram`, `linkedin`, `whatsapp`, `terms_en`, `created_at`, `updated_at`) VALUES
(1, '0', '0', 'https://gmcksa.com/storage/app/public/images/JqQfyTj3sziPWtWzXPhFzo0sHOfB1TJF2BBSzfP4.png', 'King Abdul Aziz Rd, King Abdul Aziz, Riyadh 12431, Saudi Arabia', '00966-5400-99115', 'info@gmc-sa.net', 'facebook', 'https://twitter.com/gmcsa', 'https://www.instagram.com/gmcsa/', 'linkedin', '540099114', NULL, NULL, '2021-09-02 23:54:07');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `name_ar`, `name_en`, `created_at`, `updated_at`) VALUES
(1, 'https://gmcksa.com/storage/app/public/images/B3vXjtdsO8hOgCmKzhsVCrtjfg3P7uWhYXIHtd4R.png', 'التعليم هو أقوى سلاح', NULL, '2021-02-05 18:30:59', '2021-03-29 21:25:37'),
(2, 'https://gmcksa.com/storage/app/public/images/Jv1LwKHgBOVjnT5w9T8vUCdTvIUyf6PMJ7MkTYtU.png', 'شريكك الأمثل للنجاح', NULL, '2021-03-03 22:31:53', '2021-03-29 21:25:14');

-- --------------------------------------------------------

--
-- Table structure for table `sponsored_advertisements`
--

CREATE TABLE `sponsored_advertisements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sponsored_advertisements`
--

INSERT INTO `sponsored_advertisements` (`id`, `user_id`, `course_id`, `name`, `mobile`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 9, 'Al3ttar', '1028822983', 'ahmedemadmohamed95@gmail.com', 1, '2021-08-13 18:16:35', '2021-08-13 18:16:35'),
(2, 1, 54, 'Al3ttar', '1028822983', 'ahmedemadmohamed95@gmail.com', 1, '2021-08-14 10:07:57', '2021-08-14 10:07:57'),
(3, 1, 61, 'سيف', '591393045', 'seifatef2050@gmail.com', 1, '2021-08-19 02:10:15', '2021-08-19 02:10:15'),
(4, 1, 61, 'Ahmed', '504280838', 'harbiah1@ngha.med.sa', 1, '2021-08-19 03:28:56', '2021-08-19 03:28:56'),
(5, 1, 61, 'Faisal', '555885628', 'musader30@hotmail.com', 1, '2021-08-19 11:12:30', '2021-08-19 11:12:30'),
(6, 1, 57, 'ضحى محسن', '553811867', 'too7ah20@hotmail.com', 1, '2021-08-20 01:06:11', '2021-08-20 01:06:11'),
(7, 1, 61, 'شرفي محمد عايض عسيري', '504731073', 'sharfi4292@gmail.com', 1, '2021-08-20 01:41:28', '2021-08-20 01:41:28'),
(8, 1, 57, 'مها', '500048282', 'mahamusleh@hotmail.com', 1, '2021-08-20 02:20:28', '2021-08-20 02:20:28'),
(9, 1, 57, 'دلال صالح الجهني', '555100581', 'dalalsal369@gmail.com', 1, '2021-08-20 02:45:17', '2021-08-20 02:45:17'),
(10, 1, 57, 'نوره', '555410049', 'noura.nor.11@gmail.com', 1, '2021-08-20 03:16:38', '2021-08-20 03:16:38'),
(11, 1, 57, 'امنه عبدالله المجيدل', '503984821', 'amonn019021@gmail.com', 1, '2021-08-20 03:46:20', '2021-08-20 03:46:20'),
(12, 1, 57, 'ساره احمد', '552088552', 'sis.muqbil@gmail.com', 1, '2021-08-20 04:18:33', '2021-08-20 04:18:33'),
(13, 1, 57, 'ساره احمد', '552088552', 'sis.muqbil@gmail.com', 1, '2021-08-20 04:18:36', '2021-08-20 04:18:36'),
(14, 1, 57, 'دلال عبدالله الناصر', '505253688', 'alnasserdalal368@gmail.com', 2, '2021-08-20 04:23:40', '2021-08-28 10:14:33'),
(15, 1, 57, 'Elham Hattan', '566172767', 'elhamhattan@hotmail.com', 1, '2021-08-20 04:25:14', '2021-08-20 04:25:14'),
(16, 1, 57, 'أمل الوهبي', '550828400', 'almalkiamal00@gmail.com', 1, '2021-08-20 06:33:34', '2021-08-20 06:33:34'),
(17, 1, 57, 'اعتدال عبدالله', '554221587', 'e3tedal@windowslive.com', 1, '2021-08-20 08:17:50', '2021-08-20 08:17:50'),
(18, 1, 57, 'رانية فادن', '503401235', 'raniafaden@gmail.com', 1, '2021-08-20 10:02:41', '2021-08-20 10:02:41'),
(19, 1, 57, 'خلود', '506090071', 'k.alhammad@veloute.com.sa', 1, '2021-08-20 10:33:38', '2021-08-20 10:33:38'),
(20, 1, 57, 'مديحه', '530080247', 'madiha.alali@gmail.com', 1, '2021-08-20 10:45:41', '2021-08-20 10:45:41'),
(21, 1, 57, 'Reem', '598005247', 'r_s_a_j1410@hotmail.com', 1, '2021-08-20 11:39:54', '2021-08-20 11:39:54'),
(22, 1, 57, 'Reem', '598005247', 'r_s_a_j1410@hotmail.com', 1, '2021-08-20 11:39:56', '2021-08-20 11:39:56'),
(23, 1, 57, 'هدى محمد الدخيل', '543482869', 'huda-5553@hotmail.com', 1, '2021-08-20 11:57:39', '2021-08-20 11:57:39'),
(24, 1, 57, 'هديل العمران', '554900977', 'Hadeel.alomran@gmail.com', 1, '2021-08-20 16:16:16', '2021-08-20 16:16:16'),
(25, 1, 57, 'الجوهرة محمد', '503959990', 'noura.s.aljebreen@gmail.com', 1, '2021-08-20 17:24:29', '2021-08-20 17:24:29'),
(26, 1, 57, 'منال محمد الحربي', '565152677', 'qqwwee765@icloud.com', 1, '2021-08-20 18:36:58', '2021-08-20 18:36:58'),
(27, 1, 57, 'رشا المالكي', '531242730', 'Almalkirasha5@gmail.com', 1, '2021-08-20 19:05:02', '2021-08-20 19:05:02'),
(28, 1, 61, 'حسن يحي محمد الشريف', '554566267', 'hassan6267@gmail.com', 1, '2021-08-20 20:36:29', '2021-08-20 20:36:29'),
(29, 1, 57, 'اشواق ال عبيد', '548393089', 'sky-rossse@hotmail.com', 1, '2021-08-20 21:03:19', '2021-08-20 21:03:19'),
(30, 1, 57, 'Munira', '581155188', 'munira.if.33@gmail.com', 1, '2021-08-20 23:01:49', '2021-08-20 23:01:49'),
(31, 1, 57, 'Munira', '581155188', 'munira.if.33@gmail.com', 1, '2021-08-20 23:03:10', '2021-08-20 23:03:10'),
(32, 1, 57, 'الجوهرة', '554994656', 'j_b_m_2006@hitmail.com', 1, '2021-08-21 01:42:57', '2021-08-21 01:42:57'),
(33, 1, 57, 'الجوهرة', '554994656', 'j_b_m_2006@hitmail.com', 1, '2021-08-21 01:45:17', '2021-08-21 01:45:17'),
(34, 1, 57, 'Bodi', '504109722', 'bodi34@yahoo.com', 1, '2021-08-21 02:34:09', '2021-08-21 02:34:09'),
(35, 1, 57, 'هاجر محمد البلي', '506247947', 'totx123@gmail.com', 1, '2021-08-21 02:44:11', '2021-08-21 02:44:11'),
(36, 1, 57, 'شيخة القحطاني', '500444910', 'rodin55@hotmail.com', 1, '2021-08-21 02:59:48', '2021-08-21 02:59:48'),
(37, 1, 57, 'حصة العتيبي', '503718727', 'hhh1551hhh@hotmail.com', 1, '2021-08-21 04:39:15', '2021-08-21 04:39:15'),
(38, 1, 57, 'حصة العتيبي', '503718727', 'hhh1551hhh@hotmail.com', 1, '2021-08-21 04:39:16', '2021-08-21 04:39:16'),
(39, 1, 57, 'مناهل احمد محمد', '531234444', 'manahel_2020@hotmail.com', 1, '2021-08-21 05:38:34', '2021-08-21 05:38:34'),
(40, 1, 57, 'باسمه', '501567000', 'Basmah-ss@hotmail.com', 1, '2021-08-21 06:17:37', '2021-08-21 06:17:37'),
(41, 1, 57, 'العنود', '508374298', 'alanood881@hotmail.com', 1, '2021-08-21 11:28:43', '2021-08-21 11:28:43'),
(42, 1, 57, 'Meme', '501667054', 'memeoio606@gmail.com', 1, '2021-08-21 12:12:36', '2021-08-21 12:12:36'),
(43, 1, 57, 'ايمان محمد الحربي', '559901117', 'emooalhrbi.@gmail.com', 1, '2021-08-21 12:59:51', '2021-08-21 12:59:51'),
(44, 1, 57, 'بشرى نمشان الحلافي', '599844136', 'bushraa.alhalafi@hotmail.com', 1, '2021-08-21 13:01:55', '2021-08-21 13:01:55'),
(45, 1, 57, 'ذكرى العنزي', '٠٥٥٣٨٦٠٦٥٩', 'n3ooma_87@hotmail.com', 1, '2021-08-22 05:26:15', '2021-08-28 10:15:16'),
(46, 1, 60, 'سيف', '555211332', 'bassam@gmc-sa.com', 1, '2021-08-23 23:26:31', '2021-08-23 23:26:31'),
(47, 1, 60, 'IMAN M. ALSARHANI', '560735807', 'i.alsarhani87@gmail.com', 1, '2021-08-24 01:54:15', '2021-08-24 01:54:15'),
(48, 1, 60, 'IMAN M. ALSARHANI', '560735807', 'i.alsarhani87@gmail.com', 1, '2021-08-24 01:54:15', '2021-08-24 01:54:15'),
(49, 1, 60, 'فاطمة الشهراني', '503089695', 'famh9695@gmail.com', 1, '2021-08-24 02:42:59', '2021-08-24 02:42:59'),
(50, 1, 60, 'فاطمة الشهراني', '503089695', 'famh9695@gmail.com', 1, '2021-08-24 02:43:01', '2021-08-24 02:43:01'),
(51, 1, 60, 'امل الهاشم', '541106066', 'amalsaad2904@gmail.com', 1, '2021-08-24 02:43:03', '2021-08-24 02:43:03'),
(52, 1, 60, 'امل الهاشم', '541106066', 'amalsaad2904@gmail.com', 1, '2021-08-24 02:43:05', '2021-08-24 02:43:05'),
(53, 1, 60, 'عبير صالح', '555526446', 'jsalkazmari@gmail.com', 1, '2021-08-24 03:23:18', '2021-08-24 03:23:18'),
(54, 1, 60, 'فاطمه محمد علي المقعدي', '531079852', 'Mohdfatima352@gmail.com', 1, '2021-08-24 03:26:59', '2021-08-24 03:26:59'),
(55, 1, 67, 'ابتسام عبدالله مغيني', '546978224', 'Soma17533@gmail.com', 1, '2021-08-24 04:30:16', '2021-08-24 04:30:16'),
(56, 1, 67, 'امل', '٠٥٤٢٣٩٦٢٦٧', 'amal.m85@icluod.com', 1, '2021-08-24 05:19:13', '2021-08-24 05:19:13'),
(57, 1, 67, 'صفيه نجاشي', '500812348', 'najashsafiah@gmail.com', 1, '2021-08-24 09:03:16', '2021-08-24 09:03:16'),
(58, 1, 60, 'ندى فوزان الفوزان', '503331738', 'dethar1441@icloud.com', 1, '2021-08-24 15:27:22', '2021-08-24 15:27:22'),
(59, 1, 61, 'سث', '591393045', 'info@gmc-sa.net', 1, '2021-08-24 19:33:44', '2021-08-24 19:33:44'),
(60, 1, 75, 'ست', '591393045', 'info@gmc-sa.net', 1, '2021-08-24 19:34:27', '2021-08-24 19:34:27'),
(61, 1, 60, 'منال', '٠٥٥٥٤٠١٥٠٩', 'manal.msk@gmail.com', 1, '2021-08-24 23:05:18', '2021-08-24 23:05:18'),
(62, 1, 60, 'ندى الدخيل', '548103782', 'nada1993al@gmail.com', 1, '2021-08-24 23:53:37', '2021-08-24 23:53:37'),
(63, 1, 66, 'سيف', '591393045', 'info@gmc-sa.net', 1, '2021-08-25 00:19:07', '2021-08-25 00:19:07'),
(64, 1, 67, 'حياة الغامدي', '500600481', 'hayat.gh1212@hotmail.com', 1, '2021-08-25 04:25:44', '2021-08-25 04:25:44'),
(65, 1, 67, 'حياة الغامدي', '500600481', 'hayat.gh1212@hotmail.com', 1, '2021-08-25 04:25:45', '2021-08-25 04:25:45'),
(66, 1, 66, 'زهور أحمد ام فهد', '503811971', 'zalzahrani1111@gmail.com', 1, '2021-08-25 05:02:00', '2021-08-25 05:02:00'),
(67, 1, 66, 'نوره', '555410049', 'noura.nor.11@gmail.com', 1, '2021-08-25 05:44:34', '2021-08-25 05:44:34'),
(68, 1, 67, 'نوره ال سعود', '966562224413', 'noura.s.alsaud@gmail.com', 1, '2021-08-25 08:59:49', '2021-08-25 08:59:49'),
(69, 1, 66, 'تغريد', '558560007', 'Tootah060@gmail.com', 1, '2021-08-25 09:46:41', '2021-08-25 09:46:41'),
(70, 1, 66, 'منى العقيل', '593366615', 'zezokh9@gmail.com', 1, '2021-08-25 10:52:07', '2021-08-25 10:52:07'),
(71, 1, 67, 'نوره', '505260055', 'noura.b.a@gmail.com', 1, '2021-08-25 11:35:59', '2021-08-25 11:35:59'),
(72, 1, 66, 'تهاني', '543454601', 'talmuthibi@gmail.com', 1, '2021-08-25 17:20:03', '2021-08-25 17:20:03'),
(73, 1, 75, 'محسن هاشم المغربي', '563600997', 'mohsin_maghrabi@hotmail.com', 1, '2021-08-25 17:40:03', '2021-08-25 17:40:03'),
(74, 1, 67, 'ناديه حسين العمودي', '590467676', 'nhlamoudi1@gmail.com', 1, '2021-08-25 18:06:30', '2021-08-25 18:06:30'),
(75, 1, 66, 'سحاب القحطاني', '565550858', 'sahabbq@yahoo.com', 1, '2021-08-25 18:15:36', '2021-08-25 18:15:36'),
(76, 1, 66, 'افراح محمد', '506132342', 'iafrah.2018@gmail.com', 1, '2021-08-25 22:10:52', '2021-08-25 22:10:52'),
(77, 1, 66, 'هيلة القرعاوي', '٠٥٥٣١٣٥١١١', 'hailahqarawi@gmail.com', 1, '2021-08-26 05:08:00', '2021-08-26 05:08:00'),
(78, 1, 66, 'نورة سعد', '568082639', 'barghash.nourah@gmail.com', 1, '2021-08-26 06:40:22', '2021-08-26 06:40:22'),
(79, 1, 75, 'رياض عبدالله سالم المالكي', '543453461', 'reyadh@mail.net.sa', 1, '2021-08-26 06:41:21', '2021-08-26 06:41:21'),
(80, 1, 66, 'نوره محمد المحيميد', '504960484', 'lyan5559@gmail.com', 1, '2021-08-26 14:33:47', '2021-08-26 14:33:47'),
(81, 1, 66, 'Sharefah', '500530013', 'sharefah.kh@gmail.com', 1, '2021-08-26 19:22:14', '2021-08-26 19:22:14'),
(82, 1, 66, 'سمية محمد', '504257929', 'sumaya.m41@gmail.com', 1, '2021-08-26 21:30:20', '2021-08-26 21:30:20'),
(83, 1, 66, 'نوال', '595555928', 'nawal5928@gmail.com', 2, '2021-08-27 07:56:23', '2021-08-28 22:01:42'),
(84, 1, 66, 'امل محمد', '542396267', 'amal.m85@icluod.com', 1, '2021-08-27 10:12:21', '2021-08-27 10:12:21'),
(85, 1, 66, 'ام نايف', '530088873', 'najla_1_1@hotmail.com', 1, '2021-08-28 16:20:51', '2021-08-28 16:20:51'),
(86, 1, 66, 'هلا سعود', '548666691', 'hallloo23@live.com', 1, '2021-08-28 19:12:06', '2021-08-28 19:12:06'),
(87, 1, 66, 'Ahlam', '541469944', 'hlmi@windowslive.com', 1, '2021-08-28 20:33:29', '2021-08-28 20:33:29'),
(88, 1, 66, 'Asma Alsahil', '568944450', 'alsahil.aaa@gmail.com', 1, '2021-08-28 21:14:35', '2021-08-28 21:14:35'),
(89, 1, 66, 'ابتسام ابراهيم', '546464441', 'ebenmejali@gmail.com', 1, '2021-08-28 22:48:25', '2021-08-28 22:48:25'),
(90, 1, 76, 'عبدالله حسن البعسي', '568787877', 'abdullah@alboasi.com', 1, '2021-08-29 00:43:40', '2021-08-29 00:43:40'),
(91, 1, 76, 'بشاير المديفر', '504499930', 'bashayer@ialsharidah.com', 2, '2021-08-29 02:39:38', '2021-09-01 01:26:10'),
(92, 1, 66, 'عائشة', '534691173', 'ayshh969@gmail.com', 1, '2021-08-29 03:18:03', '2021-08-29 03:18:03'),
(93, 1, 66, 'هند سلمان', '503487606', 'rose28dnh@gmail.com', 1, '2021-08-29 04:37:02', '2021-08-29 04:37:02'),
(94, 1, 66, 'Budoor', '507473020', 'bebo.ms1988@gmail.com', 1, '2021-08-29 04:42:56', '2021-08-29 04:42:56'),
(95, 1, 66, 'ام اروى', '554467880', 'Faten.mih@gmail.com', 1, '2021-08-29 09:14:58', '2021-08-29 09:14:58'),
(96, 1, 76, 'رانيا', '559199302', 'rania.od.20122@outlook.sa', 1, '2021-08-30 03:32:27', '2021-08-30 03:32:27'),
(97, 1, 76, 'بندر منصور اليحيى', '502682692', 'b.alyahya1985@gmail.com', 1, '2021-08-30 19:54:09', '2021-08-30 19:54:09'),
(98, 1, 76, 'لمياء صويلح', '558467912', 'lamyaaswelih@gmail.com', 1, '2021-08-30 22:42:01', '2021-08-30 22:42:01'),
(99, 1, 68, 'بناء المرزوقي', '٠٥٥٤٧٧٥١١٧', 'wx33@live.com', 1, '2021-09-01 00:34:55', '2021-09-01 00:34:55'),
(100, 1, 64, 'تات', '', '00000000000@xn--jgbjbtsl4ikr', 1, '2021-09-01 01:38:52', '2021-09-01 01:38:52'),
(101, 1, 69, '.0.0', '.0.', 'jbhj@gmail.com', 1, '2021-09-01 01:48:46', '2021-09-01 01:48:46'),
(102, 1, 68, 'سر التجلي', '121210', 'hbvjhvhjv@gmail.com', 1, '2021-09-01 01:56:23', '2021-09-01 01:56:23'),
(103, 1, 69, 'Eman', '59563998', 'e.almasoud@yahoo.com', 1, '2021-09-01 02:10:35', '2021-09-01 02:10:35'),
(104, 1, 65, 'حنان حميد محمد الصبحي', '506092978', 'hananhareth1435@gmail.com', 1, '2021-09-01 02:20:16', '2021-09-01 02:20:16'),
(105, 1, 65, '.0.', '12', 'AMRGMC@GMAIL.COM', 1, '2021-09-01 03:23:02', '2021-09-01 03:23:02'),
(106, 1, 65, 'مقبول العلياني', '551344939', 'maqbul.g1@gmail.com', 1, '2021-09-01 03:27:40', '2021-09-01 03:27:40'),
(107, 1, 69, 'Areej', '537776049', 'areejk908@gmail.com', 1, '2021-09-01 04:03:40', '2021-09-01 04:03:40'),
(108, 1, 69, 'Areej', '537776049', 'areejk908@gmail.com', 1, '2021-09-01 04:03:43', '2021-09-01 04:03:43'),
(109, 1, 69, 'صفية', '503314843', 'sa-fy3@hotmail.com', 1, '2021-09-01 04:20:04', '2021-09-01 04:20:04'),
(110, 1, 69, 'منال', '555401509', 'manal.msk@gmail.com', 1, '2021-09-01 06:24:20', '2021-09-01 06:24:20'),
(111, 1, 65, 'Ibrahim Alhossein alassiri', '533553233', 'ibrahim1alhossein@gmail.com', 1, '2021-09-01 06:36:41', '2021-09-01 06:36:41'),
(112, 1, 65, 'عبدالله عادل الأمين', '568333011', 'amr.alameen@hotmail.com', 1, '2021-09-01 08:02:38', '2021-09-01 08:02:38'),
(113, 1, 65, 'بدر عبدالله السنيدي', '502443755', 'b.alsnaidi@yahoo.com', 1, '2021-09-01 09:41:27', '2021-09-01 09:41:27'),
(114, 1, 64, 'عبدالله بن إبراهيم المحارب', '٠٥٠٥٢٠٣٦٠٦', 'abm606@gmail.com', 1, '2021-09-01 11:05:46', '2021-09-01 11:05:46'),
(115, 1, 65, 'فهد فرج الأحمدي', '505327884', 'alahmadi01@gmail.com', 1, '2021-09-01 13:17:49', '2021-09-01 13:17:49'),
(116, 1, 64, 'حسن ابراهيم علي عسيري', '566634583', 'he14041@hotmail.com', 1, '2021-09-01 21:06:38', '2021-09-01 21:06:38'),
(117, 1, 76, 'عبير حمد المطيري', '554215673', 'adv_web@outlook.sa', 1, '2021-09-01 22:21:35', '2021-09-01 22:21:35'),
(118, 1, 76, 'أمل عبدالعزيز رشيد', '533551578', 'iamal.abdulaziz@gmail.com', 1, '2021-09-01 22:47:32', '2021-09-01 22:47:32'),
(119, 1, 69, 'خلود', '٠٥٠٦٠٩٠٠٧١', 'k.alhammad@veloute.com.sa', 1, '2021-09-02 03:48:29', '2021-09-02 03:48:29'),
(120, 1, 76, 'Nasser M Alammar', '599010688', 'nasser61234@gmail.com', 1, '2021-09-02 06:57:56', '2021-09-02 06:57:56'),
(121, 1, 69, 'ديما حمد المشيطي', '556166611', 'ideema90m@hotmail.com', 1, '2021-09-02 07:25:53', '2021-09-02 07:25:53'),
(122, 1, 65, 'صلاح سالم الجهني', '502188988', 'salahaljohani88@gmail.com', 1, '2021-09-02 07:33:45', '2021-09-02 07:33:45'),
(123, 1, 68, 'شروق عبدالله بن عبدالعزيز الجريسي', '554473880', 'iam__shoro8@hotmail.com', 1, '2021-09-02 21:27:00', '2021-09-02 21:27:00'),
(124, 1, 76, 'عبدالعزيز القحطاني', '551644751', 'az_az177@hotmail.com', 1, '2021-09-03 08:20:03', '2021-09-03 08:20:03'),
(125, 1, 76, 'طارق حامد العمري', '٠٥٤٤٠٢٢٠٠١', 'aaa77bbb@hotmail.com', 1, '2021-09-03 14:06:15', '2021-09-03 14:06:15'),
(126, 1, 69, 'أم مشعل', '503198326', 'memagh013@gmail.com', 1, '2021-09-03 19:14:59', '2021-09-03 19:14:59'),
(127, 1, 76, 'حنين', '542288005', 'haneensaleh1993@gmail.com', 1, '2021-09-03 19:19:50', '2021-09-03 19:19:50'),
(128, 1, 76, 'حنان خلف الذبياني', '553182778', 'hananalthebyani@hotmail.com', 1, '2021-09-03 21:21:31', '2021-09-03 21:21:31'),
(129, 1, 69, 'Reem', '971423582309', 'reem_t_k@hotmail.com', 1, '2021-09-04 00:09:04', '2021-09-04 00:09:04'),
(130, 1, 69, 'سمية الثبيتي', '567386869', 'somaya91@outlook.sa', 1, '2021-09-04 03:26:35', '2021-09-04 03:26:35'),
(131, 1, 68, 'شهد', '544447440', 'shahad.oga@gmail.com', 1, '2021-09-04 03:45:24', '2021-09-04 03:45:24'),
(132, 1, 68, 'ايمان سندي', '545000762', 'ehsindi@gmail.com', 1, '2021-09-04 08:55:27', '2021-09-04 08:55:27'),
(133, 1, 68, 'عايشة العتيبي', '558182947', 'ayysh222@gmail.com', 1, '2021-09-04 21:57:34', '2021-09-04 21:57:34'),
(134, 1, 69, 'ذكرى الشريف', '543414649', 'U5l@outlook.sa', 1, '2021-09-04 22:17:36', '2021-09-04 22:17:36'),
(135, 1, 68, 'فاطمه المرشد', '555079378', 'hasa357@icloud.com', 1, '2021-09-04 22:35:00', '2021-09-04 22:35:00'),
(136, 1, 69, 'Hessa', '553503206', 'Alota.h@outlook.com', 1, '2021-09-04 23:04:14', '2021-09-04 23:04:14'),
(137, 1, 68, 'نوره القحطاني', '551143303', 't_nooora@hotmail.com', 1, '2021-09-04 23:10:06', '2021-09-04 23:10:06'),
(138, 1, 69, 'نوره القحطاني', '551143303', 't_nooora@hotmail.com', 1, '2021-09-04 23:15:41', '2021-09-04 23:15:41'),
(139, 1, 65, 'فهد محمد صبار الشمري', '535888765', 'fhkh123123@gmail.com', 1, '2021-09-05 03:26:19', '2021-09-05 03:26:19'),
(140, 1, 68, 'فاطمه الشمري', '555165782', 'Fatimaalghaleb@icloud.com', 1, '2021-09-05 05:43:37', '2021-09-05 05:43:37'),
(141, 1, 69, 'روابي', '592130679', 'rralsayer@gmail.com', 1, '2021-09-05 06:43:24', '2021-09-05 06:43:24'),
(142, 1, 68, 'Afra', '+966558111116', 'afraalsammahi@gmail.com', 1, '2021-09-05 07:05:12', '2021-09-05 07:05:12'),
(143, 1, 68, 'سيف', 'لللل', 'seifatef2050@gmail.com', 1, '2021-09-06 02:13:33', '2021-09-06 02:13:33'),
(144, 1, 69, 'دلال الجعيثن', '563505881', 'almooog32@gmail.com', 1, '2021-09-06 05:12:57', '2021-09-06 05:12:57'),
(145, 1, 69, 'لما محمد', '568657771', 'Lama.mohammed.93@gmail.com', 1, '2021-09-06 16:49:00', '2021-09-06 16:49:00'),
(146, 1, 69, '313', '54', 'amrgmc@gmail.com', 1, '2021-09-06 17:38:40', '2021-09-06 17:38:40'),
(147, 1, 68, '35', '', 'amrgmc@gmail.com', 1, '2021-09-06 17:39:23', '2021-09-06 17:39:23'),
(148, 1, 69, 'Nouf Abdullah Alayed', '563745553', 'noufi9090@gmail.com', 1, '2021-09-06 18:55:59', '2021-09-06 18:55:59'),
(149, 1, 69, 'هيفاء الشثري', '533377703', 'haifash3@gmail.com', 1, '2021-09-07 01:15:51', '2021-09-07 01:15:51'),
(150, 1, 69, 'سمر', '546602466', 'samar.khudair@hotmail.com', 1, '2021-09-07 14:09:54', '2021-09-07 14:09:54'),
(151, 1, 70, 'فرحان العنزي', '+966502554588', 'farhan.alaiban@hotmail.com', 1, '2021-09-09 02:33:44', '2021-09-09 02:33:44'),
(152, 1, 70, 'Ammar ajaj', '568056687', 'ajajammar@gmail.com', 1, '2021-09-09 02:52:51', '2021-09-09 02:52:51'),
(153, 1, 70, 'مشاري', '508577504', 'Ksa778727@gmail.com', 1, '2021-09-09 04:46:48', '2021-09-09 04:46:48'),
(154, 1, 70, 'عبدالرحمن بن سليمان بن صالح اليحياء', '506154151', 'assy2020@gmail.com', 1, '2021-09-09 04:50:21', '2021-09-09 04:50:21'),
(155, 1, 70, 'سندس اليحيى', '539536333', 'sundosalyahya@hotmail.com', 1, '2021-09-09 05:13:08', '2021-09-09 05:13:08'),
(156, 1, 70, 'عبدالله بن محمد الظاهري', '506152620', 'azoz330@gmail.com', 1, '2021-09-09 06:30:22', '2021-09-09 06:30:22'),
(157, 1, 70, 'موسى حسيكان', '580424448', 'mo_77_sa@hotmail.com', 1, '2021-09-09 06:44:47', '2021-09-09 06:44:47'),
(158, 1, 70, 'محمد شمس الدين محمد', '552688139', 'shamsmahi2@gmail.com', 1, '2021-09-09 07:07:47', '2021-09-09 07:07:47'),
(159, 1, 70, 'عبدالله علي عبدالله الحربي', '555946848', 'abdullah68480@yahoo.com', 1, '2021-09-09 07:11:15', '2021-09-09 07:11:15'),
(160, 1, 70, 'ليلى محمد الأسمري', '504872967', 'al.as.mry@hotmail.com', 1, '2021-09-09 07:57:26', '2021-09-09 07:57:26'),
(161, 1, 70, 'ليلى محمد الأسمري', '504872967', 'al.as.mry@hotmail.com', 1, '2021-09-09 07:58:41', '2021-09-09 07:58:41'),
(162, 1, 70, 'احمد البلوي', '568248916', 'ahmadalbalawi30@gmail', 1, '2021-09-09 08:39:31', '2021-09-09 08:39:31'),
(163, 1, 70, 'خلود الميزاني', '530946291', 'khookhawod@icloud.com', 1, '2021-09-09 09:19:26', '2021-09-09 09:19:26'),
(164, 1, 70, 'علي عبدالعزيز سعود', '\"٥٩٩٥٥٥٦٨٦', 'ali9070g@gmail.com', 1, '2021-09-09 11:06:36', '2021-09-09 11:06:36'),
(165, 1, 70, 'خالد البلوي', '506102103', 'alwabssi@hotmail.com', 1, '2021-09-09 11:44:22', '2021-09-09 11:44:22'),
(166, 1, 70, 'عائض عبدالله عسيري', '555754571', 'waleedhussam2018@gmail.com', 1, '2021-09-09 11:55:20', '2021-09-09 11:55:20'),
(167, 1, 70, 'محمد أحمد شبيلي ضامري', '554733595', 'mm2hh201229@gmail.com', 1, '2021-09-09 12:17:20', '2021-09-09 12:17:20'),
(168, 1, 70, 'نوف عيد شباب العتيبي', '500484901', 'otb_ne@hotmail.com', 1, '2021-09-09 13:57:21', '2021-09-09 13:57:21'),
(169, 1, 70, 'زياد ماطر رشيد العتيبي', '594747888', 'ziyad.otaibi@gmail.com', 1, '2021-09-09 14:41:27', '2021-09-09 14:41:27'),
(170, 1, 70, 'مسفر بن يحيى القحطاني', '535251511', 'misfer33333@gmail.com', 1, '2021-09-09 14:42:44', '2021-09-09 14:42:44'),
(171, 1, 70, 'سلطان محمد البشري', '542139139', 'o542139139@gmail.com', 1, '2021-09-09 16:10:56', '2021-09-09 16:10:56'),
(172, 1, 70, 'نايل ملفي المطيري', '550414945', 'Nail.m.almutairi@gmail.com', 1, '2021-09-09 16:50:44', '2021-09-09 16:50:44'),
(173, 1, 70, 'صالح ناصر الظاهري', '505291441', 'Saleh.AlZahery@icpalaces.com', 1, '2021-09-09 17:35:39', '2021-09-09 17:35:39'),
(174, 1, 70, 'ابراهيم بن علي الحاتم', '537141212', 'ibra.ali100@gmail.com', 1, '2021-09-09 18:45:59', '2021-09-09 18:45:59'),
(175, 1, 70, 'مريم محمد الشهري', '566762227', 'alshehrimariam84@gmail.com', 1, '2021-09-09 20:14:07', '2021-09-09 20:14:07'),
(176, 1, 70, 'محمد القويفل', '531269126', 'hmlhml2022@hotmail.com', 1, '2021-09-09 20:15:32', '2021-09-09 20:15:32'),
(177, 1, 70, 'عبير الجهني', '545226210', 'abeer_aj@live.com', 1, '2021-09-09 21:06:44', '2021-09-09 21:06:44'),
(178, 1, 70, 'سلطان بن ثاني بن دوشان العنزي', '530333730', 'shlat2014@gmail.com', 1, '2021-09-09 21:59:24', '2021-09-09 21:59:24'),
(179, 1, 70, 'ابرار عبدالله سالم بن عبيدالله', '541312108', 'abrar.abdullah1992@gmail.com', 1, '2021-09-09 23:58:30', '2021-09-09 23:58:30'),
(180, 1, 70, 'وريف فيصل السليماني', '545025721', 'wareeffaisal@outlook.com', 1, '2021-09-10 04:24:59', '2021-09-10 04:24:59'),
(181, 1, 70, 'عبدالله ابراهيم الداود', '590199910', 'a_aldaowd@hotmail.com', 1, '2021-09-10 07:32:01', '2021-09-10 07:32:01'),
(182, 1, 70, 'حسين سالم الفيفي', '591060106', 'hakasm@gmail.com', 1, '2021-09-10 07:38:00', '2021-09-10 07:38:00'),
(183, 1, 70, 'Saleh Al Ghamdi', '544443225', 'saleh@sunset-beach.com.sa', 1, '2021-09-10 08:47:19', '2021-09-10 08:47:19'),
(184, 1, 70, 'مشاري محمد الشهيل', '583423338', 'mishari.alshohail@hotmail.com', 1, '2021-09-10 09:11:54', '2021-09-10 09:11:54'),
(185, 1, 70, 'سامي العازمي', '555052469', 'alazmisami11@gmail.com', 1, '2021-09-10 14:30:30', '2021-09-10 14:30:30'),
(186, 1, 70, 'محمد رائد السعدون', '563129180', 'moohamdalsadoon@yahoo.com', 1, '2021-09-10 15:56:38', '2021-09-10 15:56:38'),
(187, 1, 70, 'محمد بن عبدالرحمن علي الشهري', '569068223', 'ma951m@gmail.com', 1, '2021-09-10 15:59:39', '2021-09-10 15:59:39'),
(188, 1, 70, 'محمد بن عبدالرحمن علي الشهري', '569068223', 'ma951m@gmail.com', 1, '2021-09-10 15:59:40', '2021-09-10 15:59:40'),
(189, 1, 70, 'Afrah Abdulaziz', '569924118', 'afrah5669@gmail.com', 1, '2021-09-10 17:00:17', '2021-09-10 17:00:17'),
(190, 1, 70, 'اسامه يحيى محمد نمازي', '533766333', 'osamanmazi@gmail.com', 1, '2021-09-10 17:27:05', '2021-09-10 17:27:05'),
(191, 1, 70, 'Abdulrahman Batterjee', '555356378', 'batterjee.a@gmail.com', 1, '2021-09-10 17:46:23', '2021-09-10 17:46:23'),
(192, 1, 70, 'نورة سالم محمد السبيعي', '500691828', 'm.n00r@live.com', 1, '2021-09-10 18:28:38', '2021-09-10 18:28:38'),
(193, 1, 70, 'زياد سليمان العليقي', '595229113', 'zied-9113@hotmail.com', 1, '2021-09-10 22:01:40', '2021-09-10 22:01:40'),
(194, 1, 69, 'Layla', '530601477', 'layla.it@hotmail.com', 1, '2021-09-11 17:31:45', '2021-09-11 17:31:45'),
(195, 1, 70, 'سعود سلطان سعود العنزي', '557387874', 'sultan_sra@hotmail.com', 1, '2021-09-13 19:55:10', '2021-09-13 19:55:10');

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE `trainers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `about` longtext COLLATE utf8mb4_unicode_ci,
  `experience` longtext COLLATE utf8mb4_unicode_ci,
  `qualification` longtext COLLATE utf8mb4_unicode_ci,
  `achievements` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trainers`
--

INSERT INTO `trainers` (`id`, `name`, `current_position`, `user_id`, `about`, `experience`, `qualification`, `achievements`, `created_at`, `updated_at`) VALUES
(4, 'الدكتور فهد المنصور', 'استشاري الطب النفسي.', 10, 'مدرب رائع', '<ul style=\"border: 0px; font-size: 29px; margin-right: 3em; margin-bottom: 1.5em; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; list-style-position: initial; list-style-image: initial; color: rgb(84, 84, 84); text-align: start;\"><li style=\"border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\"><span style=\"border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">استشاري الطب النفسي.</span></li></ul>', 'اختبار', '<ul style=\"border: 0px; font-size: 29px; margin-right: 3em; margin-bottom: 1.5em; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; list-style-position: initial; list-style-image: initial; color: rgb(84, 84, 84); text-align: start;\"><li style=\"border: 0px; font-style: inherit; font-weight: inherit; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\"><span style=\"border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline;\">استشاري الطب النفسي.</span></li></ul>', '2021-02-13 14:36:47', '2021-03-03 21:26:03'),
(5, 'أسامة الجامع', 'اخصائي اول ومعالج نفسي', 21, 'اخصائي ومعالج نفسي سعودي', NULL, '<ul><li><span style=\"color: rgb(51, 51, 51); font-family: GEDinarOne-Medium; font-size: 13px;\">ماجستير علم نفس اكلينيكي من كلية الطب</span></li><li><span style=\"color: rgb(51, 51, 51); font-family: GEDinarOne-Medium; font-size: 13px;\">جامعة الإمام عبدالرحمن بن فيصل، ماجستير</span></li><li><span style=\"color: rgb(51, 51, 51); font-family: GEDinarOne-Medium; font-size: 13px;\">جودة صحية من الكلية الملكية الإيرلندية</span></li><li><span style=\"color: rgb(51, 51, 51); font-family: GEDinarOne-Medium; font-size: 13px;\">للجراحين RCSI، أخصائي أول ومعالج نفسي.<br></span><br></li></ul>', NULL, '2021-05-23 20:58:02', '2021-05-23 20:58:02'),
(6, 'الجوهرة المهنا', 'مدربة معتمدة', 24, '<ul><li><li>مدربة معتمد</li></li></ul>', NULL, '<ul><li><font size=\"3\">ماجستير إدارة التسويق جامعة اكسفورد 2015م</font></li><li><font size=\"3\">بكالوريوس اقتصاد ممنزلي جامعة الأميرة نورة 2009م<br></font></li></ul><p><br></p>', NULL, '2021-05-23 21:20:43', '2021-05-23 21:20:43'),
(7, 'بشير الرشيدي', 'استشاري في التنمية البشرية ، أستاذ دكتور في جامعة الكويت سابقاً', 25, '<ul><li><span style=\"color: rgb(33, 37, 41); font-family: tadarab-ar-rg, serif; font-size: 16px;\">مؤلف اكثر من 40 كتاب في علم النفس و بناء الذات و تنمية الذات</span></li></ul>', NULL, '<ul><li>&nbsp;دكتوراة في علم النفس التربوي من جامعة ولاية أوهايو بأمريكا<br></li></ul>', NULL, '2021-05-23 21:26:47', '2021-05-23 21:26:47'),
(8, 'تركي التركي', 'مدرب محترف', 26, '<ul><li><span style=\"color: rgb(59, 58, 58); font-family: wfont_25005c_367aee8b9a5f49398bf41a72bd90889b, wf_367aee8b9a5f49398bf41a72b, orig_ge_ss_text_medium_medium; font-size: 17px;\">مدير مشاريع محترف من معهد إدارة المشاريع</span></li><li><font color=\"#3b3a3a\"><span style=\"font-size: 17px;\">مدرب محترف معتمد من المركز الكندي للتنمية البشرية.</span></font><br></li><li>التدريب والاستشارات في المجالات التالية: الهيكلة والتنظيم والتخطيط الاستراتيجي وقياس ومؤشرات الأداء وإدارة المشاريع وإنشاء وتشغيل وتطوير مكتب إدارة المشاريع (PMO) وإدارة التغيير.​<br></li></ul>', NULL, '<ul><li>بكالريوس هندسة كهربائية مع مرتبة الشرف</li></ul>', NULL, '2021-05-23 21:53:43', '2021-05-23 21:53:43'),
(9, 'حسن الجوني', 'مشرف تدريب', 27, '<ul><li style=\"padding: 0px; margin: 0px 0px 5px; outline: none; list-style: outside none disc; border: 0px none;\">تدريب المدربين</li><li style=\"padding: 0px; margin: 0px 0px 5px; outline: none; list-style: outside none disc; border: 0px none;\">التمكين المالي</li><li style=\"padding: 0px; margin: 0px 0px 5px; outline: none; list-style: outside none disc; border: 0px none;\">دورة إدارة الأزمات&nbsp;</li><li style=\"padding: 0px; margin: 0px 0px 5px; outline: none; list-style: outside none disc; border: 0px none;\">مهارات التأثير والإقناع&nbsp;</li><li style=\"padding: 0px; margin: 0px 0px 5px; outline: none; list-style: outside none disc; border: 0px none;\">منظومة القيم والقناعات<br></li></ul>', NULL, NULL, NULL, '2021-05-23 22:00:11', '2021-05-23 22:00:11'),
(10, 'خليفه المحرزي', 'رئيس مجلس إدارة دار الافكار للاستشارات وتنظيم المؤتمرات', 28, '<ul><li><span style=\"color: rgb(33, 37, 41); font-family: Tajawal, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, &quot;Noto Sans&quot;, sans-serif, &quot;Apple Color Emoji&quot;, &quot;Segoe UI Emoji&quot;, &quot;Segoe UI Symbol&quot;, &quot;Noto Color Emoji&quot;; font-size: 16px; text-align: justify;\">شهادة خبير جودة معتمد بدرجة الامتياز</span></li><li><span style=\"text-align: justify;\"><font color=\"#212529\"><span style=\"font-size: 16px;\">شهادة خبير جوده معتمد</span></font></span></li><li>المستشار العام ومقيم معتمد لجائزة ام الامارات الشيخة فاطمة بنت مبارك للام المثالية&nbsp;</li><li><span style=\"text-align: justify;\">مستشار اسري في العلاقات الأسرية<br></span><br></li></ul>', NULL, NULL, NULL, '2021-05-23 22:07:51', '2021-05-23 22:07:51'),
(11, 'سلطان العثيم', 'مدرب ومستشار معتمد في التنمية البشرية وتطوير الذات CCT', 29, '<ul><li><li>كاتب ومشرف صفحة جدد حياتك في صحيفة العرب القطرية</li><li><li>عضو الجمعية الامريكية للتدريب والتطوير ASTD</li><li><li>عضو برنامج رعاية الموهوبين في نادي القصيم الادبي</li></li></li></li></ul>', NULL, '<ul><li><li>ماجستير ادراة اعمال تنفيذية EMBA .&nbsp;<br></li></li></ul>', NULL, '2021-05-23 22:41:17', '2021-05-23 22:41:17'),
(12, 'صالح الطويان', 'مُدَرِّب فِي الوَعي الاجتماعي والإنساني', 30, '<div><br></div>', NULL, NULL, NULL, '2021-05-23 22:53:45', '2021-05-23 22:53:45'),
(13, 'عايض العمري', 'رئيس المجلس السعودي للجودة', 31, NULL, NULL, NULL, NULL, '2021-05-23 22:57:14', '2021-05-23 22:57:14'),
(14, 'عبدالرحمن عريف', 'رئيس الخدمات المسانده للموارد البشريه', 32, '<ul><li>مدير الشؤون الإداريه والتنسيق</li><li>مدير شعبه برامج التدريب&nbsp;</li><li>مطور مناهج</li><li>أخصائي تدريب</li><li>محلل الأنظمه المسانده</li><li>باحث في القوى العامله</li><li>كبير محللي القوى العامله</li><li>محلل القوى العامله<br></li></ul>', NULL, NULL, NULL, '2021-05-23 23:01:48', '2021-05-23 23:01:48'),
(15, 'عبدالله الفلاح', '.', 33, NULL, NULL, NULL, NULL, '2021-05-23 23:07:16', '2021-05-23 23:07:16'),
(16, 'عبدالله القاسم', 'حاصل على رخصة مدرب معتمد رسمياً من المؤسسة العامة للتدريب التقني والمهني', 34, NULL, NULL, NULL, NULL, '2021-05-23 23:09:27', '2021-05-23 23:09:27'),
(17, 'فهد العضياني', '.', 35, NULL, NULL, NULL, NULL, '2021-05-23 23:11:58', '2021-05-23 23:11:58'),
(18, 'فهد المنصور', 'حاصل على بكالوريوس طب وجراحة من جامعة الملك سعود', 36, '<ul><li>حاصل على افضل طبيب زمالة من جامعة الملك سعود لعام 2008</li></ul>', NULL, NULL, NULL, '2021-05-23 23:24:07', '2021-05-23 23:24:07'),
(19, 'ماجد العوشن', '.', 37, NULL, NULL, NULL, NULL, '2021-05-23 23:27:08', '2021-05-23 23:27:08'),
(20, 'ماجد بن جعفر الغامدي', '.', 38, NULL, NULL, NULL, NULL, '2021-05-23 23:30:44', '2021-05-23 23:30:44'),
(21, 'محمد البنا', 'مدرب في مجال الوعي و الحياة', 39, NULL, NULL, NULL, NULL, '2021-05-23 23:32:42', '2021-05-23 23:32:42'),
(22, 'محمد البنا', 'خبير نظم الجودة', 40, '<ul><li><strong style=\"box-sizing: inherit; font-weight: bold; color: rgb(68, 68, 68); font-family: Almarai, sans-serif; font-size: 15px;\">ماجستير إدارة الجودة -نظام إدارة الجودة</strong></li><li style=\"box-sizing: inherit;\"><strong style=\"box-sizing: inherit; font-weight: bold;\">ماجستير هندسة وتكنولوجيا طباعة المنسوجات -جامعة حلوان</strong></li></ul>', NULL, NULL, NULL, '2021-05-23 23:35:17', '2021-05-23 23:35:17'),
(23, 'محمد المغيصيب', 'رئيس لجنةالمطاعم وعضوالسياحية بالغرفةالتجاريةسابق', 41, NULL, NULL, NULL, NULL, '2021-05-23 23:37:56', '2021-05-23 23:37:56'),
(24, 'مرزوق الفهمي', 'مدرب معتمد', 42, '<ul><li>&nbsp;دكتوراة تخصص الإدارة والتخطيط من جامعة ام القرى</li><li>&nbsp;ماجستير الإدارة والتخطيط من جامعة أم القرى</li><li><li>&nbsp;بكالوريوس في الحاسب الآلي</li></li></ul>', NULL, NULL, NULL, '2021-05-23 23:40:17', '2021-05-23 23:40:17'),
(25, 'مشاعل البراهيم', 'مدرب معتمد', 43, '<ul><li style=\"margin: 0px; padding: 0px; border: 0px; font-weight: inherit; font-style: inherit; font-size: 16px; font-family: inherit; vertical-align: baseline; direction: rtl;\">لايف كوتش معتمد لايف ماستر_لندن&nbsp;</li><li style=\"margin: 0px; padding: 0px; border: 0px; font-weight: inherit; font-style: inherit; font-size: 16px; font-family: inherit; vertical-align: baseline; direction: rtl;\">لايف كوتش معتمد من المؤسسة العامة لتدريب التقني والمهني</li><li style=\"margin: 0px; padding: 0px; border: 0px; font-weight: inherit; font-style: inherit; font-size: 16px; font-family: inherit; vertical-align: baseline; direction: rtl;\">ماستر في علم الطاقة</li><li style=\"margin: 0px; padding: 0px; border: 0px; font-weight: inherit; font-style: inherit; font-size: 16px; font-family: inherit; vertical-align: baseline; direction: rtl;\">ممارس معتمد في مجموعة من العلوم الإنسانية</li><li style=\"margin: 0px; padding: 0px; border: 0px; font-weight: inherit; font-style: inherit; font-size: 16px; font-family: inherit; vertical-align: baseline; direction: rtl;\">باحثه في عدة مناهج مختلفة للوعي الإنساني&nbsp;</li><li><li>باحثة في&nbsp; الفلسفة الروسية للوعي الإنساني</li></li></ul>', NULL, NULL, NULL, '2021-05-23 23:50:34', '2021-05-23 23:50:34'),
(26, 'مصطفى أبو سعد', '- مدرب معتمد عالميًّا في البرمجة العصبية اللغوية: NLP', 44, '<ul><li><p dir=\"rtl\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(51, 51, 51); font-family: GEDinarOne-Medium; font-size: 13px;\">- مشرف على إعداد ملفات تربوية وأسرية في العديد من المجلات:</p></li><li><div><p dir=\"rtl\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(51, 51, 51); font-family: GEDinarOne-Medium; font-size: 13px;\">- مدير تحرير نشرة \"إشراقة\" التربوية التي يصدرها صندوق إعانة المرضى (متخصصة في القضايا التربوية 10 أعداد).</p></div></li><li><p dir=\"rtl\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px;\"><font color=\"#333333\"><span style=\"font-size: 13px;\">استشاري تربوي بعدد من المجلات بالكويت</span></font></p><p dir=\"rtl\" style=\"margin-right: 0px; margin-bottom: 10px; margin-left: 0px; color: rgb(51, 51, 51); font-family: GEDinarOne-Medium; font-size: 13px;\"><br></p></li></ul>', NULL, '<p><span style=\"color: rgb(51, 51, 51); font-family: GEDinarOne-Medium; font-size: 13px;\">دكتوراة في علم النفس التربوي</span><br></p>', NULL, '2021-05-23 23:54:31', '2021-05-23 23:54:31'),
(27, 'مها القحطاني', 'مدربة تنمية بشرية معتمدة', 45, NULL, NULL, NULL, NULL, '2021-05-23 23:56:26', '2021-05-23 23:56:26'),
(28, 'ناصر العباس', 'مدرب مهاري وتقني', 46, '<ul><li><span style=\"color: rgb(20, 23, 26); font-family: tahoma, arial, sans-serif; background-color: rgb(230, 236, 240);\">صانع محتوى</span><br></li></ul>', NULL, '<ul><li><span style=\"color: rgb(20, 23, 26); font-family: tahoma, arial, sans-serif; background-color: rgb(230, 236, 240);\">ماجستير في التأهيل والرعاية الإجتماعية،</span><br></li></ul>', NULL, '2021-05-23 23:57:54', '2021-05-23 23:57:54'),
(29, 'نشمي الحربي', 'مدرب سلاسل الإمداد', 47, '<ul><li>مؤسس منصة سلاسل الامداد واللوجستيات&nbsp;</li><li>عضو في المعهد القانوني للوجستيات والنقل في المملكة المتحدة&nbsp;</li></ul>', NULL, NULL, NULL, '2021-05-24 00:13:33', '2021-05-24 00:13:33'),
(30, 'ياسر الشلاحي', 'مدرّب في نظريات الإدارة', 48, '<ul><li>مدرّب في نظريات الإدارة</li><li>استراتيجيات الجودة</li></ul>', NULL, '<ul><li>ماجستير ادارة صحية&nbsp;</li></ul>', NULL, '2021-05-24 00:20:11', '2021-05-24 00:20:11'),
(31, 'يوسف الحسني', 'مدرب في الاتيكيت والبرتوكول الدولي', 49, '<ul><li><span style=\"color: rgb(38, 38, 38); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(250, 250, 250);\">باحث دكتوراة في العلاقات العامة</span></li><li><span style=\"color: rgb(38, 38, 38); font-family: -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, Helvetica, Arial, sans-serif; font-size: 16px; background-color: rgb(250, 250, 250);\">الممثل القانوني للحساب مكتب A.J.L.O للمحاماه والاستشارات القانونية<br></span><br></li></ul>', NULL, NULL, NULL, '2021-05-24 00:22:08', '2021-05-24 00:22:08'),
(32, 'جمال الملا', 'خبير ومدرب القراءة السريعة', 50, '<ul><li><span style=\"color: rgb(51, 51, 51); font-family: Regular; font-size: 15px; background-color: rgb(246, 246, 246);\">مخترع التذكر الرقمي</span></li><li><span style=\"color: rgb(51, 51, 51); font-family: Regular; font-size: 15px; background-color: rgb(246, 246, 246);\">حاصل على أفضل مغرد مؤثر عربي<br></span><br></li></ul>', NULL, NULL, NULL, '2021-05-24 01:29:23', '2021-05-24 01:29:23'),
(33, 'عبد الرحمن الحازمي', 'مستشار وكيل جامعة الملك عبد العزيز للتطوير', 51, NULL, NULL, '<ul><li><li>دكتوراة&nbsp;تنمية السياسة الإدارية</li></li></ul>', NULL, '2021-05-24 01:33:42', '2021-05-24 01:33:42'),
(34, 'علوي عطرجي', 'محاضر ومدرب في تطوير الذات', 52, '<ul><li>مستشار في القيادة والعلاقات العامة</li><li>كوتش شخصي لرفع&nbsp; الأداء</li><li>مؤلف كتاب فن إدارة المشاعر&nbsp;<br></li></ul>', NULL, NULL, NULL, '2021-05-24 01:36:06', '2021-05-24 01:36:06'),
(35, 'ياسر الشلاحي & عبدالله الفلاح', 'ياسر الشلاحي : مدرّب في نظريات الإدارة   &  عبدالله الفلاح :', 53, '<p>ياسر الشلاحي&nbsp;</p><ul><li>مدرّب في نظريات الإدارة</li><li>استراتيجيات الجودة</li></ul><p>&nbsp;عبدالله الفلاح<br></p>', NULL, '<p>ياسر الشلاحي&nbsp;</p><ul><li>ماجستير ادارة صحية&nbsp;</li></ul><p><span style=\"font-size: 0.875rem;\">&nbsp;عبدالله الفلاح</span><br></p>', NULL, '2021-05-26 20:05:03', '2021-05-26 20:12:12'),
(36, 'ياسر الشلاحي & عبدالله الفلاح', 'ياسر الشلاحي', 54, NULL, NULL, NULL, NULL, '2021-05-26 20:05:05', '2021-05-26 20:05:05'),
(37, 'ياسر الشلاحي & عبدالله الفلاح', 'ياسر الشلاحي', 55, NULL, NULL, NULL, NULL, '2021-05-26 20:05:08', '2021-05-26 20:05:08'),
(38, 'ياسر الشلاحي & عبدالله الفلاح', 'ياسر الشلاحي', 56, NULL, NULL, NULL, NULL, '2021-05-26 20:05:08', '2021-05-26 20:05:08'),
(39, 'عبدالله القاسم & حسن الجوني', 'مدرب معتمد', 59, NULL, NULL, NULL, NULL, '2021-07-11 19:40:54', '2021-07-11 19:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `social_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_type_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_verified` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `email_verified` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fire_base_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `userVerify` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `need_reset_password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `social_id`, `user_name`, `user_type_id`, `name`, `description`, `image`, `country_id`, `city`, `lat`, `lng`, `mobile`, `mobile_verified`, `email`, `email_verified_at`, `email_verified`, `password`, `fire_base_token`, `passCode`, `userVerify`, `need_reset_password`, `remember_token`, `api_token`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, '1', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 'admin@admin.com', NULL, '0', '$2b$10$ADTntZ6/wsAQOv4q9Lywv.075Y6q5MeKT2uKlnkt9GZoF.xdfpDU.', NULL, NULL, '1', '0', NULL, '075Y6q5MeKT2uKlnkt9GZoF', NULL, NULL),
(15, NULL, NULL, '4', 'ابراهيم القطاوي', NULL, NULL, NULL, NULL, NULL, NULL, '0506888677', '0', 'gmcsa269@gmail.com', NULL, '0', '$2y$10$UM6kgKj5cgpucc/OjTIQHuzOKxnLNTUI3DKrhv/9vM9yNRw4e/cHa', NULL, NULL, '1', '0', NULL, 'BLNIpx', '2021-05-19 22:44:59', '2021-05-19 22:44:59'),
(16, NULL, NULL, '4', 'سيف الاسلام', NULL, NULL, NULL, NULL, NULL, NULL, '0540099114', '0', 'seifatef2050@gmail.com', NULL, '0', '$2y$10$6YgnftgRSSipJQzu3LHuHecC3MSn5MF9G/OEjdds2EpnbjuQp9zb.', NULL, NULL, '1', '0', NULL, '70bQpl', '2021-05-22 20:55:15', '2021-05-22 20:55:15'),
(18, NULL, NULL, '6', 'احمد معروف', NULL, NULL, NULL, NULL, NULL, NULL, '0507617141', '0', 'ahmedgmcsa@gmail.com', NULL, '0', '$2y$10$HbY5p5ASAKlNEAbOtitj8e60FxYh3iTDDQoC8OWiiSOh8EQMXH1AK', NULL, NULL, '1', '0', NULL, 'xbf6Fn', '2021-05-22 21:08:57', '2021-05-22 21:08:57'),
(19, NULL, NULL, '5', 'عمرو الزاهد', NULL, NULL, NULL, NULL, NULL, NULL, '0532252226', '0', 'obabamr@gmail.com', NULL, '0', '$2y$10$qpsbBLvbG4bdB2Z2AJBrpesQOJ6XbjO/S9WjaLhG6kZQK1NicdnyO', NULL, NULL, '1', '0', NULL, 'YbbkkH', '2021-05-22 21:11:42', '2021-05-22 21:11:42'),
(20, NULL, NULL, '4', 'عمرو الزاهد', NULL, NULL, NULL, NULL, NULL, NULL, '0532252226', '0', 'amrgmcsa@gmail.com', NULL, '0', '$2y$10$50vaRCvaYSlY7tT2BJ3fJ.ytYznidfeSKqlNZoPrWAKfLuKGb5mnW', NULL, NULL, '1', '0', NULL, 'I8sifj', '2021-05-22 22:04:19', '2021-06-25 00:20:46'),
(21, NULL, NULL, '3', 'أسامة الجامع', NULL, 'https://gmcksa.com/storage/app/public/images/xluDeL6uBjK3y9vMVmW9HXivXlb37FQaikw33vkK.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 20:58:02', '2021-05-23 20:58:02'),
(22, NULL, NULL, '5', 'ابراهيم القطاوي', NULL, NULL, NULL, NULL, NULL, NULL, '0506888677', '0', 'gmcsa288264@gmail.com', NULL, '0', '$2y$10$HpAKnV7I9e4xgzFPiGgngeG0Ek3PdDWH/cS9XhLi7sfBrOv3//WJq', NULL, NULL, '1', '0', NULL, 'peU5CZ', '2021-05-23 21:10:00', '2021-05-23 21:10:00'),
(23, NULL, NULL, '5', 'سيف الاسلام', NULL, NULL, NULL, NULL, NULL, NULL, '0540099114', '0', 'siefatief2040@gmail.com', NULL, '0', '$2y$10$3qWh/HfW.ib4Q/HAzWB5/uyf70jcWLENuOhcI/CE6G6PdDQZ9zdvO', NULL, NULL, '1', '0', NULL, 'eHlZnc', '2021-05-23 21:16:26', '2021-05-23 21:16:26'),
(24, NULL, NULL, '3', 'الجوهرة المهنا', NULL, 'https://gmcksa.com/storage/app/public/images/sPJnKYBnmFzfrQTKPmMQak2Tiic7rkXXzk6hRWtq.png', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 21:20:43', '2021-05-23 21:20:43'),
(25, NULL, NULL, '3', 'بشير الرشيدي', NULL, 'https://gmcksa.com/storage/app/public/images/xjifRTVcEdYAL9meDFvRSvEJqh3u8vVxn1S0yzRR.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 21:26:47', '2021-05-23 21:26:47'),
(26, NULL, NULL, '3', 'تركي التركي', NULL, 'https://gmcksa.com/storage/app/public/images/MhR8jLkT5XWwHSuVSRcl7zKHZU2IAqH7w58hnMAT.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 21:53:43', '2021-05-24 01:10:17'),
(27, NULL, NULL, '3', 'حسن الجوني', NULL, 'https://gmcksa.com/storage/app/public/images/8I4vyQCOWKNKs8hc96uxEp0DSIKyawOq9Ve76MkG.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 22:00:11', '2021-05-24 01:09:50'),
(28, NULL, NULL, '3', 'خليفه المحرزي', NULL, 'https://gmcksa.com/storage/app/public/images/D7oGv4cC2mlPiixirHSRy8ZZhtcsPDmiwTy5GT9Q.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 22:07:51', '2021-05-24 01:09:31'),
(29, NULL, NULL, '3', 'سلطان العثيم', NULL, 'https://gmcksa.com/storage/app/public/images/suCwpj8eK8F3MpNSKB3TczI1FsC6HdWAvBaK2FR7.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 22:41:17', '2021-05-24 01:08:50'),
(30, NULL, NULL, '3', 'صالح الطويان', NULL, 'https://gmcksa.com/storage/app/public/images/X9td80t0Nr2QBCi6bJBvlkXDpJvDY2AwJCZH8qFa.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 22:53:45', '2021-05-24 01:08:21'),
(31, NULL, NULL, '3', 'عايض العمري', NULL, 'https://gmcksa.com/storage/app/public/images/ccvPkygD1y5X6dPs2o8BXzGzsQhfQPRbHQaWaz38.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 22:57:14', '2021-05-24 01:08:02'),
(32, NULL, NULL, '3', 'عبدالرحمن عريف', NULL, 'https://gmcksa.com/storage/app/public/images/cfDGvizusMdKu0BV0uOgMIY4scOJIuOzFC8Rb3MF.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:01:48', '2021-05-24 01:07:07'),
(33, NULL, NULL, '3', 'عبدالله الفلاح', NULL, 'https://gmcksa.com/storage/app/public/images/s9tNRWpHVakAHsHUf0IBOUKDmmFlcoI3kd509MRa.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:07:16', '2021-05-24 01:06:35'),
(34, NULL, NULL, '3', 'عبدالله القاسم', NULL, 'https://gmcksa.com/storage/app/public/images/VI85gzzslq56qsXK0lFmw3wRAEi68xHgYjPG6Ok5.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:09:27', '2021-05-24 01:05:45'),
(35, NULL, NULL, '3', 'فهد العضياني', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:11:58', '2021-05-23 23:11:58'),
(36, NULL, NULL, '3', 'فهد المنصور', NULL, 'https://gmcksa.com/storage/app/public/images/ia34X9TKrIwjTHCey9w3jHsKfxLqqFD75eAn7N5u.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:24:07', '2021-05-24 01:03:03'),
(37, NULL, NULL, '3', 'ماجد العوشن', NULL, 'https://gmcksa.com/storage/app/public/images/sJXJ0KMPIhlHKYg9KRGfp0tFzx3WVmpnPZNJManG.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:27:08', '2021-05-24 01:02:39'),
(38, NULL, NULL, '3', 'ماجد بن جعفر الغامدي', NULL, 'https://gmcksa.com/storage/app/public/images/86SeeiAbqjFV7XWVW57je8hK9jnjCwYZNRQUbX3V.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:30:44', '2021-05-24 00:53:37'),
(40, NULL, NULL, '3', 'محمد البنا', NULL, 'https://gmcksa.com/storage/app/public/images/ha0HL69VQ3Tr4fJs6Ax8Fuq81UQ79Xv1alLF14jI.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:35:17', '2021-05-24 00:51:06'),
(41, NULL, NULL, '3', 'محمد المغيصيب', NULL, 'https://gmcksa.com/storage/app/public/images/LuOzceeXg9lq05zqnU9ikV6zqS4o7dcxX4ffi8da.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:37:56', '2021-05-24 00:50:48'),
(42, NULL, NULL, '3', 'مرزوق الفهمي', NULL, 'https://gmcksa.com/storage/app/public/images/YZ4SPkhUFsM38IwnxIk5qZvwqXmBiDpS2TigWMOP.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:40:17', '2021-05-24 00:50:29'),
(43, NULL, NULL, '3', 'مشاعل البراهيم', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:50:34', '2021-05-23 23:50:34'),
(44, NULL, NULL, '3', 'مصطفى أبو سعد', NULL, 'https://gmcksa.com/storage/app/public/images/Im5lSJiyT684WMpTiCMXgWO6C0xekFVLEy8zfuDx.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:54:31', '2021-05-24 00:49:59'),
(45, NULL, NULL, '3', 'مها القحطاني', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:56:26', '2021-05-23 23:56:26'),
(46, NULL, NULL, '3', 'ناصر العباس', NULL, 'https://gmcksa.com/storage/app/public/images/fBJTlZbEX8ZMgSfMQsDY9NKzRWhJBbvtuCMZILra.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-23 23:57:54', '2021-05-24 00:49:25'),
(47, NULL, NULL, '3', 'نشمي الحربي', NULL, 'https://gmcksa.com/storage/app/public/images/QbFPlQmbSm0RlBjsdVX1fSYURBQlzXspDFIHKqIb.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-24 00:13:33', '2021-05-24 00:49:10'),
(48, NULL, NULL, '3', 'ياسر الشلاحي', NULL, 'https://gmcksa.com/storage/app/public/images/5LYXouYcZ7q3hJfjDVEc9YpgyVNK5BDnuFkAdLA9.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-24 00:20:11', '2021-05-24 00:48:45'),
(49, NULL, NULL, '3', 'يوسف الحسني', NULL, 'https://gmcksa.com/storage/app/public/images/0cMnyEWTbv4DBOZ7KgAomihUQWHz9U7Bjjb7O1VQ.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-24 00:22:08', '2021-05-24 00:48:27'),
(50, NULL, NULL, '3', 'جمال الملا', NULL, 'https://gmcksa.com/storage/app/public/images/JsQpWqoC00HbWrnr1fXI1zJIO9kXTDJ3zigCX7Bh.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-24 01:29:23', '2021-05-24 01:29:23'),
(51, NULL, NULL, '3', 'عبد الرحمن الحازمي', NULL, 'https://gmcksa.com/storage/app/public/images/XJoS207zymr4rBrCRpTsHd2j4Mk3TmBh1ObZKmBc.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-24 01:33:42', '2021-05-24 01:33:42'),
(52, NULL, NULL, '3', 'علوي عطرجي', NULL, 'https://gmcksa.com/storage/app/public/images/JfDzT7a36l7w9D2l03bzN8vpRc1dRWofOaQ3Tjy9.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-24 01:36:06', '2021-05-24 01:36:06'),
(53, NULL, NULL, '3', 'ياسر الشلاحي & عبدالله الفلاح', NULL, 'https://gmcksa.com/storage/app/public/images/SoObfvL3bQGd1Wi71ENaQMSRr494hAvcuayvBk9C.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-05-26 20:05:03', '2021-05-26 20:05:03'),
(57, NULL, NULL, '5', 'بسام', NULL, NULL, NULL, NULL, NULL, NULL, '0557111006', '0', 'bassam@gmail.com', NULL, '0', '$2y$10$6gL3Msp1Brw9NEqaHA6NueZgLqy.y57VqcA7CVreOYJ886Q9PWpzC', NULL, NULL, '1', '0', NULL, 'mOPk0l', '2021-05-27 19:39:07', '2021-05-27 19:39:07'),
(58, NULL, NULL, '4', 'بسام', NULL, NULL, NULL, NULL, NULL, NULL, '0557111006', '0', 'bassam1@gmail.com', NULL, '0', '$2y$10$7eZgtZj8BcafnqbMZMmpCO24FOBsAI/QAVbh46C5QCfNo7iSRvGDa', NULL, NULL, '1', '0', NULL, '5zZRJ3', '2021-05-27 19:40:13', '2021-05-27 19:40:13'),
(59, NULL, NULL, '3', 'عبدالله القاسم & حسن الجوني', NULL, 'https://gmcksa.com/storage/app/public/images/dhWrU600lzjwgctYM1pwu3wswKAkCq0UemJ5LPA2.jpg', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '0', NULL, NULL, NULL, '1', '0', NULL, NULL, '2021-07-11 19:40:54', '2021-07-11 19:40:54');

-- --------------------------------------------------------

--
-- Table structure for table `user_lastpasswords`
--

CREATE TABLE `user_lastpasswords` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `passCode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expired_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE `user_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `notification_id` int(10) UNSIGNED DEFAULT NULL,
  `viewed` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_notifications`
--

INSERT INTO `user_notifications` (`id`, `user_id`, `notification_id`, `viewed`) VALUES
(1, 6, 1, 0),
(2, 6, 2, 0),
(3, 9, 3, 0),
(4, 9, 4, 0),
(5, 11, 5, 0),
(6, 11, 6, 0),
(7, 14, 7, 0),
(8, 14, 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_en` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads_clicks`
--
ALTER TABLE `ads_clicks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisement_statuses`
--
ALTER TABLE `advertisement_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_uses`
--
ALTER TABLE `contact_uses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contract_courses`
--
ALTER TABLE `contract_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inforamtion_strings`
--
ALTER TABLE `inforamtion_strings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `licensed_courses`
--
ALTER TABLE `licensed_courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_types`
--
ALTER TABLE `payment_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questionnaires`
--
ALTER TABLE `questionnaires`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsored_advertisements`
--
ALTER TABLE `sponsored_advertisements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_lastpasswords`
--
ALTER TABLE `user_lastpasswords`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_notifications`
--
ALTER TABLE `user_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ads_clicks`
--
ALTER TABLE `ads_clicks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `advertisement_statuses`
--
ALTER TABLE `advertisement_statuses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact_uses`
--
ALTER TABLE `contact_uses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contract_courses`
--
ALTER TABLE `contract_courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `inforamtion_strings`
--
ALTER TABLE `inforamtion_strings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `licensed_courses`
--
ALTER TABLE `licensed_courses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `payment_types`
--
ALTER TABLE `payment_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questionnaires`
--
ALTER TABLE `questionnaires`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sponsored_advertisements`
--
ALTER TABLE `sponsored_advertisements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `user_lastpasswords`
--
ALTER TABLE `user_lastpasswords`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_notifications`
--
ALTER TABLE `user_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
