-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2021 at 10:30 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gmc_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(8, '2018_10_07_184333_create_notifications_table', 1),
(9, '2018_10_07_184351_create_user_notifications_table', 1),
(14, '2020_08_20_151602_create_ads_clicks_table', 1),
(15, '2020_08_20_151719_create_ads_table', 1),
(18, '2021_02_05_180054_create_trainers_table', 2),
(21, '2021_02_05_210253_create_courses_table', 5),
(22, '2021_02_08_134417_create_orders_table', 6),
(23, '2021_02_14_183708_create_requests_table', 7),
(24, '2021_02_14_184230_create_contract_courses_table', 7),
(25, '2021_03_10_160828_create_licensed_courses_table', 8),
(26, '2021_03_10_161932_create_categories_table', 8),
(36, '2020_10_14_132605_create_settings_table', 9),
(53, '2014_10_12_000000_create_users_table', 10),
(54, '2014_10_12_100000_create_password_resets_table', 10),
(55, '2016_06_01_000001_create_oauth_auth_codes_table', 10),
(56, '2016_06_01_000002_create_oauth_access_tokens_table', 10),
(57, '2016_06_01_000003_create_oauth_refresh_tokens_table', 10),
(58, '2016_06_01_000004_create_oauth_clients_table', 10),
(59, '2016_06_01_000005_create_oauth_personal_access_clients_table', 10),
(60, '2019_09_10_094330_create_user_lastpasswords_table', 10),
(61, '2019_09_18_195814_create_countries_table', 10),
(62, '2019_09_18_200241_create_cities_table', 10),
(63, '2020_05_10_155006_create_user_types_table', 10),
(64, '2020_10_15_072153_create_payment_types_table', 10),
(65, '2020_12_22_173800_create_contact_uses_table', 10),
(66, '2021_02_05_201627_create_sliders_table', 10),
(67, '2021_02_08_134417_create_sponsored_advertisements_table', 11),
(68, '2021_08_27_193102_create_advertisement_statuses_table', 12),
(69, '2019_12_09_165851_create_inforamtion_strings_table', 13),
(70, '2021_09_05_015511_create_questionnaires_table', 14),
(71, '2021_09_05_162710_create_questionnaire_friends_table', 14),
(72, '2021_09_05_163343_create_questionnaire_courses_table', 14),
(73, '2021_09_07_233555_create_complaints_table', 15);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
