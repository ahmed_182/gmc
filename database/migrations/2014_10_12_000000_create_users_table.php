<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateUsersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('users', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('social_id')->nullable();
                $table->string('user_name')->nullable();
                $table->string('user_type_id')->nullable();
                $table->string('name')->nullable();
                $table->longText('description')->nullable();
                $table->string('image')->nullable();
                $table->string('country_id')->nullable();
                $table->string('city')->nullable();
                $table->string('lat')->nullable();
                $table->string('lng')->nullable();
                $table->string('mobile')->nullable();
                $table->string('mobile_verified')->default(0);
                $table->string('email')->unique()->nullable();
                $table->timestamp('email_verified_at')->nullable();
                $table->string('email_verified')->default(0);
                $table->string('password')->nullable();
                $table->string('fire_base_token')->nullable();
                $table->string('passCode')->nullable();
                $table->string('userVerify')->default(\App\ModulesConst\UserVerify::no);
                $table->string('need_reset_password')->default(0);
                $table->rememberToken();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('users');
        }
    }
