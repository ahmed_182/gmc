<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSettingsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('settings', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('delivery_cost')->default(0);
                $table->string('tax')->default(0);

                //site name
                $table->string('logo')->nullable();
                $table->string('name_ar')->nullable();
                $table->string('name_en')->nullable();

                // About
                $table->longText('about_ar')->nullable();
                $table->longText('about_en')->nullable();
                //worktime
                $table->longText('text_ar')->nullable();
                $table->longText('text_en')->nullable();
                // Terms
                $table->longText('terms_ar')->nullable();
                $table->longText('terms_en')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('settings');
        }
    }
