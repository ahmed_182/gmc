<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeetingRoomsTable extends Migration
{
    public function up()
    {
        Schema::create('meeting_rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('host_id');
            $table->string('jwt');
            $table->string('api_key');
            $table->string('api_secret');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('meeting_rooms');
    }
}
