<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('trainer_id')->nullable();
            $table->string('image')->nullable();
            $table->string('name')->nullable();
            $table->string('price')->nullable();
            $table->string('date')->nullable();
            $table->string('duration')->nullable();
            $table->longText('description')->nullable();
            $table->longText('breif')->nullable();
            $table->longText('features')->nullable();
            $table->string('link')->nullable();
            $table->integer('is_special')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
