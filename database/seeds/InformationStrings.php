<?php

use App\ModulesConst\UserTyps;
use Illuminate\Database\Seeder;

class InformationStrings extends Seeder
{
    public function run()
    {
        \App\InforamtionString::query()->truncate();
        $this->add('سياسه الاستخدام', 'terms_conditions', 1); // 1
        $this->add('عقد اعتماد مكتب وكيل', 'confirm_office', 2); // 2
        $this->add('عقد طلب اعلان', 'contract_add_buying_offer', 3); // 3
        $this->add('عقد طلب شراء عقار', 'contract_to_buying_property', 4); // 4
        $this->add('عقد بيع وشراء ( عروض ومزايده )', 'contract_to_buy_and_sell_offers_and_bid', 5); // 5
        $this->add('عقد طلب بيع عقار ( عروض )', 'contract_sell_offers', 6); // 6
        $this->add('عقد طلب شراء عقار ( مزايده )', 'contract_sell_bid', 7); // 7
        $this->add('عقد طلب بيع عقار ( مزايده )', 'contract_buy_bid', 8); // 8
        $this->add('عقد اضافه اعلان في التطبيق اعلان ترويجي', 'contract_add_advs', 9); // 8

    }

    public function add($name_ar, $name_en, $string_type_id)
    {
        $data['name_ar'] = $name_ar;
        $data['name_en'] = $name_en;
        $data['string_type_id'] = $string_type_id;
        \App\InforamtionString::create($data);
    }

}
