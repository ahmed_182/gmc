<?php

use App\ModulesConst\UserTyps;
use Illuminate\Database\Seeder;

class RealStateType extends Seeder
{
    public function run()
    {
        \App\RealStateType::query()->truncate();
        $this->add('اخري', 'Other');
        $this->add('ارض', 'Land');
        $this->add('شقه', 'Flat');
        $this->add('عماره', 'Building');
        $this->add('فيلا', 'villa');
        $this->add('استراحه', 'Break');
        $this->add('مزرعه', 'Farm');
        $this->add('فندق', 'Hotel');
        $this->add('مستشفي', 'Hospital');
        $this->add('سوق', 'Market');
        $this->add('محطه بنزين', 'Gas Station');

    }

    public function add($name_ar, $name_en)
    {
        $data['name_ar'] = $name_ar;
        $data['name_en'] = $name_en;
        \App\RealStateType::create($data);
    }

}
