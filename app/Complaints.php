<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaints extends Model
{
    protected $fillable = [
        'name',
        'email',
        'title',
        'message',
        'created_at',

    ];
    public function join_from()
    {
        return $this->created_at->diffforhumans();
    }
}
