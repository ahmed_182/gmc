<?php

namespace App;

use App\ModulesConst\UserVerify;
use Illuminate\Database\Eloquent\Model;

class Sponsored_advertisements extends Model
{
    protected $fillable = [
        'user_id',
        'course_id',
        'country_id',
        'name',
        'mobile',
        'email',
        'status',
        'other',
        'paid',
    ];
    public function getDashStatusNameAttribute()
    {
        if ($this->paid == UserVerify::yes) {
            return trans("language.paid");
        } else {
            return trans("language.not_paid");
        }
    }

    public function getDashCourseAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->course)
            $attribute = $this->course->name;
        return $attribute;
    }
    public function getDashLinkAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->course)
            $attribute = $this->course->link;
        return $attribute;
    }

    public function getDashMarkterAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->markter)
            $attribute = $this->markter->name;
        return $attribute;
    }

    public function getDashCodeAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->country)
            $attribute = $this->country->code;
        return $attribute;
    }
    public function getDashStatusAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->orderstatus)
            $attribute = $this->orderstatus->DashName;
        return $attribute;
    }
    public function markter()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function course()
    {
        return $this->belongsTo('App\Course', 'course_id');
    }  public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }

    public function orderstatus()
    {
        return $this->belongsTo(AdvertisementStatus::class, 'status');
    }
}
