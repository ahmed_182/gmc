<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    protected $fillable = [
        'name',
        'current_position',
        'user_id',
        'about',
        'experience',
        'qualification',
        'achievements',

    ];

    public function getDashCurrentPositionAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->current_position)
            $attribute = $this->current_position;
        return $attribute;
    }
    public function getDashNameAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->name)
            $attribute = $this->name;
        return $attribute;
    }
    public function getDashImageAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->user)
            $attribute = $this->user->image;
        return $attribute;
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
