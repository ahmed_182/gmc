<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingRoom extends Model
{
    protected $fillable = ['name', 'jwt', 'host_id', 'api_key', 'api_secret'];
}
