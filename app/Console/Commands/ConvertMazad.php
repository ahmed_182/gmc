<?php

namespace App\Console\Commands;

use App\ModulesConst\MazadType;
use App\ModulesConst\PropertyType;
use App\Property;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ConvertMazad extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'run:ConvertMazad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get all mazad properties ( today Mazad ) ..
        $items = Property::sale()->NotSold()->where('mazad_type', MazadType::today)->where("property_type", PropertyType::mazad)->get();
        foreach ($items as $item) {
            if ($item->mazad_status() != true) {
                // its mean that this item maxad time has been expired so we need to convert it to Month mazads ..
                $data['mazad_type'] = MazadType::month;
                $month_long = 30 * 24 * 60 * 60 * 1000;
                $time = (strtotime(Carbon::now()) * 1000) + ($month_long);
                $data['mazad_end_time'] = $time;
                $item->update($data);
            }
        }
    }
}
