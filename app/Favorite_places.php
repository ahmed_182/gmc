<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite_places extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'lat',
        'lng',
        'address',
        'comments',
        'apartment_number',
        'building_number',
        'street',
        'floor_number',
        'flat_number',
        'user_name',
        'mobile',
        'area_id',
        'land',
        'home_no',
        'apartment_no',
        'gada',
        'notes',
    ];

    public function toArray()
    {
        //            $data["user"] = $this->user;
        $data["id"] = $this->id;
        $data["user_id"] = $this->user_id;
        $data["name"] = $this->name;
        $data["lat"] = $this->lat;
        $data["lng"] = $this->lng;
        $data["address"] = $this->address;
        $data["apartment_number"] = $this->apartment_number;
        $data["building_number"] = $this->building_number;
        $data["street"] = $this->street;
        $data["floor_number"] = $this->floor_number;
        $data["flat_number"] = $this->flat_number;
        $data["user_name"] = $this->user_name;
        $data["mobile"] = $this->mobile;
        $data["area"] = $this->area;
        $data["land"] = $this->land;
        $data["home_no"] = $this->home_no;
        $data["apartment_no"] = $this->apartment_no;
        $data["gada"] = $this->gada;
        $data["notes"] = $this->notes;
        $data["comments"] = $this->comments;
        return $data;
    }


    public function area()
    {
        return $this->belongsTo(District::class, 'area_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
