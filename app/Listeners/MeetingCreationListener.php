<?php

namespace App\Listeners;

use App\Events\MeetingCreationEvent;
use App\Mail\MeetingCreationEmail;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Mail;

class MeetingCreationListener
{
    private array $mails = array();

    public function handle(MeetingCreationEvent $event)
    {
        $course = $event->course;
        $ids = Order::whereCourseId($course->id)->pluck('trainee_id')->toArray();
        $this->mails =  User::whereIn('id', $ids)->pluck('email')->toArray();

        if ($email = $course->trainer->user->email)
            array_push($this->mails, $email);

        Mail::to($this->mails)->queue(new MeetingCreationEmail($event->course, $event->meeting));
    }
}
