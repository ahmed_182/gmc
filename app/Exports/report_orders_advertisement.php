<?php

    namespace App\Exports;

    use App\Course;
    use App\ModulesConst\OrderStatus;
    use App\ModulesConst\UserTyps;
    use App\Order;
    use App\Sponsored_advertisements;
    use App\User;
    use Illuminate\Http\Request;
    use Maatwebsite\Excel\Concerns\FromCollection;
    use Maatwebsite\Excel\Concerns\WithHeadings;
    use Maatwebsite\Excel\Concerns\WithMapping;


    class report_orders_advertisement implements FromCollection,  WithMapping, WithHeadings
    {




     /*   /**
         * @return \Illuminate\Support\Collection
         */
        public $id;

        public function collection()
        {
           $id= request()->id;
         $orders=Sponsored_advertisements::where('course_id',$id)->get();
//dd($orders);
            return $orders;

        }

        public function map($orders): array
        {
/*            $data["markter_name"] = $orders->dash_markter;*/
            $data["name_course"] = $orders->dash_course;
            $data["client_name"] = $orders->name;
            $data["mobile"] = $orders->mobile;
            $data["email"] = $orders->email;
            $data["status"] = $orders->dash_status;

            // mediator data :-
            return $data;
        }

        public function headings(): array
        {
            return [
                [
/*                    trans('language.markter_name'),*/
                    trans('language.name_course'),
                    trans('language.client_name'),
                    trans('language.mobile'),
                    trans('language.email'),
                    trans('language.status'),

                ],
            ];
        }
    }
