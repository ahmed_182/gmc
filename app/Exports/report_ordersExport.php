<?php

    namespace App\Exports;

    use App\Course;
    use App\ModulesConst\OrderStatus;
    use App\ModulesConst\UserTyps;
    use App\Order;
    use App\User;
    use Illuminate\Http\Request;
    use Maatwebsite\Excel\Concerns\FromCollection;
    use Maatwebsite\Excel\Concerns\WithHeadings;
    use Maatwebsite\Excel\Concerns\WithMapping;


    class report_ordersExport implements FromCollection,  WithMapping, WithHeadings
    {




     /*   /**
         * @return \Illuminate\Support\Collection
         */
        public $id;

        public function collection()
        {
           $id= request()->id;
         $orders=Order::where('course_id',$id)->get();
//dd($orders);
            return $orders;

        }

        public function map($orders): array
        {
            $data["markter_name"] = $orders->dash_markter;
            $data["client_name"] = $orders->name;
            $data["mobile"] = $orders->mobile;
            $data["identity"] = $orders->identity;
            $data["qualification"] = $orders->qualification_id;

            // mediator data :-
            return $data;
        }

        public function headings(): array
        {
            return [
                [
                    trans('language.markter_name'),
                    trans('language.client_name'),
                    trans('language.mobile'),
                    trans('language.identity'),
                    trans('language.qualification'),

                ],
            ];
        }
    }
