<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract_course extends Model
{
    protected $fillable = [
        'name',
        'entity',
        'email',
        'mobile',
        'city',
        'course',
        'created_at',

    ];

    public function join_from()
    {
        return $this->created_at->diffforhumans();
    }
}
