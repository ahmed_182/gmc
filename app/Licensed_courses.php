<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licensed_courses extends Model
{
    protected $fillable = [
        "name","no_accreditation"
    ];
}
