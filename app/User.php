<?php

    namespace App;

    use App\ModulesConst\UserVerify;
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Contracts\Auth\MustVerifyEmail;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Laravel\Passport\HasApiTokens;


    class User extends Authenticatable
    {
        use Notifiable, HasApiTokens;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'user_type_id',
            'social_id',
            'user_name',
            'name',
            'description',
            'image',
            'country_id',
            'city',
            'mobile',
            'mobile_verified',
            'email',
            'email_verified_at',
            'email_verified',
            'password',
            'fire_base_token',
            'passCode',
            'MobileCode',
            'address',
            'userVerify',
            'delivery_cost',
            'api_token',
        ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden = [
            'password', 'remember_token',
        ];

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts = [
//            'created_at' => 'timestamp',
            'email_verified_at' => 'bool',
            'mobile_verified_at' => 'datetime',
            'email_verified' => 'boolean',
            'online_at' => 'datetime',
            'online' => 'boolean',
            'userVerify' => 'boolean',
            'mobile_verified' => 'boolean',
            'need_reset_password' => 'boolean',
        ];

        public function toArray()
        {
            $data["id"] = $this->id;
/*            $data["image"] = $this->image;
            $data["social_id"] = $this->social_id;*/
            $data["user_type_id"] = $this->user_type_id;
            $data["userVerify"] = $this->userVerify;
            $data["name"] = $this->name;
/*            $data["description"] = $this->description;*/
            $data["mobile"] = $this->mobile;
            $data["email"] = $this->email;
            $data["join_from"] = $this->join_from();
            $data["is_data_complete"] = $this->is_data_complete();
            return $data;
        }

        public function profile()
        {
            $data["id"] = $this->id;
            /*$data["image"] = $this->image;
            $data["social_id"] = $this->social_id;*/
            $data["user_type_id"] = $this->user_type_id;
            $data["userVerify"] = $this->userVerify;
            $data["name"] = $this->name;
/*            $data["description"] = $this->description;*/
            $data["mobile"] = $this->mobile;
            $data["email"] = $this->email;
            $data["join_from"] = $this->join_from();
            $data["is_data_complete"] = $this->is_data_complete();
            return $data;
        }


        public function is_data_complete()
        {
            if (!$this->name and !$this->email) {
                return false;
            }
            return true;
        }

        public function lastpasswords()
        {
            return $this->hasMany('App\User_lastpasswords', 'user_id');
        }

        public function user_type()
        {
            return $this->belongsTo(User_type::class, 'user_type_id');
        }


        public function country()
        {
            return $this->belongsTo(Country::class, 'country_id');
        }

        public function categories_list()
        {
            $sub_cats_ids = Shop_categories::where('user_id', $this->id)->pluck('sub_category_id');
            $cats = Sub_category::whereIn('id', $sub_cats_ids)->orderBy('id', 'desc')->get();
            return $cats;
        }


        public function bank()
        {
            return $this->belongsTo(Bank::class, 'bank_id');
        }

        public function accessTokens()
        {
            return $this->hasMany('App\OauthAccessToken');
        }


        public function getDashNameAttribute()
        {
            $attribute =  trans('language.notSelected');
            if ($this->name)
                $attribute = $this->name;
            return $attribute;
        }


        public function getDashEmailAttribute()
        {
            $attribute =  trans('language.notSelected');
            if ($this->email)
                $attribute = $this->email;
            return $attribute;
        }

        public function getDashMobileAttribute()
        {
            $attribute =  trans('language.notSelected');
            if ($this->mobile)
                $attribute = $this->mobile;
            return $attribute;
        }



        public function getDashStatusNameAttribute()
        {
            if ($this->userVerify == UserVerify::yes) {
                return trans("language.verfied");
            } else {
                return trans("language.not_verfied");
            }
        }

        public function getDashImageAttribute()
        {
            if ($this->image)
                return $attribute = $this->image;
            else
                return "https://cdn3.iconfinder.com/data/icons/avatars-round-flat/33/avat-01-512.png";
        }

        public function getServCityAttribute()
        {
            $att = trans('language.notSelected');
            if ($this->city) {
                $att = $this->city->serv_name;
            }
            return $att;
        }


        public function getServDistractAttribute()
        {
            $att = trans('language.notSelected');
            if ($this->district) {
                $att = $this->district->serv_name;
            }
            return $att;
        }

        public function getServUserTypeNameAttribute()
        {
            $att = trans('language.notSelected');
            if ($this->user_type) {
                $att = $this->user_type->serv_name;
            }
            return $att;
        }

        public function is_block()
        {
            if ($this->block_date != null) {
                return true;
            } else {
                return false;
            }
        }


        public function getServBlockDateAttribute()
        {
            if ($this->block_date != null) {
                $now_time = strtotime(now()) * 1000;
                return $now_time;
            } else {
                return false;
            }
        }

        public function roles()
        {
            return $this->belongsToMany(Role::class);
        }

        public function authorizeRoles($roles)
        {
            if (is_array($roles)) {
                return $this->hasAnyRole($roles) ||
                    abort(401, 'This action is unauthorized.');
            }
            return $this->hasRole($roles) ||
                abort(401, 'This action is unauthorized.');
        }

        public function hasAnyRole($roles)
        {
            return null !== $this->roles()->whereIn(‘name’, $roles)->first();
        }

        public function hasRole($role)
        {
            return null !== $this->roles()->where(‘name’, $role)->first();
        }


        // Shop Relations :-
        public function services_list()
        {
            $service_ids = User_service::where("user_id", $this->id)->pluck('service_id');
            $servies = Service::whereIn('id', $service_ids)->get();
            return $servies;
        }


        public function provider_service_sections_list()
        {
            $service_ids = User_service::where("user_id", $this->id)->pluck('service_id');
            $servies = User_service_section::whereIn('id', $service_ids)->get();
            return $servies;
        }

        public function join_from()
        {
            return $this->created_at->diffforhumans();
        }


        public function shop_images()
        {
            return Shop_images::where('user_id', $this->id)->get();
        }

        public function postion()
        {
            $trainer= Trainer::where('user_id', $this->id)->first();
            //dd($trainer);
            return $trainer->current_position;
        }
    }
