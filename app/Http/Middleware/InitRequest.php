<?php

namespace App\Http\Middleware;

use App\Default_image;
use App\Http\Controllers\Controller;
use Auth;
use Closure;

class InitRequest extends Controller
{
    public function handle($request, Closure $next)
    {

        // block Check if this token blocked return false with blocked message :-
        if (Auth::guard("api")->user()) {
            $user = Auth::guard("api")->user();
            if ($user->is_block() == true) {
                return $this->apiResponse($request, trans('language.block_alert_notification'), null, false, 400);
            }
        }
        if (auth()->check()) {
            if (auth()->user()->is_block() == true) {
                Auth::logout();
                $request->session()->regenerate();
                session()->flash('danger', trans(trans('language.block_alert_notification')));
                return redirect('/login');
            }
        }

        $data = $request->all();
        if ($request->password)
            $data['password'] = bcrypt($request->password);
        $request['data'] = $data;
        return $next($request);
    }
}
