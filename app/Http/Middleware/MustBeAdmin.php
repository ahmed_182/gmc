<?php

namespace App\Http\Middleware;

use App\ModulesConst\UserTyps;
use Closure;

class MustBeAdmin
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (auth()->check() and auth()->user()->user_type_id == 1) // admin
            return $next($request);
        session()->flash('danger', trans('الحساب غير موجود لدينا في قاعده البيانات .'));
        return redirect('/admin');


    }
}
