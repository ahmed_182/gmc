<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class ShopListResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         * @return array
         */
        public function toArray($request)
        {
            return [
                'id' => $this->user_id,
                'image' => $this->image,
                'name' => $this->serv_name,
                'lat' => $this->user->lat,
                'lng' => $this->user->lng,
                'slogan' => $this->serv_slogan,
                'services' => ServiceSectionsResources::collection($this->user->services_list()),
            ];
        }
    }
