<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class DocumentsResource extends JsonResource
{


    public function toArray($request)
    {
        return [

            'user' => $this->user,
            'criminalRecord' => $this->criminalRecord,
            'drivingLicense' => $this->drivingLicense,
            'vehicleLicense' => $this->vehicleLicense,
            'document_verified' => $this->document_verified
        ];
    }
}
