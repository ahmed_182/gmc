<?php


namespace App\Http\Resources\User;


use Illuminate\Http\Resources\Json\JsonResource;

class DriverCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => $this->user,
            'car' => $this->car,
            'online' => $this->online,
            'document_verified' => $this->document_verified,
            'require_password' => $this->require_password,
            'created_at' => $this->created_at,
            'order_count' => $this->order_count,
        ];
    }
}
