<?php

    namespace App\Http\Resources\Order;

    use App\Http\Resources\Itto\ittoSectionTypeResponse;
    use App\Http\Resources\ProviderProfile;
    use App\Http\Resources\Technician\TechnicianProfile;
    use Illuminate\Http\Resources\Json\JsonResource;

    class OrderResource extends JsonResource
    {

        public function toArray($request)
        {
          /*  $data["id"] = $this->id;
            $data["tax"] = $this->tax;*/
            $data["date"] = $this->created_at->format("Y/m/d");
            $data["name"] = $this->name;
            $data["mobile"] = $this->mobile;
            $data["order_id"] = $this->id;
            $data["service"] = $this->serv_service;
            $data["receipt_type"] = $this->dash_receipt;
            $data["location"] = $this->ServLocation;
            $data["payment_type"] = $this->dash_payment;
            $data["price"] = $this->dash_cost;
            $data["delivery_cost"] = $this->delivery_tax;
            $data["total_price"] = $this->dash_cost + $this->delivery_tax;

            return $data;
        }
    }
