<?php

    namespace App\Http\Resources\Order;

    use App\Http\Resources\Itto\ittoSectionTypeResponse;
    use App\Http\Resources\Technician\TechnicianProfile;
    use Illuminate\Http\Resources\Json\JsonResource;

    class MiniOrderResource extends JsonResource
    {

        public function toArray($request)
        {
            return [
                'id' => $this->id,
                'order_cost' => $this->total_cost,
                'delivery_cost' => $this->delivery_cost,
                'currency' => $this->getServCurrencyAttribute(),
                'tax' => $this->tax,
                'payment' => $this->payment_type,
                'orderStatus' => $this->Order_statues,
                'favourite_place' => $this->favourite_place,
                'rate' => $this->rate,
                'client' => $this->client->profile(),
                'shop' => $this->shop->shopProfile(),
               'created_at' => $this->created_at * 1000
            ];
        }


    }
