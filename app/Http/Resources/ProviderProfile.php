<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderProfile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->user_id,
            'country' => $this->user->country,
            'user_code_number' => $this->user->user_code_number,
            'user_type_id' => $this->user->user_type_id,
            'mobile' => $this->user->mobile,
            'userVerify' => $this->user->userVerify,
            'is_profile_complete' => (bool)$this->is_profile_complete,
            'is_services_complete' => (bool)$this->is_services_complete,
            'image' => $this->user->image,
            'identity_id' => $this->identity_id,
            'name' => $this->user->name,
            'name_for_client' => $this->name_for_client,
            'manufacturing_year' => $this->manufacturing_year,
            'plate_number' => $this->plate_number,
            'identity_image' => $this->identity_image,
            'driving_license_image' => $this->driving_license_image,
            'car_insurance_image' => $this->car_insurance_image,
            'car_image' => $this->car_image,
            'services' => $this->user->provider_service_sections_list(),
            // todo calck
            'rate' => 5,
            'day_money' => 0,
            'month_money' => 0,
        ];
    }
}
