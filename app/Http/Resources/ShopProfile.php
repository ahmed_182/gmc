<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShopProfile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data["id"] = $this->id;
        $data["image"] = $this->image;
        $data["social_id"] = $this->social_id;
        $data["user_type_id"] = $this->user_type_id;
        $data["userVerify"] = $this->userVerify;
        $data["name"] = $this->name;
        $data["description"] = $this->description;
        $data["mobile"] = $this->mobile;
        $data["email"] = $this->email;
        $data["join_from"] = $this->join_from();
        $data["images"] = $this->shop_images();
        $data["categories"] = $this->categories_list();
        return $data;
    }
}
