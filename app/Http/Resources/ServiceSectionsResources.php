<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class ServiceSectionsResources extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         * @return array
         */
        public function toArray($request)
        {
            return [
                'id' => $this->id,
                'icon' => $this->icon,
                'name' => $this->serv_name,
                'description' => $this->serv_desc,
                'type' => $this->type,
                'type_name' => $this->type_name,
                'sections' => $this->sections,
            ];
        }
    }
