<?php

    namespace App\Http\Controllers\Site\Login;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function index()
        {
            return view("site.login.index");
        }

        public function siteLogout(Request $request)
        {
            \Auth::logout();
            $request->session()->invalidate();
            $request->session()->regenerate();
            alert()->success( "تم تسجيل الخروج بنجاح");

            return redirect('/');
        }

        public function login(Request $request)
        {

            $request->validate([
                'email' => ['required'],
                'password' => ['required', 'string', 'min:6'],
            ]);
            $data = $request->all();


            if (\Auth::attempt(['email' => $data["email"], 'password' => $request->password])) {
                // The user is active, not suspended, and exists.
                $user = \Auth::user();
                \Auth::login($user);
                alert()->success( "تم تسجيل الدخول بنجاح");
                return redirect("/");
            } else {
                alert()->error( "خطا في بيانات تسجيل دخول");
                return back();
            }
        }
    }
