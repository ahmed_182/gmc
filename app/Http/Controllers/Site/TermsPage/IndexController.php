<?php

namespace App\Http\Controllers\Site\TermsPage;

use App\Bank_account;
use App\InforamtionString;
use App\ModulesConst\InforamtionStringType;
use App\Terms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function terms_and_conditions(){
        $item = InforamtionString::first();
        return view("site.terms.index",compact("item"));
    }

    public function e_learning(){
        $item = InforamtionString::first();
        return view("site.terms.e_learning",compact("item"));
    }

    public function privacy(){
        $item = InforamtionString::first();
        return view("site.terms.privacy",compact("item"));
    }

    public function property_rights(){
        $item = InforamtionString::first();
        return view("site.terms.property_rights",compact("item"));
    }

    public function integrity(){
        $item = InforamtionString::first();
        return view("site.terms.integrity",compact("item"));
    }

    public function cheat(){
        $item = InforamtionString::first();
        return view("site.terms.cheat",compact("item"));
    }


}
