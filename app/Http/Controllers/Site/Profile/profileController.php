<?php

namespace App\Http\Controllers\Site\Profile;

use App\Follow;
use App\Http\Controllers\Auth\VerificationController;
use App\Mail\EmailVerification;
use App\ModulesConst\Paginate;
use App\ModulesConst\ProductStatus;
use App\Order;
use App\Product;
use App\Trainer;
use App\Traits\storeImage;
use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Mail;
use Session;

class profileController extends Controller
{
    use storeImage;

    public function my_sold_products($id)
    {
        $items = Product::where("user_id", $id)->where("status", ProductStatus::sold)->paginate();
        return view('site.user.my_sold_products', compact('items'));
    }

    public function index(Request $request)
    {
        $user = Auth::user();
      //  $itemuser = Order::where("id",$user->id)->first();
        if ($user->user_type_id==2)
        return view('site.user.profile', compact('user'));
        elseif ($user->user_type_id==3)
            $item = Trainer::where("user_id",$user->id)->first();
            return view('site.user.trainerprofile', compact('item'));

    }

    public function updatePasswordView(Request $request)
    {
        $user = Auth::user();
        return view('site.user.updatePassword', compact('user'));
    }

    public function editProfile(Request $request)
    {
        $user = Auth::user();
        return view('site.user.editProfile', compact('user'));
    }

    public function update(Request $request)
    {

        $data = $request->validate([
            'name' => '',
            'user_name' => '',
            'image' => '',
            'mobile' => '',
            'email' => '',
            'bio' => '',
            'address' => '',
            'region_id' => '',
            'country_id' => '',
            'city_id' => '',
            'hide_mobile' => '',
        ]);

        $user = $request->user();
        $data["emailVerificationCode"] = rand(1111, 9999);
        if ($request->user_name) {
            // user name
            // check if this user_name existed ..
            $user_nameCheck = User::where('id', '!=', $user->id)->where('name', $request->name)->get();
            if ($user_nameCheck->count() > 0) {
                alert()->success('', 'اسم المستخدم موجود بالفعل لدي مستخدم اخر .');
            }
        }
        $user->update($data);
//             Mail::to($request->email)->send(new EmailVerification($user));
        alert()->success('', 'تم تحديث الملف الشخصي لك بنجاح .');
        return redirect("/profile");
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'c_password' => 'required',
            'password' => 'required',
            'r_password' => 'required',
        ], [
            'c_password.required' => 'لابد من ادخال كلمه المرور الحاليه',
            'password.required' => 'لابد من ادخال كلمه المرور الجديدة',
            'r_password.required' => 'لابد من ادخال تأكيد كلمه المرور الجديدة',
        ]);

        $user = $request->user();

        if (Hash::check($request->c_password, $user->password)) {

            if ($request->password == $request->r_password) {
                $data['password'] = Hash::make($request->password);
                $user->update($data);
                session()->flash('success', 'تم تحديث كلمه المرور الخاص بك بنجاح.');
                return back();
            } else {
                session()->flash('alert', 'عذرا يرجي التأكد من تأكيد كلمه المرور يشابه كلمه المرور المدخله .');
                return back();
            }

        } else {
            session()->flash('alert', 'عذرا , كلمه المرور الحاليه لا تشبه كلمه المرور التي قمت بأدخالها .');
            return back();
        }


    }

    public function logout(Request $request)
    {

    }


    public function followers($id)
    {

        $followers = Follow::where("person_two", $id)->orderBy("id", "desc")->paginate(Paginate::value);
        return view("site.user.userProfile.followersList", compact("followers"));
    }

    public function following($id)
    {
        $followings = Follow::where("person_one", $id)->orderBy("id", "desc")->paginate(Paginate::value);
        return view("site.user.userProfile.followingList", compact("followings"));
    }

    public function userProducts($user_id)
    {
        $items = Product::where("user_id", $user_id)
            ->where("status", ProductStatus::pending)
            ->orderBy("id", "desc")->paginate(10);
        $is_my_products = false;
        if (auth()->user() and auth()->user()->id == $user_id)
            $is_my_products = true;
        $user = User::find($user_id);
        return view("site.user.userProducts", compact("items", "user", "is_my_products"));
    }
}
