<?php

namespace App\Http\Controllers\Site\Home;

use App\Categories;
use App\City;
use App\Course;
use App\Distrct;
use App\Licensed_courses;
use App\ModulesConst\ProductStatus;
use App\Notification;
use App\Product;
use App\Product_options;
use App\Property;
use App\Regoin;
use App\Trainer;
use App\User;
use App\User_notification;
use Auth;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class indexController extends Controller
{

    public function index()
    {

        return view("site.home.index");
    }
    public function contract_courses()
    {

        return view("site.contract_courses.index");
    }


    public function courses(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $items = Course::where("end_date", '>=',$date);
        $items = $this->filter_category($request, $items);

        return view("site.course.index",compact('items'));
    }
    public function offers(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $items = Course::where('is_special',1)->where("end_date", '>=',$date);
        $items = $this->filter_category($request, $items);

        return view("site.offer.index",compact('items'));
    }

    public function filter_category($request, $items)
    {
        if ($request->category_id)
            $items = $items->where("category_id",  $request->category_id );
        $items = $items->orderBy("id", "desc")->paginate(9);
        return $items;
    }
    public function archives(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $items = Course::where("end_date","<",$date);
        $items = $this->filter_category($request, $items);
        return view("site.archives.index",compact('items'));
    }
    public function registerCourse(Request $request)
    {
        //dd("ghfhf");
        $data = $request->all();
        // Check start of mobile must be without zero :-
        $mob = $data['mobile'];
        $MobstartWith = substr($data['mobile'], 0, 1);
        if ($MobstartWith == 0) {
            $str = ltrim($mob, '0');
            $data["mobile"] = $str;
        }
        $msg ="تم ارسال الطلب بنجاح سيتم التواصل معك قريبا";
        $user=\App\Request::create($data);

      // dd($msg);
        $d= $this->sendSms($msg, $user);
       // dd( $d);
        
        $this->register_Course($data,$user);

        alert()->success('تم الارسال بنجاح', 'سوف نتواصل معك قريبا');
        return back();
    }
    public function courseDetails($id)
    {
        $date = Carbon::now()->format('Y-m-d');
        $item = Course::find($id);
       $archive=$item->end_date<$date;

        return view("site.course.details",compact('item','archive'));
    }
    public function trainerDetails($id)
    {
        $item = Trainer::where("id",$id)->first();
        return view("site.trainer.details",compact('item'));
    }


    public function table_courses(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $items = Course::where("end_date", '>=',$date);
        $items = $this->filter($request, $items);
        return view("site.table_courses.index",compact('items'));
    }

    public function licensed_courses(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $items = Licensed_courses::query();
        $items = $this->filtercourse($request, $items);
        return view("site.licensed_courses.index",compact('items'));
    }


    public function filter($request, $items)
    {

        if ($request->name)
            $items = $items->where("name", 'LIKE', '%' . $request->name . '%');

        if ($request->date)
            $items = $items->where("date",  $request->date );
        $items = $items->orderBy("date", "Asc")->paginate(9);
        return $items;
    }

    public function filtercourse($request, $items)
    {

        if ($request->name)
            $items = $items->where("name", 'LIKE', '%' . $request->name . '%');

        if ($request->no_accreditation)
            $items = $items->where("no_accreditation", $request->no_accreditation );


        $items = $items->orderBy("id", "desc")->paginate(9);
        return $items;
    }
}

