<?php

    namespace App\Http\Controllers\Site\Contract_courses;

    use App\Contact_us;
    use App\Contract_course;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function index()
        {
            return view("site.contract_courses.index");
        }

        public function store(Request $request)
        {
            //dd("ghfhf");
            $data = $request->all();
            Contract_course::create($data);
            alert()->success('تم الارسال بنجاح', 'سيتم التواصل معك قريبا جدا من قبل الادارة');
            return back();
        }
    }
