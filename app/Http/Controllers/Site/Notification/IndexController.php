<?php

namespace App\Http\Controllers\Admin\Notification;

use App\Notification;
use  App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $items = Notification::orderBy('id', 'desc')->paginate(10);

        return view('admin.notifications.index', compact('items'));
    }



    public function destroy($id)
    {
        $item = Notification::findOrFail($id);
        $item->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/notifications'));
    }


}
