<?php

    namespace App\Http\Controllers\Site\AjaxController;

    use App\Categories;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function getCategories(Request $request)
        {
            $categories = Categories::where("parent_id", $request->category_id)->orderBy("sort","desc")->get();
            $data['categories'] = $categories;
            if (count($categories) > 0) {
                $data["status"] = true;
            } else {
                $data["status"] = false;
            }
            return response()->json($data);
        }
    }
