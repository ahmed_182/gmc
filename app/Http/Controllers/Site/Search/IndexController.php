<?php

namespace App\Http\Controllers\Site\Search;

use App\Categories;
use App\City;
use App\Course;
use App\Distrct;
use App\ModulesConst\Paginate;
use App\Product;
use App\Regoin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            "keyword" => "",
        ]);

        $items = Course::query();


        if ($request->keyword) {
            $items = $items->where('name', 'LIKE', "%$request->keyword%");
        }

        $items = $items->orderBy("id", "desc")->paginate(9);
        return view("site.search.index", compact("items"));

    }

}
