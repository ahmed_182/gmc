<?php

namespace App\Http\Controllers\Site\SponsoredAdvertisement;

use App\Course;
use App\Order;
use App\Setting;
use App\Sponsored_advertisements;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function sponsored_advertisement($id,$token)
    {
        // dd("hjky");
        $item = User::where("api_token", $token)->first();
        //dd($item);
        $course = Course::where("id", $id)->first();
        $setting = Setting::first();
        // dd($item->id);
        if (!$item) {
            return view('auth.passwords.not_auth');
        }
        return view('site.sponserd.sponsored_advertisement',compact('item','course'));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        // Check start of mobile must be without zero :-
        $mob = $data['mobile'];
        $MobstartWith = substr($data['mobile'], 0, 1);
        if ($MobstartWith == 0) {
            $str = ltrim($mob, '0');
            $data["mobile"] = $str;
        }
        $user=Sponsored_advertisements::create($data);
 /*       $msg =" ($user->dash_course) تم الاشتراك بنجاج فى دورة   ";
       // dd($msg);
       $d= $this->sendSms($msg, $user);
       // dd($d);

        $this->confirm_password($data,$user);*/

        alert()->success('تم التسجيل بنجاح');
        return redirect("/");
    }
}
