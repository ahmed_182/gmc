<?php

    namespace App\Http\Controllers\Site\Questionnaire;

    use App\Contact_us;
    use App\Contract_course;
    use App\Course;
    use App\questionnaire;
    use App\Questionnaire_courses;
    use App\Questionnaire_friends;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function index()
        {
            $date = Carbon::now()->format('Y-m-d');
            $courses = Course::where("end_date", '>=',$date)->orderBy("id", "desc")->paginate(10);
            return view("site.questionnaire.index",compact('courses'));
        }

        public function store(Request $request)
        {
            //dd("ghfhf");
            $data = $request->all();
            $contact=questionnaire::create($data);

            alert()->success('تم ارسال الاستبيان بنجاح');
            return back();
        }
    }
