<?php

    namespace App\Http\Controllers\Site\Register;

    use App\Contact_us;
    use App\ModulesConst\UserTyps;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Validator;

    class IndexController extends Controller
    {
        public function index()
        {
            return view("site.register.index");
        }

        public function siteRegisterForm(Request $request)
        {

            $validator = Validator::make(
                $request->all(),
                [
                    'name' => 'required',
                    'email' => 'required',
                    'user_type_id' => 'required',
                    'mobile' => 'required|numeric',
                    'password' => 'required',
                    'confirm-password' => 'required',
                ]
            );
            if ($validator->fails()) {
                dd($validator->errors()->first());
            }

            $data = $request->all();

           // check if this user_name existed ..
            $user_nameCheck = User::where('name', $request->name)->get();
            if ($user_nameCheck->count() > 0) {
                session()->flash("alert", trans('language.Existuser_name'));
                return back();
            }


            $data["name"] = $request->name;
            $data['passCode'] = rand(1111, 9999);
         /*   $data["country_id"] = $request->country_id;*/
            //$data['api_token'] = rand(99999999, 999999999) . time();
            $data["password"] = \Hash::make($request->password);
/*            $data["user_type_id"] = UserTyps::user;*/
            $data["fire_base_token"] = $request->fire_base_token;

            $user = User::create($data);
            auth()->login($user);
            // Send Sms To active Account
            $v_msg = trans('language.activation_code') . ' ( ' . $data['passCode'] . ' ) ';
           // $this->sms($v_msg, $user);
            $this->sendVerfiyMail($user);
            session()->flash("alert", "تم انشاء حساب جديد بنجاح");
            return redirect("/verfiyMobilePage");
        }


        public function verfiyMobilePage()
        {
            //dd((auth()->user()->email));

            return view('site.auth.verify_mobile');
        }
        public function verifyaccountWithemail(Request $request)
        {
            $request->validate([
/*                'email' => 'required',*/
                'passCode' => 'required',
            ]);
            $data = $request->all();

            $user = User::where('email', auth()->user()->email)->first();
            // check from Code ?
            if ($user->passCode == $request->passCode) {
                $user->userVerify = 1;
                $user->save();
                \Auth::login($user);
                alert()->success('تم التحقق بنجاح');
                return redirect("/");
                //Auth::logout();

            } else {
                session()->flash('alert', 'عذرا , هناك خطأ في كود التحقق .');
                //Auth::logout();
                return back();
            }

        }

    }
