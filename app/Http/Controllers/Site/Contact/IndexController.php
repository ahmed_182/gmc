<?php

    namespace App\Http\Controllers\Site\Contact;

    use App\Contact_us;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function index()
        {
            return view("site.contact.index");
        }

        public function store(Request $request)
        {
            //dd("ghfhf");
            $data = $request->all();
            $contact=Contact_us::create($data);
            $msg =" ($contact->name) سيتم التواصل معك قريبا   ";
           //dd($msg);
            $d= $this->sendSms($msg, $contact);
             //dd($d);
            alert()->success('تم الارسال بنجاح', 'سيتم التواصل معك قريبا');
            return back();
        }
    }
