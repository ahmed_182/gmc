<?php

    namespace App\Http\Controllers\Site\Error;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function editProductNotBelongsToYouError()
        {
            return view("site.errors.editProductNotBelongsToYouError");
        }
    }
