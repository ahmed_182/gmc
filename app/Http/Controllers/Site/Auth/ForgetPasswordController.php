<?php

namespace App\Http\Controllers\Site\Auth;

use App\User;
use Auth;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ForgetPasswordController extends Controller
{
    public function index(Request $request)
    {
        return view('site.auth.forgetPassword');
    }


    public function forgetPassword(Request $request)
    {
        $request->validate([
            'mobile' => 'required'
        ]);

        $data = $request->all();

        $mob = $data['mobile'];
        $MobstartWith = substr($data['mobile'], 0, 1);
        if ($MobstartWith == 0) {
            $str = ltrim($mob, '0');
            $data["mobile"] = $str;
        }

        $checkIfThisNumberExistInSystem = User::where('mobile', $data["mobile"])->first();

        if (!$checkIfThisNumberExistInSystem) {
            session()->flash('alert', 'عذرا رقم الجوال غير مسجل لدينا');
            return back();
        }

        // Send sms to mobile number ?
        $code = rand(1111, 9999);
        $message = "الكود الخاص باسترجاع كلمه المرور هو : $code ";
        $this->sms($message, $checkIfThisNumberExistInSystem);

        //update passCode
        $checkIfThisNumberExistInSystem->passCode = $code;
        $checkIfThisNumberExistInSystem->save();
        //Auth::login($checkIfThisNumberExistInSystem);

        return view("site.auth.verifyPasswordWithMobile", ['mobile' => $data["mobile"]]);
    }

    public function verifyPasswordWithMobile(Request $request)
    {
        $request->validate([
            'mobile' => 'required',
            'passCode' => 'required',
        ]);
        $data = $request->all();
        $mob = $data['mobile'];
        $MobstartWith = substr($data['mobile'], 0, 1);
        if ($MobstartWith == 0) {
            $str = ltrim($mob, '0');
            $data["mobile"] = $str;
        }
        $user = User::where('mobile', $data["mobile"])->first();
        // check from Code ?
        if ($user->passCode == $request->passCode) {
            $user->passCode = 1;
            $user->save();
            Auth::login($user);
            return view("site.auth.resetNewPassword");
        } else {
            session()->flash('alert', 'عذرا , هناك خطأ في كود التحقق .');
            //Auth::logout();
            return back();
        }

    }

    public function resetnewpassword(Request $request)
    {
        $request->validate([
            'password' => 'required',
            'r_password' => 'required',
        ], [
            'password' => 'لابد من ادخال كلمه المرور الجديدة',
            'r_password' => 'لابد من ادخال تأكيد كلمه المرور الجديدة',
        ]);

        $user = $request->user();
        if ($request->password == $request->r_password) {
            $data['password'] = Hash::make($request->password);
            $user->update($data);
            session()->flash('success', 'تم تحديث كلمه المرور الخاص بك بنجاح.');
            return redirect("/");
        } else {
            session()->flash('alert', 'عذرا يرجي التأكد من تأكيد كلمه المرور يشابه كلمه المرور المدخله .');
            return view("site.auth.resetNewPassword");
        }


    }
}
