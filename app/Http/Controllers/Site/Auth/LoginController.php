<?php

namespace App\Http\Controllers\Site\Auth;

use App\Course;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'country_id' => ['required'],
            'mobile' => ['required'],
            'password' => ['required', 'string', 'min:6'],
        ]);
        $data = $request->all();
        $mob = $data['mobile'];
        $MobstartWith = substr($data['mobile'], 0, 1);
        if ($MobstartWith == 0) {
            $str = ltrim($mob, '0');
            $data["mobile"] = $str;
        }


        if (Auth::attempt(['mobile' => $data["mobile"], 'password' => $request->password, "country_id" => $request->country_id])) {
            // The user is active, not suspended, and exists.
            $user = Auth::user();
            Auth::login($user);
            toast('تم تسجيل الدخول بنجاح', 'success')->position("bottom-end");
            return response()->json(['message' => 'تم تسجيل الدخول بنجاح', 'status_' => 1]);
        } else {
            toast('خطا في بيانات تسجيل دخول', 'alert')->position("bottom-end");
            return response()->json(['message' => 'خطا في بيانات تسجيل دخول', 'status_' => 0]);
        }
    }

    public function siteLogout(Request $request)
    {
        Auth::logout();
        $request->session()->regenerate();
        return redirect('/');
    }

    public function login()
    {
        if (Auth::check()) {
            $user = Auth::user()->userVerify;
            if ($user == 1) {
                return redirect("/");

            } else {
                return redirect("/verfiyMobilePage");
            }
        }
        return view("site.auth.login");
    }


}
