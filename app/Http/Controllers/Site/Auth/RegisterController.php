<?php

namespace App\Http\Controllers\Site\Auth;

use App\ModulesConst\UserTyps;
use App\User;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegisterController extends Controller
{
    public function siteRegisterForm(Request $request)
    {
        $request->validate([
            'location' => '',
            'name' => 'required',
            'user_name' => 'required',
            'mobile' => 'required',
            'password' => 'required',
            'country_id' => 'required',
            'city_id' => 'required',
        ]);

        $data = $request->all();

        $mob = $data['mobile'];
        $MobstartWith = substr($data['mobile'], 0, 1);
        if ($MobstartWith == 0) {
            $str = ltrim($mob, '0');
            $data["mobile"] = $str;
        }

        if ($request->country_id == 0) {
            return response()->json(['message' => 'لابد من اختيار الدوله', 'status_' => 0]);
        }
        // check if this mobile existed ..
        $mobileCheck = User::where('mobile', $data['mobile'])->get();
        if ($mobileCheck->count() > 0) {
            return response()->json(['message' => 'رقم الجوال موجود مسبقا لدي عميل أخر', 'status_' => 0]);
        }

        // check if this user_name existed ..
        $user_nameCheck = User::where('user_name', $request->user_name)->get();
        if ($user_nameCheck->count() > 0) {
            return response()->json(['message' => trans('language.Existuser_name'), 'status_' => 0]);
        }


        if ($request->location) {
            $location = explode(",", $request->location);
            $data["lat"] = $location[0];
            $data["lng"] = $location[1];
        }

        $data["name"] = $request->name;
        $data["user_name"] = $request->user_name;
        $data["address"] = $request->address;
        $data['passCode'] = rand(1111, 9999);
        $data["mobile"] = $data["mobile"];
        $data["country_id"] = $request->country_id;
        $data['api_token'] = rand(99999999, 999999999) . time();
        $data["password"] = Hash::make($request->password);
        $data["user_type_id"] = UserTyps::user;
        $data["fire_base_token"] = $request->fire_base_token;
        $data["online_at"] = Carbon::now();
        $user = User::create($data);
        auth()->login($user);
        // Send Sms To active Account
        $v_msg = trans('language.activation_code') . ' ( ' . $data['passCode'] . ' ) ';
        $this->sms($v_msg, $user);
        return response()->json(['message' => 'تم انشاء حساب جديد بنجاح', 'status_' => 1]);
    }
}
