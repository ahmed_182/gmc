<?php

namespace App\Http\Controllers\Site\Notifications;

use App\ModulesConst\NotificationTyps;
use App\ModulesConst\Paginate;
use App\Notification;
use App\User_notification;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    public function index()
    {
        // get allNotifications :-
        $user = Auth::user();
        $items = User_notification::whereUserId($user->id)->orderBy('id', 'desc')->paginate(Paginate::value);;
        foreach ($items as $item) {
            $item->update(['viewed' => 1]);
        }
        return view('site.user.notifications', compact('items'));
    }

    public function delete_notification($id)
    {
        $item = User_notification::find($id);
        if ($item) {
            $item->delete();
        }
        alert()->success('تم حذف الاشعار', 'تم حذف الاشعار .');
        return back();
    }

    public function delete_all_notification()
    {
        User_notification::whereUserId(Auth::id())->delete();
        alert()->success('تم حذف جميع الاشعارات', 'تم حذف جميع الاشعارات .');
        return back();
    }

    public function show($id)
    {
        $item = User_notification::find($id);
        $noti = Notification::find($item->notification_id);

        $item->update(['viewed' => 1]);
        if ($item->notification_relation->type == NotificationTyps::product) {
            return redirect(url("/all_products"));

        }

        if ($item->notification_relation->type == NotificationTyps::review) {
            return redirect(url("/profile"));

        }

        if ($item->notification_relation->type == NotificationTyps::message) {
            return redirect(url("/chatProduct"));

        }
        if ($item->notification_relation->type == NotificationTyps::userFollow) {
            return redirect(url("/followers/$item->user_id"));

        }
        if ($item->notification_relation->type == NotificationTyps::normal) {
            return redirect(url("/home"));
        }
        if ($item->notification_relation->type == NotificationTyps::productFollow) {
            return redirect(url("/productDetails/$noti->item_id"));
        }
        if ($item->notification_relation->type == NotificationTyps::comment) {
            return redirect(url("/productDetails/$noti->item_id"));
        }
        //return redirect("$item->href");
    }

}
