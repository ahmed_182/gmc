<?php

namespace App\Http\Controllers\Site\Zoom;

use App\Http\Controllers\Controller;
use App\MeetingRoom;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

class ZoomController extends Controller
{
    public function join(): View
    {
        return view('zoom.join');
    }

    public function getRoom($hostId): JsonResponse
    {
        $room = MeetingRoom::whereHostId($hostId)->first();
        if (!$room)
            return response()->json(['error' => 'There is no room with provided host id'], 404);
        return response()->json(['data' => $room], 200);
    }
}
