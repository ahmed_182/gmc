<?php

    namespace App\Http\Controllers;

    use App\InforamtionString;
    use App\Mail\confirmRegister;
    use App\Mail\EmailVerification;
    use App\Mail\registerCourse;
    use App\Mail\resetPassworMail;
    use App\Mail\sendEmail;
    use App\ModulesConst\InforamtionStringType;
    use App\ModulesConst\NotificationTyps;
    use App\ModulesConst\OrderStatus;
    use App\Notification;
    use App\Order;
    use App\Pay_count;
    use App\Property;
    use App\Setting;
    use App\User;
    use App\User_notification;
    use GuzzleHttp\Client;
    use Hamcrest\Core\Set;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use Illuminate\Http\Request;
    use Illuminate\Routing\Controller as BaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Kreait\Firebase\Factory;
    use Kreait\Firebase\ServiceAccount;
    use LaravelFCM\Facades\FCM;
    use LaravelFCM\Message\OptionsBuilder;
    use LaravelFCM\Message\PayloadDataBuilder;
    use LaravelFCM\Message\PayloadNotificationBuilder;
    use Mail;

    class Controller extends BaseController
    {


        use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

        public function fireBaseConnectionJson()
        {
            $data['type'] = "service_account";
            $data['project_id'] = "kadamaty-36d45";
            $data['private_key_id'] = "e7902f8db6992f93e12440464a0660c14ab2c11e";
            $data['private_key'] = "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCsjLL3ouTYOGbC\nTiiF4lnAGxI2eHleF7WBm8c9T40mSBLOoRn6cLao4E9xpPLy/Jp40xuo7Wd6/fM9\np4UOyWaoZi3PWjjyILWp2+RHfkqwaSUgFITaZjKY76jxwiq7T4T85eG8spwwYoFr\nQJAS0y5pQFHB5ky+M42etODqWn7v/ghk9+PnvEffeFwRIZLrMGLRSsn/tnbIPpSn\n989z2Z6aX8d+QNUIqbhIt0BC8iLnNdWPghCwaAX2RnqwHs5P7V06ENvFnFOyxgCi\nL4DhhCap7cdWSFXSKr9685GT4PRw9xUO75zruWwUPKvHyPmnWWMGlXS/5D5D+trr\nQX0VhTrfAgMBAAECggEAKFImnhSMGnSvmpcuA/89TtVkn6LuZYzvxq4y1EJrLvdn\noa5mxi2hmXv1RI6xkRNzO2nFjGeRp12BgjZLswVgPljGOq6QIRCAswUl5oj+BhRD\n7yesVebMyw0x8vCXELdH0dscMX6hXfMe4AOxxxxfO4wirsdUxpuAbAZDirAD+quF\nHjHBR4sROiyPwld09xpwtchg5Pv7PWgDQWS5uNsE02lP9DvceE4S7gkGcPnAHREn\niNZLhi8EzeyPHAyePC6qbhctLPp0RRB5yscCWNTLW8QdgxghGXU4uff3I3WrM8ys\nzY7nPfXU3+W+Kxt9Qz22bTgAsWlmBlMrtmII8IHNSQKBgQDjUtGp2j8VhRkTlUip\nw1A5tMXbfdojz4HSptK+FgVHn2stkcyAAyeBOk9XPqycaOGtr1XCw46+9d7/re3w\nEnbAWUFawGNL9DE1HDHNGzESEOOFXB/YqpmieSTHnBNktkbPZoh63ID1mdqAutXC\nfY4kBKCmcXmOCSTD18pFQjir+QKBgQDCUQM9f+WtOspePdOfkfMi9ugH1acQBnLg\nwWXf4qCMzJlkQ5DHl2pGCDritND8q+0Ol7PxG+MqSLzIWj95K0T441ul5MqMgQt7\neUX4+3ztHxzXapa+325fvkVsAe+rCSGdTiPv9Hky8NuChUD5FXerttF2A0rtwACj\n+/VnNOrjlwKBgCbVszPNMRHAFzSdQDigQUhadaK1sVqEzKzsCvymTmiZ93llewYz\ng8Az8fH5pwthb4x4c7I+Lli5MEuFryxgOyFJHj0euNf086MbGfbsOknX0MarolUl\nb/GtuHbRVw7RhO9RmOdEXFOY4Rc00+fSA0Jr9/X3VMjLDxjV+Jw0X/IhAoGBALC4\n4vyEnAqvBCsviY5/OyPab+3oJpcRdlroKNG0F5XV4c9pD3wmPfXROFTEY6jYZ7E0\n0OcAGNp06pRAV6fIbWL5y3kVmgystBeolLW7asNw31cezlhrdcNHlXWEUUc498RL\ns61gpvVoTgrnS8788ihODTOKDRQq4apB1M73NoSjAoGAW9hnpG8cj2deOAqgblKC\nsnjKQzTXawPV/x7ZC3URhXVnybbZaa6MyAYNSJGOuAL/iFIlK0oATa4JUEPXnGKx\neww0Hpcy5EqfSd3b9+/ZywEHgEB+aycRC5Y46f4N9XzyUBmbs5x37pFKyjMyWXpD\nHYLxogjow5xycOsHGJkt1n8=\n-----END PRIVATE KEY-----\n";
            $data['client_email'] = "firebase-adminsdk-z6njq@kadamaty-36d45.iam.gserviceaccount.com";
            $data['client_id'] = "105307528991280075666";
            $data['auth_uri'] = "https://accounts.google.com/o/oauth2/auth";
            $data['token_uri'] = "https://oauth2.googleapis.com/token";
            $data['auth_provider_x509_cert_url'] = "https://www.googleapis.com/oauth2/v1/certs";
            $data['client_x509_cert_url'] = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-z6njq%40kadamaty-36d45.iam.gserviceaccount.com";
            $json = json_encode($data);
            return $json;
        }

        public function setUpFirebase()
        {
            $json = $this->fireBaseConnectionJson();
            $serviceAccount = ServiceAccount::fromJson($json);
            $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->withDatabaseUri('https://lahzah-d2743.firebaseio.com/')
                ->create();
            $database = $firebase->getDatabase();
            return $database;
        }


        public function notificationHandler($title, $body, $users_ids, $notiType = null, $notiTypeId = null)
        {
            $data["title"] = $title;
            $data["body"] = $body;
            $notification = Notification::create($data);
            foreach ($users_ids as $user_id) {
                $user = User::find($user_id);
                // dd($user);
                $data["user_id"] = $user_id;
                $data["notification_id"] = $notification->id;
                $data["user_id"] = $user->id;
                User_notification::create($data);
                if ($user->fire_base_token) {
                    $this->push_notification([$user->fire_base_token], $data);
                }
            }
        }


        function push_notification($tokens, $data = [])
        {

            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);
            $notificationBuilder = new PayloadNotificationBuilder($data['title']);
            $notificationBuilder->setBody($data['body'])
                ->setSound('default');
//        $notificationBuilder->setclickAction($data['type']);
//        $notificationBuilder->setChannelId($data['item_id']);

            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData($data);
            $option = $optionBuilder->build();
            $notification = $notificationBuilder->build();
            $data = $dataBuilder->build();
            $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();
            $downstreamResponse->tokensToDelete();
            $downstreamResponse->tokensToModify();
            $downstreamResponse->tokensToRetry();
            $downstreamResponse->tokensWithError();

//        $optionBuilder = new OptionsBuilder();
//        $optionBuilder->setTimeToLive(60 * 20);
//        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
//        $notificationBuilder->setBody($data['title'])
//            ->setSound('default');
//        $dataBuilder = new PayloadDataBuilder();
//        $dataBuilder->addData($data);
//        $option = $optionBuilder->build();
//        $notification = $notificationBuilder->build();
//        $data = $dataBuilder->build();
//        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
//        $downstreamResponse->numberSuccess();
//        $downstreamResponse->numberFailure();
//        $downstreamResponse->numberModification();
//        $downstreamResponse->tokensToDelete();
//        $downstreamResponse->tokensToModify();
//        $downstreamResponse->tokensToRetry();
//        $downstreamResponse->tokensWithError();

        }

        public function generatePassCode()
        {
            return rand(1111, 9999);
        }

        public function sms_verify($user)
        {
            // todo Send Sms Code ( passCode To Verify User Mobile ) ...
//            $rand = rand(1111, 9999);
            $rand = 123456;
            $user->update(['MobileCode' => $rand]);
            $msg = "your active code for printing App is : " . $rand;
            $this->sendSms($msg, $user);
        }

        public function email_verify($request, $user)
        {
            // todo Send Email Code ( passCode To Verify User Email ) ...
            $rand = 123456;
            $user->update(['passCode' => $rand]);
            Mail::to($user->email)->send(new EmailVerification($user));
        }
        public
        function sendSms($msg, $client)
        {
            $username = "isjgmc";
            $password = "12345678";
            $destinations = "966" . $client->mobile;
            $messages = $msg;
            $sender = "GMC";
            $url = "http://www.goldenskills.sa/api/sendsms.php?username=$username&password=$password&message=$messages&numbers=$destinations&sender=$sender&unicode=E&return=full";
           //dd($url);
            $client = new  Client();
            $res = $client->get($url);
            //dd($res);
            $result = $res->getBody()->getContents();
            //dd($result);
            return $result;
        }

/*        public function sendSms($msg, $client)
        {
            $url = "http://www.goldenskills.sa/api/sendsms.php";
            $data = [
                "username" => "GMC-AD",
                "password" => "lehzah",
                "numbers" => '00966' . $client->mobile,
                "sender" => "training",
                "message" => $msg
            ];

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    "Cookie: userLang=Ar; SERVERID=MBE1"
                ),
            ));
            $response = curl_exec($curl);
           $d= curl_close($curl);
            dd($d);

        }*/




        // Order Send Notification
        public function fireBaseHandlerOrderNotification(Order $order)
        {
            $client = $order->client;
            $token = null;
            switch ($order->order_status_id) {
                case OrderStatus::driverAcceptOrder:
                case OrderStatus::driverInHisWayToPickup:
                case OrderStatus::driverArrivedToPickup:
                case OrderStatus::driverInHisWayToDestination:
                case OrderStatus::driverArrivedToDestination:
                case OrderStatus::driverCancelOrder:
                case OrderStatus::driverCompleteOrder:
                case OrderStatus::driverNotFound:
                    {
                        $token = $client->fire_base_token;
                        $user_id = $client->id;
                        break;
                    }
                case OrderStatus::clientCancelOrder:
                    {
                        if ($order->driver != null) {
                            $token = $order->driver->fire_base_token;
                            $user_id = $order->driver_id;
                        } else {
                            break;
                        }

                    }
            }


            $user_lang = User::find($user_id);
            if ($user_id)
                $lang = $user_lang->lang;
            else
                $lang = 'ar';
            // change app lang
            $this->setLocaleLang($lang);

            $data['body'] = $order->orderStatusrel->serv_name . ' ( ' . trans('language.order_id') . "  : $order->id " . ' ) ';
            $data['order_id'] = $order->id;
            // save notification  :-push and mysql
            $this->SendPSNotificationHandler($token, $data, $user_id);
        }

        public function SendPSNotificationHandler($token, $data, $user_id)
        {
            $data['title'] = trans('language.app_name');
            $data['fire_base_token'] = $token;
            $data["notification_type_id"] = '1';
            $notification = Notification::create($data);
            $users_ids = User::where('id', $user_id)->pluck('id')->unique();
            foreach ($users_ids as $user_id) {
                $user = User::find($user_id);
                $data["user_id"] = $user_id;
                $data["notification_id"] = $notification->id;
                $data["user_id"] = $user->id;
                User_notification::create($data);
                if ($token) {
                    $this->push_notification($token, $data);
                }
            }
        }

        public function setLocaleLang($lang)
        {
            session()->put('lang', $lang);
            app()->setLocale($lang);
        }


        public function sendVerfiyMail($user)
        {
            Mail::to($user->email)->send(new EmailVerification($user));
        }

        public function confirm_password($request, $user)
        {
          //  dd($request->course_id);
            // todo Send Email confirm Register in course ...
           $d= Mail::to($user->email)->send(new confirmRegister($user));
           //dd($d);
        }
        public function register_Course($request, $user)
        {
          //  dd($request->course_id);
            // todo Send Email confirm Register in course ...
           $d= Mail::to($user->email)->send(new registerCourse($user));
       // dd($d);
        }



     public function send_link_email( $orders)
        {
           // dd($orders);
          //  dd($request->course_id);

            foreach($orders as $order) {
                Mail::to($order->email)->send(new sendEmail($order));

            }

        }


    }
