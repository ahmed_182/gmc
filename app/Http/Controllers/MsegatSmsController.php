<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MsegatSmsController extends Controller
{
    protected $url;
    protected $data;

    public function __construct($msg, $phoneNumber)
    {
        $this->url = "https://www.msegat.com/gw/sendsms.php";
        $this->data = [
            "userName" => "Maher-new",
            "numbers" => $phoneNumber,
            "userSender" => "MSEGAT-AD",
            "apiKey" => "cddfd64ded5c187dcbb3e98746d9cde1",
            "msg" => $msg
        ];
    }

    public function sendSms()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($this->data),
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json",
                "Cookie: userLang=Ar; SERVERID=MBE1"
            ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $filtered_data = json_decode($response);
        $result = $this->filterResponse($filtered_data);
        return $result;

    }

    public function filterResponse($response)
    {
        $errors = array(
            'M0001' => [
                'en' => 'Variables missing',
                'ar' => 'المتغيرات غير مكتمله'
            ],
            'M0002' => [
                'en' => 'Invalid login info',
                'ar' => 'بيانات الحساب غير صحيحه'
            ],
            'M0022' => [
                'en' => 'Exceed number of senders allowed',
                'ar' => 'لا يوجد رصيد'
            ],
            'M0023' => [
                'en' => 'The service not available for now',
                'ar' => 'الخدمة غير متوفرة الآن'
            ],
            '1120' => [
                'en' => 'Mobile numbers is not correct',
                'ar' => 'رقم الهاتف غير صحيح'
            ],
            'M0024' => [
                'en' => 'Sender Name should be in English or number',
                'ar' => 'اسم الراسل يجب ان يكون باللغه الانجليزيه'
            ],
            'M0027' => [
                'en' => 'Activation Code is not Correct',
                'ar' => 'كود التفعيل غير صحيح' //'خطأ فى عنوان الوصول'
            ],
            '1010' => [
                'en' => 'Variables missing',
                'ar' => 'المتغيرات غير مكتمله'
            ],
            '1020' => [
                'en' => 'Invalid login info',
                'ar' => 'بيانات الحساب غير صحيحه'
            ],
            '1050' => [
                'en' => 'MSG body is empty',
                'ar' => 'نص الرساله فارغ'
            ],
            '1060' => [
                'en' => 'Balance is not enough',
                'ar' => 'لا يوجد لديك رصيد متاح'
            ],
            '1061' => [
                'en' => 'MSG duplicated',
                'ar' => 'رساله مكرره'
            ],
            '1110' => [
                'en' => 'Sender name is missing or incorrect',
                'ar' => 'اسم الراسل غير صحيح او غير موجود'
            ],
            '1140' => [
                'en' => 'MSG length is too long',
                'ar' => 'محتوي الرساله اكبر من اللازم'
            ],
        );
        $search = $this->searchByValue($response->code, $errors);
        if (isset($search) && !empty($search))
            return $search;
        else
            return false;
    }

    function searchByValue($code, $array)
    {

        foreach ($array as $key => $val) {
            if ($key == $code) {
                $resultSet['en'] = $val['en'];
                $resultSet['key'] = $key;
                $resultSet['ar'] = $val['ar'];
                return $resultSet;
            }
        }
        return null;
    }

    // Call functions :-
    public function sendSmsCode($phone, $msg)
    {

        //stop sending sms
        $result = $this->sendSmsCode($request->phone, $msg);
        if (!empty($result['ar']))
            return $this->sendError($result['ar']);
        //func
        $sms = new MsegatSmsController($msg, $phone);
        return $result = $sms->sendSms();
    }

}
