<?php

    namespace App\Http\Controllers\Admin;

    use App\Exports\report_orders_advertisement;
    use App\Exports\report_ordersExport;
    use App\Http\Controllers\Controller;
    use App\Order;
    use App\Setting;
    use App\Sponsored_advertisements;
    use App\User;

    use Auth;
    use Illuminate\Http\Request;
    use Maatwebsite\Excel\Facades\Excel;


    class ExcelController extends Controller
    {

        public function report_orders($id)
        {

            $orders = Order::where("course_id",$id)->get();
            return Excel::download(new report_ordersExport($orders), 'report_orders.xlsx');
        }

        public function orders_advertisement($id)
        {

            $orders = Sponsored_advertisements::where("course_id",$id)->get();
            return Excel::download(new report_orders_advertisement($orders), 'orders_advertisement.xlsx');
        }







    }
