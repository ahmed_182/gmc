<?php

    namespace App\Http\Controllers\Admin\Trainer;

    use App\ModulesConst\UserTyps;
    use App\Trainer;
    use App\Traits\storeImage;
    use App\User;
    use Hash;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;
        public function __construct()
        {
            $this->middleware('MustBeAdmin')->except('logout');
        }
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $items = User::where('user_type_id', UserTyps::trainer);
            $items = $this->filter($request, $items);
            return view('admin.trainers.index', compact('items'));

        }

        public function filter($request, $items)
        {

            if ($request->name)
                $items = $items->where("name", 'LIKE', '%' . $request->name . '%');

            $items = $items->orderBy("id", "desc")->paginate(10);
            return $items;
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.trainers.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->all();
           // dd($data);

            $data['user_type_id'] = UserTyps::trainer;
            if ($request->image) {
                $data['image'] = $this->storeImage($request->image);
            }


            $user = User::create($data);
            $data['user_id'] = $user->id;
            $data['name'] = $user->name;

            $trainer = Trainer::create($data);
       //dd($trainer);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/trainers'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = User::findOrFail($id);
            $trainer = Trainer::where('user_id', $item->id)->first();
            return view('admin.trainers.edit', compact('item','trainer'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = User::findOrFail($id);
            $data = $request->all();
            if ($request->image) {
                $data['image'] = $this->storeImage($data['image']);
            }
            $item->update($data);
            $trainer = Trainer::where('user_id', $item->id)->first();
            $trainer->save();
            $trainer->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/trainers'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = User::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/trainers'));
        }
    }
