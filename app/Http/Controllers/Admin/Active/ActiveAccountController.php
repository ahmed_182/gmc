<?php

    namespace App\Http\Controllers\Admin\Active;

    use App\Course;
    use App\ModulesConst\blockStatus;
    use App\ModulesConst\UserVerify;
    use App\Order;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class ActiveAccountController extends Controller
    {
        public function index($id)
        {
            $user = User::find($id);
            $user->userVerify = UserVerify::yes;
            $user->save();
            // Send Notifiction to user to inform him that his account has been activated ..
            $user_ids = User::where('id', $user->id)->pluck('id');
            $body = trans('language.admin_active_you_account');
            $this->notificationHandler(trans("language.appName"), $body, $user_ids);
            session()->flash('success', trans('language.done'));
            return back();
        }
        public function paid_action($id)
        {
            $order = Order::find($id);
            $order->paid = UserVerify::yes;
            $order->save();
            session()->flash('success', trans('language.paid'));
            return back();
        }
        public function not_paid_action($id)
        {
            $order = Order::find($id);
            $order->paid = UserVerify::no;
            $order->save();
            session()->flash('success', trans('language.not_paid'));
            return back();
        }

        public function confirm_course($id)
        {
            $course = Course::find($id);
            $course->confirm = UserVerify::yes;
            $course->save();
            session()->flash('success', trans('language.confirm_courses'));
            return back();
        }
public function not_confirm_course($id)
        {
            $course = Course::find($id);
            $course->confirm = UserVerify::yes;
            $course->save();
            session()->flash('success', trans('language.not_confirm_courses'));
            return back();
        }


        public function drActive_account($id)
        {
            $user = User::find($id);
            $user->userVerify = UserVerify::no;
            $user->save();
            // Send Notifiction to user to inform him that his account has been baned ..
            $user_ids = User::where('id', $user->id)->pluck('id');
            $body = trans('language.admin_deactive_you_account');
            $this->notificationHandler(trans("language.appName"), $body, $user_ids);
            session()->flash('success', trans('language.done'));
            return back();
        }

        public function send_email($id)
        {
            $course = Course::where('confirm',1)->find($id);
            //dd($course);
            // Send Notifiction to user to inform him that his account has been baned ..
            $orders= Order::where('course_id', $course->id)->where('paid',1)->get();
            //dd($users);

            $this->send_link_email($orders);

            session()->flash('success', trans('language.done_link_email'));
            return back();
        }

        public function add_offer($id)
        {
            $course = Course::find($id);
            $course->is_special = 1;
            $course->save();
            session()->flash('success', 'تم اضافة الدورة للعروض');
            return back();
        }
        public function cancel_offer($id)
        {
            $course = Course::find($id);
            $course->is_special = 0;
            $course->save();
            session()->flash('success', 'تم الغاء العرض');
            return back();
        }
    }
