<?php

namespace App\Http\Controllers\Admin\Archive;
use Carbon\Carbon;

use App\Course;
use App\Traits\storeImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $items = Course::where("end_date","<",$date);
        $items = $this->filter($request, $items);
        return view('admin.courses.archives', compact('items'));
    }

    public function filter($request, $items)
    {

        if ($request->name)
            $items = $items->where("name", 'LIKE', '%' . $request->name . '%');

        $items = $items->orderBy("id", "desc")->paginate(10);
        return $items;
    }
    public function destroy($id)
    {
        Course::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/archives'));
    }
}
