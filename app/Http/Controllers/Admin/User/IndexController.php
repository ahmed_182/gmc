<?php

    namespace App\Http\Controllers\Admin\User;

    use App\ModulesConst\UserTyps;
    use App\Traits\storeImage;
    use App\User;
    use Hash;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;
        public function __construct()
        {
            $this->middleware('MustBeAdmin')->except('logout');
        }
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            $items = User::where('user_type_id', UserTyps::user);
            $items = $this->filter($request, $items);
            return view('admin.users.index', compact('items'));

        }

        public function filter($request, $items)
        {

            if ($request->name)
                $items = $items->where("name", 'LIKE', '%' . $request->name . '%');
            if ($request->email) {
                $items = $items->where("email", 'LIKE', '%' . $request->email . '%');
            }
            if ($request->phone) {
                $items = $items->where("mobile", 'LIKE', '%' . $request->phone . '%');
            }
            $items = $items->orderBy("id", "desc")->paginate(10);
            return $items;
        }


        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.users.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->all();
            $data['password'] = Hash::make($data['password']);
            $data['user_type_id'] = UserTyps::user;
            if ($request->image) {
                $data['image'] = $this->storeImage($request->image);
            }

            $user = User::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/users'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = User::findOrFail($id);
            return view('admin.users.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = User::findOrFail($id);
            $data = $request->all();
            if ($request->password) {
                $data['password'] = Hash::make($data['password']);
            }
            if ($request->image) {
                $data['image'] = $this->storeImage($data['image']);
            }
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/users'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = User::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/users'));
        }
    }
