<?php

namespace App\Http\Controllers\Admin\Zoom;

use App\Course;
use App\Events\MeetingCreationEvent;
use App\Http\Controllers\Controller;
use App\MeetingRoom;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ZoomController extends Controller
{
    public function create(): View
    {
        $courses = Course::all();
        $rooms = MeetingRoom::all();
        return view('admin.zoom.create', compact('courses', 'rooms'));
    }

    public function generateMeeting(Request $request): View
    {
        $room = json_decode($request->get('room'));
        $course = Course::with('trainer.user')->findOrFail($request->get('course_id'));
        $res = createZoomMeeting($room->host_id, $room->jwt, $request->all())['response'];
        event(new MeetingCreationEvent($course, $res));

        return view('admin.zoom.details', $this->getMeetingDetails($course, $res));
    }

    private function getMeetingDetails($course, $res): array
    {
        return [
            'course_name' => $course->name,
            'meeting_id' => $res->id,
            'meeting_host_id' => $res->host_id,
            'meeting_topic' => $res->topic,
            'meeting_password' => $res->password,
            'meeting_url' => $res->join_url,
            'meeting_duration' => $res->duration,
            'meeting_start_time' => $res->start_time,
        ];

    }
}
