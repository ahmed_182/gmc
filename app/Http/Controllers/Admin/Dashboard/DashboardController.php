<?php

    namespace App\Http\Controllers\Admin\Dashboard;

    use App\Order;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class DashboardController extends Controller
    {
        public function index()
        {
          /* $orders = Order::latest()->take(5)->get();*/
            return view('admin.dashboard.index');
        }
    }
