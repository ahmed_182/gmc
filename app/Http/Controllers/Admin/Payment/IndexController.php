<?php

    namespace App\Http\Controllers\Admin\Payment;

    use App\Payment_type;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {

        public function index()
        {
            $items = Payment_type::orderBy('id', 'desc')->paginate(10);
            return view('admin.payment_types.index', compact('items'));
        }

        public function create()
        {
            return view('admin.payment_types.create');
        }

        public function store(Request $request)
        {
            $data = $request->validate([
                'name_ar' => 'required',
                'name_en' => 'required'
            ]);
            Payment_type::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/payment_types'));
        }

        public function show($id)
        {
            //
        }

        public function edit($id)
        {
            $item = Payment_type::findOrFail($id);
            return view('admin.payment_types.edit', compact('item'));
        }


        public function update(Request $request, $id)
        {
            $item = Payment_type::findOrFail($id);
            $data = $request->validate([
                'name_ar' => '',
                'name_en' => ''
            ]);

            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/payment_types'));
        }


        public function destroy($id)
        {
            Payment_type::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/payment_types'));
        }
    }
