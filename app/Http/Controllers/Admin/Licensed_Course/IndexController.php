<?php

namespace App\Http\Controllers\Admin\Licensed_Course;

use App\Country;
use App\Licensed_courses;
use App\Open_screen;
use App\Slider;
use App\Traits\storeImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Licensed_courses::orderBy('id', 'desc')->paginate(10);
        return view('admin.licensed_courses.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.licensed_courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'no_accreditation' => 'required',
            'name' => 'required',
        ]);

        Licensed_courses::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/licensed_course'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Licensed_courses::findOrFail($id);
        return view('admin.licensed_courses.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Licensed_courses::findOrFail($id);
        $data = $request->validate([
            'no_accreditation' => '',
            'name' => '',
        ]);

        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/licensed_course'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Licensed_courses::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/licensed_course'));
    }
}
