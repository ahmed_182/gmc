<?php

namespace App\Http\Controllers\Admin\Service;

use App\Product;
use App\Product_images;
use App\ProductImage;
use App\Service;
use App\ServiceImages_required;
use App\Traits\storeImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImagerequiredController extends Controller
{
//    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($service_id)
    {
        $items = ServiceImages_required::where('service_id', $service_id)->orderBy('id', 'desc')->paginate(10);
        return view('admin.services.images.index', compact('items', 'service_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($service_id)
    {
        $service = Service::findorfail($service_id);
        return view('admin.services.images.create', compact('service'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $service_id)
    {
        $data = $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
            ]);

        $data["service_id"] = $service_id;
        ServiceImages_required::create($data);
        session()->flash('success', trans('language.done'));
        return redirect("admin/services/$service_id/image_required");
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($service_id, $id)
    {
        $item = ServiceImages_required::findOrFail($id);
        $item->delete();
        session()->flash('success', trans('language.done'));
        return redirect("admin/services/$service_id/image_required");
    }
}
