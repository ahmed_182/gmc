<?php

namespace App\Http\Controllers\Admin\Course;
use Carbon\Carbon;
use Auth;
use App\Course;
use App\Traits\storeImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');
        $items = Course::where("end_date", '>=',$date);

        $items = $this->filter($request, $items);
        return view('admin.courses.index', compact('items'));
    }

    public function markter_courses(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');

        $items = Course::where("end_date", '>=',$date);
        $items = $this->filter($request, $items);

        return view('admin.courses.markter_courses', compact('items'));
    }

    public function coordinators_courses(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');

        $items = Course::where("coordinator_id",Auth::user()->id)->where("end_date", '>=',$date);
        $items = $this->filter($request, $items);

        return view('admin.courses.coordinators_courses', compact('items'));
    }
    public function financial_management(Request $request)
    {
        $date = Carbon::now()->format('Y-m-d');

        $items = Course::where("end_date", '>=',$date);
        $items = $this->filter($request, $items);

        return view('admin.courses.financial_management', compact('items'));
    }


    public function filter($request, $items)
    {

        if ($request->name)
            $items = $items->where("name", 'LIKE', '%' . $request->name . '%');
        $items = $items->orderBy("id", "desc")->paginate(10);
        //dd($items);

        return $items;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }

        $course = Course::create($data);

        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/courses'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Course::findOrFail($id);
        //dd($item);
        return view('admin.courses.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Course::findOrFail($id);
        $data = $request->all();
        //dd($data);
        if ($request->image) {
            $data['image'] = $this->storeImage($data['image']);
        }
        $item->update($data);

        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/courses'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Course::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/courses'));
    }
}
