<?php

    namespace App\Http\Controllers\Admin\Contract_courses;

    use App\City;
    use App\Contract_course;
    use App\Country;
    use App\Open_screen;
    use App\Order;
    use App\Order_extra_products;
    use App\Order_image;
    use App\Order_products;
    use App\Order_status;
    use App\Order_status_log;
    use App\ServiceImages_required;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function __construct()
        {
            $this->middleware('MustBeAdmin')->except('logout');
        }
        public function index(Request $request )
        {
            $items = Contract_course::query();
            $items = $this->filter($request, $items);

            return view('admin.contract_courses.index', compact('items'));
        }




    public function filter($request, $items)
    {
        if ($request->mobile) {
            $items = $items->where("mobile",'LIKE', '%' . $request->mobile. '%');

            //dd($items);*/
        }
        if ($request->start_at and $request->end_at) {
            $from = $request->start_at;
            $to = $request->end_at;
            $items = $items->whereBetween('created_at', ["$from", "$to"]);
        }
        $items = $items->orderBy("id", "desc")->paginate(10);


        return $items;
    }



        public function destroy($id)
        {
            $test=Contract_course::findOrFail($id)->delete();
           // dd($test);
            session()->flash('success', trans('language.done'));
            return back();
        }




    }
