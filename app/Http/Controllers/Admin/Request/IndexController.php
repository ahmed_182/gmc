<?php

    namespace App\Http\Controllers\Admin\Request;


    use App\Order;
    use App\Sponsored_advertisements;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function __construct()
        {
            $this->middleware('MustBeAdmin')->except('logout');
        }
        public function order_requests(Request $request ,$id)
        {
            $items = \App\Request::where("course_id",$id);
            $items = $this->filter($request, $items);

            return view('admin.order_requests.index', compact('items'));
        }

        public function orders_advertisement(Request $request ,$id)
        {
           // dd("dfgdf");
            $items = Sponsored_advertisements::where("course_id",$id);
            $items = $this->filter($request, $items);

            return view('admin.orders_advertisements.index', compact('items','id'));
        }
        public function index(Request $request )
        {
            $items = \App\Request::query();
            $items = $this->filter($request, $items);

            return view('admin.order_requests.index', compact('items'));
        }




    public function filter($request, $items)
    {
        if ($request->mobile) {
            $items = $items->where("mobile",'LIKE', '%' . $request->mobile. '%');

            //dd($items);*/
        }
        if ($request->start_at and $request->end_at) {
            $from = $request->start_at;
            $to = $request->end_at;
            $items = $items->whereBetween('created_at', ["$from", "$to"]);
        }
        $items = $items->orderBy("id", "desc")->paginate(10);


        return $items;
    }



        public function destroy($id)
        {
            $test=\App\Request::findOrFail($id)->delete();
           // dd($test);
            session()->flash('success', trans('language.done'));
            return back();
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Sponsored_advertisements::findOrFail($id);

            return view('admin.order_requests.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Sponsored_advertisements::findOrFail($id);

            $data = $request->validate([

                'status' => '',
            ]);

            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/courses/$item->course_id/orders_advertisement"));
        }




    }
