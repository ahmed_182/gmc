<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Request extends Model
{
    protected $fillable = [
        'name',
        'email',
        'mobile',
        'country_id',
        'course_id',
        'created_at',

    ];

    public function getDashCourseAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->course)
            $attribute = $this->course->name;
        return $attribute;
    }

    public function getDashCodeAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->country)
            $attribute = $this->country->code;
        return $attribute;
    }

    public function course()
    {
        return $this->belongsTo('App\Course', 'course_id');
    }
    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }
}
