<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Setting extends Model
    {
        protected $fillable =
            [
                'logo',
                'address',
                'mobile',
                'email',
                'facebook',
                'twitter',
                'instagram',
                'terms_ar',
               'whatsapp',

                'terms_ar',
                'tax',
                'delivery_cost'
            ];


        public function toArray()
        {
            $data['logo'] = $this->logo;
            $data['text'] = $this->serv_text;

            return $data;
        }

        public function getServTextAttribute()
        {
            if (request()->lang == "ar")
                return $this->name_ar;
            else
                return $this->name_en;
        }


  public function getServAboutAttribute()
        {
            if (request()->lang == "ar")
                return $this->about_ar;
            else
                return $this->about_en;
        }

        public function getServWorkTimeAttribute()
        {
            if (request()->lang == "ar")
                return $this->text_ar;
            else
                return $this->text_en;
        }

        public function getServTermsAttribute()
        {
            if (request()->lang == "ar")
                return $this->terms_ar;
            else
                return $this->terms_en;
        }

        public function getServCurrencyAttribute()
        {
            return trans('language.app_currency');
        }

    }
