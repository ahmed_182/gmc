<?php

namespace App;

use App\ModulesConst\UserVerify;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'trainer_id',
        'coordinator_id',
        'image',
        'name',
        'price',
        'date',
        'end_date',
        'duration',
        'description',
        'breif',
        'features',
        'link',
        'offer',
        'link_youtube',
        'category_id',
        'is_special',
        'confirm',

    ];
    public function getDashConfirmAttribute()
    {
        if ($this->confirm == UserVerify::yes) {
            return trans("language.confirm_courses");
        } else {
            return trans("language.not_confirm_courses");
        }
    }

    public function getDashCurrentPositionAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->current_position)
            $attribute = $this->current_position;
        return $attribute;
    }
    public function getDashNameAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->name)
            $attribute = $this->name;
        return $attribute;
    }
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function trainer()
    {
        return $this->belongsTo('App\Trainer', 'trainer_id');
    }
}
