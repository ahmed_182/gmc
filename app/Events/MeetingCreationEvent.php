<?php

namespace App\Events;

use App\Course;
use Illuminate\Foundation\Events\Dispatchable;

class MeetingCreationEvent
{
    use Dispatchable;

    public $course , $meeting;

    public function __construct(Course $course , $meeting)
    {
        $this->course = $course;
        $this->meeting = $meeting;
    }
}
