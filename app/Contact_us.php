<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact_us extends Model
{
    protected $fillable = [
        'name',
        'email',
        'mobile',
        'title',
        'message',
        'created_at',

    ];

    public function join_from()
    {
        return $this->created_at->diffforhumans();
    }
}
