<?php

namespace App\Providers;

use App\Repositories\Brands\BrandsCacheRepository;
use App\Repositories\Brands\BrandsRepository;
use App\Repositories\Brands\BrandsRepositoryInterface;
use App\Repositories\Brands\PostsCacheRepository;
use App\Repositories\Brands\PostsRepositoryInterface;
use App\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer(['user.auth.register', '*'], function ($view) {
            $setting = Setting::find(1);
            $view->with('setting', $setting);
        });

    }
}
