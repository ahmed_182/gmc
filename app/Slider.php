<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = [
        "name_ar", "name_en",  "image" ,
    ];

    protected $casts = [
        'tax' => 'int',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['title'] = $this->serv_name;
        $data['image'] = $this->serv_image;
        return $data;
    }



    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }
    public function getServDescriptionAttribute()
    {
        if (\request()->lang == "en")
            return $this->description_en;
        else
            return $this->description_ar;
    }

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getServImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/logo.png');
    }
}
