<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'image',
        'name_ar',
        'name_en',
        'currency_id',
        'code',
        'start_with',
        'number_count',
        'identity_count',
        'identity_startWith',
        'iban_word',
        'iban_number_count',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['image'] = $this->serv_image;
        $data['name'] = $this->serv_name;
        $data['code'] = $this->code;
        $data['start_with'] = $this->start_with;
        $data['number_count'] = $this->number_count;
        //$data['identity_count'] = $this->identity_count;
        //$data['identity_startWith'] = $this->identity_startWith;
        $data['currency'] = $this->currency;
        //$data['iban_word'] = $this->iban_word;
       // $data['iban_number_count'] = $this->iban_number_count;
        return $data;
    }

    public function getServIsMyCountryAttribute()
    {

        if (Auth::guard("api")->user()) {
            $user = User::where('id', Auth::guard("api")->user()->id)->first();
            $check = User::where('id', $user->id)->where('country_id', $this->id)->first();
            if ($check) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getServImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/logo.png');
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getServCurrencyAttribute()
    {
        if (request()->lang == "en")
            return $this->curreny_en;
        else
            return $this->curreny_ar;
    }

//dashboard
    public function getDashImageAttribute()
    {
        $attribute = asset('assets/admin/images/logo.png');
        if ($this->image)
            $attribute = $this->image;
        return $attribute;
    }

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashCurrencyAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->curreny_en;
        else
            return $this->curreny_ar;
    }

    public function cities()
    {
        return $this->hasMany(City::class, 'country_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

}
