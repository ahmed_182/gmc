<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaire extends Model
{
    protected $fillable = [
        'name',
        'email',
        'qty1',
        'qty2',
        'qty3',
        'qty4',
        'qty5',
        'qty6',
        'qty7',
        'qty8',
        'qty9',
        'qty10',
        'qty11',
        'qty12',
        'qty13',
    ];
}
