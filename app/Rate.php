<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Rate extends Model
    {

        protected $fillable = [
            'order_id',
            'driver_rate',
            'driver_feedBack',
            'client_rate',
            'client_feedBack',
            'client_id',
            'driver_id'

        ];


        protected $visible = [
            'id',
            'driver_rate',
            'driver_feedBack',
            'client_rate',
            'client_feedBack'
        ];


        protected function client()
        {
            return $this->belongsTo(User::class, 'client_id');
        }

        protected function driver()
        {
            return $this->belongsTo(User::class, 'driver_id');
        }

        public function getDashNameAttribute()
        {
            $attribute = "غير محدد";
            if ($this->client)
                $attribute = $this->client->dash_name;
            return $attribute;
        }
        public function getDashClientImageAttribute()
        {
            $attribute = "غير محدد";
            if ($this->client)
                $attribute = $this->client->dash_image;
            return $attribute;
        }

        public function getDashDriverNameAttribute()
        {
            $attribute = "غير محدد";
            if ($this->driver)
                $attribute = $this->driver->dash_name;
            return $attribute;
        }
        public function getDashDriverImageAttribute()
        {
            $attribute = "غير محدد";
            if ($this->driver)
                $attribute = $this->driver->dash_image;
            return $attribute;
        }
    }
