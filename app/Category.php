<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        "name"
    ];

    public function getDashNameAttribute()
    {
        $attribute =  trans('language.notSelected');
        if ($this->name)
            $attribute = $this->name;
        return $attribute;
    }
}
