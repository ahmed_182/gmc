<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $fillable = [
        'name_ar',
        'name_en',
        'symbol_ar',
        'symbol_en',
        'code',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->serv_name;
        return $data;
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashNameAttribute()
    {
        return $this->getNameAttribute();
    }

    public function getSymbolAttribute()
    {
        if (\request()->lang == "en")
            return $this->symbol_en;
        else
            return $this->symbol_ar;
    }

    public function getDashSymbolAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->symbol_en;
        else
            return $this->symbol_ar;
    }


    public function country()
    {
        return $this->hasMany('App\Currency', 'currency_id');
    }
}
