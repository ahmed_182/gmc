<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class MeetingCreationEmail extends Mailable
{
    public $meeting , $course;

    public function __construct($course , $meeting)
    {
        $this->course = $course;
        $this->meeting = $meeting;
    }

    public function build(): MeetingCreationEmail
    {
        return $this->markdown('emails.meeting-creation-email');
    }
}
