<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $fillable = [
        'image',
        'link',
        'type',

    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['image'] = $this->image;
        $data['link'] = $this->link;
        $data['type'] = $this->type;
        return $data;
    }
}
