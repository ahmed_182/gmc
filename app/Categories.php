<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Null_;

class Categories extends Model
{
    protected $fillable = [
        "name_ar", "name_en", "parent_id", "image", "background"
    ];


    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->serv_name;
        $data['image'] = $this->serv_image;
        $data['has_subcategory'] = $this->serv_has_subcategory;
        return $data;
    }


    public function getServHasSubcategoryAttribute()
    {
        $attribute = "";
        if (count($this->children) > 0) {
            $attribute = true;
        } else {
            $attribute = false;
        }
        return $attribute;
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getServImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/logo.png');
    }

    public function parent()
    {
        return $this->belongsTo(Categories::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Categories::class, 'parent_id');
    }

    public function product()
    {
        return $this->hasMany(Product::class, 'category_id');
    }

    // recursive, loads all descendants
    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    public function getDashNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashImageAttribute()
    {
        $attribute = asset('assets/admin/images/default.png');
        if ($this->image)
            $attribute = $this->image;
        return $attribute;
    }

    /*  public function getDashImageAttribute()
      {
          if ($this->image) {
              return str_replace('app/public/', '', $this->image);
          } else {
              return asset('assets/admin/images/default.png');
          }

      }*/

    public function totalProducts()
    {
        $childrenIds = $this->getChildrenIds($this->id);
        $childrenIds[] = $this->id;
        return Product::whereIn('category_id', $childrenIds);

    }

    public function totalSearchProducts()
    {
        $childrenIds = $this->getChildrenIds($this->id);
        $childrenIds[] = $this->id;
        return Product::whereIn('category_id', $childrenIds);

    }


    public function totalSiteProducts()
    {
        $childrenIds = $this->getChildrenIds($this->id);
        $childrenIds[] = $this->id;
        return Product::whereIn('category_id', $childrenIds);

    }


    public function getChildrenIds($parentId = null)
    {

        if (is_null($parentId)) {
            $parentId = $this->id;
        }
        $childrenIds = Categories::where('parent_id', $parentId)->pluck('id')->toArray();
        foreach ($childrenIds as $childId) {
            $childrenIds = array_merge($childrenIds, $this->getChildrenIds($childId));
        }
        return $childrenIds;
    }


    public function getparentIds($Category_id = null)
    {

        if (is_null($Category_id)) {
            $Category_id = $this->id;
        }

        $chekcFromParent = Categories::where('id', $Category_id)->where("parent_id", null)->first();
        if ($chekcFromParent) {
            return [];
        } else {
            $cat = Categories::find($Category_id);
        }
        $childrenIds = Categories::where('id', $cat->id)->pluck('parent_id')->toArray();
        foreach ($childrenIds as $childId) {
            $cat = Categories::where("id", $childId)->where("parent_id", null)->first();
            if (!$cat) {
                $childrenIds = array_merge($childrenIds, $this->getparentIds($childId));
            }
        }
        return $childrenIds;
    }

    public function getDataofParents($childrenIds)
    {
        $data = Categories::whereIn("id", $childrenIds)->get();
        return $data;

    }


}
