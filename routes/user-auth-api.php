<?php

Route::group(['prefix' => 'user'], function () {
    Route::post('/profile', 'ProfileController@profile');
    Route::post('/change-password', 'ProfileController@updatePassword');
    Route::post('/update-profile', 'ProfileController@update_profile');
    Route::post('/active-profile', 'ProfileController@active');

    // orders :-
    Route::post('/orders', 'Order\IndexController@index');
    Route::post('/order-details', 'Order\IndexController@details');
    Route::post('/make-order', 'Order\IndexController@store');
});


// Fail Api
Route::fallback(function (Request $request) {
    $response['message'] = "Page Not Found.If error persists,contact info@wings.com";
    $response['statusCode'] = 404;
    $statusCode = 404;
    return \Response::json($response, $statusCode);
});
