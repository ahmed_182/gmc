<?php


    Route::get('/dash', 'Dashboard\DashboardController@index');
    // Public :-


    Route::resource('/users', 'User\IndexController');
    Route::resource('/markters', 'Markter\IndexController');
    Route::resource('/sliders', 'Slider\IndexController');
    Route::resource('/trainers', 'Trainer\IndexController');
    Route::resource('/coordinators', 'Coordinators\IndexController');
    Route::resource('/financial_managements', 'Financial_management\IndexController');
    Route::resource('/courses', 'Course\IndexController');
     Route::resource('/archives', 'Archive\IndexController');
     Route::resource('/offers', 'Offer\IndexController');
     Route::resource('/categories', 'Category\IndexController');
     Route::resource('/licensed_course', 'Licensed_Course\IndexController');


// Active , deActive accounts ( user ) :-
    Route::get('/active_account/{id}', 'Active\ActiveAccountController@index');
    Route::get('/deActive_account/{id}', 'Active\ActiveAccountController@drActive_account');

Route::get('/paid_action/{id}', 'Active\ActiveAccountController@paid_action');
Route::get('/not_paid_action/{id}', 'Active\ActiveAccountController@not_paid_action');

Route::get('/confirm_course/{id}', 'Active\ActiveAccountController@confirm_course');
Route::get('/not_confirm_course/{id}', 'Active\ActiveAccountController@not_confirm_course');

Route::get('/send_email/{id}', 'Active\ActiveAccountController@send_email');
Route::get('/add_offer/{id}', 'Active\ActiveAccountController@add_offer');
Route::get('/cancel_offer/{id}', 'Active\ActiveAccountController@cancel_offer');




Route::resource('/orders', 'Order\IndexController');//
Route::resource('/order_requests', 'Request\IndexController');//

Route::get('/courses/{id}/orders', 'Order\IndexController@orders');//
//archives
Route::get('/archives/{id}/orders', 'Order\IndexController@archives');//
//order_requests
Route::get('/courses/{id}/order_requests', 'Request\IndexController@order_requests');//*/
Route::get('/courses/{id}/orders_advertisement', 'Request\IndexController@orders_advertisement');//*/
Route::resource('/contract_courses', 'Contract_courses\IndexController');//
   Route::resource('/setting', 'Setting\IndexController');//

Route::get('/markter_courses', 'Course\IndexController@markter_courses');
Route::get('/coordinators_courses', 'Course\IndexController@coordinators_courses');
Route::get('/financial_management', 'Course\IndexController@financial_management');



    Route::resource('/contact', 'Contact\IndexController');

Route::get('/userNotifiy/{id}', 'User\UserNotificationController@index');
Route::post('/userNotifiyStore', 'User\UserNotificationController@userNotifiyStore');


//
Route::get('/export-report_orders/{id}', 'ExcelController@report_orders');
Route::get('/export-orders_advertisement/{id}', 'ExcelController@orders_advertisement');



///
Route::resource('/terms_and_conditions', 'Term\IndexController');//

//
Route::resource('/complaints', 'Complaint\IndexController');


/* ZOOM ROUTES */
Route::group(['prefix' => 'zoom', 'as' => 'zoom.', 'namespace' => 'Zoom'], function () {
    Route::get('create', 'ZoomController@create')->name('create');
    Route::post('generate', 'ZoomController@generateMeeting')->name('generate-meeting');
});
