<?php

    Route::group(['prefix' => 'user'], function () {
        Route::post('/login', 'User\LoginController@index');
        Route::post('/register', 'User\RegistrationController@index');
        Route::post('/social-login', 'User\SocialLoginUserController@loginBySocialMedia');
    });
    Route::post('/ministry', 'Ministry\IndexController@index');
    Route::post('/openScreen', 'openScreen\IndexController@openScreen');
    Route::post('/services', 'Service\IndexController@index');
    Route::post('/service-center', 'ServiceCenter\IndexController@index');
    Route::post('/receipt', 'Receipt\IndexController@index');
    Route::post('/about-app', 'Setting\IndexController@about');
    Route::post('/terms-app', 'Setting\IndexController@terms');
    Route::post('/work-time', 'Setting\IndexController@workTime');
    Route::post('/delivery-cost', 'Setting\IndexController@delivery_cost');
    Route::post('/settings', 'Setting\IndexController@settings');
    Route::post('/countries', 'Country\IndexController@index');
    Route::post('/cities', 'City\IndexController@index');
    Route::post('/districts', 'Distract\IndexController@index');
    Route::post('/list-districts', 'Distract\IndexController@list');
    Route::post('/contact-us', 'ContactUs\IndexController@index');
    Route::post('/forgetPassword', 'ResetPassword\IndexController@index');

    Route::post('/payment-types', 'PaymentType\IndexController@index');
    Route::post('/order-status', 'OrderStatus\IndexController@index');
    Route::post('/pdf_file', 'ContactUs\IndexController@pdf_file');

//ads
Route::post('/ads', 'Ads\IndexController@index');

// Fail Api
    Route::fallback(function (Request $request) {
        $response['message'] = "Page Not Found.If error persists,contact info@shopbeek.com";
        $response['statusCode'] = 404;
        $statusCode = 404;
        return \Response::json($response, $statusCode);
    });
