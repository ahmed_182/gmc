<?php

// Slider ( slider-click )
    Route::post('/slider-click', 'Slider\SliderClickController@index');

// miss password
   /* Route::post('/confirmPasswordCode', 'ResetPassword\IndexController@confirm_code');
    Route::post('/resendCode', 'ResetPassword\IndexController@resend_password_code');
    Route::post('/resetNewPassword', 'ResetPassword\IndexController@reset_new_password');*/
    Route::post('/updatePassword', 'ResetPassword\IndexController@updatePassword');

// FavouritePlaces
Route::post('/favourite-places', 'FavouritePlace\IndexController@index');
Route::post('/add-favourite-place', 'FavouritePlace\IndexController@store');
Route::post('/edit-favourite-place', 'FavouritePlace\IndexController@update');
Route::post('/remove-favourite-place', 'FavouritePlace\IndexController@remove');

// Notification
    Route::post('/user-notifications', 'Notification\IndexController@index');
    Route::post('/user-notifications-count', 'Notification\IndexController@count');
    Route::post('/user-seen-notifications', 'Notification\IndexController@seen');

    // Favourite
    Route::post('/user-favourites', 'UserFavourite\IndexController@index');
    Route::post('/add-product-favourite', 'UserFavourite\IndexController@add_item_favourite');
    Route::post('/remove-product-favourite', 'UserFavourite\IndexController@remove_item_favourite');


// Order Apis :-
    Route::post('/make-order', 'User\Order\IndexController@store');
    Route::post('/myOrders', 'User\Order\IndexController@index');
    Route::post('/order-details', 'User\Order\IndexController@details');
    Route::post('/order-mini-details', 'User\Order\DetailsController@mini_details');


// Fail Api
    Route::fallback(function (Request $request) {
        $response['message'] = "Page Not Found.If error persists,contact info@sqam.com";
        $response['statusCode'] = 404;
        $statusCode = 404;
        return \Response::json($response, $statusCode);
    });
