<?php

Route::get('/verfiyEmail/{id}/{code}', 'Admin\Auth\LoginController@verfiyEmail');
Route::view('/thanks_page', 'site.thanks_page');


Route::get('/admin', 'Admin\Auth\LoginController@index');
Route::get('/markter', 'Admin\Auth\LoginController@index');
Route::post('/adminlogin', 'Admin\Auth\LoginController@adminlogin');
Route::post('/adminlogout', 'Admin\Auth\LoginController@adminlogout');

//Confirm Register
Route::get('/register_form/{id}/{token}', 'Site\ConfirmRegister\IndexController@register_form');
Route::post('/confirm-register', 'Site\ConfirmRegister\IndexController@store');

//sponsored advertisement

Route::get('/sponsored_advertisement/{id}/{token}', 'Site\SponsoredAdvertisement\IndexController@sponsored_advertisement');
Route::post('/sponsored_advertisement', 'Site\SponsoredAdvertisement\IndexController@store');

Route::get('/', 'Site\Home\IndexController@index');
Route::get('/home', 'Site\Home\IndexController@index');
Route::get('/courses', 'Site\Home\IndexController@courses');
Route::get('/courseDetails/{id}', 'Site\Home\IndexController@courseDetails');
Route::post('/register-course', 'Site\Home\IndexController@registerCourse');

Route::get('/trainer-details/{id}', 'Site\Home\IndexController@trainerDetails');
Route::get('/offers', 'Site\Home\IndexController@offers');
Route::get('/archives', 'Site\Home\IndexController@archives');
Route::get('/table_courses', 'Site\Home\IndexController@table_courses');
Route::get('/licensed_courses', 'Site\Home\IndexController@licensed_courses');
Route::get('/contact', 'Site\Contact\IndexController@index');
Route::post('/contact-us', 'Site\Contact\IndexController@store');

Route::get('/contract_courses', 'Site\Contract_courses\IndexController@index');
Route::post('/make_contract_courses', 'Site\Contract_courses\IndexController@store');

// Search
Route::get('/search', 'Site\Search\IndexController@index');


// Terms And Conditions
Route::get('/terms_and_conditions', 'Site\TermsPage\IndexController@terms_and_conditions');

// Terms And Conditions
Route::get('/terms_and_conditions', 'Site\TermsPage\IndexController@terms_and_conditions');
Route::get('/e_learning', 'Site\TermsPage\IndexController@e_learning');
Route::get('/privacy', 'Site\TermsPage\IndexController@privacy');
Route::get('/property_rights', 'Site\TermsPage\IndexController@property_rights');
Route::get('/integrity', 'Site\TermsPage\IndexController@integrity');
Route::get('/cheat', 'Site\TermsPage\IndexController@cheat');


//
Route::get('/complaints', 'Site\Complaint\IndexController@index');
Route::post('/send_complaints', 'Site\Complaint\IndexController@store');


Route::get('/questionnaire', 'Site\Questionnaire\IndexController@index');
Route::post('/make_questionnaire', 'Site\Questionnaire\IndexController@store');


//register
Route::get('/register', 'Site\Register\IndexController@index');
Route::post('/siteRegisterForm', 'Site\Register\IndexController@siteRegisterForm');
Route::get('/verfiyMobilePage', 'Site\Register\IndexController@verfiyMobilePage');
Route::post('/verifyaccountWithemail', 'Site\Register\IndexController@verifyaccountWithemail');


//login
Route::get('/login', 'Site\Login\IndexController@index');
Route::post('/login_site', 'Site\Login\IndexController@login');
Route::get('/siteLogout', 'Site\Login\IndexController@siteLogout');

// User Profile
Route::get('/profile', 'Site\Profile\profileController@index');
Route::get('/editProfile', 'Site\Profile\profileController@editProfile');
Route::post('/updateProfile', 'Site\Profile\profileController@update');

/* ZOOM ROUTES */
Route::group(['prefix' => 'zoom', 'as' => 'zoom.', 'namespace' => 'Site\Zoom'], function () {
    Route::get('join', 'ZoomController@join')->name('join');
    Route::get('getRoom/{hostId}' , 'ZoomController@getRoom')->name('get-room');
});
