<a href="{{url("admin/$href")}}"
   class="{{$class}} btn-sm"
   data-toggle="tooltip"
   data-original-title="{{$title}}">

    <i class="" data-feather="{{$feather}}" ></i>
</a>
