@extends('admin.layout.table.index')
@section('page-title',trans('language.orders'))
@section('nav')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.order_requests')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.name_course')}}</th>

    <th>{{trans('language.client_name')}}</th>
    <th>{{trans('language.mobile')}}</th>
    <th>{{trans('language.email')}}</th>

    <th>{{trans('language.date')}}</th>

    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{$item->dash_course}}</td>
            <td>{{$item->name}}</td>
            <td> <a href="https://api.whatsapp.com/send?phone=+{{$item->dashcode}}{{$item->mobile}}" target="_blank">{{$item->mobile}}</a></td>
            <td>{{$item->email}}</td>
            <td>{{$item->created_at->format("Y-m-d")}}</td>

{{--
            <td>{{$item->dash_cost +$item->delivery_tax }}  {{trans('language.app_currency')}}   </td>
--}}

            <td>
                @includeIf("admin.components.buttons.delete",["message" =>  trans('language.order_number') . ' ( ' . $item->id . ' ) '  ,  "action" => url("admin/orders/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/order_requests/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="mobile" value="{{request()->mobile}}"
                       placeholder="{{trans('language.mobile')}}">
            </div>
            <div class="col-md-3">
                <input type="date" class="form-control start_at" name="start_at" value="{{request()->start_at}}"
                       placeholder="{{trans('language.start_at')}}">
            </div>

            <div class="col-md-3">
                <input type="date" class="form-control end_at " name="end_at" value="{{request()->end_at}}"
                       placeholder="{{trans('language.end_at')}}">
            </div>

            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>

        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');

        });
    </script>



@endsection
