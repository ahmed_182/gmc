@extends('admin.layout.table.index')
@section('page-title',trans('language.orders'))
@section('nav')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.orders_finished')}}</li>
    </ol>
@endsection
@section('thead')
    <th>{{trans('language.order_number')}}</th>
    <th>{{trans('language.user')}}</th>
    <th>{{trans('language.mobile')}}</th>

    <th>{{trans('language.date')}}</th>

    <th>{{trans('language.paymenttype')}}</th>
    <th>{{trans('language.receipttype')}}</th>
    <th>{{trans('language.service')}}</th>
    <th>{{trans('language.center_service')}}</th>

    <th>{{trans('language.total_price')}}</th>
{{--
    <th>{{trans('language.added_tax')}}</th>
--}}
{{--
    <th>{{trans('language.status')}}</th>
--}}
{{--
    <th>{{trans('language.order_images_requireds')}}</th>
--}}
    <th>{{trans('language.show')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
      {{--  {{dd($item->created_at)}}--}}
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{$item->DashLocationUserName}}</td>
            <td>{{$item->DashLocationMobile}}</td>
            <td>{{$item->created_at->format("Y-m-d")}}</td>
            <td>{{@$item->payment_type->dash_name}}</td>
            <td>{{$item->dash_receipt}}</td>
            <td>{{$item->dash_service}}</td>
            <td>{{$item->dash_service_center}}</td>
          {{--  <td>{{$item->dash_cost}}   {{trans('language.app_currency')}}  </td>
            <td>{{$item->delivery_tax}}   {{trans('language.app_currency')}}  </td>--}}
            <td>{{$item->dash_cost +$item->delivery_tax }}  {{trans('language.app_currency')}}   </td>
            {{--<td>{{$item->tax}}% <br>
                <span> <strong> {{   ($item->total_cost * $item->tax / 100 )  }}  {{trans('language.app_currency')}} </strong>  </span>
            </td>--}}
            {{--<td>
                {{@$item->favourite_place->name}}
                <br>
                <small> {{@$item->favourite_place->address}}</small>
            </td>--}}
{{--
            <td>{{$item->DashOrderStatues}}</td>
--}}
            {{--<td>
                @includeIf("admin.components.buttons.custom" , ["href" => "order_images_requireds/$item->id", 'class' => 'bi bi-bell' , 'title'=> trans('language.show'), 'feather' => 'list'])
            </td>--}}
            <td>
                @includeIf("admin.components.buttons.custom" , ["href" => "orders/$item->id", 'class' => 'bi bi-bell' , 'title'=> trans('language.show'), 'feather' => 'list'])
            </td>
            <td>
                @includeIf("admin.components.buttons.custom" , ["href" => "orders/$item->id/statues", 'class' => 'btn btn-warning' , 'title'=> trans('language.status'), 'feather' => 'plus-circle'])
                @includeIf("admin.components.buttons.delete",["message" =>  trans('language.order_number') . ' ( ' . $item->id . ' ) '  ,  "action" => url("admin/orders/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/orders_finished/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="mobile" value="{{request()->mobile}}"
                       placeholder="{{trans('language.mobile')}}">
            </div>
            <div class="col-md-3">
                <select required required class="form-control "  name="service_center_id">
                    <option value="0">{{trans('language.service_centers')}}</option>
                    @foreach(\App\Service_centers::all() as $service)
                        <option value="{{$service->id}}">{{$service->dash_name}}</option>
                    @endforeach
                </select>
                {{--
                                @includeIf('admin.components.form.add.select', ['label' => trans("language.country"),'name'=>'country_id', 'items'=> \App\Models\Category::all() , 'icon' => 'fa fa-list',])
                --}}

            </div>
            <div class="col-md-2">
                <input type="date" class="form-control start_at" name="start_at" value="{{request()->start_at}}"
                       placeholder="{{trans('language.start_at')}}">
            </div>

            <div class="col-md-2">
                <input type="date" class="form-control end_at " name="end_at" value="{{request()->end_at}}"
                       placeholder="{{trans('language.end_at')}}">
            </div>

            <div class="col-md-2">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>

        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');

        });
    </script>



@endsection
