@extends('admin.layout.forms.edit.index')
@section('action' , "order_requests/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.status'))
@section('form-groups')
    @includeIf('admin.components.form.edit.select', ['label' => trans("language.status"),'name'=>'status', 'items'=> \App\AdvertisementStatus::all() , 'icon' => 'fa fa-grid',])

@endsection
@section('submit-button-title' , trans('language.edit'))
