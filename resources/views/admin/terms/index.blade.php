@extends('admin.layout.forms.edit.index')
@section('action' , "terms_and_conditions/$item->id")
@section('title' , "السياسات والشروط")
@section('page-title',trans('language.terms'))
@section('form-groups')
    <br>
<!--    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">الشروط والاحكام</h4>
                <textarea id="terms" name="terms">{{$item->terms}}</textarea>
            </div>
        </div>
    </div>
    <br>-->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">سياسية الخصوصية</h4>
                <textarea id="privacy" name="privacy">{{$item->privacy}}</textarea>
            </div>
        </div>
    </div>

    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">سياسية النزاهة الأكاديمية</h4>
                <textarea id="integrity" name="integrity">{{$item->integrity}}</textarea>
            </div>
        </div>
    </div>
    <br>
  <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">الالتزام بالحقوق الملكية الفكرية </h4>
                <textarea id="property_rights" name="property_rights">{{$item->property_rights}}</textarea>
            </div>
        </div>
    </div>
    <br>

    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">سياسة التعليم الالكترونى </h4>
                <textarea id="e-Learning" name="e_learning">{{$item->e_learning}}</textarea>
            </div>
        </div>
    </div>
    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">سياسة الغش </h4>
                <textarea id="cheat" name="cheat">{{$item->cheat}}</textarea>
            </div>
        </div>
    </div>
    <br>


@endsection
@section('submit-button-title' , trans('web.edit'))
@section('extra_js')





    <script>
        $(document).ready(function () {


            $('#terms').summernote();
            $('#privacy').summernote();
            $('#integrity').summernote();
            $('#property_rights').summernote();
            $('#e-Learning').summernote();
            $('#cheat').summernote();

        });

    </script>




@endsection
