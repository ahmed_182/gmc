@extends('admin.layout.forms.edit.index')
@section('action' , "courses/$item->id")
@section('root' , "courses")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.courses'))
@section('form-groups')
    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'4'])
    @includeIf('admin.components.form.edit.select', ['label' => trans("language.category"),'name'=>'category_id', 'items'=> \App\Category::all() , 'icon' => 'fa fa-grid',])
    @includeIf('admin.components.form.edit.select', ['label' => trans("language.trainers"),'name'=>'trainer_id', 'items'=> \App\Trainer::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.coordinators"),'name'=>'coordinator_id', 'items'=> \App\User::where("user_type_id",App\ModulesConst\UserTyps::coordinators)->get() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.edit.text', ['label' => trans('language.name_course'),'name'=>'name', 'placeholder'=>trans('language.name_course'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['label' => trans('language.price'),'name'=>'price', 'placeholder'=>trans('language.price'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.date', ['label' => trans('language.start_date_course'),'name'=>'date', 'placeholder'=>trans('language.start_date_course'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.date', ['label' => trans('language.end_date'),'name'=>'end_date', 'placeholder'=>trans('language.end_date'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['label' => trans('language.duration'),'name'=>'duration', 'placeholder'=>trans('language.duration'),'valid'=>trans('language.vaildation')])

    @includeIf('admin.components.form.edit.textarea', ['label' => trans('language.brief_course'),'name'=>'breif', 'placeholder'=>trans('language.brief_course'),'valid'=>trans('language.vaildation')])
<br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.offer')}}</h4>
                <textarea id="offer" name="offer">{{$item->offer}}</textarea>
            </div>
        </div>
    </div>
    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.description_course')}}</h4>
                <textarea id="experience" name="description">{{$item->description}}</textarea>
            </div>
        </div>
    </div>

    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.features_course')}}</h4>
                <textarea id="qualification" name="features">{{$item->features}}</textarea>
            </div>
        </div>
    </div>
    <br>
    @includeIf('admin.components.form.edit.text', ['label' => trans('language.link_youtube'),'name'=>'link_youtube', 'placeholder'=>trans('language.link_youtube'),'valid'=>trans('language.price')])

    @includeIf('admin.components.form.edit.text', ['label' => trans('language.link_course'),'name'=>'link', 'placeholder'=>trans('language.link_course'),'valid'=>trans('language.price')])

@endsection
@section('submit-button-title' , trans('web.edit'))
@section('extra_js')





    <script>
        $(document).ready(function () {


            $('#about').summernote();
            $('#experience').summernote();
            $('#qualification').summernote();
            $('#achievements').summernote();
            $('#offer').summernote();

        });

    </script>




@endsection
