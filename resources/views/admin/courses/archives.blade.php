@extends('admin.layout.table.index')
@section('page-title',trans('language.courses'))
@section('root' , "users")
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.archives')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name_course')}}</th>
    <th>{{trans('language.trainer_name')}}</th>
    <th>{{trans('language.orders_name')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
            <td>{{$item->dash_name}}</td>
            <td>{{@$item->trainer->dash_name}}</td>
            <td> @includeIf("admin.components.buttons.custom" , ["href" => "archives/$item->id/orders", 'class' => 'bi bi-bell' , 'title'=> trans('language.list'), 'feather' => 'list'])
            </td>
            <td>
<!--              <a class="btn btn-primary "
                   href="{{url("admin/send_email/$item->id")}}" title="{{trans('language.send_link_email')}}"> <i class="" data-feather="send" ></i> </a>-->

{{--
                @includeIf("admin.components.buttons.edit" , ["href" => "courses/$item->id/edit"])
--}}
                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/archives/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/archives/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="name" value="{{request()->name}}"
                       placeholder="{{trans('language.name_course')}}">
            </div>


            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>
        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');
            $('.email_input').val('');
            $('.mobile_input').val('');
        });
    </script>

@endsection


