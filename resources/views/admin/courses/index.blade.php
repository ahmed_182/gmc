@extends('admin.layout.table.index')
@section('page-title',trans('language.courses'))
@section('root' , "users")
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.courses')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name_course')}}</th>
    <th>{{trans('language.trainer_name')}}</th>
    <th>{{trans('language.category')}}</th>
    <th>{{trans('language.orders')}}</th>
    <th>{{trans('language.order_requests')}}</th>
    <th>{{trans('language.orders_advertisement')}}</th>
    <th>{{trans('language.add_course_to_offers')}}</th>
    <th>{{trans('language.link_register_course')}}</th>
    <th>{{trans('language.link_sponsored_advertisement')}}</th>

    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
            <td>{{$item->dash_name}}</td>
            <td>{{@$item->trainer->dash_name}}</td>
            <td>{{@$item->category->dash_name}}</td>
            <td> @includeIf("admin.components.buttons.custom" , ["href" => "courses/$item->id/orders", 'class' => 'bi bi-bell' , 'title'=> trans('language.list'), 'feather' => 'list'])
            </td>
            <td> @includeIf("admin.components.buttons.custom" , ["href" => "courses/$item->id/order_requests", 'class' => 'bi bi-bell' , 'title'=> trans('language.list'), 'feather' => 'list'])
            </td>
            <td> @includeIf("admin.components.buttons.custom" , ["href" => "courses/$item->id/orders_advertisement", 'class' => 'bi bi-bell' , 'title'=> trans('language.list'), 'feather' => 'list'])
            </td>
            <td>
                @if($item->is_special==0)
                <a class="btn btn-primary "
                      href="{{url("admin/add_offer/$item->id")}}" title="{{trans('language.add_course_to_offers')}}"> <i class="" data-feather="check-square" ></i> </a>
                @else
                    <a class="btn btn-primary "
                       href="#" title=""> الدورة متاحة فى العروض حاليا</a>
                @endif
            </td>
            <td><a class="btn default btn-outline  " target="_blank" href="{{url("register_form/$item->id")}}/{{auth()->User()->api_token}}">
                    {{trans('language.link_register_course')}}
                </a>
            </td>
            <td><a class="btn default btn-outline  " target="_blank" href="{{url("sponsored_advertisement/$item->id")}}/{{auth()->User()->api_token}}">
                    {{trans('language.link_sponsored_advertisement')}}
                </a>

            <td>
                <a class="btn btn-primary "
                   href="{{url("admin/send_email/$item->id")}}" title="{{trans('language.send_link_email')}}"> <i class="" data-feather="send" ></i> </a>

                @includeIf("admin.components.buttons.edit" , ["href" => "courses/$item->id/edit"])


                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/courses/$item->id")])


            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/courses/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="name" value="{{request()->name}}"
                       placeholder="{{trans('language.name_course')}}">
            </div>


            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>
        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');
            $('.email_input').val('');
            $('.mobile_input').val('');
        });
    </script>

@endsection


