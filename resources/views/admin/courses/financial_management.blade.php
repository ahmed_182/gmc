@extends('admin.layout.table.index')
@section('page-title',trans('language.courses'))
@section('root' , "users")
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.courses')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name_course')}}</th>
    <th>{{trans('language.trainer_name')}}</th>
    <th>{{trans('language.category')}}</th>
    <th>{{trans('language.orders')}}</th>
    <th>{{trans('language.settings')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
            <td>{{$item->dash_name}}</td>
            <td>{{@$item->trainer->dash_name}}</td>
            <td>{{@$item->category->dash_name}}</td>
            <td> @includeIf("admin.components.buttons.custom" , ["href" => "courses/$item->id/orders", 'class' => 'bi bi-bell' , 'title'=> trans('language.list'), 'feather' => 'list'])
            </td>
            <td>

                <strong style="color: #0c5460;font-weight: bold">{{$item->dash_confirm}}</strong>
                <br>
                <br>
                @if($item->confirm == \App\ModulesConst\UserVerify::no)
                    <a class="btn btn-success "
                       href="{{url("admin/confirm_course/$item->id")}}" title="{{trans('language.confirm_course')}} "> <i class="" data-feather="check" ></i> </a>
                @else
                    <a class="btn btn-danger "
                       href="{{url("admin/not_confirm_course/$item->id")}}" title="{{trans('language.not_confirm_course')}} "> <i class="" data-feather="x" ></i> </a>
                @endif
            </td>

        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/markter_courses/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="name" value="{{request()->name}}"
                       placeholder="{{trans('language.name_course')}}">
            </div>


            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>
        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');
            $('.email_input').val('');
            $('.mobile_input').val('');
        });
    </script>

@endsection


