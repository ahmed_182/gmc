@extends('admin.layout.table.index')
@section('page-title',trans('language.subCategories'))
@section('nav')
    <div class="row">
        <div class="col-md-10">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
                <li class="breadcrumb-item"><a href="{{url("admin/category")}}">  {{trans('language.category')}}</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">{{trans('language.subCategories')}}</li>

            </ol>
        </div>
        <div class="col-md-2">
            @includeIf("admin.components.buttons.addbtn" , ["href" => "subcategory/create",'class' => 'btn btn-success' , 'title'=> trans('language.add'), ])

        </div>
    </div>


@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
            <td>{{$item->dash_name}}</td>
            <td>
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/category/$category_id/subcategory/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


