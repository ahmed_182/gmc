@extends('admin.layout.forms.edit.index')
@section('action' , "licensed_course/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.licensed_course'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_course'),'name'=>'name', 'placeholder'=>trans('language.name_course'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.no_accreditation'),'name'=>'no_accreditation', 'placeholder'=>trans('language.no_accreditation'),'valid'=>trans('language.vaildation')])

@endsection
@section('submit-button-title' , trans('language.edit'))
