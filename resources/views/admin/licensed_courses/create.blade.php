@extends('admin.layout.forms.add.index')
@section('action' , "licensed_course")
@section('title' , trans('language.add'))
@section('page-title',trans('language.licensed_course'))
@section('form-groups')

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_course'),'name'=>'name', 'placeholder'=>trans('language.name_course'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.number', ['icon' => 'fa fa-user','label' => trans('language.no_accreditation'),'name'=>'no_accreditation', 'placeholder'=>trans('language.no_accreditation'),'valid'=>trans('language.vaildation')])


@endsection
@section('submit-button-title' , trans('language.add'))
