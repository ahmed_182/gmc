@extends('admin.layout.table.index')
@section('page-title',trans('language.licensed_course'))
@section('buttons')

@stop
@section('nav')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.licensed_course')}}</li>
    </ol>
@endsection

@section('thead')
    <th>#</th>
    <th>{{trans('language.name_course')}}</th>
    <th>{{trans('language.no_accreditation')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->no_accreditation}}</td>
{{--
            <td><a target="_blank" href="{{url("$item->link")}}" class="btn btn-info">{{trans('language.show')}}</a>
--}}
{{--
            </td>
--}}
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "licensed_course/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/licensed_course/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


