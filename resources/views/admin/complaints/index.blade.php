@extends('admin.layout.table.index')
@section('page-title',trans('language.complaints'))
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.complaints')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.title')}}</th>
    <th>{{trans('language.content')}}</th>
    <th>{{trans('language.created_at')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->title}}</td>
            <td>{{$item->message}}</td>
            <td>{{$item->join_from() }}</td>
            <td>
                @includeIf("admin.components.buttons.delete",["message" => ($item->name) ,  "action" => url("admin/complaints/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


