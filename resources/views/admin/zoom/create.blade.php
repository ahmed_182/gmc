<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="{{asset('assets/site/css/custom.css')}}">
    <title>انشاء اجتماع</title>
</head>
@includeIf("admin.layout.head")
<body class="{{(app()->getLocale() == 'ar')?'rtl':'ltr'}}">
<div class="main-wrapper">
    @includeIf("admin.layout.aside.index")
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <a href="{{url("admin/")}}/@yield("root")">
                    <h3 class="text-themecolor">@yield("page-title")</h3>
                </a>
            </div>
        </div>
        <!-- partial:partials/_navbar.html -->
    @includeIf("admin.layout.header")
    <!-- partial -->
        <div class="page-content">
            <div class="container-fluid">
                @if (Session::has('danger'))
                    <div class="alert alert-danger top-alert alert-icon-left mb-0 fade-message" role="alert">
                        <span style="display: inline" class="alert-icon"><i class="fa fa-ban"></i></span>
                        <p style="display: inline" class="mb-0">{{ Session::get('danger') }}</p>
                    </div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-danger top-alert alert-icon-left mb-0 fade-message" role="alert">
                        <span style="display: inline" class="alert-icon"><i class="fa fa-ban"></i></span>
                        <p style="display: inline" class="mb-0">{{ Session::get('error') }}</p>
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success top-alert alert-icon-left mb-0 fade-message" role="alert">
                        <span style="display: inline" class="alert-icon"><i class="fa fa-check"></i></span>
                        <p style="display: inline" class="mb-0">{{ Session::get('success') }}</p>
                    </div>
                @endif
            </div>
            <div style="margin: auto;" class="col-lg-8 table-container">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">انشاء اجتماع جديد</h4>
                        <form class="form p-t-20 " enctype="multipart/form-data" method="post"
                              action="{{route('zoom.generate-meeting')}}">
                            @csrf
                            <div class="form-group">
                                <label>الدورة</label>
                                <div class="input-group">
                                    <select name="course_id" required class="course_select"
                                            oninvalid="this.setCustomValidity('من فضلك اختر الدورة المرد انشاء الاجتماع لها ')"
                                            oninput="setCustomValidity('')">
                                        >
                                        @foreach($courses as $course)
                                            <option value="{{$course->id}}">{{$course->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>الغرفة</label>
                                <div class="input-group">
                                    <select name="room" required class="room_select"
                                            oninvalid="this.setCustomValidity('من فضلك اختر الغرفة')"
                                            oninput="setCustomValidity('')">
                                        >
                                        @foreach($rooms as $room)
                                            <option value="{{$room}}">{{$room->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputuname">الموضوع</label>
                                <div class="input-group">
                                    <input class="form-control" type="text" required name="topic"
                                           oninvalid="this.setCustomValidity('من فضلك ضع عنوان للاجتماع ')"
                                           oninput="setCustomValidity('')">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputuname">مدة الاجتماع بالدقائق  (40)</label>
                                <div class="input-group">
                                    <input class="form-control" type="number" name="duration">
                                </div>
                            </div>
                            <button type="submit"
                                    class="btn btn-success waves-effect waves-light m-r-10 addFormRequest">انشاء
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
            <p class="text-muted text-center text-md-right">Copyright © 2021 <a href="#" target="_blank">Genius
                    Training Foundation</a>. All rights reserved
            </p>
        </footer>
    </div>
</div>
@includeIf("admin.layout.scripts")
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('.course_select').select2();
        $('.room_select').select2();
    });

</script>
</body>
</html>
