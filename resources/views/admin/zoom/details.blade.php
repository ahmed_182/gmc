<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="{{asset('assets/site/css/custom.css')}}">
    <title>بيانات الاجتماع</title>
</head>
@includeIf("admin.layout.head")
<body class="{{(app()->getLocale() == 'ar')?'rtl':'ltr'}}">
<div class="main-wrapper">
    @includeIf("admin.layout.aside.index")
    <div class="page-wrapper">
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <a href="{{url("admin/")}}/@yield("root")">
                    <h3 class="text-themecolor">@yield("page-title")</h3>
                </a>
            </div>
        </div>
        <!-- partial:partials/_navbar.html -->
    @includeIf("admin.layout.header")
    <!-- partial -->
        <div class="page-content">
            <div class="container-fluid">
                @if (Session::has('danger'))
                    <div class="alert alert-danger top-alert alert-icon-left mb-0 fade-message" role="alert">
                        <span style="display: inline" class="alert-icon"><i class="fa fa-ban"></i></span>
                        <p style="display: inline" class="mb-0">{{ Session::get('danger') }}</p>
                    </div>
                @endif
                @if (Session::has('error'))
                    <div class="alert alert-danger top-alert alert-icon-left mb-0 fade-message" role="alert">
                        <span style="display: inline" class="alert-icon"><i class="fa fa-ban"></i></span>
                        <p style="display: inline" class="mb-0">{{ Session::get('error') }}</p>
                    </div>
                @endif
                @if (Session::has('success'))
                    <div class="alert alert-success top-alert alert-icon-left mb-0 fade-message" role="alert">
                        <span style="display: inline" class="alert-icon"><i class="fa fa-check"></i></span>
                        <p style="display: inline" class="mb-0">{{ Session::get('success') }}</p>
                    </div>
                @endif
            </div>
            <div style="margin: auto;" class="col-lg-8 table-container">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title" style="font-size: 1.2rem"> بيانات اجتماع دورة ( {{$course_name}}) </h2>
                        <hr>
                        <div class="form-group">
                            <div class="input-group" style="margin-right: 21px">
                                <b> موضوع الاجتماع :</b> &nbsp;&nbsp; <span>{{$meeting_topic}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" style="margin-right: 21px">
                                <b> رقم المضيف :</b> &nbsp;&nbsp; <span>{{$meeting_host_id}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" style="margin-right: 21px">
                                <b> رقم الاجتماع :</b> &nbsp;&nbsp; <span>{{$meeting_id}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" style="margin-right: 21px">
                                <b> كلمة المرور :</b> &nbsp;&nbsp; <span>{{$meeting_password}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" style="margin-right: 21px">
                                <b> مدة الاجتماع :</b> &nbsp;&nbsp; <span>{{$meeting_duration}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" style="margin-right: 21px">
                                <b> موعد البدء :</b> &nbsp;&nbsp; <span>{{$meeting_start_time}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group" style="margin-right: 21px">
                                <b> لينك الدخول :</b> &nbsp;&nbsp; <span>{{$meeting_url}}</span>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group bg-primary text-center col-md-10" style="height: 23px">
                           <b style="color: #FFFFFF"> تم ارسال بيانات الاجتماع على البريد الالكترونى لكلا من المدرب و المتدربين المشتركين فى الدورة </b>
                       </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
            <p class="text-muted text-center text-md-right">Copyright © 2021 <a href="#" target="_blank">Genius
                    Training Foundation</a>. All rights reserved
            </p>
        </footer>
    </div>
</div>
@includeIf("admin.layout.scripts")
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('.course_select').select2();
    });

</script>
</body>
</html>
