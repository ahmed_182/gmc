@extends('admin.layout.forms.edit.index')
@section('action' , "services/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.openScreens'))
@section('form-groups')
    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.price'),'name'=>'price', 'placeholder'=>trans('language.price'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.select', ['label' => trans("language.ministries"),'name'=>'ministry_id', 'items'=> \App\Ministry::all() , 'icon' => 'fa fa-list',])
    <h5> # {{trans('language.receipts')}}</h5>
    <br>
    @foreach(App\Order_receipt::all() as $receipt)
        <div class="checkbox checkbox-success col-md-12">

            <input type="checkbox" name="receipts[]" value="{{ $receipt->id }}"
                   @if( in_array($receipt->id,\App\Service_receipts::where('service_id',$item->id)->pluck("id")->toArray()))
                   checked
                @endif>
{{--
            @dd($receipt->id,\App\Service_receipts::where('service_id',$item->id)->get()->toArray())
--}}
            <label class="label-filter" for="{{ $receipt->id }}"> {{ $receipt->dash_name }}</label>
        </div>
    @endforeach
@endsection
@section('submit-button-title' , trans('language.edit'))
