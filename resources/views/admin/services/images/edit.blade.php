@extends('admin.layout.forms.edit.index')
@section('action' , "stores/$store->id/products/$item->id")
@section('title' , trans('language.products'))
@section('page-title',trans('language.products'))
@section('form-groups')

    @includeIf('admin.components.form.edit.select', ['label' => trans("language.categories"),'name'=>'category_id', 'items'=> $categories , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.description_ar'),'name'=>'description_ar', 'placeholder'=>trans('language.description_ar'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.description_en'),'name'=>'description_en', 'placeholder'=>trans('language.description_en'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.price'),'name'=>'price', 'placeholder'=>trans('language.price'),'valid'=>trans('language.vaildation')])

@endsection
@section('submit-button-title' ,trans('language.edit'))


@section('extra_js')


@endsection
