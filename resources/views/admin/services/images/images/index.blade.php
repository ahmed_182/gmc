@extends('admin.layout.table.index')
@section('page-title',trans('language.images'))
@section('buttons')
    <div style="display: flex">
        <div class="col-md-3">
            @includeIf("admin.components.buttons.addbtn" , ["href" => "images/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
        </div>

    </div>

@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.images')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "stores/$store_id/products/$product_id/images/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" =>  '' ,  "action" => url("admin/stores/$store_id/products/$product_id/images/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection

