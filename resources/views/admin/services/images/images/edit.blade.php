@extends('admin.layout.forms.edit.index')
@section('action' , "stores/$store_id/products/$product_id/images/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.products'))
@section('form-groups')
    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

@endsection
@section('submit-button-title' ,trans('web.edit'))
