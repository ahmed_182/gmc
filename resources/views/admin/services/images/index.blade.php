@extends('admin.layout.table.index')
@section('page-title',trans('language.products'))
@section('buttons')
    <div style="display: flex">
        <div class="col-md-3">
            @includeIf("admin.components.buttons.addbtn" , ["href" => "image_required/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
        </div>
    </div>
@stop
@section('nav')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item"><a href="{{url("admin/services")}}">  {{trans('language.services')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.service_images_requireds')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.text')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            {{--            <td> @includeIf("admin.components.image.index" , ["url" => $item->one_image]) </td>--}}
            <td>{{$item->dash_name}}</td>

            <td>
{{--
                @includeIf("admin.components.buttons.edit" , ["href" => "services/$service_id/images/$item->id/edit"])
--}}
                @includeIf("admin.components.buttons.delete",["message" => "" ,  "action" => url("admin/services/$service_id/image_required/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


