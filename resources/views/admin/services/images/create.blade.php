@extends('admin.layout.forms.add.index')
@section('action' , "services/$service->id/image_required")
@section('title' , trans('language.add'))
@section('page-title',trans('language.products'))
@section('form-groups')

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en'),'valid'=>trans('language.vaildation')])




@endsection
@section('submit-button-title' ,trans('web.add'))
