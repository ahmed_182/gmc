@extends('admin.layout.table.index')
@section('page-title',trans('language.services'))
@section('buttons')

@stop
@section('nav')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.services')}}</li>
    </ol>
@endsection

@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.ministry')}}</th>
    <th>{{trans('language.service_images_requireds')}}</th>

    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->serv_image])</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_ministry}}</td>
            <td>
                @includeIf("admin.components.buttons.custom" , ["href" => "services/$item->id/image_required", 'class' => 'bi bi-bell' , 'title'=> trans('language.service_images_requireds'), 'feather' => 'grid'])

            </td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "services/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/services/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


