@extends('admin.layout.forms.add.index')
@section('action' , "services")
@section('title' , trans('language.add'))
@section('page-title',trans('language.services'))
@section('form-groups')

    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.price'),'name'=>'price', 'placeholder'=>trans('language.price'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.ministries"),'name'=>'ministry_id', 'items'=> \App\Ministry::all() , 'icon' => 'fa fa-list',])

    <h5> # {{trans('language.receipts')}}</h5>
    <br>
    @foreach(App\Order_receipt::all() as $receipt)
        @includeIf('admin.components.form.add.checklist', ['label' => $receipt->dash_name,'icon' => 'fa fa-user','id'=>$receipt->id,'name'=>'receipts','value'=>$receipt->id])
    @endforeach

@endsection
@section('submit-button-title' , trans('language.add'))
