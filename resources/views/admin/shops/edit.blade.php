@extends('admin.layout.forms.edit.index')
@section('action' , "shops/$item->id")
@section('root' , "shops")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.edit'))
@section('form-groups')
    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.edit.text', ['label' => trans('language.name'),'name'=>'name', 'placeholder'=>trans('language.name'),'valid'=>trans('language.vaildation')])
      @includeIf('admin.components.form.edit.text', ['label' => trans('language.delivery_cost'),'name'=>'delivery_cost', 'placeholder'=>trans('language.delivery_cost'),'valid'=>trans('language.vaildation')])

@endsection
@section('submit-button-title' , trans('web.edit'))
