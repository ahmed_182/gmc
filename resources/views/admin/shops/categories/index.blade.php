@extends('admin.layout.table.index')
@section('page-title',trans('language.categories'))
@section('nav')
    <div class="row">
        <div class="col-md-10">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
                <li class="breadcrumb-item"><a href="{{url("admin/shops")}}">  {{trans('language.shops')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{trans('language.categories')}}</li>
            </ol>
        </div>
        <div class="col-md-2">
            @includeIf("admin.components.buttons.addbtn" , ["href" => "categories/create",'class' => 'btn btn-success' , 'title'=> trans('language.add'), ])
        </div>
    </div>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
   <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ @$item->sub_category->dash_name }}</td>
             <td>
               @includeIf("admin.components.buttons.delete",["message" =>  @$item->sub_category->dash_name  ,  "action" => url("admin/shops/$shop_id/categories/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection

