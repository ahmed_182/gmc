@extends('admin.layout.forms.add.index')
@section('action' , "shops/$shop_id/categories")
@section('title' , trans('language.add'))
@section('page-title',trans('language.categories'))
@section('form-groups')
    <div class="col-md-6">
        <div class="form-group">
            <label>{{trans("language.categories")}}</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class='fa fa-list'></i></span>
                </div>
                <select required class="form-control main_category_id_list main_category_id"
                        id="main_category_id"
                        name="main_category_id">
                    <option value="0">اختر القسم</option>
                    @foreach(\App\Category::get() as $category)
                        <option name="main_category_id"
                                value="{{$category->id}}">{{$category->dash_name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <!-- end of Category Select -->

    <!-- sub cat Category Select -->
    <div class="col-md-6">
        <div class="form-group  category_List">
            <label>{{trans("language.subCategories")}}</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text"><i class='fa fa-list'></i></span>
                </div>
                <select id="category_id" name="sub_category_id"
                        class=" form-control category_id_list category_id">
                    <option name="category_id" class="basicOption" value="0">اختر القسم
                    </option>
                </select>
            </div>
        </div>
    </div>
    <!-- sen if sub cat Category Select -->




@endsection
@section('submit-button-title' , trans('language.add'))
@section('extra_js')
    {{--  Get Category and Sub_Category With Ajax Function :-   --}}
    <script>

        $(document).ready(function () {
            $('.main_category_id').on('change', function () {
                var category_id = $('.main_category_id').val();

                $.post("{{url("/admin/getSubCategories")}}",

                    {
                        category_id: category_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {

                        if (data.status == true) {

                            $(".category_List").removeClass("hide");
                            $('.category_id_list').html('');
                            if (data.status == true) {
                                $(".category_id_list").prepend('' +
                                    '<option  value="0"> اختر قسم فرعي</option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    console.log(item);
                                    $(".category_id_list").prepend('' +
                                        '<option name="category_id"  value="' + item.id + '">' + item.name + '</option>');
                                });
                            });
                        } else {
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '
                            })
                            $('.category_id_list').html('');
                            $(".category_id_list").prepend('' +
                                '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');
                        }
                    });
            });


        });


    </script>
    @endsection

