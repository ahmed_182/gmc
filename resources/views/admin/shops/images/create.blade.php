@extends('admin.layout.forms.add.index')
@section('action' , "users/$user_id/orders/$order_id/examinations/$item->id/attachment")
@section('title' , trans('language.add'))
@section('page-title',trans('language.products'))
@section('form-groups')
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

@endsection
@section('submit-button-title' ,trans('web.add'))
