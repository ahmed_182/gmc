@extends('admin.layout.table.index')
@section('page-title',trans('language.images'))

@section('thead')
    <th>#</th>
    <th>{{trans('language.images')}}</th>
   <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
        </tr>
    @endforeach
@endsection

