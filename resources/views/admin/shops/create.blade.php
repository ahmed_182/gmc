@extends('admin.layout.forms.add.index')
@section('action' , "shops")
@section('root' , "shops")
@section('title' , trans('language.add'))
@section('page-title',trans('language.shops'))
@section('form-groups')

    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.add.text', ['label' => trans('language.name'),'name'=>'name', 'placeholder'=>trans('language.name'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['label' => trans('language.delivery_cost'),'name'=>'delivery_cost', 'placeholder'=>trans('language.delivery_cost'),'valid'=>trans('language.vaildation')])

    <!-- description input -->
    <div class="col-md-12">
        <div class="form-group">
            <label style="padding-top: 15px"
                   for="">{{trans('language.description')}}</label>
            <textarea name="description" class="form-control" rows="5"></textarea>
        </div>
    </div>
    <!-- end of  description  -->

    <!-- add capacity -->

    <div class="col-12">

        <div class="col-12">
            <div class="row">

                <div class="col-3">
                    <button type="button" id="Showcapacity" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        اضافه صور للمتجر
                    </button>
                </div>

                <div class="col-3">
                    <button type="button" id="removecapacity" class="btn btn-danger" style="display: none">
                        <i class="fa fa-minus"></i>
                        ازالة الصور
                    </button>
                </div>

                <div class="col-3">
                    <button class="btn  btn-info Add_capacity addcapacity " style="display: none;" type="button"
                            onclick="addNewRow()">{{trans('اضافه صوره جديده')}}
                    </button>
                </div>


            </div>

        </div>


        <div class="col-12">
            <div class="addcapacity" style="display: none;">

                <ul id="MMYTABEL" class="UL_capacity">
                    <li>


                    </li>
                </ul>
            </div>
        </div>
        <br>
        <br>
    </div>






@endsection
@section('submit-button-title' , trans('web.add'))
@section('extra_css')
    <style>
        p {
            font-size: 20px !important;
        }
    </style>
@endsection
@section('extra_js')

    <script>

        // add capacity
        $('#Showcapacity').click(function () {
            $(this).css("display", "none");
            $('.addcapacity').css("display", "block");
            $('#removecapacity').css("display", "block");
        });

        $('#removecapacity').click(function () {
            $(this).css("display", "none");
            $('.addcapacity').css("display", "none");
            $('.inputcapacity').val("");
            $('.removeeee').remove();
            $('#Showcapacity').css("display", "block");
            $('.UL_capacity').html("");

        });
        // add new li
        counter = 1;

        function addNewRow() {
            counter++;
            $('#MMYTABEL').append(
                '<li>' +
                '<div class="row removeeee col-md-12">' +
                '<div class="col-md-6">' +
                '<div class="form-group">' +
                '<label>{{trans('language.image')}}</label>' +
                '<div class="input-group">' +
                '<input type="file" name="shop_images[]" class="dropify"data-max-file-size="2M"/>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6" style="padding-top:20px" >' +
                '<button id="deleterow" class="btn btn-danger">{{trans('language.delete')}}</button>' +
                '</div>' +
                '</div>' +
                '</li>'
            );
        };

        $(document).on("click", "#deleterow", function () {
            $(this).closest('li').remove();
        });


    </script>

@endsection
