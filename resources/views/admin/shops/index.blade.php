@extends('admin.layout.table.index')
@section('page-title',trans('language.shops'))
@section('root' , "shops")
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.shops')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.description')}}</th>
    <th>{{trans('language.delivery')}}</th>
    <th>{{trans('language.verfied_status')}}</th>
    <th>{{trans('language.products')}}</th>
    <th>{{trans('language.categories')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_description}}</td>
            <td>{{$item->delivery_cost}}</td>
            <td>
                @if($item->userVerify == \App\ModulesConst\UserVerify::no)
                    <a class="btn btn-primary"
                       href="{{url("admin/active_account/$item->id")}}"> {{trans('language.active')}} {{trans('language.account')}} </a>
                @else
                    <a class="btn btn-primary"
                       href="{{url("admin/deActive_account/$item->id")}}"> {{trans('language.deActive')}} {{trans('language.account')}} </a>
                @endif
            </td>
            <td>
                <a class="btn btn-facebook"
                   href="{{url("admin/shops/$item->id/products")}}"> {{trans('language.products')}}   </a>
            </td>
            <td>
                <a class="btn btn-facebook"
                   href="{{url("admin/shops/$item->id/categories")}}"> {{trans('language.categories')}}   </a>
            </td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "shops/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/shops/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/shops/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="name" value="{{request()->name}}"
                       placeholder="{{trans('language.name')}}">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control email_input" name="email" value="{{request()->email}}"
                       placeholder="{{trans('language.email')}}">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control  mobile_input " name="mobile" value="{{request()->mobile}}"
                       placeholder="{{trans('language.mobile')}}">
            </div>
            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>
        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');
            $('.email_input').val('');
            $('.mobile_input').val('');
        });
    </script>

@endsection


