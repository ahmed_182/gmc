@extends('admin.layout.index')
@section('content')

    <div class="page-content">

      {{--  <nav class="page-breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Special pages</a></li>
                <li class="breadcrumb-item active" aria-current="page">Invoice</li>
            </ol>
        </nav>--}}

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="container-fluid d-flex justify-content-between">
                            <div class="col-lg-3 pl-0">
                                <a href="#" class="noble-ui-logo d-block mt-3">{{trans('language.order_details')}}{{--<span>UI</span>--}}</a>
                                <br>
                                <p class="mt-1 mb-1"><b>{{trans('language.user_name')}}  : <span>{{$item->DashLocationUserName}}</span></b></p>
                                <h5 class="mt-5 mb-2 text-muted"> {{trans('language.service')}}   :</h5>
                                <p>{{$item->dash_service}}.</p>

                                <h5 class="mt-5 mb-2 text-muted"> {{trans('language.center_service')}}   :</h5>
                                <p>{{$item->dash_service_center}}.</p>
                            </div>
                            <div class="col-lg-3 pr-0">
                                <h4 class="font-weight-medium text-uppercase text-right mt-4 mb-2">{{trans('language.order_id')}}</h4>
                                <h6 class="text-right mb-5 pb-4"># {{$item->id}}</h6>

                                <h5 class="font-weight-medium text-uppercase text-right mt-4 mb-2">{{trans('language.mobile')}}</h5>
                                <h6 class="text-right mb-5 pb-4"> {{$item->DashLocationMobile}}</h6>
                                <h6 class="mb-0 mt-3 text-right font-weight-normal mb-2"><span class="text-muted">{{trans('language.paymenttype')}} :</span> {{@$item->payment_type->dash_name}}</h6>
                                <h6 class="text-right font-weight-normal"><span class="text-muted">{{trans('language.date')}} :</span> {{$item->created_at->format("Y-m-d")}}</h6>
                                <h6 class="mb-0 mt-3 text-right font-weight-normal mb-2"><span class="text-muted">{{trans('language.receipttype')}} :</span> {{$item->dash_receipt}}</h6>

                            </div>
                        </div>
                        <div class="container-fluid mt-5 d-flex justify-content-center w-100">
                            <div class="table-responsive w-100">
                                <table class="table table-bordered">
                                    <thead>
                                  {{--  <tr>
                                        <th class="text-center">title</th>
                                        <th class="text-center">{{trans('language.order_images_requireds')}}</th>

                                    </tr>--}}
                                    </thead>
                                    <tbody>
                                        <tr class="text-right">
{{--
                                        <td class="text-center">{{ $loop->iteration }}</td>
--}}
                                            @foreach($services as $service  )

                                            <td class="text-center">{{ $service->dash_name }}</td>
                                            @endforeach
                                        </tr>

                                        <tr class="text-right">
                                        @foreach($images as $image)

                                            <td class="text-center"> <a class="btn default btn-outline" target="_blank" href="{{$image->image}}">
                                                <img class="img-responsive imageInList" style="width: 150px; height: 150px ;border-radius: 0!important;" src="{{$image->image}}">
                                            </a></td>
                                            @endforeach
                                        </tr>


                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="container-fluid mt-5 w-100">
                            <div class="row">
                                <div class="col-md-6 ml-auto">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>{{trans('language.price')}}</td>
                                                <td class="text-right">{{$item->dash_cost}}   {{trans('language.app_currency')}}</td>
                                            </tr>
                                            <tr>
                                                <td>{{trans('language.delivery_cost')}}</td>
                                                <td class="text-right">{{$item->delivery_tax}}   {{trans('language.app_currency')}}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-bold-800">{{trans('language.total_price')}}</td>
                                                <td class="text-bold-800 text-right">{{$item->dash_cost +$item->delivery_tax }}  {{trans('language.app_currency')}}</td>
                                            </tr>
                                         {{--   <tr>
                                                <td>Payment Made</td>
                                                <td class="text-danger text-right">(-) $ 4,688.00</td>
                                            </tr>
                                            <tr class="bg-light">
                                                <td class="text-bold-800">Balance Due</td>
                                                <td class="text-bold-800 text-right">$ 12,000.00</td>
                                            </tr>--}}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container-fluid w-100">
{{--
                            <a href="#" class="btn btn-primary float-right mt-4 ml-2"><i data-feather="send" class="ml-2 icon-md"></i>Send Invoice</a>
--}}
                            <a href="#" id="printPageButton" class="btn btn-outline-primary float-right mt-4"  onclick="document.title = '{{trans('language.order_id')}}:{{$item->id}}-{{trans('language.mobile')}}:{{$item->DashLocationMobile}}';window.print();"><i data-feather="printer" class="ml-2 icon-md"></i>Pdf</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section("extra_css")

    <style>
        @media print {
            #printPageButton {
                display: none;
            }
        }

    </style>



@endsection
