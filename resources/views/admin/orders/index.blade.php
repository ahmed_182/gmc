@extends('admin.layout.table.index')
@section('page-title',trans('language.orders'))

@section('nav')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.orders')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.name_course')}}</th>
    <th>{{trans('language.markter_name')}}</th>

    <th>{{trans('language.client_name')}}</th>
    <th>{{trans('language.mobile')}}</th>
    <th>{{trans('language.identity')}}</th>
    <th>{{trans('language.qualification')}}</th>

    <th>{{trans('language.date')}}</th>

    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{$item->dash_course}}</td>
            <td>{{$item->dash_markter}}</td>
            <td>{{$item->name}}</td>


            <td> <a href="https://api.whatsapp.com/send?phone=+966{{$item->mobile}}" target="_blank">{{$item->mobile}}</a></td>
            <td>{{$item->identity}}</td>
            @if($item->qualification_id=="1")
                <td>{{$item->other}}</td>
            @else
                <td>{{$item->qualification_id}}</td>
            @endif
            <td>{{$item->created_at->format("Y-m-d")}}</td>

{{--
            <td>{{$item->dash_cost +$item->delivery_tax }}  {{trans('language.app_currency')}}   </td>
--}}

            <td>

                <strong style="color: #0c5460;font-weight: bold">{{$item->dash_status_name}}</strong>
                <br>
                <br>
                @if($item->paid == \App\ModulesConst\UserVerify::no)
                    <a class="btn btn-success "
                       href="{{url("admin/paid_action/$item->id")}}" title="{{trans('language.paid_action')}} "> <i class="" data-feather="check" ></i> </a>
                @else
                    <a class="btn btn-danger "
                       href="{{url("admin/not_paid_action/$item->id")}}" title="{{trans('language.not_paid_action')}} "> <i class="" data-feather="x" ></i> </a>
                @endif
            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/orders/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="mobile" value="{{request()->mobile}}"
                       placeholder="{{trans('language.mobile')}}">
            </div>
            <div class="col-md-3">
                <input type="date" class="form-control start_at" name="start_at" value="{{request()->start_at}}"
                       placeholder="{{trans('language.start_at')}}">
            </div>

            <div class="col-md-3">
                <input type="date" class="form-control end_at " name="end_at" value="{{request()->end_at}}"
                       placeholder="{{trans('language.end_at')}}">
            </div>

            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>

        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');

        });
    </script>



@endsection
