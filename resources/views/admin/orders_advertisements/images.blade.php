@extends('admin.layout.table.index')
@section('page-title',trans('language.products'))
@section('nav')
    <div class="row">
        <div class="col-md-10">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.appName')}}</a></li>
                <li class="breadcrumb-item"><a href="{{url("admin/orders")}}">  {{trans('language.orders')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{trans('language.order_images_requireds')}}</li>

            </ol>
        </div>
    </div>

@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.order_images_requireds')}}</th>
{{--
    <th>{{trans('language.order_images_requireds')}}</th>
--}}
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
            {{--<td>                    <a class="btn btn-behance" href="{{$item->image}}">Download PDF </a>
            </td>--}}
        </tr>
    @endforeach
@endsection


