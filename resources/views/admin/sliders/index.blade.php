@extends('admin.layout.table.index')
@section('page-title',trans('language.sliders'))
@section('buttons')

@stop
@section('nav')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.sliders')}}</li>
    </ol>
@endsection

@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.title')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
            <td>{{$item->name_ar}}</td>
{{--
            <td><a target="_blank" href="{{url("$item->link")}}" class="btn btn-info">{{trans('language.show')}}</a>
--}}
{{--
            </td>
--}}
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "sliders/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/sliders/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


