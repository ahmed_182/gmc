@extends('admin.layout.forms.edit.index')
@section('action' , "sliders/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.sliders'))
@section('form-groups')
    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.title'),'name'=>'name_ar', 'placeholder'=>trans('language.title')])

@endsection
@section('submit-button-title' , trans('language.edit'))
