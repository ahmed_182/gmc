@extends('admin.layout.index')
@section('content')


    @php

        $all_countries =\App\Country::get();
        $all_courses =\App\Course::get();
        $all_orders =\App\Order::get();
        $all_users =\App\User::where('user_type_id', App\ModulesConst\UserTyps::user)->get();
        $all_markters =\App\User::where('user_type_id', App\ModulesConst\UserTyps::markter)->get();
        $all_trainers =\App\User::where('user_type_id', App\ModulesConst\UserTyps::trainer)->get();



    @endphp


    @if(Auth::user()->user_type_id ==\App\ModulesConst\UserTyps::admin)

    <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
        <div>
            <h4 class="mb-3 mb-md-0">{{trans('language.welcome_msg')}}</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">

                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/users")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.users')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($all_users)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/markters")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.markters')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($all_markters)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/trainers")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.trainers')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($all_trainers)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>



            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">

                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/courses")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.courses')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($all_courses)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-4 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/orders")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.orders')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($all_orders)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>



            </div>
        </div>

    </div>
    @endif





@endsection
@section("extra_js")
@endsection
