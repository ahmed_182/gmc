<nav class="sidebar">
    <div class="sidebar-header">
        <a href="{{url("admin/")}}" class="sidebar-brand">
            {{trans('language.appName')}}
        </a>
        <div class="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="sidebar-body">
        <ul class="nav">
            <br>
            @if(Auth::user()->user_type_id ==\App\ModulesConst\UserTyps::admin)

            <li class="nav-item">
                @includeIf("admin.layout.aside.main-item" ,["href" => "#sliders" , "icon"=>"user", "title" => trans('language.sliders') , "description" => trans('web.list'), "feather"=>"book"])
                <div class="collapse" id="sliders">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "sliders" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "sliders/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>

<!--            <li class="nav-item">
                @includeIf("admin.layout.aside.main-item" ,["href" => "#users_section" , "icon"=>"user", "title" => trans('language.users') , "description" => trans('web.list'), "feather"=>"user"])

                <div class="collapse" id="users_section">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "users" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "users/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>-->
                <br>
                <li class="nav-item">
                    @includeIf("admin.layout.aside.main-item" ,["href" => "#markters_section" , "icon"=>"user", "title" => trans('language.markters') , "description" => trans('web.list'), "feather"=>"grid"])

                    <div class="collapse" id="markters_section">
                        <ul class="nav sub-menu">
                            @includeIf("admin.layout.aside.sub-item" ,["href" => "markters" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                            @includeIf("admin.layout.aside.sub-item" ,["href" => "markters/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                        </ul>
                    </div>
                </li>
            <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#trainers" , "icon"=>"user", "title" => trans('language.trainers') , "description" => trans('web.list'), "feather"=>"users"])

                <div class="collapse" id="trainers">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "trainers" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "trainers/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>
                <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#coordinators" , "icon"=>"user", "title" => trans('language.coordinators') , "description" => trans('web.list'), "feather"=>"users"])

                <div class="collapse" id="coordinators">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "coordinators" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "coordinators/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>
                <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#financial_managements" , "icon"=>"user", "title" => trans('language.financial_management') , "description" => trans('web.list'), "feather"=>"users"])

                <div class="collapse" id="financial_managements">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "financial_managements" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "financial_managements/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>

            <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#categories" , "icon"=>"user", "title" => trans('language.categories') , "description" => trans('web.list'), "feather"=>"grid"])

                <div class="collapse" id="categories">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "categories" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "categories/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>
                <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#courses" , "icon"=>"user", "title" => trans('language.courses') , "description" => trans('web.list'), "feather"=>"grid"])

                <div class="collapse" id="courses">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "courses" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "courses/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>
                <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#licensed_course" , "icon"=>"user", "title" => trans('language.licensed_course') , "description" => trans('web.list'), "feather"=>"list"])

                <div class="collapse" id="licensed_course">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "licensed_course" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "licensed_course/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>
                <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#archives" , "icon"=>"user", "title" => trans('language.archives') , "description" => trans('web.list'), "feather"=>"grid"])

                <div class="collapse" id="archives">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "archives" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                    </ul>
                </div>
            </li>

                <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#offers" , "icon"=>"user", "title" => trans('language.offers') , "description" => trans('web.list'), "feather"=>"box"])

                <div class="collapse" id="offers">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "offers" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                    </ul>
                </div>
            </li>

                <br>
                <li class="nav-item">

                    @includeIf("admin.layout.aside.main-item" ,["href" => "#orders" , "icon"=>"user", "title" => trans('language.orders') , "description" => trans('web.list'), "feather"=>"shopping-cart"])

                    <div class="collapse" id="orders">
                        <ul class="nav sub-menu">
                            @includeIf("admin.layout.aside.sub-item" ,["href" => "orders" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        </ul>
                    </div>
                </li>
                <br>
                <li class="nav-item">

                    @includeIf("admin.layout.aside.main-item" ,["href" => "#order_requests" , "icon"=>"user", "title" => trans('language.order_requests') , "description" => trans('web.list'), "feather"=>"shopping-cart"])

                    <div class="collapse" id="order_requests">
                        <ul class="nav sub-menu">
                            @includeIf("admin.layout.aside.sub-item" ,["href" => "order_requests" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        </ul>
                    </div>
                </li>
                <br>
                <li class="nav-item">

                    @includeIf("admin.layout.aside.main-item" ,["href" => "#contract_courses" , "icon"=>"user", "title" => trans('language.contract_courses') , "description" => trans('web.list'), "feather"=>"shopping-cart"])

                    <div class="collapse" id="contract_courses">
                        <ul class="nav sub-menu">
                            @includeIf("admin.layout.aside.sub-item" ,["href" => "contract_courses" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        </ul>
                    </div>
                </li>

            <br>
            <li class="nav-item">
                @includeIf("admin.layout.aside.main-item" ,["href" => "#contact" , "icon"=>"user", "title" => trans('language.contact_us') , "description" => trans('web.list'), "feather"=>"list"])
                <div class="collapse" id="contact">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "contact" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>
                     <br>
            <li class="nav-item">
                @includeIf("admin.layout.aside.main-item" ,["href" => "#complaints" , "icon"=>"user", "title" => trans('language.complaints') , "description" => trans('web.list'), "feather"=>"list"])
                <div class="collapse" id="complaints">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "complaints" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-plus"])
                    </ul>
                </div>
            </li>

           <br>
                <li class="nav-item">

                    @includeIf("admin.layout.aside.main-item" ,["href" => "#terms_section" , "icon"=>"user", "title" => trans('language.terms') , "description" => trans('web.list'), "feather"=>"settings"])

                    <div class="collapse" id="terms_section">
                        <ul class="nav sub-menu">
                            @includeIf("admin.layout.aside.sub-item" ,["href" => "terms_and_conditions" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        </ul>
                    </div>
                </li>

            <br>

                <br>
                <li class="nav-item">

                    @includeIf("admin.layout.aside.main-item" ,["href" => "#meetings" , "icon"=>"user", "title" => trans('language.meetings') , "feather"=>"settings"])

                    <div class="collapse" id="meetings">
                        <ul class="nav sub-menu">
                            @includeIf("admin.layout.aside.sub-item" ,["href" => "zoom/create" , "title" => trans('web.create') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        </ul>
                    </div>
                </li>

                <br>

            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#setting_section" , "icon"=>"user", "title" => trans('language.settings') , "description" => trans('web.list'), "feather"=>"settings"])

                <div class="collapse" id="setting_section">
                    <ul class="nav sub-menu">
                        @includeIf("admin.layout.aside.sub-item" ,["href" => "setting" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                    </ul>
                </div>
            </li>
                @elseif(Auth::user()->user_type_id ==\App\ModulesConst\UserTyps::markter)

                <li class="nav-item">
                    @includeIf("admin.layout.aside.main-item" ,["href" => "#coursers1" , "icon"=>"user", "title" => trans('language.courses') , "description" => trans('web.list'), "feather"=>"grid"])

                    <div class="collapse" id="coursers1">
                        <ul class="nav sub-menu">
                            @includeIf("admin.layout.aside.sub-item" ,["href" => "markter_courses" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        </ul>
                    </div>
                </li>
            @elseif(Auth::user()->user_type_id ==\App\ModulesConst\UserTyps::coordinators)

                <li class="nav-item">
                    @includeIf("admin.layout.aside.main-item" ,["href" => "#coordinators" , "icon"=>"user", "title" => trans('language.courses') , "description" => trans('web.list'), "feather"=>"grid"])

                    <div class="collapse" id="coordinators">
                        <ul class="nav sub-menu">
                            @includeIf("admin.layout.aside.sub-item" ,["href" => "coordinators_courses" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        </ul>
                    </div>
                </li>
            @elseif(Auth::user()->user_type_id ==\App\ModulesConst\UserTyps::financial_management)

                <li class="nav-item">
                    @includeIf("admin.layout.aside.main-item" ,["href" => "#financial_management" , "icon"=>"user", "title" => trans('language.courses') , "description" => trans('web.list'), "feather"=>"grid"])

                    <div class="collapse" id="financial_management">
                        <ul class="nav sub-menu">
                            @includeIf("admin.layout.aside.sub-item" ,["href" => "financial_management" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                        </ul>
                    </div>
                </li>
                  @endif

        </ul>
    </div>
</nav>

