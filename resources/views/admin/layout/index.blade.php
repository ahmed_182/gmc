<!DOCTYPE html>
<html lang="en">

<head>
    <style>
        .select2-container--default .select2-selection--single {
            border-radius: 8px !important;
        }
        .select2-container .select2-selection--single {
            height: 39px !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__arrow b {
            border-style: none !important;
        }
        .select2-search--dropdown .select2-search__field {
            text-align: right !important;
        }
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 20px !important;
        }
        .select2-container .select2-selection--single .select2-selection__rendered {
            padding-right: 1px !important;
            line-height: 20px !important;
        }
    </style>
</head>

@includeIf("admin.layout.head")

@if(app()->getLocale() == 'ar')
    <body class="rtl">
    @else
        <body>
        @endif
        <div class="main-wrapper">

            @includeIf("admin.layout.aside.index")

            <div class="page-wrapper">
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">

                        <a href="{{url("admin/")}}/@yield("root")"><h3 class="text-themecolor">@yield("page-title")</h3>
                        </a>
                    </div>
                </div>

                <!-- partial:partials/_navbar.html -->
            @includeIf("admin.layout.header")
            <!-- partial -->
                <div class="page-content">
                    <div class="container-fluid">

                        @if (\Session::has('danger'))
                            <div class="alert alert-danger top-alert alert-icon-left mb-0 fade-message" role="alert">
                                <span style="display: inline" class="alert-icon"><i class="fa fa-ban"></i></span>
                                <p style="display: inline" class="mb-0">{{ \Session::get('danger') }}</p>
                            </div>
                        @endif

                        @if (\Session::has('error'))
                            <div class="alert alert-danger top-alert alert-icon-left mb-0 fade-message" role="alert">
                                <span style="display: inline" class="alert-icon"><i class="fa fa-ban"></i></span>
                                <p style="display: inline" class="mb-0">{{ \Session::get('error') }}</p>
                            </div>
                        @endif

                        @if (\Session::has('success'))
                            <div class="alert alert-success top-alert alert-icon-left mb-0 fade-message" role="alert">
                                <span style="display: inline" class="alert-icon"><i class="fa fa-check"></i></span>
                                <p style="display: inline" class="mb-0">{{ \Session::get('success') }}</p>
                            </div>
                        @endif
                    </div>

                    @yield("content")
                </div>


                <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
                    <p class="text-muted text-center text-md-right">Copyright © 2021 <a href="#" target="_blank">Genius Training Foundation</a>. All rights reserved</p>
                </footer>


            </div>


        </div>


        @includeIf("admin.components.modals.delete-modal")
        @includeIf("admin.components.modals.show-modal")

        @includeIf("admin.layout.scripts")

        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
        <script>
            $(document).ready(function() {
                $('.course_select').select2();
            });
        </script>

        </body>
</html>
