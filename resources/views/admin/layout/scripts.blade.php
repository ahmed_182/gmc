<script src="{{asset("assets/admin/")}}/js/jquery.min.js"></script>

<!-- core:js -->
<script src="{{asset("assets/admin/")}}/vendors/core/core.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<script src="{{asset("assets/admin/")}}/vendors/chartjs/Chart.min.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/jquery.flot/jquery.flot.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/jquery.flot/jquery.flot.resize.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/apexcharts/apexcharts.min.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/progressbar.js/progressbar.min.js"></script>
<!-- end plugin js for this page -->
<!-- inject:js -->
<script src="{{asset("assets/admin/")}}/vendors/feather-icons/feather.min.js"></script>
<script src="{{asset("assets/admin/")}}/js/template.js"></script>
<!-- endinject -->
<!-- custom js for this page -->
<script src="{{asset("assets/admin/")}}/js/dashboard.js"></script>
<script src="{{asset("assets/admin/")}}/js/datepicker.js"></script>
<script src="{{asset("assets/admin/")}}/js/data-table.js"></script>
<!-- end custom js for this page -->

<!-- plugin js for this page -->
<script src="{{asset("assets/admin/")}}/vendors/datatables.net/jquery.dataTables.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="{{asset('assets/admin/')}}/js/setMap.js"></script>

<script type="text/javascript" src="{{asset('assets/admin/js/map/setMapCreate.js')}}"></script>
<script id="dropify_js" src="{{asset("assets/admin/")}}/js/dropify.min.js"></script>

<script src="{{asset('assets/admin/')}}/js/open-delete-modal.js"></script>

<script>
    $(document).ready(function () {
        $('.dropify').dropify();
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function (event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function (event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function (event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function (e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>


<script>
    $(function () {
        setTimeout(function () {
            $('.fade-message').slideUp();
        }, 2000);
    });
</script>

<!-- end plugin js for this page -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>
{{--<link href="{{asset("assets/admin/")}}/js/summernote-bs4.min.css" rel="stylesheet">
<link href="{{asset("assets/admin/")}}/js/summernote-bs4.css" rel="stylesheet">
<script src="{{asset("assets/admin/")}}/js/summernote-bs4.min.js"></script>
<script src="{{asset("assets/admin/")}}/js/summernote-bs4.js"></script>
<script src="{{asset("assets/admin/")}}/js/summernote.ttf"></script>
<script src="{{asset("assets/admin/")}}/js/summernote.woff"></script>
<script src="{{asset("assets/admin/")}}/js/summernote.woff2"></script>--}}

<script>
    $(document).ready(function () {
        $('#summernote_ar').summernote();
    });
    $(document).ready(function () {
        $('#summernote_en').summernote();
    });
    $(document).ready(function () {
        $('#summernote').summernote();
    });
</script>
@yield('extra_js')

