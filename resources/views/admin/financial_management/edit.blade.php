@extends('admin.layout.forms.edit.index')
@section('action' , "financial_managements/$item->id")
@section('root' , "financial_management")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.edit'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['label' => trans('language.name'),'name'=>'name', 'placeholder'=>trans('language.name'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.email', ['label' => trans('language.email'),'name'=>'email', 'placeholder'=>trans('language.email')])
    @includeIf('admin.components.form.edit.text', ['label' => trans('language.mobile'),'name'=>'mobile', 'placeholder'=>trans('language.mobile'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.password', ['label' => trans('language.password'),'name'=>'password', 'placeholder'=>trans('language.password')])
    @includeIf('admin.components.form.edit.password', ['label' => trans('language.confirm-password'),'name'=>'password_confirmation', 'placeholder'=>trans('language.confirm-password')])
@endsection
@section('submit-button-title' , trans('web.edit'))
