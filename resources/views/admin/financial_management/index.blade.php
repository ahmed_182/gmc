@extends('admin.layout.table.index')
@section('page-title',trans('language.financial_management'))
@section('root' , "markters")
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.financial_management')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.email')}}</th>
    <th>{{trans('language.mobile')}}</th>
    <th>{{trans('language.verfied_status')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_email}}</td>
            <td>{{$item->dash_mobile}}</td>

            <td>

                <strong style="color: #0c5460;font-weight: bold">{{$item->dash_status_name}}</strong>
                <br>
                <br>
                @if($item->userVerify == \App\ModulesConst\UserVerify::no)
                    <a class="btn btn-success "
                       href="{{url("admin/active_account/$item->id")}}" title="{{trans('language.active')}} {{trans('language.account')}}"> <i class="" data-feather="check" ></i> </a>
                @else
                    <a class="btn btn-danger "
                       href="{{url("admin/deActive_account/$item->id")}}" title="{{trans('language.deActive')}} {{trans('language.account')}}"> <i class="" data-feather="x" ></i> </a>
                @endif
            </td>
          {{--  <td>
                @if($item->userVerify == \App\ModulesConst\UserVerify::no)
                    <a class="btn btn-primary"
                       href="{{url("admin/active_account/$item->id")}}"> {{trans('language.active')}} {{trans('language.account')}} </a>
                @else
                    <a class="btn btn-primary"
                       href="{{url("admin/deActive_account/$item->id")}}"> {{trans('language.deActive')}} {{trans('language.account')}} </a>
                @endif
            </td>--}}
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "financial_managements/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/financial_managements/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/financial_managements/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control name_input " name="name" value="{{request()->name}}"
                       placeholder="{{trans('language.name')}}">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control email_input" name="email" value="{{request()->email}}"
                       placeholder="{{trans('language.email')}}">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control  mobile_input " name="mobile" value="{{request()->mobile}}"
                       placeholder="{{trans('language.mobile')}}">
            </div>
            <div class="col-md-3">
                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                <button style="width: 45%" type="button"
                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
            </div>
        </div>
    </form>
@stop

@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');
            $('.email_input').val('');
            $('.mobile_input').val('');
        });
    </script>

@endsection


