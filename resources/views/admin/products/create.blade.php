@extends('admin.layout.forms.add.index')
@section('action' , "products")
@section('title' , trans('language.add'))
@section('page-title',trans('language.products'))
@section('form-groups')

    {{--  Categories :-  --}}
    <div class="form-group">
        <label>{{trans("language.category")}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class='fa fa-list'></i></span>
            </div>
            <select required class="form-control main_category_id_list main_category_id" id="main_category_id"
                    name="main_category_id">
                <option value="0">اختر القسم</option>
                @foreach(\App\Categories::where("parent_id",null)->get() as $category)
                    <option name="main_category_id" value="{{$category->id}}">{{$category->dash_name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group  hide category_List hide ">
        <select required id="category_id" name="category_id"
                class=" form-control category_id_list category_id">
        </select>
    </div>




    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name'),'name'=>'name', 'placeholder'=>trans('language.name'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.stock_count'),'name'=>'stock_count', 'placeholder'=>trans('language.stock_count'),'valid'=>trans('language.vaildation')])
<<<<<<< HEAD
=======
    @includeIf('admin.components.form.add.color', ['icon' => 'fa fa-user','label' => trans('language.color'),'name'=>'color', 'placeholder'=>trans('language.color'),'valid'=>trans('language.vaildation')])

>>>>>>> a266069c5ec60aa6aa7115ceef66a8bf05e970a8

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{trans('language.description')}}</h4>
                    <textarea rows="10" class="form-control" id="mymce" name="description"></textarea>
                </div>
            </div>
        </div>

    </div>



    <!-- add capacity -->
    <div class="col-12">

        <div class="col-12">
            <div class="row">

                <div class="col-3">
                    <button type="button" id="Showcapacity" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        اضافه احجام للمنتج
                    </button>
                </div>

                <div class="col-3">
                    <button type="button" id="removecapacity" class="btn btn-danger" style="display: none">
                        <i class="fa fa-minus"></i>
                        ازالة الاحجام
                    </button>
                </div>

                <div class="col-3">
                    <button class="btn  btn-info Add_capacity addcapacity " style="display: none;" type="button"
                            onclick="addNewRow()">{{trans('اضافه حجم ')}}
                    </button>
                </div>


            </div>

        </div>


        <div class="col-12">
            <div class="addcapacity" style="display: none;">
                <br>


                <ul id="MMYTABEL" class="UL_capacity">


                    <li>
                        <div class="row removeeee">
                            <div class="col-md-5">
                                <input type="text" placeholder="اسم الحجم" name="size_names[]"
                                       class="inputcapacity form-control">
                            </div>
                            <div class="col-md-5">
                                <input type="text" placeholder="السعر " name="prices[]"
                                       class="inputcapacity form-control">
                            </div>
                            <div class="col-md-2">
                                <button id="deleterow" type="button"
                                        class="btn btn-danger"> {{trans("language.delete")}} </button>
                            </div>
                        </div>
                        <br>
                    </li>


                </ul>

            </div>
        </div>
        <br>
        <br>
    </div>



    <!-- add Images -->
    <div class="col-12">

        <div class="col-12">
            <div class="row">

                <div class="col-3">
                    <button type="button" id="Showcapaimage" class="btn btn-primary">
                        <i class="fa fa-plus"></i>
                        اضافه صور للمتجر
                    </button>
                </div>

                <div class="col-3">
                    <button type="button" id="removecapaimage" class="btn btn-danger" style="display: none">
                        <i class="fa fa-minus"></i>
                        ازالة الصور
                    </button>
                </div>

                <div class="col-3">
                    <button class="btn  btn-info Add_capacityimage addcapaimage " style="display: none;" type="button"
                            onclick="addNewRowimage()">{{trans('اضافه صوره جديده')}}
                    </button>
                </div>


            </div>

        </div>


        <div class="col-12">
            <div class="addcapaimage" style="display: none;">

                <ul id="MMYTABELimage" class="UL_capaimage">
                    <li>
                        <div class="row removeeeeimage col-md-12">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label>{{trans('language.image')}}</label>
                                    <div class="input-group">
                                        <input type="file" name="product_images[]" class="   "
                                               data-max-file-size="2M"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" style="padding-top:20px">
                                <button id="deleterowimage" type="button"
                                        class="btn btn-danger">{{trans('language.delete')}}</button>
                            </div>
                        </div>
                        </br>
                    </li>

                </ul>


            </div>
        </div>
        <br>
        <br>
    </div>


@endsection
@section('submit-button-title' , trans('language.add'))
@section('extra_css')
    <style>
        .hide {
            display: none;
        }
    </style>
@endsection
@section('extra_js')


    <script>

        // add capacity
        $('#Showcapacity').click(function () {
            $(this).css("display", "none");
            $('.addcapacity').css("display", "block");
            $('#removecapacity').css("display", "block");
        });

        $('#removecapacity').click(function () {
            $(this).css("display", "none");
            $('.addcapacity').css("display", "none");
            $('.inputcapacity').val("");
            $('.removeeee').remove();
            $('#Showcapacity').css("display", "block");
            $('.UL_capacity').html("");

        });
        // add new li
        counter = 1;

        function addNewRow() {
            counter++;
            $('#MMYTABEL').append(
                '<li>' +
                '<div class="row removeeee">' +
                '<div class="col-md-5">' +
                '<input type="text" placeholder="اسم الحجم" name="size_names[]" class="inputcapacity form-control">' +
                '</div>' +
                '<div class="col-md-5">' +
                '<input type="text" placeholder="السعر " name="prices[]" class="inputcapacity form-control">' +
                '</div>' +
                '<div class="col-md-2">' +
                '<button id="deleterow" type="button" class="btn btn-danger"> {{trans("language.delete")}} </button>' +
                '</div>' +
                '</div>' +
                '</br>' +
                '</li>'
            );
        };

        $(document).on("click", "#deleterow", function () {
            $(this).closest('li').remove();
        });


    </script>

    {{--    images  --}}
    <script>

        // add capacity
        $('#Showcapaimage').click(function () {
            $(this).css("display", "none");
            $('.addcapaimage').css("display", "block");
            $('#removecapaimage').css("display", "block");
        });

        $('#removecapaimage').click(function () {
            $(this).css("display", "none");
            $('.addcapaimage').css("display", "none");
            $('.inputcapaimage').val("");
            $('.removeeeeimage').remove();
            $('#Showcapaimage').css("display", "block");
            $('.UL_capaimage').html("");

        });
        // add new li
        counterimage = 1;

        function addNewRowimage() {
            counterimage++;
            $('#MMYTABELimage').append(
                '<li>' +
                '<div class="row removeeeeimage col-md-12">' +
                '<div class="col-md-10">' +
                '<div class="form-group">' +
                '<label>{{trans('language.image')}}</label>' +
                '<div class="input-group">' +
                '<input type="file" name="product_images[]" class=" dropify "data-max-file-size="2M"/>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-2" style="padding-top:20px" >' +
                '<button id="deleterowimage" type="button"  class="btn btn-danger">{{trans('language.delete')}}</button>' +
                '</div>' +
                '</div>' +
                '</br>' +
                '</li>'
            );
        };

        $(document).on("click", "#deleterowimage", function () {
            $(this).closest('li').remove();
        });


    </script>


    <script>

        $(document).ready(function () {
            $('.main_category_id').on('change', function () {
                var main_category_id = $('.main_category_id').val();
                $.post("{{url("/admin/getCategories")}}",
                    {
                        category_id: main_category_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {

                        if (data.status == true) {
                            $(".category_List").removeClass("hide");
                            $('.category_id_list').html('');
                            if (data.status == true) {
                                $(".category_id_list").prepend('' +
                                    '<option  value="0"> اختر قسم فرعي</option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    console.log(item);
                                    $(".category_id_list").prepend('' +
                                        '<option name="category_id"  value="' + item.id + '">' + item.name + '</option>');
                                });
                            });
                        } else {
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '
                            })
                            $('.category_id_list').html('');
                            $(".category_id_list").prepend('' +
                                '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');
                        }
                    });
            });
            $('.category_id').on('change', function () {
                var category_id = $('.category_id').val();
                $.post("{{url("/admin/getCategories")}}",
                    {
                        category_id: category_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {
                        if (data.status == true) {
                            console.log(data)
                            $('.category_id_list').removeClass('hide');
                            $('.category_id_list').html('');
                            if (data.status == true) {
                                $(".category_id_list").prepend('' +
                                    '<option name="category_id"  class="basicOption"  value="0"> اختر قسم فرعي</option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    $(".category_id_list").prepend('' +
                                        '<option name="category_id"  value="' + item.id + '">' + item.name + '</option>');
                                });
                            });
                        } else {
                            $('.basicOption').addClass('hide');
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '
                            })
                        }
                    });
            });

        });

    </script>


@endsection
