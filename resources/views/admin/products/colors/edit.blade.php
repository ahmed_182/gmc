@extends('admin.layout.forms.edit.index')
@section('action' , "products/$product->id/colors/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.edit'))
@section('form-groups')
    @includeIf('admin.components.form.edit.color', ['icon' => 'fa fa-user','label' => trans('language.color'),'name'=>'color', 'placeholder'=>trans('language.color'),'valid'=>trans('language.vaildation')])
 @endsection
@section('submit-button-title' , trans('language.edit'))
