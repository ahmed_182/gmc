@extends('admin.layout.table.index')
@section('page-title',trans('language.colors'))
@section('buttons')
    <div style="display: flex">
        <div class="col-md-3">
            @includeIf("admin.components.buttons.addbtn" , ["href" => "colors/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
        </div>
    </div>
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.color')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>
                <p style="width: 200px;color: {{$item->color}} !important;"> ( {{$item->color}} )
                    <span style="margin: 20px;height: 10px;width: 10px;background-color: {{$item->color}}">000000000000000000000000000000000000000000</span></p>
                <br>
            </td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "products/$product_id/colors/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  '' ,  "action" => url("admin/products/$product_id/colors/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection

