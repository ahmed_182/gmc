@extends('admin.layout.forms.add.index')
@section('action' , "products/$product->id/colors")
@section('title' , trans('language.add'))
@section('page-title',trans('language.sizes'))
@section('form-groups')
    @includeIf('admin.components.form.add.color', ['icon' => 'fa fa-user','label' => trans('language.color'),'name'=>'color', 'placeholder'=>trans('language.color'),'valid'=>trans('language.vaildation')])

@endsection
@section('submit-button-title' ,trans('web.add'))
