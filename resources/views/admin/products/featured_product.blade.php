@extends('admin.layout.table.index')
@section('page-title',trans('language.products'))
@section('nav')
    <div class="row">
        <div class="col-md-10">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{trans('language.featured_products')}}</li>
            </ol>
        </div>
        <div class="col-md-2">
            @includeIf("admin.components.buttons.addbtn" , ["href" => "products/create",'class' => 'btn btn-success' , 'title'=> trans('language.add'), ])
        </div>
    </div>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.images')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.description')}}</th>
    <th>{{trans('language.colors')}}</th>
    <th>{{trans('language.sizes')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>
                <div style="width: 200px" id="carouselExampleControls_{{$loop->iteration}}" class="carousel slide"
                     data-ride="carousel">
                    <div class="carousel-inner">

                        @foreach($item->images as $image)
                            <div class="carousel-item @if ($loop->first) active @endif">
                                <img style="height: 200px; width: 200px"
                                     src="{{$image->image}}"
                                     class="" alt="...">
                            </div>
                        @endforeach


                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls_{{$loop->iteration}}" role="button"
                       data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls_{{$loop->iteration}}" role="button"
                       data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <br>
                <a style="width: 60%" href="{{url("/admin/products/$item->id/images")}}"
                   class="btn btn-linkedin"> <i class="fa fa-eye" data-feather="image"
                                                aria-hidden="true"></i> {{trans('language.images_list')}} <i
                        class=""></i> </a>
            </td>
            <td>
                <p class="descCut">{{$item->name}}</p>
                <br>
                @includeIf("admin.components.buttons.showPop",["message" => $item->name ])
            </td>
            <td style="width: 250px"><p class="descCut">{{$item->description}}</p>
                <br>
                @includeIf("admin.components.buttons.showPop",["message" => $item->description ])
            </td>
            <td>
                @foreach(\App\Product_colors::where('product_id',$item->id)->get() as $color)

                    <p style="width: 200px;color: {{$color->color}} !important;"> ( {{$color->color}} )
                        <span style="height: 10px;width: 10px;background-color: {{$color->color}}">00000</span></p>
                    <br>
                @endforeach
                <br>
                <a class="btn btn-info"
                   href="{{url("/admin/products/$item->id/colors")}}"> {{trans('language.color')}}   </a>
            </td>
            <td>
                @foreach(\App\Product_sizes::where('product_id',$item->id)->get() as $size)
                    <p style="width: 200px"> ( {{$size->size}} - {{$size->price}} ) </p>
                @endforeach
                <br>
                <a class="btn btn-info"
                   href="{{url("/admin/products/$item->id/sizes")}}"> {{trans('language.edit_sizes')}}   </a>
            </td>


            <td>
                @if ($item->is_discount())
                    @includeIf("admin.components.buttons.custom" , ["href" => "offers/".$item->discountOfferId()."/edit", 'class' => 'btn btn-primary ' , 'title'=> trans('language.edit_offer'), 'feather' => 'plus'])
                @else
                    @includeIf("admin.components.buttons.custom" , ["href" => "offers/$item->id/create", 'class' => 'btn btn-warning' , 'title'=> trans('language.add_offer'), 'feather' => 'plus-circle'])
                @endif

                @includeIf("admin.components.buttons.edit" , ["href" => "products/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/products/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


