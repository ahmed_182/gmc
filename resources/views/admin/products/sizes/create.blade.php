@extends('admin.layout.forms.add.index')
@section('action' , "products/$product->id/sizes")
@section('title' , trans('language.add'))
@section('page-title',trans('language.sizes'))
@section('form-groups')
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.size'),'name'=>'size', 'placeholder'=>trans('language.size'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.price'),'name'=>'price', 'placeholder'=>trans('language.price'),'valid'=>trans('language.vaildation')])

@endsection
@section('submit-button-title' ,trans('web.add'))
