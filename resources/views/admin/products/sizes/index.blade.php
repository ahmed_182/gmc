@extends('admin.layout.table.index')
@section('page-title',trans('language.sizes'))
@section('buttons')
    <div style="display: flex">
        <div class="col-md-3">
            @includeIf("admin.components.buttons.addbtn" , ["href" => "sizes/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
        </div>

    </div>

@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.size')}}</th>
    <th>{{trans('language.price')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->size}}</td>
            <td>{{$item->price}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "products/$product_id/sizes/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  '' ,  "action" => url("admin/products/$product_id/sizes/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection

