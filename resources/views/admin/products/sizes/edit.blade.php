@extends('admin.layout.forms.edit.index')
@section('action' , "products/$product->id/sizes/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.edit'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.size'),'name'=>'size', 'placeholder'=>trans('language.size'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.price'),'name'=>'price', 'placeholder'=>trans('language.price'),'valid'=>trans('language.vaildation')])
@endsection
@section('submit-button-title' , trans('language.edit'))
