@extends('admin.layout.forms.edit.index')
@section('action' , "setting/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.settings'))
@section('form-groups')
    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.logo'),'name'=>'logo', 'max'=>'2'])

    @includeIf('admin.components.form.edit.textarea', ['icon' => 'fa fa-user','label' => trans('language.address'),'name'=>'address', 'placeholder'=>trans('language.address')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.email'),'name'=>'email', 'placeholder'=>trans('language.email')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.mobile'),'name'=>'mobile', 'placeholder'=>trans('language.mobile')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.facebook'),'name'=>'facebook', 'placeholder'=>trans('language.facebook')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.twitter'),'name'=>'twitter', 'placeholder'=>trans('language.twitter')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.instagram'),'name'=>'instagram', 'placeholder'=>trans('language.instagram')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.linkedin'),'name'=>'linkedin', 'placeholder'=>trans('language.linkedin')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.whatsapp'),'name'=>'whatsapp', 'placeholder'=>trans('language.whatsapp')])

@endsection
@section('submit-button-title' , trans('web.edit'))
@section('extra_js')





    <script>
        $(document).ready(function () {


            $('#about_ar').summernote();
            $('#about_en').summernote();
            $('#text_ar').summernote();
            $('#text_en').summernote();

        });

    </script>




@endsection
