@extends('admin.layout.forms.edit.index')
@section('action' , "about/$item->id")
@section('title' , trans('language.add'))
@section('page-title',trans('language.about'))
@section('form-groups')

    @includeIf('admin.components.form.edit.textarea', ['icon' => 'fa fa-user','label' => trans('language.description_ar'),'name'=>'description_ar', 'placeholder'=>trans('language.description_ar')])
    @includeIf('admin.components.form.edit.textarea', ['icon' => 'fa fa-user','label' => trans('language.description_en'),'name'=>'description_en', 'placeholder'=>trans('language.description_en')])

@endsection
@section('submit-button-title' ,"تعديل ")
