@extends('admin.layout.table.index')
@section('page-title',trans('language.categories'))
@section('buttons')

@stop
@section('nav')

    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.categories')}}</li>
    </ol>
@endsection

@section('thead')
    <th>#</th>
    <th>{{trans('language.category')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->name}}</td>
{{--
            <td><a target="_blank" href="{{url("$item->link")}}" class="btn btn-info">{{trans('language.show')}}</a>
--}}
{{--
            </td>
--}}
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "categories/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/categories/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


