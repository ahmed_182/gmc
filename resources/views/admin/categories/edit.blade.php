@extends('admin.layout.forms.edit.index')
@section('action' , "categories/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.categories'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.category'),'name'=>'name', 'placeholder'=>trans('language.category'),'valid'=>trans('language.vaildation')])

@endsection
@section('submit-button-title' , trans('language.edit'))
