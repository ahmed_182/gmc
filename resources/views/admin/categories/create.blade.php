@extends('admin.layout.forms.add.index')
@section('action' , "categories")
@section('title' , trans('language.add'))
@section('page-title',trans('language.categories'))
@section('form-groups')

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.category'),'name'=>'name', 'placeholder'=>trans('language.category'),'valid'=>trans('language.vaildation')])


@endsection
@section('submit-button-title' , trans('language.add'))
