@extends('admin.layout.forms.edit.index')
@section('action' , "trainers/$item->id")
@section('root' , "trainers")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.trainers'))
@section('form-groups')
    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

    @includeIf('admin.components.form.edit.text', ['label' => trans('language.trainer_name'),'name'=>'name', 'placeholder'=>trans('language.trainer_name'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.edit.text1', ['label' => trans('language.current_position'),'name'=>'current_position', 'placeholder'=>trans('language.current_position'),'valid'=>trans('language.current_position')])
    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.brief')}}</h4>
                <textarea id="about" name="about">{{$trainer->about}}</textarea>
            </div>
        </div>
    </div>

    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.experience')}}</h4>
                <textarea id="experience" name="experience">{{$trainer->experience}}</textarea>
            </div>
        </div>
    </div>

    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.qualification')}}</h4>
                <textarea id="qualification" name="qualification">{{$trainer->qualification}}</textarea>
            </div>
        </div>
    </div>
    <br>
<!--    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.achievements')}}</h4>
                <textarea id="achievements" name="achievements">{{$trainer->achievements}}</textarea>
            </div>
        </div>
    </div>-->
@endsection
@section('submit-button-title' , trans('web.edit'))
@section('extra_js')





    <script>
        $(document).ready(function () {


            $('#about').summernote();
            $('#experience').summernote();
            $('#qualification').summernote();
            $('#achievements').summernote();

        });

    </script>




@endsection
