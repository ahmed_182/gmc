@extends('admin.layout.forms.add.index')
@section('action' , "trainers")
@section('root' , "trainers")
@section('title' , trans('language.add'))
@section('page-title',trans('language.trainers'))
@section('form-groups')
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

    @includeIf('admin.components.form.add.text', ['label' => trans('language.trainer_name'),'name'=>'name', 'placeholder'=>trans('language.trainer_name'),'valid'=>trans('language.vaildation')])
    @includeIf('admin.components.form.add.text', ['label' => trans('language.current_position'),'name'=>'current_position', 'placeholder'=>trans('language.current_position'),'valid'=>trans('language.current_position')])
    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.brief')}}</h4>
                <textarea id="about" name="about"></textarea>
            </div>
        </div>
    </div>

    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.experience')}}</h4>
                <textarea id="experience" name="experience"></textarea>
            </div>
        </div>
    </div>

    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.qualification')}}</h4>
                <textarea id="qualification" name="qualification"></textarea>
            </div>
        </div>
    </div>
<!--    <br>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.achievements')}}</h4>
                <textarea id="achievements" name="achievements"></textarea>
            </div>
        </div>
    </div>-->
@endsection
@section('submit-button-title' , trans('web.add'))
@section('extra_js')





    <script>
        $(document).ready(function () {


            $('#about').summernote();
            $('#experience').summernote();
            $('#qualification').summernote();
            $('#achievements').summernote();

        });

    </script>




@endsection
