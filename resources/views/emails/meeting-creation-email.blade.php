{{-- You can change this template using File > Settings > Editor > File and Code Templates > Code > Laravel Ideal Markdown Mail --}}
@component('mail::message')
# {{$course->name}}  بيانات اجتماع دورة

<div style="margin: auto;" class="col-lg-8 table-container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title" style="font-size: 1.2rem"> بيانات اجتماع دورة ( {{$course->name}}) </h4>
            <hr>
            <div class="form-group">
                <div class="input-group" style="margin-right: 21px">
                    <b> موضوع الاجتماع :</b> &nbsp;&nbsp; <span>{{$meeting->topic}}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" style="margin-right: 21px">
                    <b> رقم المضيف :</b> &nbsp;&nbsp; <span>{{$meeting->host_id}}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" style="margin-right: 21px">
                    <b> رقم الاجتماع :</b> &nbsp;&nbsp; <span>{{$meeting->id}}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" style="margin-right: 21px">
                    <b> كلمة المرور :</b> &nbsp;&nbsp; <span>{{$meeting->password}}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" style="margin-right: 21px">
                    <b> مدة الاجتماع :</b> &nbsp;&nbsp; <span>{{$meeting->duration}}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" style="margin-right: 21px">
                    <b> موعد البدء :</b> &nbsp;&nbsp; <span>{{$meeting->start_time}}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group" style="margin-right: 21px">
                    <b> لينك الدخول :</b> &nbsp;&nbsp; <span>{{$meeting->join_url}}</span>
                </div>
            </div>
            <hr>
        </div>
    </div>

@component('mail::button', ['url' => route('zoom.join')])
Join Meeting
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
