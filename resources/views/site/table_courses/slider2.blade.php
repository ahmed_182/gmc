<div class="search_container_block overlay_dark_part">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="container margin_ead">
                    @auth
                        <div class="col-md-12" style="margin-top: 25px;margin-bottom: 10px">
                            <form action="{{url('/search')}}" method="get" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="my_city_id" value="{{Auth::user()->city_id}}">
                                <button class="button fullwidth_block margin-top-5">البحث من خلال مدينتك</button>
                                <br> <br>
                            </form>
                        </div>
                    @endauth

                    <form action="{{ url('/search') }}" method="get"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="trtr">
                            <div class="row">

                                <div class="col-md-2">
                                    <div class="Slid_head" style="margin-top:0">
                                        <button type="submit" class="button">بحث <i
                                                class="sl sl-icon-magnifier"></i></button>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="main_input_search_part_item">
                                        <input class="text-right" type="text" placeholder="عن ماذا تبحث ؟"
                                               value="{{request()->keyword}}" name="keyword">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <hr>
                                    <div class="Rig_Left">

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h3 class="margin-top-40 text-center"> بحث مفصل</h3>
                                            </div>
                                            <!-- الاقسام -->
                                            <div class="col-md-6">
                                                <div class="margin-top-10">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <h4 class="Head_search">القسم</h4>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="main_input_search_part_item">
                                                                <select style="height: 60px" id="main_category_id"
                                                                        name="main_category_id"
                                                                        class="main_category_id_list main_category_id">
                                                                    <option value="0">اختر القسم</option>
                                                                    @foreach(\App\Categories::where("parent_id",null)->orderBy("sort","ASC")->get() as $category)
                                                                        <option name="main_category_id"

                                                                                value="{{$category->id}}">{{$category->dash_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="main_input_search_part_item">
                                                                <select style="height: 60px" id="category_id_2"
                                                                        name="category_id"
                                                                        class="category_id_list_2 category_id_2">
                                                                    <option name="category_id" class="" value="0">
                                                                        اختر القسم
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="col-sm-12">
                                                            <div class="main_input_search_part_item">
                                                                <select style="height: 60px" id="category_id_3"
                                                                        name="category_id_2"
                                                                        class="category_id_list_3 category_id_3">
                                                                    <option name="category_id_2" class="" value="0">
                                                                        اختر القسم
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="main_input_search_part_item">
                                                                <select style="height: 60px" id="category_id_4"
                                                                        name="category_id_3"
                                                                        class="category_id_list_4 category_id_4">
                                                                    <option name="category_id_3" class="basicOption"
                                                                            value="0">اختر القسم
                                                                    </option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- المكان -->
                                            <div class="col-md-6">
                                                <div class="margin-top-10">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <h4 class="Head_search">الموقع</h4>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="main_input_search_part_item">
                                                                <select style="height: 60px" class="country_id"
                                                                        name="country_id">
                                                                    <option value="0">اخترالدوله</option>
                                                                    @foreach(\App\Country::all() as $country)
                                                                        <option
                                                                            value="{{$country->id}}">{{$country->dash_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="main_input_search_part_item">
                                                                <select id="regoin_id" style="height: 60px"
                                                                        class="regoin_id" name="regoin_id">
                                                                    <option value="0">اختر المنطقه</option>
                                                                </select>
                                                            </div>
                                                        </div>


                                                        <div class="col-sm-12">
                                                            <div class="main_input_search_part_item">
                                                                <select id="distract_id" style="height: 60px"
                                                                        class="distract_id"
                                                                        name="distract_id">
                                                                    <option value="0">اختر المدينه</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <div class="main_input_search_part_item">
                                                                <select id="city_id" style="height: 60px"
                                                                        class="city_id" name="city_id">
                                                                    <option value="0">اختر الحي</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 mb-2">
                                    <hr>
                                    <div class="Slid_head" >
                                        <button style="width:80% ;" type="submit" class="button" onclick=" ">أستكشف
                                            <i class="sl sl-icon-magnifier"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
