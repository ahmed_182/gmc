<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <h3 style="margin-top:70px" class="headline_part centered hello ">
                الاقسام
                <!-- <span>تصفح الفئات الأكثر شهرة</span> -->
            </h3>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12" style="direction: rtl">
            <div class="container_categories_box margin-top-5 margin-bottom-30">
                @foreach(\App\Categories::where("parent_id",null)->orderBy("sort","ASC")->get() as $cat)
                    <a href="{{url("/categoryDetails/$cat->id")}}" class="utf_category_small_box_part "
                       style="background-color: #faebeb;height: 150px">
                        <img class="im imgicon" src="{{$cat->dash_image}}">
                        <h4 style="color: black !important;font-weight: bold">{{$cat->dash_name}}</h4>
                        <span
                            style="color: black !important;font-weight: bold">{{$cat->totalProducts()->count()}}</span>
                    </a>
                @endforeach
            </div>
            {{--            <div class="col-md-12 centered_content"><a href="#" class="button border margin-top-20">عرض المزيد</a></div>--}}
        </div>
    </div>
</div>
{{--
@section('extra_css')
  <style>
      @media only screen and (device-width: 768px) {
          /* For general iPad layouts */
          .hello{
              margin-top: 27% !important;
          }
      }
  </style>
@endsection
--}}
