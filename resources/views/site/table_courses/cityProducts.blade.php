<div class="container padding-bottom-70">
    <div class="row">
        <div class="col-md-12">
            <h3 class="headline_part centered margin-bottom-35 margin-top-70">
                المدن
                <!-- <span> استكشف الاعلانات من خلال المدن الاكثر شعبيه  </span> -->
            </h3>
        </div>

        @foreach( $distracts as $distract)
            <div class=" col-md-2 col-sm-3 float-r_edit">
                <a href="{{url("distractProducts/$distract->id")}}" class="img-box"
                   data-background-image="" style="height: 150px">
                    <div class="utf_img_content_box " style="top: 70px">
                        <h5 style="color: #FFFFFF">{{$distract->dash_name}} </h5>
                        <span style="text-align: right"> <b>اعلان</b>  ( {{$distract->product_count}} ) </span>
                    </div>
                </a>
            </div>
        @endforeach
        <div class="col-md-12 centered_content"><a href="{{url('/all_products')}}" class="button border margin-top-20">
                عرض اعلانات السوق العام</a></div>

    </div>
</div>
