@extends('.site.layout.container')
@section('title','جدول الدورات')
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg " style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title"> جدول الدوارات</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <section class="courses-details">
        <div class="container">

            <div class="row flex-row-reverse rtl-direction">
                <div class="col-lg-12">
                    <form method="get" action="{{url("/table_courses/")}}">

                        <div style="display: flex">
                            <div class="col-md-3">
                                <input type="text" class="form-control name_input " name="name" value="{{request()->name}}"
                                       placeholder="{{trans('language.name_course')}}">
                            </div>
                            <div class="col-md-3">
                                <input type="date" class="form-control start_at" name="date" value="{{request()->date}}"
                                       placeholder="{{trans('language.start_at')}}">
                            </div>

                            <div class="col-md-3">
                                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                                <button style="width: 45%" type="button"
                                        class="btn btn-info  reset_inputs ">{{trans('language.reset')}}</button>
                            </div>

                        </div>
                    </form>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered text-right">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>اسم الدورة</th>
                                <th>المدرب</th>
                                <th>مدتها</th>
                                <th>تاريخ انعقادها</th>
                                <th>السعر بالريال السعودي</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($items as $item)

                                <tr>
                                <td>{{ $loop->iteration }}</td>
                                    <td>{{$item->dash_name}}</td>
                                    <td>{{@$item->trainer->dash_name}}</td>
                                <td>{{$item->duration}}</td>
                                <td>{{$item->date}}</td>
                                <td>{{$item->price}}
                                </td>
                            </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <ul class="pagination-items text-center">
                {{$items->links()}}

            </ul>
        </div>
    </section>


@endsection


@section("extra_js")

    <script>
        $('.reset_inputs').click(function () {
            $('.name_input').val('');
            $('.email_input').val('');
            $('.mobile_input').val('');
        });
    </script>

@endsection

