<section class="fullwidth_block padding-bottom-75">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="headline_part centered margin-top-80">كيف يعمل سوق سماكو
                    <span class="margin-top-10">استكشف المنتاجات وقم الان بالتواصل مع المالك واشتريها فوراً</span> </h2>
            </div>
        </div>
        <div class="row container_icon">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="box_icon_two box_icon_with_line"> <i class="im im-icon-Money"></i>
                    <h3>ادفع العموله</h3>
                    <p>بعد العمليه الشراء يجب عليك دفع قيمه عموله السوق حتي يتتمكن من الاستمتاع بمميزات الاستخدام.</p>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="box_icon_two box_icon_with_line"> <i class="im im-icon-Mail-Add"></i>
                    <h3>تواصل مع المالك</h3>
                    <p>يمكنك التواصل مع المالك بسهوله من خلال رقم الهاتف او رسائل المحادثه والاتفاق علي السعر المناسب لك </p>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="box_icon_two"> <i class="im im-icon-Administrator"></i>
                    <h3>ابحث عن المنتج</h3>
                    <p>يمكنك من خلال سوق سماكو البحث عن الاعلانات بسهوله من خلال خصائص متعده للبحث لتسهيل عمليه ايجاد الاعلان المناسب لك </p>
                </div>
            </div>
        </div>
    </div>
</section>
