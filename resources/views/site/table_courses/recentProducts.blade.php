<section class="fullwidth_block margin-top-65 padding-top-75 padding-bottom-70" data-background-color="#f9f9f9"
         style="background: rgb(249, 249, 249);">
    <div class="container">
        <div class="row ">
            <div class="col-md-12">
                <h3 class="headline_part centered margin-bottom-45">
                    السوق
                    <!-- <span>استكشف احدث الاعلانات في سوق سماكو الالكتروني   </span> -->
                </h3>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 ">
                    <div class="tt">
                        <div class="row">
                        @foreach (\App\Product::orderBy("id","desc")->take(12)->get() as $product)
                            <div class="col-md-3 col-sm-3  float-r_edit">
                                <div style="display:block;height: 350px;" >
                                    <a href="{{url("/productDetails/$product->id")}}"  style="height: 150px !important ; display:block;box-shadow: 0 5px 15px 0 rgba(0,0,0,0.3);">
                                        <div class="PRODUCT_img">
                                            <img src="{{$product->serv_one_image}}" alt="">
                                        </div>
                                        <div class="PRODUCT_details">
                                            <h3>{{$product->name}}</h3>
                                            <h5 style="color:black"> <i class="sl sl-icon-user"></i> {{$product->dash_user_name}} </h5>
                                            <div class="row">
                                                <div class="CCustom_Height col-xs-6 col-sm-6">
                                                    <span style="color:black;padding-right:10px;"><i class="sl sl-icon-hourglass"></i> {{$product->time}} </span>
                                                </div>
                                                <div class="CCustom_Height col-xs-6 col-sm-6">
                                                    <span style=" color:black;"><i class="sl sl-icon-location"></i> {{$product->dash_city_name}} </span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach

                            <div class="col-md-12 centered_content"><a href="{{url('/all_products')}}" class="button border margin-top-20">
                                    عرض اعلانات السوق العام</a></div>




                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</section>
@section("extra_js")
    {{--<script>--}}

    {{--    $('.like-icon').on('click', function(e) {--}}
    {{--        $(".registerBtn").click(function () {--}}
    {{--            var product_id = $('.product_id').val();--}}
    {{--            $.ajax({--}}
    {{--                type: 'POST',--}}
    {{--                url: '{{url("siteRegisterForm")}}',--}}
    {{--                data: {--}}
    {{--                    _token: "{{csrf_token()}}",--}}
    {{--                    name: name,--}}
    {{--                    mobile: mobile,--}}
    {{--                    password: password--}}
    {{--                },--}}
    {{--                success: function (data, status) {--}}
    {{--                    if (data.success == false) {--}}
    {{--                        $('.register_noError').text(data.message)--}}
    {{--                    } else {--}}
    {{--                        if (data.status_ == 1) {--}}
    {{--                            window.location.reload();--}}
    {{--                        } else {--}}

    {{--                            $('.register_noError').text(data.message)--}}
    {{--                        }--}}
    {{--                    }--}}
    {{--                }--}}
    {{--            });--}}
    {{--        });--}}
    {{--        $(this).toggleClass('liked');--}}
    {{--    });--}}
    {{--</script>--}}
@endsection
