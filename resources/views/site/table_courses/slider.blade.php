<div id="utf_rev_slider_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classicslider1"
     style="margin: 0px auto; background-color: transparent; padding: 0px; overflow: visible; height: 900px;">
    <div class="main_inner_search_block">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="container main_inner_search_block margin_ead">
                        <form action="{{ url('/search') }}" method="get"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="trtr">
                                <div class="row">

                                    <div class="col-md-2">
                                        <div class="Slid_head" style="margin-top:0">
                                            <button type="submit" class="button">بحث  <i class="sl sl-icon-magnifier"></i></button>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="main_input_search_part_item">
                                            <input type="text" placeholder="عن ماذا تبحث ؟" value="" name="keyword">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <hr>
                                        <div class="Rig_Left">

                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <h3 class="margin-top-40 text-center"> بحث مفصل</h3>
                                                </div>
                                                <!-- الاقسام -->
                                                <div class="col-md-6">
                                                    <div class="margin-top-10">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <h3 class="Head_search">القسم</h3>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="main_input_search_part_item">
                                                                    <select class="ed_select">
                                                                        <option value="0" selected="">اختر القسم  </option>
                                                                        <option value="1">القسم </option>
                                                                        <option value="2"> القسم</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="main_input_search_part_item">
                                                                    <select class="ed_select">
                                                                        <option value="0" selected="">اختر القسم  </option>
                                                                        <option value="1">القسم </option>
                                                                        <option value="2"> القسم</option>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-12">
                                                                <div class="main_input_search_part_item">
                                                                    <select class="ed_select">
                                                                        <option value="0" selected="">اختر القسم  </option>
                                                                        <option value="1">القسم </option>
                                                                        <option value="2"> القسم</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="main_input_search_part_item">
                                                                    <select class="ed_select">
                                                                        <option value="0" selected="">اختر القسم  </option>
                                                                        <option value="1">القسم </option>
                                                                        <option value="2"> القسم</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>




                                                <!-- المكان -->
                                                <div class="col-md-6">
                                                    <div class="margin-top-10">
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <h3 class="Head_search">الموقع</h3>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="main_input_search_part_item">
                                                                    <select class="ed_select">
                                                                        <option value="0" selected="">المدينة </option>
                                                                        <option value="1">المدينة </option>
                                                                        <option value="2"> المدينة</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="main_input_search_part_item">
                                                                    <select class="ed_select">
                                                                        <option value="0" selected="">المنطقة </option>
                                                                        <option value="1"> المنطقة</option>
                                                                        <option value="2"> المنطقة</option>
                                                                    </select>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-12">
                                                                <div class="main_input_search_part_item">
                                                                    <select class="ed_select">
                                                                        <option value="0" selected="">الحي </option>
                                                                        <option value="1"> الحي</option>
                                                                        <option value="2"> الحي</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <div class="main_input_search_part_item">
                                                                    <select class="ed_select">
                                                                        <option value="0" selected="">الدولة </option>
                                                                        <option value="1"> فرعي</option>
                                                                        <option value="2"> فرعي</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>






                                    <!-- other -->

                                    <!-- <div class="col-md-3 col-sm-6">
                                        <div class="main_input_search_part_item">
                                        <select class="ed_select">
                                            <option value="0" selected="">خيارات اخري </option>
                                            <option value="1"> اخري</option>
                                            <option value="2"> اخري</option>
                                        </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6">
                                        <div class="main_input_search_part_item">
                                        <select class="ed_select">
                                            <option value="0" selected="">خيارات اخري </option>
                                            <option value="1"> اخري</option>
                                            <option value="2"> اخري</option>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="main_input_search_part_item">
                                        <select class="ed_select">
                                            <option value="0" selected="">خيارات اخري </option>
                                            <option value="1"> اخري</option>
                                            <option value="2"> اخري</option>
                                        </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6">
                                        <div class="main_input_search_part_item">
                                        <select class="ed_select">
                                            <option value="0" selected="">خيارات اخري </option>
                                            <option value="1"> اخري</option>
                                            <option value="2"> اخري</option>
                                        </select>
                                        </div>
                                    </div> -->



                                    <div class="col-sm-12">
                                        <hr>
                                        <div class="Slid_head">
                                            <button style="width:80% ;" type="submit" class="button" onclick=" ">أستكشف <i class="sl sl-icon-magnifier"></i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
 </div>
