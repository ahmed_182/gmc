@extends('.site.layout.container')
@section('title',"سياسة الشكاوى")

@section('content')



    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title">سياسة الشكاوى</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Contact Start ======-->

    <section class="contact-area">
        <div class="container">

                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="contact-title text-center">
                            <h3 class="title">سياسة الشكاوى </h3>
                            <p>يسعى مركزنا لتحسين الجودة ' ورضا المتدربين ' فإن كان لديك أي شكوى يرجى كتابتها بالتفصيل ' وسنسعى لمعالجتها بأسرع وقت 'وبخصوصية تامة.</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="contact-form-wrapper">
                            <form  action="{{url("/send_complaints")}}"
                                  enctype="multipart/form-data" method="post">
                                @csrf
                                <div class="row rtl-direction">
                                    <div class="col-md-6">
                                        <div class="single-form">
                                            <input type="text" name="name" class="form-control"  placeholder="الاسم بالكامل" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single-form">
                                            <input type="email" name="email" class="form-control"  placeholder="البريد الالكتروني" required>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="single-form">
                                            <input type="text" name="title" class="form-control" placeholder="موضوع الرسالة" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="single-form">
                                            <textarea name="message" class="form-control" placeholder="نص رسالتك..." required></textarea>
                                        </div>
                                    </div>
                                    <p class="form-message"></p>
                                    <div class="col-md-12">
                                        <div class="single-form text-center">
                                            <button class="main-btn" type="submit">تأكيد</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>

    <!--====== Contact Ends ======-->
@endsection
