<!--====== Header Start ======-->

<header class="header-area">
    <div class="header-top">
        <div class="container">
            <div class="header-top-wrapper d-flex flex-wrap justify-content-sm-between rtl-direction">
                <div class="header-top-left mt-10">
                    <ul class="header-meta">

                        <li><a href="mailto:info@gmc-sa.net">info@gmc-sa.net</a></li>
                    </ul>
                </div>

                    @guest
                    <div class="header-top-right mt-10">
                        <div class="header-link">
                        <a class="login" href="{{url("/login")}}">تسجيل الدخول </a>
                        <a class="register" href="{{url("/register")}}">إنشاء حساب</a>
                    </div>

                    @endguest
                    @auth
                        @if(auth()->user()->userVerify == 1)

                                <div class="header-top-right mt-10">
                                    <div class="header-link">
                                        <a class="login" href="{{url("/siteLogout")}}">تسجيل الخروج </a>
                                        <a class="register" href="{{url("/profile")}}">الحساب</a>
                                    </div>

                        @endif

                    @endauth
                </div>
            </div>
        </div>
    </div>

    <div id="navigation" class="navigation navigation-landscape">
        <div class="container position-relative">
            <div class="row align-items-center">

                <div class="col-lg-2 position-relative">
                    <div class="header-search">
                        <form  action="{{ url('/search') }}" method="get"
                               enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="text" placeholder="ابحث عن دورة"  value="{{request()->keyword}}" name="keyword">
                            <button><i class="fas fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col-lg-8 position-static rtl-direction">
                    <div class="nav-toggle"></div>
                    <nav class="nav-menus-wrapper">
                        <ul class="nav-menu">
                            <li>
                                <a class=" @if (str_contains(url()->full(), '/')) active @endif" href="{{url("/")}}">الرئيسية</a>
                            </li>
                            <li>
                                <a class=" @if (str_contains(url()->full(), '/courses')) active @endif"
                                    href="{{url("/courses")}}">الدورات</a>
                            </li>
                            <li>
                                <a class=" @if (str_contains(url()->full(), '/offers')) active @endif"  href="{{url("/offers")}}">العروض</a>
                            </li>
                            <li>
                                <a class=" @if (str_contains(url()->full(), '/contract_courses')) active @endif"  href="{{url("/contract_courses")}}">الدورات التعاقدية</a>
                            </li>
                            <li>
                                <a class=" @if (str_contains(url()->full(), '/table_courses')) active @endif" href="{{url("/table_courses")}}">جدول الدوارات </a>
                            </li>
                            <li>
                                <a class=" @if (str_contains(url()->full(), '/licensed_courses')) active @endif" href="{{url("/licensed_courses")}}">الدورات المرخصة</a>
                            </li>
                            <li>
                                <a class=" @if (str_contains(url()->full(), '/contact')) active @endif" href="{{url("/contact")}}">تواصل معنا</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="col-lg-2 text-right">
                    <div class="header-logo">
                        <a href="{{url("/")}}"><img src="{{asset("/assets/site")}}/images/logo.png" alt="" height="90px"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<!--====== Header Ends ======-->
<div class="clearfix"></div>

@section("extra_js")


@endsection
