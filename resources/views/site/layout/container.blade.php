<!DOCTYPE html>
<html lang="en" dir="">
@include('site.layout.head')
<body >

@include('site.layout.nav')
<div class="content">

    @yield('content')
</div>
    @include('site.layout.footer')
    @include('site.layout.scripts')
</body>
</html>
