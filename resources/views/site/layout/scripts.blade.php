<!-- Scripts -->
{{--<script src="https://code.jquery.com/jquery-3.3.1.js"></script>--}}


<!--====== Jquery js ======-->
<script src="{{asset("/assets/site")}}/js/vendor/jquery-3.5.1.min.js"></script>
<script src="{{asset("/assets/site")}}/js/vendor/modernizr-3.7.1.min.js"></script>

<!--====== All Plugins js ======-->
<script src="{{asset("/assets/site")}}/js/plugins/popper.min.js"></script>
<script src="{{asset("/assets/site")}}/js/plugins/bootstrap.min.js"></script>
<script src="{{asset("/assets/site")}}/js/plugins/slick.min.js"></script>
<script src="{{asset("/assets/site")}}/js/plugins/jquery.magnific-popup.min.js"></script>
<script src="{{asset("/assets/site")}}/js/plugins/jquery.appear.min.js"></script>
<script src="{{asset("/assets/site")}}/js/plugins/imagesloaded.pkgd.min.js"></script>
<script src="{{asset("/assets/site")}}/js/plugins/isotope.pkgd.min.js"></script>
<script src="{{asset("/assets/site")}}/js/plugins/wow.min.js"></script>
<script src="{{asset("/assets/site")}}/js/plugins/ajax-contact.js"></script>


<!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

<!-- <script src="{{asset("/assets/site")}}/js/plugin.js"></script>-->


<!--====== Main Activation  js ======-->
<script src="{{asset("/assets/site")}}/js/main.js"></script>


@yield('extra_js')

