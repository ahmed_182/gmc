<head>
    <meta name="author" content="">
    <meta name="description"
          content=" ">
    <meta https-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0;">






    <title>@yield('title')</title>

    <!-- Favicon -->

    <link rel="shortcut icon" href="{{url('assets/admin/images/logo.png')}}" type="image/png">


    <!-- CSS
    ============================================ -->

    <!--===== Vendor CSS (Bootstrap & Icon Font) =====-->

    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/plugins/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/plugins/fontawesome.min.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/plugins/default.css">


    <!--===== Plugins CSS (All Plugins Files) =====-->
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/plugins/animate.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/plugins/slick.css">
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/plugins/magnific-popup.css">

    <!--====== Main Style CSS ======-->
    <link rel="stylesheet" href="{{asset("/assets/site")}}/css/style.css">

    @yield('extra_css')

</head>
