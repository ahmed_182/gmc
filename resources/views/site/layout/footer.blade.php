<!-- Footer -->
{{--@include('sweetalert::alert')--}}
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

<?php

$footer=\App\Setting::first();
?>

<!--====== Newsletter Start ======-->

<section class="newsletter-area">
    <div class="container">
        <div class="newsletter-wrapper  wow zoomIn" data-wow-duration="1s" data-wow-delay="0.2s" style="background-image: url({{asset("/assets/site")}}//images/newsletter-bg-1.jpg);">
            <div class="row align-items-center rtl-direction text-right">
                <div class="col-lg-5">
                    <div class="section-title-2 mt-25">
                        <h2 class="title">اشترك في النشرة الإخبارية لدينا</h2>
                        <span class="line"></span>
                        <p>اشترك في النشرة الإخبارية لدينا ليصلك كل جديد</p>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="newsletter-form mt-30">
                        <form action="#">
                            <input type="text" placeholder="ادخل البريد الالكتروني هنا">
                            <button class="main-btn main-btn-2">إشترك الان</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--====== Newsletter Ends ======-->

<!--====== Footer Start ======-->

<section class="footer-area bg_cover" style="background-image: url({{asset("/assets/site")}}/images/counter-bg.jpg);">
    <div class="footer-widget">
        <div class="container">
            <div class="row rtl-direction text-right">
                <div class="col-md-12 col-sm-6">
                    <div class="footer-link mt-45">
                        <h4 class="footer-title">معلومات التواصل</h4>
                        <ul class="link-list">
                            <li>
                                <p>
                                    {{$footer->address}}</p>
                            </li>
                            <li>
                                <p><a href="tel:{{$footer->mobile}}">{{$footer->mobile}} </a></p>
                            </li>
                            <li>
                                <p><a href="mailto:{{$footer->email}}">{{$footer->email}}</a></p>
{{--                                <p><a href="#">www.example.com</a></p>--}}

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-widget-wrapper">
                <div class="footer-social">
                    <ul class="social">
                        <li><a href="{{$footer->facebook}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="{{$footer->twitter}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="{{$footer->instagram}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="{{$footer->linkedin}}" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                    </ul>
                </div>
                <div class="footer-menu">
                    <ul class="menu">
                        <li>
                            <a class="active" href="{{url("/")}}">الرئيسية</a>
                        </li>
                        <li>
                            <a href="{{url("/courses")}}">الدورات</a>
                        </li>
                        <li>
                            <a href="{{url("/offers")}}">العروض</a>
                        </li>
                        <li>
                            <a href="{{url("/archives")}}">الأرشيف</a>
                        </li>
                        <li>
                            <a href="{{url("/contract_courses")}}">الدورات التعاقدية</a>
                        </li>
                        <li>
                            <a href="{{url("/table_courses")}}">جدول الدوارات </a>
                        </li>
                        <li>
                            <a href="{{url("/licensed_courses")}}">الدورات المرخصة </a>
                        </li>
                        <li>
                            <a href="{{url("/contact")}}">تواصل معنا</a>
                        </li>

                        <li>
                            <a href="{{url("/terms_and_conditions")}}">الشروط والأحكام</a>
                        </li>

                    </ul>
                    <br>
                    <ul class="menu">
                        <li>
                            <a href="{{url("/complaints")}}">سياسية الشكاوى</a>
                        </li>

                        <li>
                            <a href="{{url("/questionnaire")}}">سياسية قياس رضا المستفيدين</a>
                        </li>



                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="copyright text-center">
                <p>&copy; جميع الحقوق محفوظه <a href="{{url("/")}}">مركز صناعة العبقرية للتدريب</a></p>
            </div>
        </div>
    </div>
</section>

<!--====== Footer Ends ======-->

<!--====== BACK TOP TOP PART START ======-->

<a href="#" class="back-to-top"><i class="fal fa-chevron-up"></i></a>

