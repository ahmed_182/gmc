@extends('.site.layout.container')
@section('title',"تواصل معانا")

@section('content')



    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title">تواصل معنا</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Contact Start ======-->

    <section class="contact-area">
        <div class="container">
            <div class="row rtl-direction text-right">
                <div class="col-md-4">
                    <div class="single-contact-info mt-30">
                        <div class="info-icon">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="info-content">
                            <h5 class="title">العنوان</h5>
                            <p>King Abdul Aziz Rd, King Abdul Aziz, Riyadh 12431, Saudi Arabia</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-contact-info mt-30">
                        <div class="info-icon">
                            <i class="fas fa-phone"></i>
                        </div>
                        <div class="info-content">
                            <h5 class="title">رقم التواصل</h5>
                            <p><a href="tel:00966-5400-99115">00966-5400-99115</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-contact-info mt-30">
                        <div class="info-icon">
                            <i class="far fa-globe"></i>
                        </div>
                        <div class="info-content">
                            <h5 class="title">البريد الالكتروني</h5>
                            <p><a href="mailto:info@gmc-sa.net">info@gmc-sa.net</a></p>
                        </div>
                    </div>
                </div>
            </div>

            <iframe style="width:100%;height:400px; "frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=%D8%A7%D9%84%D8%B9%D8%A8%D9%82%D8%B1%D9%8A%D8%A9%20%D9%84%D9%84%D8%AA%D8%AF%D8%B1%D9%8A%D8%A8%20%D8%A7%D9%84%D8%B1%D9%8A%D8%A7%D8%B6%20%D8%A7%D9%84%D9%85%D9%85%D9%84%D9%83%D8%A9%20%D8%A7%D9%84%D8%B9%D8%B1%D8%A8%D9%8A%D8%A9%20%D8%A7%D9%84%D8%B3%D8%B9%D9%88%D8%AF%D9%8A%D8%A9&amp;t=m&amp;z=16&amp;output=embed&amp;iwloc=near" title="العبقرية للتدريب الرياض المملكة العربية السعودية" aria-label="العبقرية للتدريب الرياض المملكة العربية السعودية"></iframe>            <div class="contact-form">
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="contact-title text-center">
                            <h3 class="title">تواصل معنا </h3>
                            <p>نرحب باستفساراتكم واقتراحاتكم عبر ارسال النموذج التالي</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-8">
                        <div class="contact-form-wrapper">
                            <form  action="{{url("/contact-us")}}"
                                  enctype="multipart/form-data" method="post">
                                @csrf
                                <div class="row rtl-direction">
                                    <div class="col-md-6">
                                        <div class="single-form">
                                            <input type="text" name="name" class="form-control"  placeholder="الاسم بالكامل" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single-form">
                                            <input type="email" name="email" class="form-control"  placeholder="البريد الالكتروني" required>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="single-form">
                                            <input type="text" name="mobile" class="form-control"  placeholder="رقم الجوال" required>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="single-form">
                                            <select name="country_id">
                                                @foreach(\App\Country::get() as $country)

                                                <option value="{{$country->id}}" >{{$country->symbol}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="single-form">
                                            <input type="text" name="subject" class="form-control" placeholder="موضوع الرسالة" required>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="single-form">
                                            <textarea name="message" class="form-control" placeholder="اكتب رسالتك هنا..." required></textarea>
                                        </div>
                                    </div>
                                    <p class="form-message"></p>
                                    <div class="col-md-12">
                                        <div class="single-form text-center">
                                            <button class="main-btn" type="submit">تأكيد</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Contact Ends ======-->
@endsection
