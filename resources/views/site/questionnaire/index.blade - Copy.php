@extends('.site.layout.container')
@section('title','نموذج الأستبيان')
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title">نموذج الأستبيان </h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Login Start ======-->

    <section class="login-register">
        <div class="container">
            <div class="row text-right">
                <div class="col-lg-12">
                    <div class="login-register-content">
                        <h4 class="title">يرجى تعبئة النموذج التالي </h4>

                        <div class="login-register-form">
                            <form action="{{url("/make_questionnaire")}}"
                                  enctype="multipart/form-data" method="post">
                                @csrf

                                <div class="single-form col-md-6 float-right">
                                    <label>الاسم بالكامل*</label>
                                    <input type="text" name="name" class="form-control"  placeholder="الاسم بالكامل" required>
                                </div>
                                <div class="single-form col-md-6 float-right">
                                    <label> البريد الالكترونى *</label>
                                    <input type="email" name="email" class="form-control"  placeholder="البريد الالكترونى" required>
                                </div>

                                <div class="single-form col-md-4 float-right">
                                    <label>الحالة الاجتماعية*</label>
                                    <input type="text" name="social_status" class="form-control"  placeholder="الحالة الاجتماعية" required>
                                </div>
                                <div class="single-form col-md-4 float-right">
                                    <label>المؤهل العلمى*</label>
                                    <input type="text" name="qualification" class="form-control"  placeholder="المؤهل العلمى" required>
                                </div>
                                <div class="single-form col-md-4 float-right">
                                    <label>جهة العمل  *</label>
                                    <input  type="text" name="work_place" class="form-control"  placeholder="جهة العمل " required>
                                </div>

                                <div class="single-form col-md-4 float-right">
                                    <label>المدينة*</label>
                                    <input type="text" name="city" class="form-control"  placeholder="المدينة" required>
                                </div>
                                <div class="single-form col-md-4 float-right">
                                    <label>جوال 1</label>
                                    <input type="text" name="mobile" class="form-control"  placeholder="جوال 1" required>
                                </div>
                                <div class="single-form col-md-4 float-right  mb-40">
                                    <label>جوال 2  </label>
                                    <input  type="text" name="mobile2" class="form-control"  placeholder="جوال 2  " required>
                                </div>

                                <br>

                                <div class="single-form col-md-6 float-right">
                                    <label>أسلوب المدرب</label>
                                    <input type="text" name="trainer_style" class="form-control"  placeholder="أسلوب المدرب" >
                                </div>
                                <div class="single-form col-md-6 float-right  mb-20">
                                    <label>مستوى المدرب </label>
                                    <input  type="text" name="trainer_level" class="form-control"  placeholder="مستوى المدرب   " >
                                </div>

                                <div class="single-form col-md-6 float-right">
                                    <label>المادة التدربية</label>
                                    <input type="text" name="course" class="form-control"  placeholder="المادة التدربية" >
                                </div>
                                <div class="single-form col-md-6 float-right  mb-40">
                                    <label>مستوى المادة </label>
                                    <input  type="text" name="course_level" class="form-control"  placeholder="مستوى المادة  " >
                                </div>
                                <br>

                                <h6 class="title mb-20">ما هى الوسيلة التى وصلت بها الدورة ؟</h6>

                                <div class="form-check col-md-3 float-right ">
                                    <input class="form-check-input" type="radio" name="method_course" id="flexRadioDefault1" value="رسالة جوال">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        رسالة جوال
                                    </label>
                                </div>
                                <div class="form-check col-md-3 float-right">
                                    <input class="form-check-input" type="radio" name="method_course" id="flexRadioDefault2" value="                                       بريد الكترونى
">
                                    <label class="form-check-label" for="flexRadioDefault2">
                                       بريد الكترونى
                                    </label>
                                </div>
                                <div class="form-check col-md-3 float-right">
                                    <input class="form-check-input" type="radio" name="method_course" id="flexRadioDefault2" value="الموقع الالكترونى" >
                                    <label class="form-check-label" for="flexRadioDefault2">
                                       الموقع الالكترونى
                                    </label>
                                </div>
                                <div class="form-check col-md-3 float-right  mb-20">
                                    <input class="form-check-input" type="radio" name="method_course" id="flexRadioDefault2" value="أخرى" >
                                    <label class="form-check-label" for="flexRadioDefault2">
                                       أخرى
                                    </label>
                                </div>

          <br>   <h6 class="title mb-20">من باب نشر العلم نرجو ان ترشحوا لنا أصدقاء أو أقارب للالتحاق بمثل هذة الدورات </h6>
          <br>
                                <table class="table table-bordered" style="direction: rtl;
}" >
                                    <thead>
                                    <tr>

                                        <th scope="col">الاسم</th>
                                        <th scope="col">الجوال</th>
                                        <th scope="col">الاسم</th>
                                        <th scope="col">الجوال</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row"> <input type="text" name="name_questionnaire[]" class="form-control"  >
                                        </th>
                                        <td><input type="text" name="mobile_questionnaire[]" class="form-control" >
                                        </td>
                                        <td><input type="text" name="name_questionnaire[]" class="form-control"  >
                                        </td>
                                        <td> <input type="text" name="mobile_questionnaire[]" class="form-control"  >
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"> <input type="text" name="name_questionnaire[]" class="form-control"  >
                                        </th>
                                        <td><input type="text" name="mobile_questionnaire[]" class="form-control" >
                                        </td>
                                        <td><input type="text" name="name_questionnaire[]" class="form-control"  >
                                        </td>
                                        <td> <input type="text" name="mobile_questionnaire[]" class="form-control"  >
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"> <input type="text" name="name_questionnaire[]" class="form-control"  >
                                        </th>
                                        <td><input type="text" name="mobile_questionnaire[]" class="form-control" >
                                        </td>
                                        <td><input type="text" name="name_questionnaire[]" class="form-control"  >
                                        </td>
                                        <td> <input type="text" name="mobile_questionnaire[]" class="form-control"  >
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row"> <input type="text" name="name_questionnaire[]" class="form-control"  >
                                        </th>
                                        <td><input type="text" name="mobile_questionnaire[]" class="form-control" >
                                        </td>
                                        <td><input type="text" name="name_questionnaire[]" class="form-control"  >
                                        </td>
                                        <td> <input type="text" name="mobile_questionnaire[]" class="form-control"  >
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                                <h6 class="title mb-20"> دورات ترغب بحضورها :</h6>
                                @foreach($courses as $item)
                                <div class="form-check col-md-3 float-right ">
                                    <input class="form-check-input" type="checkbox" name="course_new[]" id="flexRadioDefault1" value=" {{ $item->id }}">
                                    <label class="form-check-label" for="flexRadioDefault1">
                                        {{ $item->name }}
                                    </label>
                                </div>
                                @endforeach
                                <br>
                                <br>
                                <br>
                                <div class="single-form">
                                    <button class="main-btn btn-block" type="submit">تأكيد</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--====== Login Ends ======-->


@endsection
@section('extra_css')
   {{-- <style>
        .footer_sticky_part {
            padding-top: 100px !important;
        }
    </style>--}}
@endsection

