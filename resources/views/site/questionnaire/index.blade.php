@extends('.site.layout.container')
@section('title','سياسة قياس رضا المستفيدين')
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title">سياسة قياس رضا المستفيدين</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Login Start ======-->

    <section class="login-register">
        <div class="container">
            <div class="row text-right">
                <div class="col-lg-12">
                    <div class="login-register-content">
                        <div>
                            <h8 class="title">بين يديك استبيان يهدف إلى معرفة مدى رضا المتدرب عن التعليم الإلكتروني ، والمشاكل التي تواجه المتدرب ، بحيث تحد من تفعيل التعليم الإلكتروني ، نرجو التكرم بتعبئتها ،، </h8>
                        </div>


                        <div class="login-register-form">
                            <form action="{{url("/make_questionnaire")}}"
                                  enctype="multipart/form-data" method="post">
                                @csrf

                                <div class="single-form col-md-6 float-right">
                                    <label>اسم المتدرب(اختيارى)</label>
                                    <input type="text" name="name" class="form-control"  placeholder="اسم المتدرب(اختيارى)" >
                                </div>
                                <div class="single-form col-md-6 float-right mb-40">
                                    <label> البريد الالكترونى *</label>
                                    <input type="email" name="email" class="form-control"  placeholder="البريد الالكترونى" required>
                                </div>


                                <table class="table table-bordered" style="direction: rtl;
}" >
                                    <thead>
                                    <tr>

                                        <th scope="col">م</th>
                                        <th scope="col">نص السؤال</th>
                                        <th scope="col">أوافق</th>
                                        <th scope="col">محايد</th>
                                        <th scope="col">لا أوفق</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">
                                            1
                                        </th>
                                        <td>
                                            أرى أن التعليم الإلكتروني ضروري لزيادة مخرجات التدريب.
                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty1" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty1" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty1" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            2
                                        </th>
                                        <td>
                                            هناك عوائق مادية تحول دون استخدام أنظمة التدريب الإلكتروني.             </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty2" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty2" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty2" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                           3
                                        </th>
                                        <td>
                                            هناك عوائق معرفية تحول دون استخدام التدريب الإلكتروني .
                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty3" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty3" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty3" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            4
                                        </th>
                                        <td>
                                            هناك عوائق معرفية تحول دون استخدام التدريب الإلكتروني .      </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty4" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty4" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty4" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                           5
                                        </th>
                                        <td>
                                            أحتاج إلى معلومات حول استخدام أنظمة التدريب الإلكتروني.   </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty5" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty5" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty5" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                           6
                                        </th>
                                        <td>
                                            أعلم بوجود فيديوهات توضيحية على موقع التدريب الإلكتروني.
                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty6" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty6" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty6" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            7
                                        </th>
                                        <td>
                                            أشعر بالرضا عن مدى الاستفادة من التدريب الإلكتروني.
                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty7" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty7" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty7" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                           8
                                        </th>
                                        <td>
                                            أشعر بالرضا من برامج التدريب الإلكتروني.   </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty8" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty8" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty8" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                           9
                                        </th>
                                        <td>
                                            أشعر بالرضا من خطة التدريب السنوية للتطوير في مجال تطبيقات تكنولوجية التدريب وتنفيذها.
                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty9" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty9" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty9" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                           10
                                        </th>
                                        <td>
                                            أشعر بالرضا من وجود قاعدة بيانات للمدربين والدورات التدريبية في مجال التعليم الإلكتروني والرجوع إليها عند الحاجة .
                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty10" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty10" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty10" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            11
                                        </th>
                                        <td>
                                            أشعر بالرضا في استخدام الموقع الإلكتروني للمركز.
                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty11" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty11" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty11" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                           12
                                        </th>
                                        <td>
                                            أشعر بالرضا عن تجهيز القاعات والمنصات الافتراضية بأدوات التدريب الإلكتروني.    </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty12" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty12" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty12" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">
                                            13
                                        </th>
                                        <td>
                                            أشعر بالرضا عن مدى تفعيل المركز لتقنيات التدريب الإلكتروني.   </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty13" id="flexRadioDefault1" value="أوافق">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty13" id="flexRadioDefault1" value="محايد">

                                        </td>
                                        <td>
                                            <input class="form-check-input" type="radio" name="qty13" id="flexRadioDefault1" value="لا أوافق">

                                        </td>
                                    </tr>


                                    </tbody>
                                </table>

                                <br>
                                <br>
                                <br>
                                <div class="single-form">
                                    <button class="main-btn btn-block" type="submit">تأكيد</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--====== Login Ends ======-->


@endsection
@section('extra_css')
   {{-- <style>
        .footer_sticky_part {
            padding-top: 100px !important;
        }
    </style>--}}
@endsection

