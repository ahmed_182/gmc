@extends('.site.layout.container')
@section('title','تسجيل الدخول')
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg " style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title"> تسجيل الدخول</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->


    <!--====== Login Start ======-->

    <section class="login-register">
        <div class="container">
            <div class="row justify-content-center text-right">
                <div class="col-lg-6">
                    <div class="login-register-content">
                        <h4 class="title">قم بتسجيل الدخول لحسابك</h4>

                        <div class="login-register-form">
                            <form class="login-form" method="post"  action="{{url("/login_site")}}" enctype="multipart/form-data" >

                                @csrf
                                <div class="single-form">
                                    <label>البريد الالكتروني</label>
                                    <input type="email" class="form-control" placeholder="{{trans('language.email')}}" name="email" required="">
                                </div>
                                <div class="single-form">
                                    <label>كلمة المرور</label>
                                    <input type="password" class="form-control" placeholder="{{trans('language.password')}}" name="password" required="">
                                </div>
                                <div class="single-form">
                                    <button class="main-btn btn-block">تسجيل الدخول</button>
                                </div>
                                <div class="single-form d-flex justify-content-between rtl-direction">
                                    <div class="checkbox">
                                        <input type="checkbox" id="remember">
                                        <label for="remember"><span></span>تذكرني</label>
                                    </div>
                                    <div class="forget">
                                        <a href="#">نسيت كلمة المرور؟</a>
                                    </div>
                                </div>
                                <div class="single-form">
                                    <label>ليس لديك حساب </label>
                                    <a href="{{url("/register")}}" class="main-btn main-btn-2 btn-block">إنشاء حساب الان</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Login Ends ======-->


@endsection
@section('extra_css')
    {{--   <style>
           .footer_sticky_part {
               padding-top: 100px !important;
           }
       </style>--}}
@endsection

