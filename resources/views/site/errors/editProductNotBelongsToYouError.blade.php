@extends('.site.layout.container')
@section('content')

    <div class="not_found_block">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <section id="not-found" class="center">
                        <h2 style="color: #ff2222"> 404</h2>
                        <p>الصفحة غير موجودة</p>
                        <div class="utf_error_description_part utf_ferror_description">
                            <strong class="f-primary animated v fadeInLeft">عذرا، هذه الصفحة غير مسموح لك الدخول اليها  </strong>
                            <span class="f-primary animated v fadeInRight">هذا الاعلان ليس ملك لك وبعتبر ذلك اختراق لقوانين الموقع وقد تقوم الاداره بأيقاف الحساب الخاص بك </span> </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <!-- Subscribe -->
    @include('site.home.subscribe')

@endsection
