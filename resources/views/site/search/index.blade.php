@extends('.site.layout.container')
@section('title','نتيجة البحث')
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg " style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title"> نتيجة البحث</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <section class="blog-page">

        <div class="container">
            <div class="row rtl-direction">
                <div class="col-lg-12">
                    <form method="get" action="{{url("/courses/")}}">

                        <div style="display: flex">
                            <div class="col-md-3">
                                <select class="form-control" name="category_id">
                                    <option value="">{{trans('language.category')}}</option>
                                    @foreach(\App\Category::all() as $selectItem)
                                        <option value="{{$selectItem->id}}">{{$selectItem->dash_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-3">
                                <input style="width: 45%" type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                                <button style="width: 45%" type="submit"
                                        class="btn btn-info  reset_inputs ">اعادة البحث</button>
                            </div>

                        </div>
                    </form>
                    <br>
                    @foreach ($items as $course)
                        <div class="col-lg-4 col-md-6 flo-right">
                            <div class="single-blog mt-30">
                                <div class="blog-image">
                                    <a href="{{url("/courseDetails/$course->id")}}">
                                        <img src="{{$course->image}}" alt="blog">
                                    </a>
                                </div>
                                <div class="blog-content">
                                    <h4 class="blog-title text-right"><a href="{{url("/courseDetails/$course->id")}}">{{$course->name}}</a></h4>
                                    <a href="{{url("/courseDetails/$course->id")}}" class="more"><i class="fal fa-chevron-left"></i> المزيد </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <ul class="pagination-items text-center">
                {{$items->links()}}
            </ul>
        </div>
    </section>
    <!--====== Counter Start ======-->

    <div class="counter-area-2">
        <div class="container">
            <div class="counter-wrapper-2 " style="background-image: url({{asset("/assets/site")}}/images/counter-bg-2.jpg);">
                <div class="row">
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30">
                            <span class="counter-count"><span class="count">3652</span> +</span>
                            <p>المتدربين</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30">
                            <span class="counter-count"><span class="count">105</span> +</span>
                            <p>المدربين</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30">
                            <span class="counter-count"><span class="count">120</span> +</span>
                            <p>الدورات</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30">
                            <span class="counter-count"><span class="count">30</span> +</span>
                            <p>الجوائز</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====== Counter Ends ======-->


@endsection
@section('extra_css')
    {{--   <style>
           .footer_sticky_part {
               padding-top: 100px !important;
           }
       </style>--}}
@endsection

