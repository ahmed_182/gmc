@extends('.site.layout.container')
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="headline_part centered margin-top-75">
                    الاقسام الفرعية
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="direction: rtl">
                <div class="container_categories_box margin-top-5 margin-bottom-30" >
                    @if(count($categories) > 0)
                        @foreach($categories as $cat)
                            <div class="col-lg-2 col-md-4 col-xs-6">
                            <a href="{{url("/categoryDetails/$cat->id")}}" class="utf_category_small_box_part " style="background-color: #faebeb;height: 150px; width:100%">
                                <img class="im imgicon" src="{{$cat->dash_image}}">
                                <h3 style="color: black !important;font-weight: bold">{{$cat->dash_name}}</h3>
                                <span style="color: black !important;font-weight: bold" >{{$cat->totalProducts()->count()}}</span>
                            </a>
                            </div>
                        @endforeach
                    @else
                        <h3>لا تحتوي علي اقسام فرعيه .</h3>
                    @endif
                </div>
                {{--            <div class="col-md-12 centered_content"><a href="#" class="button border margin-top-20">عرض المزيد</a></div>--}}
            </div>
        </div>

        <div class="col-md-12">
            <h3 class="headline_part centered margin-top-75">
                منتاجات القسم
                (
                {{$category->dash_name}}
                )
            </h3>
        </div>
        <div class="row">
            <div class="col-md-12" style="direction: rtl">
                <div class="container_categories_box margin-top-5 margin-bottom-30">
                    @if(count($items) > 0)
                        @foreach ($items as $product)
                            <div class=" col-md-2 col-sm-3 float-r_edit ">
                                <a href="{{url("/productDetails/$product->id")}}" class="img-box"
                                   data-background-image="{{$product->serv_one_image}}" style="height: 150px">
                                    <div class="utf_img_content_box " style="top: 70px">
                                        <h5 style="color: #FFFFFF">{{$product->serv_product_name}} </h5>
                                        <span><i class="sl sl-icon-location"></i> {{$product->dash_city_name}}</span>
                                    </div>
                                </a>
                            </div>

                        @endforeach
                    @else
                        <h3>لا تحتوي علي منتاجات .</h3>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
