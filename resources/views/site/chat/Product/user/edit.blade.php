@extends('.site.layout.container')
@section('content')
    <section class="fullwidth_block  padding-top-75 padding-bottom-70" data-background-color=""
             style="background: rgb(249, 249, 249);">

        <form id="contactform" method="post" action="{{url("/updateProduct")}}">
            @csrf
            <input type="hidden" class="location" id="location" name="location" value=" 24.7246242,46.8197233">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12">
                        <h3 class="headline_part centered margin-bottom-45">
                            تعديل اعلان<span> لابد من ادخال بيانات الاعلان بصوره صحيحه طبقا   <a style="color: red"
                                                                                                 href="{{url("/terms")}}">للشروط و الاحكام</a>   </span>
                        </h3>
                    </div>
                    <div class="col-md-12" style="direction: rtl">
                        <section id="contact" class=" ">
                            <div class="row">

                                <input type="hidden" value="{{$item->id}}" name="product_id" >

                                <a href="{{url("/EditProductImages/$item->id")}}" style="width: 170px" class="button">
                                    تعديل صور المنتج </a>

{{--                                <div class="form-group col-xs-12">--}}
{{--                                    <select style="height: 60px" id="main_category_id" name="main_category_id"--}}
{{--                                            class="main_category_id_list main_category_id">--}}
{{--                                        <option value="0">اختر القسم</option>--}}
{{--                                        @foreach(\App\Categories::where("parent_id",null)->get() as $category)--}}
{{--                                            <option name="main_category_id"--}}
{{--                                                    value="{{$category->id}}">{{$category->dash_name}}</option>--}}
{{--                                        @endforeach--}}
{{--                                    </select>--}}
{{--                                </div>--}}


{{--                                <div class="form-group col-xs-12 hide category_List">--}}
{{--                                    <select style="height: 60px" id="category_id" name="category_id"--}}
{{--                                            class="category_id_list category_id">--}}
{{--                                        <option name="category_id" class="basicOption" value="0">اختر القسم</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}

                                <br>


                                <div class="col-md-12">
                                    <input name="name" type="text" placeholder="عنوان الاعلان" required=""
                                           value="{{$item->name}}">
                                </div>



                                <div class="col-md-12">
                                    <input name="mobile" type="text" placeholder="وسيله الاتصال" required=""
                                           value="{{$item->mobile}}">
                                </div>

                                <div class="col-md-12">
                                    <select style="height: 60px" class="" name="city_id">
                                        <option value="0">اختر المدينه</option>
                                        @foreach($cities as $city)
                                            <option
                                                @if($item->city_id == $city->id) selected @endif
                                            value="{{$city->id}}">{{$city->dash_name}}</option>
                                        @endforeach
                                    </select>
                                </div>



                                <div class="col-md-12">
                                    <textarea name="description" cols="40" rows="2" id="comments"
                                              placeholder="وصف الاعلان ..."
                                              required="">{{$item->description}}</textarea>
                                </div>

                                <!--The div element for the map -->
                                <div id="map"></div>

                                <!-- $options -->
                                <div class="add_utf_listing_section margin-top-45">
                                    <div class=" ">
                                        <h4> خيارات اخري </h4>
                                    </div>
                                    <div class="checkboxes in-row amenities_checkbox">
                                        <ul>
                                            @foreach($options as $option)
                                                <li style="direction: rtl;float: right;text-align: right;width: 25% !important;">
                                                    <input id="check-a{{$option->id}}" type="checkbox"
                                                           @if(in_array($option["id"], $productOptions))
                                                           checked
                                                           @else

                                                           @endif

                                                           value="{{$option->id}}" name="options[]">
                                                    <label for="check-a{{$option->id}}">{{$option->dash_name}}</label>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <input type="submit" style="width: 150px" class="submit button" id="submit"
                                   value="تحديث البيانات">
                        </section>
                    </div>
                </div>
            </div>
        </form>
    </section>

@endsection
@section('extra_css')
    <style>
        .imupl-files-list {
            width: 100%;
            min-height: 200px;
            border: 1px solid grey;
            border-radius: 10px;
            background: #fff;
            position: relative;
            padding: 25px 0;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            margin-bottom: 30px;
        }

        .imupl-file-item {
            width: 200px;
            height: 200px;
            background-color: #f5f5f5;
            background-size: contain;
            background-position: center center;
            background-repeat: no-repeat;
            background-image: url(https://d13yacurqjgara.cloudfront.net/users/82092/screenshots/1073359/spinner.gif);
            margin: 25px;
            margin-top: 0;
            display: inline-block;
            border-radius: 10px;
            border: 2px solid #e6e6e6;
            overflow: hidden;
            position: relative;
            cursor: move;
        }

        .imupl-file-item:nth-child(1) {
            border: 2px solid blue;
        }

        .imupl-file-item:nth-child(1)::before {
            content: 'MAIN';
            display: block;
            position: absolute;
            right: 0;
            bottom: 0;
            border-top-left-radius: 10px;
            background: blue;
            color: white;
            font-size: 12px;
            font-weight: bold;
            padding: 3px 6px;
        }

        .imupl-file-item .imupl-button-remove, .imupl-file-item .imupl-button-edit, .imupl-file-item .imupl-button-rotate-cw, .imupl-file-item .imupl-button-rotate-ccw {
            display: block;
            position: absolute;
            background: #e6e6e6;
            font-size: 18px;
            padding: 3px 6px;
            cursor: pointer;
        }

        .imupl-file-item.loading .imupl-button-remove, .imupl-file-item.loading .imupl-button-edit, .imupl-file-item.loading .imupl-button-rotate-cw, .imupl-file-item.loading .imupl-button-rotate-ccw {
            display: none;
        }

        .imupl-file-item .imupl-button-remove {
            left: 0;
            top: 0;
            border-bottom-right-radius: 10px;
            padding-right: 10px;
        }

        .imupl-file-item .imupl-button-edit {
            right: 0;
            top: 0;
            border-bottom-left-radius: 10px;
            padding-left: 10px;
        }

        .imupl-file-item .imupl-button-rotate-cw {
            bottom: 0;
            left: 30px;
            border-top-right-radius: 10px;
            padding-left: 10px;
        }

        .imupl-file-item .imupl-button-rotate-ccw {
            bottom: 0;
            left: 0;
            padding-left: 10px;
        }

        @media (max-width: 400px) {
            .imupl-file-item {
                width: 150px;
                height: 150px;
            }
        }

        .imupl-fileinput {
            width: 1px;
            height: 1px;
            opacity: 0;
            position: fixed;
            left: -10px;
        }

        .imupl-dragdrop-hover {
            z-index: 9999;
            position: fixed;
            top: 25px;
            left: 25px;
            right: 25px;
            bottom: 25px;
            border: 5px rgba(0, 150, 0, 0.5) dashed;
            border-radius: 25px;
            background: rgba(0, 150, 0, 0.1);
            display: none;
            pointer-events: none;
        }

        .imupl-dragdrop-hover.active {
            display: initial;
        }

        .imupl-edit-overlay {
            display: flex;
            justify-content: center;
            align-items: center;
            opacity: 0;
            pointer-events: none;
            background: rgba(0, 0, 0, 0.5);
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 999;
            transition: 0.2s opacity;
        }

        .imupl-edit-overlay .thumbnail {
            transform: translateY(-100%);
            transition: 0.5s transform ease-in-out;
        }

        .imupl-edit-overlay .thumbnail .img {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
        }

        .imupl-edit-overlay.active {
            opacity: 1;
            pointer-events: initial;
        }

        .imupl-edit-overlay.active .thumbnail {
            transform: translateY(0);
        }

        .imupl-crop-wrapper {
            position: relative;
            margin-bottom: 15px;
        }

        .imupl-cropper {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            border: 2px solid rgba(0, 0, 0, 0.5);
        }

        .imupl-cropper-start, .imupl-cropper-end {
            position: absolute;
            width: 20px;
            height: 20px;
            cursor: nwse-resize;
        }

        .imupl-cropper-start {
            top: -2px;
            left: -2px;
            border-top: 4px solid black;
            border-left: 4px solid black;
        }

        .imupl-cropper-end {
            bottom: -2px;
            right: -2px;
            border-bottom: 4px solid black;
            border-right: 4px solid black;
        }


    </style>
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 400px; /* The height is 400 pixels */
            width: 100%; /* The width is the width of the web page */
        }
    </style>
@endsection
@section('extra_js')
    <script>
        var imuplFilesMax = 6;
        var imuplMaxDimension = 1200;
        var imuplJpgQuality = 0.75;
        var imuplMaxEditorSize = 0.8;

        var imuplFilesCurrent = 0;
        var imuplOffset = 1;
        var imuplFiles = [];
        var imuplEditorLoaded = 0;
        var imuplCropperDragging = 0;
        var imuplEditorRatio = 1;

        var $imuplFilesList = $('.imupl-files-list'),
            $imuplFilesCurrent = $('.imupl-files-current'),
            $imuplFilesMax = $('.imupl-files-max'),
            $imuplEditorImg = $('.imupl-edit-overlay .thumbnail .img'),
            $imuplEditorOverlay = $('.imupl-edit-overlay'),
            $imuplCropWrapper = $('.imupl-crop-wrapper'),
            $imuplCropper = $('.imupl-cropper'),
            $imuplCropperStart = $('.imupl-cropper-start'),
            $imuplCropperEnd = $('.imupl-cropper-end');

        $imuplFilesList.sortable();

        function imuplUpdateFileCount() {
            $imuplFilesCurrent.html(imuplFilesCurrent);
            $imuplFilesMax.html(imuplFilesMax);
        }

        imuplUpdateFileCount();

        function imuplRender(offset) {
            var img = imuplFiles[offset];
            var thisElement = $('div[data-imupl-offset="' + offset + '"]');

            var cropWidth = img.cropEndX - img.cropStartX;
            var cropHeight = img.cropEndY - img.cropStartY;
            var finalWidth = 0;
            var finalHeight = 0;
            if (cropWidth <= imuplMaxDimension && cropHeight <= imuplMaxDimension) {
                finalWidth = cropWidth;
                finalHeight = cropHeight;
            } else if (cropWidth > cropHeight) {
                finalWidth = imuplMaxDimension;
                finalHeight = cropHeight * (imuplMaxDimension / cropWidth);
            } else {
                finalHeight = imuplMaxDimension;
                finalWidth = cropWidth * (imuplMaxDimension / cropHeight);
            }

            var canvas = document.createElement('canvas');
            canvas.width = finalWidth;
            canvas.height = finalHeight;
            var context = canvas.getContext('2d');

            context.drawImage(img.imgObj, img.cropStartX, img.cropStartY, cropWidth, cropHeight, 0, 0, finalWidth, finalHeight);
            img.resultData = canvas.toDataURL('image/jpeg', imuplJpgQuality);

            thisElement.css('background-image', 'url(' + img.resultData + ')');
            $('input', thisElement).val(img.resultData);
            thisElement.removeClass('loading');
        }

        function imuplRotateCW(offset) {
            var img = imuplFiles[offset];
            var newWidth = img.origHeight,
                newHeight = img.origWidth;
            var oldCropStartX = img.cropStartX,
                oldCropEndX = img.cropEndX,
                oldCropStartY = img.cropStartY,
                oldCropEndY = img.cropEndY;

            img.cropStartX = img.origHeight - oldCropEndY;
            img.cropStartY = oldCropStartX;
            img.cropEndX = img.origHeight - oldCropStartY;
            img.cropEndY = oldCropEndX;
            img.origHeight = newHeight;
            img.origWidth = newWidth;

            var canvas = document.createElement('canvas');
            canvas.width = newWidth;
            canvas.height = newHeight;
            var context = canvas.getContext('2d');

            context.save();
            context.translate(newWidth / 2, newHeight / 2);
            context.rotate(90 * Math.PI / 180);
            context.drawImage(img.imgObj, -(newHeight / 2), -(newWidth / 2));
            context.restore();

            img.rawData = canvas.toDataURL();
            var imgObj = new Image;
            imgObj.onload = function () {
                img.imgObj = imgObj;
                imuplRender(offset);
            };
            imgObj.src = img.rawData;
        }

        function imuplRotateCCW(offset) {
            var img = imuplFiles[offset];
            var newWidth = img.origHeight,
                newHeight = img.origWidth;
            var oldCropStartX = img.cropStartX,
                oldCropEndX = img.cropEndX,
                oldCropStartY = img.cropStartY,
                oldCropEndY = img.cropEndY;

            img.cropStartX = oldCropStartY;
            img.cropStartY = img.origWidth - oldCropEndX;
            img.cropEndX = oldCropEndY;
            img.cropEndY = img.origWidth - oldCropStartX;
            img.origHeight = newHeight;
            img.origWidth = newWidth;

            var canvas = document.createElement('canvas');
            canvas.width = newWidth;
            canvas.height = newHeight;
            var context = canvas.getContext('2d');

            context.save();
            context.translate(newWidth / 2, newHeight / 2);
            context.rotate(-90 * Math.PI / 180);
            context.drawImage(img.imgObj, -(newHeight / 2), -(newWidth / 2));
            context.restore();

            img.rawData = canvas.toDataURL();
            var imgObj = new Image;
            imgObj.onload = function () {
                img.imgObj = imgObj;
                imuplRender(offset);
            };
            imgObj.src = img.rawData;
        }


        function imuplCloseEditor() {
            $imuplEditorOverlay.removeClass('active');
            var img = imuplFiles[imuplEditorLoaded];
            img.cropStartX = parseInt($imuplCropper.css('left')) / imuplEditorRatio;
            img.cropStartY = parseInt($imuplCropper.css('top')) / imuplEditorRatio;
            img.cropEndX = ($imuplCropWrapper.width() - parseInt($imuplCropper.css('right'))) / imuplEditorRatio;
            img.cropEndY = ($imuplCropWrapper.height() - parseInt($imuplCropper.css('bottom'))) / imuplEditorRatio;
            console.log(img);
            setTimeout(function () {
                imuplRender(imuplEditorLoaded);
                imuplEditorLoaded = 0;
            }, 1);
        }

        function imuplOpenEditor(offset) {
            var img = imuplFiles[offset];
            imuplEditorLoaded = offset;
            imuplCropperDragging = 0;

            $imuplEditorImg.attr('style', '');
            $imuplEditorImg.css('background-image', 'url(' + img.rawData + ')');

            var imgWidth = img.origWidth,
                imgHeight = img.origHeight,
                ratio = 1;
            if (imgWidth > $(window).width() * imuplMaxEditorSize) {
                ratio *= $(window).width() * imuplMaxEditorSize / imgWidth;
            }
            if (imgHeight > $(window).height() * imuplMaxEditorSize) {
                ratio *= $(window).height() * imuplMaxEditorSize / imgHeight;
            }
            imgWidth = img.origWidth * ratio;
            imgHeight = img.origHeight * ratio;

            $imuplEditorImg.css('width', imgWidth);
            $imuplEditorImg.css('height', imgHeight);

            $imuplCropper.css('left', img.cropStartX * ratio).css('top', img.cropStartY * ratio);
            $imuplCropper.css('right', imgWidth - img.cropEndX * ratio).css('bottom', imgHeight - img.cropEndY * ratio);

            imuplEditorRatio = ratio;
            $imuplEditorOverlay.addClass('active');
        }

        function imuplAddFile(f) {
            if (imuplFilesCurrent >= imuplFilesMax || f.type.indexOf("image") !== 0) {
                return;
            }
            var thisOffset = imuplOffset++;
            var thisElement = $('<div class="imupl-file-item loading" data-imupl-offset="' + thisOffset + '"><div class="imupl-button-remove"><i class="fa fa-trash-o"></i></div><div class="imupl-button-edit"><i class="fa fa-crop"></i></div><div class="imupl-button-rotate-ccw"><i class="fa fa-rotate-left"></i></div><div class="imupl-button-rotate-cw"><i class="fa fa-rotate-right"></i></div><input type="hidden" name="imupl_payload[]" value="" /></div>');
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = new Image;
                img.onload = function () {
                    var newImage = {
                        rawData: e.target.result,
                        resultData: e.target.result,
                        imgObj: img,
                        origWidth: img.width,
                        origHeight: img.height,
                        cropStartX: 0,
                        cropStartY: 0,
                        cropEndX: img.width,
                        cropEndY: img.height
                    };
                    imuplFiles[thisOffset] = newImage;
                    imuplRender(thisOffset);
                };
                img.src = e.target.result;
            }
            reader.readAsDataURL(f);
            thisElement.appendTo($imuplFilesList);
            $('.imupl-button-remove', thisElement).click(function () {
                var o = $(this).parent().attr('data-imupl-offset');
                delete imuplFiles[o];
                $(this).parent().remove();
                imuplFilesCurrent--;
                imuplUpdateFileCount();
            });
            $('.imupl-button-rotate-cw', thisElement).click(function () {
                var o = $(this).parent().attr('data-imupl-offset');
                $('div[data-imupl-offset="' + o + '"]').addClass('loading').css('background-image', '');
                setTimeout(function () {
                    imuplRotateCW(o);
                }, 1);
            });
            $('.imupl-button-rotate-ccw', thisElement).click(function () {
                var o = $(this).parent().attr('data-imupl-offset');
                $('div[data-imupl-offset="' + o + '"]').addClass('loading').css('background-image', '');
                setTimeout(function () {
                    imuplRotateCCW(o);
                }, 1);
            });
            $('.imupl-button-edit', thisElement).click(function () {
                var o = $(this).parent().attr('data-imupl-offset');
                $('div[data-imupl-offset="' + o + '"]').addClass('loading').css('background-image', '');
                imuplOpenEditor(o);
            });
            imuplFilesCurrent++;
            imuplUpdateFileCount();
        }

        $('.imupl-button-choose').click(function (e) {
            e.preventDefault();
            $('.imupl-fileinput').trigger('click');
        });

        $('.imupl-fileinput').on('change', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var files = e.target.files;
            for (var i = 0, f; f = files[i]; i++) {
                imuplAddFile(f);
            }
        });

        $('body').on('drop', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (imuplFilesCurrent >= imuplFilesMax || imuplEditorLoaded != 0) {
                return;
            }
            $('.imupl-dragdrop-hover').removeClass('active');
            var files = e.originalEvent.dataTransfer.files;
            for (var i = 0, f; f = files[i]; i++) {
                imuplAddFile(f);
            }
        });

        function imuplMoveCropper(e) {
            if (!imuplCropperDragging) {
                return;
            }
            e.preventDefault();
            if (imuplCropperDragging == 1) {
                var posX = e.pageX - $imuplCropWrapper.offset().left;
                var posY = e.pageY - $imuplCropWrapper.offset().top;
                posX = Math.max(0, posX);
                posY = Math.max(0, posY);
                posX = Math.min(posX, $imuplCropWrapper.width() - parseInt($imuplCropper.css('right')) - 40);
                posY = Math.min(posY, $imuplCropWrapper.height() - parseInt($imuplCropper.css('bottom')) - 40);
                $imuplCropper.css('left', posX);
                $imuplCropper.css('top', posY);
            }
            if (imuplCropperDragging == 2) {
                var posX = e.pageX - $imuplCropWrapper.offset().left;
                var posY = e.pageY - $imuplCropWrapper.offset().top;
                posX = Math.min($imuplCropWrapper.width(), posX);
                posY = Math.min($imuplCropWrapper.height(), posY);
                posX = Math.max(posX, parseInt($imuplCropper.css('left')) + 40);
                posY = Math.max(posY, parseInt($imuplCropper.css('top')) + 40);
                posX = $imuplCropWrapper.width() - posX;
                posY = $imuplCropWrapper.height() - posY;
                $imuplCropper.css('right', posX);
                $imuplCropper.css('bottom', posY);
            }
        }

        $('body').mousemove(imuplMoveCropper);

        $imuplCropperStart.mousedown(function (e) {
            e.preventDefault();
            imuplCropperDragging = 1;
        });

        $imuplCropperEnd.mousedown(function (e) {
            e.preventDefault();
            imuplCropperDragging = 2;
        });

        $('body').mouseup(function () {
            imuplCropperDragging = 0;
        });

        $('.imupl-button-edit-save').click(imuplCloseEditor);

        $('body').on('dragover', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (imuplFilesCurrent >= imuplFilesMax || imuplEditorLoaded != 0) {
                return;
            }
            $('.imupl-dragdrop-hover').addClass('active');
        });

        $('body').on('dragleave', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('.imupl-dragdrop-hover').removeClass('active');
        });
    </script>

    <script>
        function initMap() {
            var lat = "{{$item->lat}}"
            var lng = "{{$item->lng}}"
            var myLatlng = new google.maps.LatLng(lat, lng);
            var mapOptions = {
                zoom: 11,
                center: myLatlng
            }
            var geocoder = new google.maps.Geocoder;
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                animation: google.maps.Animation.DROP
            });

            // To add the marker to the map, call setMap();
            marker.setMap(map);

            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(event.latLng);
            });

            function placeMarker(location) {
                marker.setPosition(location);
                console.log(location)
                geocodeLatLng(geocoder, map, location);
                latLngLocation = location.toString().slice(1, -1); // remove ()
                $('#location').val(latLngLocation);
            }

            function geocodeLatLng(geocoder, map, latlng) {
                geocoder.geocode({'location': latlng}, function (results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            address = results[0].formatted_address;

                            $('#pac-input').val(address);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
            }
        }

    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvZ8TXE8hvtoapELAaHIjb9NC78KOq9Xo&callback=initMap">
    </script>

    <script>

        $(document).ready(function () {
            $('.main_category_id').on('change', function () {
                var main_category_id = $('.main_category_id').val();
                $.post("{{url("/getCategories")}}",
                    {
                        category_id: main_category_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {

                        if (data.status == true) {
                            $(".category_List").removeClass("hide");
                            $('.category_id_list').html('');
                            if (data.status == true) {
                                $(".category_id_list").prepend('' +
                                    '<option  value="0"> اختر قسم فرعي</option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    console.log(item);
                                    $(".category_id_list").prepend('' +
                                        '<option name="category_id"  value="' + item.id + '">' + item.name + '</option>');
                                });
                            });
                        } else {
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '
                            })
                            $('.category_id_list').html('');
                            $(".category_id_list").prepend('' +
                                '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');
                        }
                    });
            });
            $('.category_id').on('change', function () {
                var category_id = $('.category_id').val();
                $.post("{{url("/getCategories")}}",
                    {
                        category_id: category_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {
                        if (data.status == true) {
                            console.log(data)
                            $('.category_id_list').html('');
                            if (data.status == true) {
                                $(".category_id_list").prepend('' +
                                    '<option name="category_id"  class="basicOption"  value="0"> اختر قسم فرعي</option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    $(".category_id_list").prepend('' +
                                        '<option name="category_id"  value="' + item.id + '">' + item.name + '</option>');
                                });
                            });
                        } else {
                            $('.basicOption').addClass('hide');
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '
                            })
                        }
                    });
            });

        });


    </script>
@endsection
