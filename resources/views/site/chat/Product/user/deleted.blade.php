@extends('.site.layout.container')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="headline_part centered margin-top-75">

                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12" style="direction: rtl">
                <div class="utf_dashboard_list_box margin-top-0">
                    <h4><i class="sl sl-icon-list"></i> قائمه اعلاناتي المؤرشفه</h4>
                    <ul>
                        @if(count($items) > 0)
                            @foreach ($items as $product)
                                <li>
                                    <div class="utf_list_box_listing_item">
                                        <div class="utf_list_box_listing_item-img"><a href="{{url("/productDetails/$product->id")}}">
                                                <img  style="height: 240px !important;" src="{{$product->serv_one_image}}" alt=""></a></div>
                                        <div class="utf_list_box_listing_item_content">
                                            <div class="inner">
                                                <a href="{{url("/productDetails/$product->id")}}">
                                                <p style="color: black;font-weight: bold">{{$product->name}}</p>
                                                </a>
                                                <span><i class="im im-icon-Hotel"></i> {{$product->dash_category_name}}</span>
                                                <span><i
                                                        class="sl sl-icon-location"></i> {{$product->dash_city_name}}</span>
                                                <span><i class="sl sl-icon-phone"></i> {{$product->dash_user_mobile}}</span>
                                                <p>{{$product->description}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="buttons-to-right">
                                        <a href="{{url("/productRestore/$product->id")}}" class="button gray">استرجاع <i class="fas fa-trash-restore"></i> </a>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <div class="col-md-12">
                            <h3>لا تحتوي علي منتاجات .</h3>
                            </div>
                        @endif
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="utf_pagination_container_part margin-top-30 margin-bottom-30">
                </div>
            </div>
        </div>
    </div>

@endsection
