@extends('.site.layout.container')

@section('content')


    <div class="container" style="direction: rtl">
        <div class="row">
            <div class="col-md-12">
                <h3 class="headline_part centered margin-top-75">
                    صور الاعلان
                    <br>
                    {{$item->serv_product_full_name}}
                </h3>
            </div>
        </div>
        <a href="{{url("/NewProductImage/$item->id")}}" style="width: 200px;position: relative;top:-30px;text-align: center" class="button">أضافه صور جديده </a>

        <div class="gallery" style="direction: rtl">
            @foreach($images as $image)
                <div class="col-md-12">
                    <img src="{{$image->image}}" alt="" style="height: 200px;width: 500px" class="gallery-img "/>
                    <a href="{{url("/DeleteProductImage/$image->id")}}" style="width: 150px;position: relative;top:-30px;text-align: center" class="button">حذف الصوره</a>
                </div>
            @endforeach
        </div>
    </div>






@endsection
@section("extra_css")
    <style>
        .gallery {
            -webkit-column-count: 3;
            -moz-column-count: 3;
            column-count: 3;
            -webkit-column-gap: 10px;
            -moz-column-gap: 10px;
            column-gap: 10px;
            margin-top: 10px;
            overflow: hidden;
        }

        .gallery img {
            width: 100%;
            height: auto;
            transition: 500ms;
            margin-bottom: 10px;
            opacity: 0.8;
            page-break-inside: avoid; /* For Firefox. */
            -webkit-column-break-inside: avoid; /* For Chrome & friends. */
            break-inside: avoid; /* For standard browsers like IE. :-) */
        }

        .gallery img:hover {
            opacity: 1;
        }

        /* .modal {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-direction: center;
        } */

        .modal-img, .model-vid {
            margin: 0;
            padding: 0;
            width: 100%;
            height: 100%;
            display: table
        }

        .modal-body {
            padding: 0px;
        }

        .modal-dialog {
            height: 100%;
            position: relative;
            margin: auto;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .modal-content {
            border: none;
        }

        @media screen and (max-width: 767px) {
            .gallery {
                -webkit-column-count: 2;
                -moz-column-count: 2;
                column-count: 2;
            }

            .gallery div {
                margin: 0;
                width: 200px;
            }

            .modal-dialog {
                margin: 0 8vw;
            }
        }

        @media screen and (max-width: 479px) {
            .gallery {
                -webkit-column-count: 1;
                -moz-column-count: 1;
                column-count: 1;
            }

            .gallery div {
                margin: 0;
                width: 200px;
            }
        }
    </style>
@endsection
@section("extra_js")
    <script>
        $(document).ready(function () {
            $(".gallery-img").click(function () {
                var t = $(this).attr("src");
                $(".modal-body").html("<a href='" + t + "' target='_blank'> <img src='" + t + "' class='modal-img'> </a>");
                $("#myModal").modal();
            });
        });
    </script>
@endsection
