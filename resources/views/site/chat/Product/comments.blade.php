@extends('.site.layout.container')
@section('content')
    <div class="container" style="direction: rtl">
        <div class="row utf_sticky_main_wrapper">
            <div class="col-lg-12 col-md-12">
                <div id="titlebar" class="utf_listing_titlebar">
                    <div class="utf_listing_titlebar_title">
                        <h2>{{$item->name}}</h2>
                    </div>
                </div>

                <div id="utf_listing_reviews" class="utf_listing_section">
                    <h3 class="utf_listing_headline_part   margin-bottom-20">التعليقات <span>({{$item->comments->count()}})</span>
                    </h3>
                    <div class="clearfix"></div>

                    <div class="comments utf_listing_reviews">
                        <ul>
                            @foreach($comments as $comment)
                                <li>
                                    <div class="avatar"><img src="{{$comment->dash_user_image}}" alt=""></div>
                                    <div class="utf_comment_content">
                                        <div class="utf_arrow_comment"></div>

                                        <div class="utf_by_comment">{{$comment->dash_user_name}}<span class="date"><i
                                                    class="sl sl-icon-pencil"></i> : {{$comment->time}}</span></div>
                                        <p>{{$comment->comment}}</p>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="col-md-12">
                        <div class="utf_pagination_container_part margin-top-30 margin-bottom-30">
                            <nav class="pagination">
                                <div style="display: flex; justify-content: center;" class="text-center">
                                    {{$comments->links()}}
                                </div>
                            </nav>
                        </div>
                    </div>

                </div>
                <div id="utf_add_review" class="utf_add_review-box">
                    <h3 class="utf_listing_headline_part margin-bottom-20">اضف تعليقك </h3>
                    @auth
                        <fieldset>
                            <div>
                                <input class="productId" type="hidden" name="productId" value="{{$item->id}}">
                                <textarea class="commentval" cols="40" placeholder="اكتب تعليقك ..." rows="3"
                                          name="commentval"></textarea>
                            </div>

                        </fieldset>
                        <button class="addComment button">ارسال</button>
                        <div class="clearfix"></div>
                    @endauth
                    @guest
                        <div>
                            <br>
                            <a href="#dialog_signin_part" class="button border sign-in popup-with-zoom-anim">
                                <i class=""></i> قم بتسجيل الدخول لعرض المفضله
                            </a>
                        </div>
                    @endguest
                </div>
            </div>
@endsection
