@extends('.site.layout.container')
@section('content')
    <br>
    <div class="container">
        <div id="jssor_1"
             style="position:relative;margin:0 auto;top:0px;left:0px;width:980px;height:480px;overflow:hidden;visibility:hidden;">
            <!-- Loading Screen -->
            <div data-u="loading" class="jssorl-009-spin"
                 style="position:absolute;top:0px;left:0px;width:100%;height:100%;text-align:center;background-color:rgba(0,0,0,0.7);">
                <img style="margin-top:-19px;position:relative;top:50%;width:38px;height:38px;"
                     src="{{asset("assets/site/")}}/images/spin.svg"/>
            </div>


            <div data-u="slides"
                 style="cursor:default;position:relative;top:0px;left:0px;width:980px;height:380px;overflow:hidden;">
                @foreach($item->images as $image)

                    <div>
                        <img data-u="image" src="{{$image->image}}"/>
                        <img data-u="thumb" src="{{$image->image}}"/>
                    </div>
                @endforeach


                <a data-u="any" href="https://www.jssor.com" style="display:none">web animation</a>
            </div>
            <!-- Thumbnail Navigator -->
            <div data-u="thumbnavigator" class="jssort101"
                 style="position:absolute;left:0px;bottom:0px;width:980px;height:100px;background-color:#000;"
                 data-autocenter="1" data-scale-bottom="0.75">
                <div data-u="slides">
                    <div data-u="prototype" class="p" style="width:190px;height:90px;">
                        <div data-u="thumbnailtemplate" class="t"></div>
                        <svg viewbox="0 0 16000 16000" class="cv">
                            <circle class="a" cx="8000" cy="8000" r="3238.1"></circle>
                            <line class="a" x1="6190.5" y1="8000" x2="9809.5" y2="8000"></line>
                            <line class="a" x1="8000" y1="9809.5" x2="8000" y2="6190.5"></line>
                        </svg>
                    </div>
                </div>
            </div>
            <!-- Arrow Navigator -->
            <div data-u="arrowleft" class="jssora106" style="width:55px;height:55px;top:162px;left:30px;"
                 data-scale="0.75">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                    <polyline class="a" points="7930.4,5495.7 5426.1,8000 7930.4,10504.3 "></polyline>
                    <line class="a" x1="10573.9" y1="8000" x2="5426.1" y2="8000"></line>
                </svg>
            </div>
            <div data-u="arrowright" class="jssora106" style="width:55px;height:55px;top:162px;right:30px;"
                 data-scale="0.75">
                <svg viewbox="0 0 16000 16000" style="position:absolute;top:0;left:0;width:100%;height:100%;">
                    <circle class="c" cx="8000" cy="8000" r="6260.9"></circle>
                    <polyline class="a" points="8069.6,5495.7 10573.9,8000 8069.6,10504.3 "></polyline>
                    <line class="a" x1="5426.1" y1="8000" x2="10573.9" y2="8000"></line>
                </svg>
            </div>
        </div>
    </div>


    <div class="container" style="direction: rtl">
        <div class="row utf_sticky_main_wrapper">
            <div class="col-lg-8 col-md-8">
                <div id="titlebar" class="utf_listing_titlebar">
                    <div class="utf_listing_titlebar_title">
                        <h2>{{$item->name}}</h2>
                        <span class="call_now">#{{$item->id}}<i class="sl sl-icon-list "></i></span>
                        <span class="call_now">{{$item->dash_category_parent}}<i class="sl sl-icon-grid "></i></span>
                        <span> <a href="#utf_listing_location" class="listing-address" style="direction: ltr">{{$item->dash_city_name}} <i
                                    class="sl sl-icon-location"></i></a> </span>
                        <span style="" class="call_now">{{$item->dash_user_mobile}}<i class="sl sl-icon-phone"></i></span>
                        <span class="call_now">{{$item->time}}<i class="sl sl-icon-pencil "></i></span>
                        @if(Auth::id() != $item->user_id )
                            <ul class="listing_item_social">
                                @if($item->dash_is_favourite == true)
                                    <li><a href="{{url("/deleteFavourite/$item->id")}}">حذف من المفضله <i
                                                class="sl sl-icon-pin"></i></a></li>
                                @else
                                    <li><a href="{{url("/addFavourite/$item->id")}}">اضف للمفضله <i
                                                class="sl sl-icon-pin"></i></a></li>
                                @endif
                                <li><a href="#utf_add_review">اضف تعليق <i class="sl sl-icon-pencil "></i></a></li>
                                <li><a href="{{url("/makeProductReport/$item->id")}}">ابلاغ <i
                                            class="sl sl-icon-flag"></i></a>
                                </li>
                                <li><a target="_blank"
                                       href="https://api.whatsapp.com/send?phone=+2{{$item->dash_user_mobile}}&text={{url("/productDetails/$item->id")}}"
                                       class="now_open">رساله واتساب</a></li>
                            </ul>
                        @endif

                    </div>
                    <div id="share"></div>

                </div>
                <div id="utf_listing_overview" class="utf_listing_section">
                    <h3 class="utf_listing_headline_part margin-top-30 margin-bottom-30">وصف الاعلان</h3>
                    <p>{{$item->description}}</p>
                </div>

                <div id="utf_listing_amenities" class="utf_listing_section">
                    <h3 class="utf_listing_headline_part margin-top-10 margin-bottom-40">خصائص الاعلان </h3>
                    <ul class="utf_listing_features checkboxes margin-top-0">
                        @foreach($item->options as $option)
                            <li>{{$option->dash_name}}</li>
                        @endforeach

                    </ul>
                </div>

                <div id="utf_listing_reviews" class="utf_listing_section ">
                    <h3 class="utf_listing_headline_part margin-top-75 margin-bottom-20">التعليقات <span>({{$item->comments->count()}})</span>
                    </h3>
                    <div class="clearfix"></div>

                    <div class="comments utf_listing_reviews">
                        <ul>
                            @if(count($comments) > 0)
                                @foreach($comments as $comment)
                                    <li>
                                        <div class="avatar"><img src="{{$comment->dash_user_image}}" alt=""></div>
                                        <div class="utf_comment_content">
                                            <div class="utf_arrow_comment"></div>

                                            <div class="utf_by_comment">{{$comment->dash_user_name}}<span
                                                    class="date"><i
                                                        class="sl sl-icon-pencil"></i> : {{$comment->time}}</span></div>
                                            <p>{{$comment->comment}}</p>
                                        </div>
                                    </li>
                                @endforeach

                        </ul>
                        <div class="col-md-12 centered_content"><a
                                href="{{url("/productComments/$item->id")}}"
                                class="button border margin-top-20">عرض المذيد</a></div>
                        @endif
                    </div>
                </div>
                <div id="utf_add_review" class="utf_add_review-box">
                    <h3 class="utf_listing_headline_part margin-bottom-20">اضف تعليقك </h3>
                    @auth
                        <fieldset>
                            <div>
                                <input class="productId" type="hidden" name="productId" value="{{$item->id}}">
                                <textarea class="commentval" cols="40" placeholder="اكتب تعليقك ..." rows="3"
                                          name="commentval"></textarea>
                            </div>

                        </fieldset>
                        <button class="addComment button">ارسال</button>
                        <div class="clearfix"></div>
                    @endauth
                    @guest
                        <div>
                            <br>
                            <a href="{{url("/login_site")}}" class="button border sign-in ">
                                <i class=""></i> قم بتسجيل الدخول
                            </a>
                        </div>
                    @endguest
                </div>
            </div>

            <!-- Sidebar -->
            <div class="col-lg-4 col-md-4 margin-top-75 sidebar-search">
                <div class="utf_box_widget ">
                    <h3><i class="sl sl-icon-phone"></i> معلومات التواصل</h3>
                    <div class="utf_hosted_by_user_title"><a href="{{url("/user_profile/$item->user_id")}}"
                                                             class="utf_hosted_by_avatar_listing"><img
                                src="{{$item->user->image}}" alt=""></a>
                        <h4>
                            <a href="{{url("/user_profile/$item->user_id")}}">{{$item->dash_user_name}}</a><span>{{$item->user->time}}</span>
                            @if($item->is_pin == 1 )
                                <span>  <i class="sl sl-icon-tag"></i> ممول </span>
                            @endif
                        </h4>
                    </div>

                    <ul class="utf_listing_detail_sidebar">
                        @if($item->user->address )
                            <li><i class="sl sl-icon-map"></i> {{$item->user->address}}</li>
                        @endif
                        @if($item->user->mobile )
                            <li><i class="sl sl-icon-phone"></i> {{$item->user->serv_mobile}}</li>
                        @endif
                            @if($item->user->email )
                        <li><i class="sl sl-icon-envelope-open"></i> <a href="">{{$item->dash_user_email}}</a></li>
                            @endif
                    </ul>
                </div>


                <div class="utf_box_widget opening-hours review-avg-wrapper margin-top-35">
                    <h3><i class="sl sl-icon-star"></i> تقيم العميل </h3>
                    <div class="box-inner">
                        <div class="rating-avg-wrapper text-theme clearfix">
                            <div class="rating-avg">{{$item->dash_user_rate}}</div>
                            <div class="rating-after">
                                <div class="rating-mode">/5 متوسط</div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="utf_box_widget listing-share margin-top-35 margin-bottom-40 no-border">
                    <h3><i class="sl sl-icon-pin"></i> قائمه المفضله</h3>
                    @if(count($item->favourites) > 0)
                        <span>قام {{count($item->favourites)}} شخص بوضع هذا الاعلان في المفضله .</span>
                    @else
                        <span></span>
                    @endif
                    @auth
                        <a class="utf_progress_button button fullwidth_block margin-top-5"
                           href="{{url("/favouriteList")}}">
                            عرض قائمه المفضله
                            <div class="progress-bar"></div>
                        </a>

                    @endauth
                    @guest

                        <a href="{{url("/login_site")}}" class="button border sign-in ">
                            <i class=""></i> قم بتسجيل الدخول لعرض المفضله
                        </a>
                    @endguest
                    <div class="clearfix"></div>
                </div>

                <div class="utf_box_widget margin-top-35">
                    <h3><i class="sl sl-icon-phone"></i> تواصل معانا للمساعده !</h3>
                    <p>يمكنك التواصل معنا بصوره اسرع من خلال روابط التواصل الاجتماعي الخاصه بسوق سماكو ليتم الرد عليك في
                        اسرع وقت ممكن .</p>
                    {{--                    <ul class="utf_social_icon rounded">--}}
                    {{--                        @foreach(\App\SocialMedia::all() as $social)--}}
                    {{--                            <li><a class="{{$social->icon}}" href="{{$social->url}}"></a></li>--}}
                    {{--                        @endforeach--}}
                    {{--                    </ul>--}}
                    <a class="utf_progress_button button fullwidth_block margin-top-5" href="{{url("/contact")}}">تواصل
                        معنا
                        <div class="progress-bar"></div>
                    </a>
                </div>


            </div>
        </div>


    </div>

    @if(count($Simlar_products) > 0)
        <div class="container">
            <div class="row ">
                <div class="col-md-12">
                    <h3 class="headline_part centered margin-bottom-45">
                        اعلانات مشابهه <span>استكشف احدث الاعلانات في سوق سماكو  </span>
                    </h3>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        @foreach ($Simlar_products as $productinfo)
                            <div class=" col-md-2 col-sm-3 float-r_edit ">
                                <a href="{{url("/productDetails/$productinfo->id")}}" class="img-box"
                                   data-background-image="{{$productinfo->serv_one_image}}" style="height: 150px">
                                    <div class="utf_img_content_box " style="top: 70px">
                                        <h5 style="color: #FFFFFF">{{$productinfo->serv_product_name}} </h5>
                                        <span><i class="sl sl-icon-location"></i> {{$productinfo->dash_city_name}}</span>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection
@section('extra_css')
    <style>
        /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from {
                transform: rotate(0deg);
            }
            to {
                transform: rotate(360deg);
            }
        }

        /*jssor slider arrow skin 106 css*/
        .jssora106 {
            display: block;
            position: absolute;
            cursor: pointer;
        }

        .jssora106 .c {
            fill: #fff;
            opacity: .3;
        }

        .jssora106 .a {
            fill: none;
            stroke: #000;
            stroke-width: 350;
            stroke-miterlimit: 10;
        }

        .jssora106:hover .c {
            opacity: .5;
        }

        .jssora106:hover .a {
            opacity: .8;
        }

        .jssora106.jssora106dn .c {
            opacity: .2;
        }

        .jssora106.jssora106dn .a {
            opacity: 1;
        }

        .jssora106.jssora106ds {
            opacity: .3;
            pointer-events: none;
        }

        /*jssor slider thumbnail skin 101 css*/
        .jssort101 .p {
            position: absolute;
            top: 0;
            left: 0;
            box-sizing: border-box;
            background: #000;
        }

        .jssort101 .p .cv {
            position: relative;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            border: 2px solid #000;
            box-sizing: border-box;
            z-index: 1;
        }

        .jssort101 .a {
            fill: none;
            stroke: #fff;
            stroke-width: 400;
            stroke-miterlimit: 10;
            visibility: hidden;
        }

        .jssort101 .p:hover .cv, .jssort101 .p.pdn .cv {
            border: none;
            border-color: transparent;
        }

        .jssort101 .p:hover {
            padding: 2px;
        }

        .jssort101 .p:hover .cv {
            background-color: rgba(0, 0, 0, 6);
            opacity: .35;
        }

        .jssort101 .p:hover.pdn {
            padding: 0;
        }

        .jssort101 .p:hover.pdn .cv {
            border: 2px solid #fff;
            background: none;
            opacity: .35;
        }

        .jssort101 .pav .cv {
            border-color: #fff;
            opacity: .35;
        }

        .jssort101 .pav .a, .jssort101 .p:hover .a {
            visibility: visible;
        }

        .jssort101 .t {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            border: none;
            opacity: .6;
        }

        .jssort101 .pav .t, .jssort101 .p:hover .t {
            opacity: 1;
        }
    </style>
@stop

@section('extra_js')

    <script src="{{asset("/assets/site")}}/scripts/jssor.slider-28.0.0.min.js"></script>
    <script type="text/javascript">
        window.jssor_1_slider_init = function () {

            var jssor_1_SlideshowTransitions = [
                {
                    $Duration: 800,
                    x: 0.3,
                    $During: {$Left: [0.3, 0.7]},
                    $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    x: -0.3,
                    $SlideOut: true,
                    $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    x: -0.3,
                    $During: {$Left: [0.3, 0.7]},
                    $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    x: 0.3,
                    $SlideOut: true,
                    $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    y: 0.3,
                    $During: {$Top: [0.3, 0.7]},
                    $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    y: -0.3,
                    $SlideOut: true,
                    $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    y: -0.3,
                    $During: {$Top: [0.3, 0.7]},
                    $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    y: 0.3,
                    $SlideOut: true,
                    $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    x: 0.3,
                    $Cols: 2,
                    $During: {$Left: [0.3, 0.7]},
                    $ChessMode: {$Column: 3},
                    $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    x: 0.3,
                    $Cols: 2,
                    $SlideOut: true,
                    $ChessMode: {$Column: 3},
                    $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    y: 0.3,
                    $Rows: 2,
                    $During: {$Top: [0.3, 0.7]},
                    $ChessMode: {$Row: 12},
                    $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    y: 0.3,
                    $Rows: 2,
                    $SlideOut: true,
                    $ChessMode: {$Row: 12},
                    $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    y: 0.3,
                    $Cols: 2,
                    $During: {$Top: [0.3, 0.7]},
                    $ChessMode: {$Column: 12},
                    $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    y: -0.3,
                    $Cols: 2,
                    $SlideOut: true,
                    $ChessMode: {$Column: 12},
                    $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    x: 0.3,
                    $Rows: 2,
                    $During: {$Left: [0.3, 0.7]},
                    $ChessMode: {$Row: 3},
                    $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    x: -0.3,
                    $Rows: 2,
                    $SlideOut: true,
                    $ChessMode: {$Row: 3},
                    $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    x: 0.3,
                    y: 0.3,
                    $Cols: 2,
                    $Rows: 2,
                    $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]},
                    $ChessMode: {$Column: 3, $Row: 12},
                    $Easing: {$Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    x: 0.3,
                    y: 0.3,
                    $Cols: 2,
                    $Rows: 2,
                    $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]},
                    $SlideOut: true,
                    $ChessMode: {$Column: 3, $Row: 12},
                    $Easing: {$Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    $Delay: 20,
                    $Clip: 3,
                    $Assembly: 260,
                    $Easing: {$Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    $Delay: 20,
                    $Clip: 3,
                    $SlideOut: true,
                    $Assembly: 260,
                    $Easing: {$Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    $Delay: 20,
                    $Clip: 12,
                    $Assembly: 260,
                    $Easing: {$Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                },
                {
                    $Duration: 800,
                    $Delay: 20,
                    $Clip: 12,
                    $SlideOut: true,
                    $Assembly: 260,
                    $Easing: {$Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear},
                    $Opacity: 2
                }
            ];

            var jssor_1_options = {
                $AutoPlay: 1,
                $SlideshowOptions: {
                    $Class: $JssorSlideshowRunner$,
                    $Transitions: jssor_1_SlideshowTransitions,
                    $TransitionsOrder: 1
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,
                    $SpacingX: 5,
                    $SpacingY: 5
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 980;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                } else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    <script type="text/javascript">jssor_1_slider_init();</script>

    <script>

        $(".addComment").click(function () {
            var comment = $('.commentval').val();
            var product_id = $('.productId').val();
            $.ajax({
                type: 'POST',
                url: '{{url("addComment")}}',
                data: {
                    _token: "{{csrf_token()}}",
                    comment: comment,
                    product_id: product_id
                },
                success: function (data, status) {
                    if (data.success == false) {
                        alert(data.message);
                        // $('.login_noError').text(data.message)
                    } else {
                        if (data.status_ == 1) {
                            window.location.reload();
                        } else {
                            $('.login_noError').text(data.message)
                        }
                    }
                }
            });
        });
    </script>
@stop
