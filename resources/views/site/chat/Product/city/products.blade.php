@extends('.site.layout.container')
@section('content')

    <div class="container">
        <div class="col-md-12">
            <h3 class="headline_part centered margin-top-75">
                منتاجات مدينه
                (
                {{$city->dash_name}}
                )
            </h3>
        </div>
        <div class="row">
            <div class="col-md-12" style="direction: rtl">
                <div class="container_categories_box margin-top-5 margin-bottom-30">
                    @if(count($items) > 0)
                        @foreach ($items as $product)
                            <div class=" col-md-3 col-sm-3 " style="float: right">
                                <a href="{{url("/productDetails/$product->id")}}" class="img-box"
                                   data-background-image="{{$product->serv_one_image}}" style="height: 150px;;background-image: url({{$product->serv_one_image}})">
                                    <div class="utf_img_content_box " style="top: 70px;background-image: url({{$product->serv_one_image}})">
                                        <h5 style="color: #FFFFFF">{{$product->serv_product_name}} </h5>
                                        <span><i class="sl sl-icon-location"></i> {{$product->dash_city_name}}</span>
                                    </div>
                                </a>
                            </div>

                        @endforeach
                    @else
                        <h3>لا تحتوي علي منتاجات .</h3>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
