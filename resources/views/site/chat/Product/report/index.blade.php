@extends('.site.layout.container')
@section('content')

    <section class="fullwidth_block  padding-top-75 padding-bottom-70" data-background-color=""
             style="background: rgb(249, 249, 249);">

        <div class="container">
            <div class="row ">
                <div class="col-md-12">
                    <h3 class="headline_part centered margin-bottom-45">
                        ابلاغ عن اعلان<span> في حاله القيام بالابلاغ عن منتج سيتم ارسال البلاغ الي الاداره لتحديد اذا كان الاعلان يوجد به خلل   </span>
                    </h3>
                </div>

                <div class="col-md-12" style="direction: rtl">
                    <section id="contact" class=" ">
                        <h4><i class="sl sl-icon-flag"></i> ابلاغ عن اعلان مخالف </h4>
                        <form id="contactform" method="post" action="{{url("/addProductReport")}}">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="hidden" name="product_id" value="{{$product->id}}">
                                <textarea name="message" cols="40" rows="2" id="comments" placeholder="اكتب محتوي البلاغ ..."
                                          required=""></textarea>
                                </div>
                            </div>
                            <input type="submit" style="width: 120px" class="submit button" id="submit" value="ارســال">
                        </form>
                    </section>
                </div>
            </div>
        </div>
    </section>
@endsection
