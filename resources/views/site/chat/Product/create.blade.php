@extends('.site.layout.container')
@section('content')

    <section class="fullwidth_block  padding-top-75 padding-bottom-70" data-background-color=""
             style="background: rgb(249, 249, 249);">

        <form id="contactform" method="post" action="{{url("/storeProduct")}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" class="location" id="location" name="location" value=" 24.7246242,46.8197233">
            <div class="container">
                <div class="row ">
                    <div class="col-md-12">
                        <h3 class="headline_part centered margin-bottom-45">
                            اضافه اعلان<span> لابد من ادخال بيانات الاعلان بصوره صحيحه طبقا   <a style="color: red"
                                                                                                 href="{{url("/add_product_terms")}}">للشروط و الاحكام</a>   </span>
                        </h3>
                    </div>

                    <div class="col-md-12" style="direction: rtl">
                        <section id="contact" class=" ">
                            <div class="row">


                                <div class="form-group col-xs-12">
                                    <select style="height: 60px" id="main_category_id" name="main_category_id"
                                            class="main_category_id_list main_category_id">
                                        <option value="0">اختر القسم</option>
                                        @foreach(\App\Categories::where("parent_id",null)->get() as $category)
                                            <option name="main_category_id"
                                                    value="{{$category->id}}">{{$category->dash_name}}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group col-xs-12  category_List_2">
                                    <select style="height: 60px" id="category_id_2" name=""
                                            class="category_id_list_2 category_id_2">
                                        <option name="category_id" class="" value="0">اختر القسم</option>
                                    </select>
                                </div>

                                <div class="form-group col-xs-12  category_List_3">
                                    <select style="height: 60px" id="category_id_3" name=""
                                            class="category_id_list_3 category_id_3">
                                        <option name="category_id" class="" value="0">اختر القسم</option>
                                    </select>
                                </div>

                                <div class="form-group col-xs-12  category_List_4">
                                    <select style="height: 60px" id="category_id_4" name="category_id"
                                            class="category_id_list_4 category_id_4">
                                        <option name="category_id" class="basicOption" value="0">اختر القسم</option>
                                    </select>
                                </div>


                                <div class="div_foraddphoto">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="container1">
                                                <label class="label1" for="input1">اضف صوره</label>

                                                <div class="input1">
                                                    <input name="images[]" id="file1" type="file"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="container2">
                                                <label class="label2" for="input2">اضف صوره</label>

                                                <div class="input2">
                                                    <input name="images[]" id="file2" type="file"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="container3">
                                                <label class="label3" for="input3">اضف صوره</label>

                                                <div class="input3">
                                                    <input name="images[]" id="file3" type="file"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="container4">
                                                <label class="label4" for="input4">اضف صوره</label>

                                                <div class="input4">
                                                    <input name="images[]" id="file4" type="file"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="container5">
                                                <label class="label5" for="input5">اضف صوره</label>

                                                <div class="input5">
                                                    <input name="images[]" id="file5" type="file"/>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="container6">
                                                <label class="label6" for="input6">اضف صوره</label>

                                                <div class="input6">
                                                    <input name="images[]" id="file6" type="file"/>
                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                </div>


                                <div class="col-md-12">
                                    <input name="name" type="text" placeholder="عنوان الاعلان" required="">
                                </div>

                                <div class="col-md-12">
                                    <input name="mobile" type="tel" placeholder="وسيله الاتصال" required="">
                                </div>

                                <div class="col-md-12">
                                    <select style="height: 60px" class="country_id" name="country_id">
                                        <option value="0">اخترالدوله</option>
                                        @foreach(\App\Country::all() as $country)
                                            <option value="{{$country->id}}">{{$country->dash_name}}</option>
                                        @endforeach

                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <select id="regoin_id" style="height: 60px" class="regoin_id" name="regoin_id">
                                        <option value="0">اختر المنطقه</option>
                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <select id="distract_id" style="height: 60px" class="distract_id"
                                            name="distract_id">
                                        <option value="0">اختر المدينه</option>
                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <select id="city_id" style="height: 60px" class="city_id" name="city_id">
                                        <option value="0">اختر الحي</option>
                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <textarea name="message" cols="40" rows="2" id="comments"
                                              placeholder="وصف الاعلان ..."
                                              required=""></textarea>
                                </div>

                                <!--The div element for the map -->
                                <div id="map"></div>

                                <!-- $options -->
                                <div class="add_utf_listing_section margin-top-45">
                                    <div class=" ">
                                        <h4> خيارات اخري </h4>
                                    </div>
                                    <div class="checkboxes in-row amenities_checkbox">
                                        <ul>
                                            @foreach($options as $option)
                                                <li style="direction: rtl;float: right;text-align: right;width: 25% !important;">
                                                    <input id="check-a{{$option->id}}" type="checkbox"
                                                           value="{{$option->id}}" name="options[]">
                                                    <label for="check-a{{$option->id}}">{{$option->dash_name}}</label>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <input type="submit" style="width: 120px" class="submit button" id="submit"
                                   value="اضــافـه">
                        </section>
                    </div>


                </div>

            </div>
        </form>
    </section>



@endsection
@section('extra_css')
    <style>
        .imupl-files-list {
            width: 100%;
            min-height: 200px;
            border: 1px solid grey;
            border-radius: 10px;
            background: #fff;
            position: relative;
            padding: 25px 0;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            margin-bottom: 30px;
        }

        .imupl-file-item {
            width: 200px;
            height: 200px;
            background-color: #f5f5f5;
            background-size: contain;
            background-position: center center;
            background-repeat: no-repeat;
            background-image: url(https://d13yacurqjgara.cloudfront.net/users/82092/screenshots/1073359/spinner.gif);
            margin: 25px;
            margin-top: 0;
            display: inline-block;
            border-radius: 10px;
            border: 2px solid #e6e6e6;
            overflow: hidden;
            position: relative;
            cursor: move;
        }

        .imupl-file-item:nth-child(1) {
            border: 2px solid blue;
        }

        .imupl-file-item:nth-child(1)::before {
            content: 'الرئيسية';
            display: block;
            position: absolute;
            right: 0;
            bottom: 0;
            border-top-left-radius: 10px;
            background: blue;
            color: white;
            font-size: 12px;
            font-weight: bold;
            padding: 3px 6px;
        }

        .imupl-file-item .imupl-button-remove, .imupl-file-item .imupl-button-edit, .imupl-file-item .imupl-button-rotate-cw, .imupl-file-item .imupl-button-rotate-ccw {
            display: block;
            position: absolute;
            background: #e6e6e6;
            font-size: 18px;
            padding: 3px 6px;
            cursor: pointer;
        }

        .imupl-file-item.loading .imupl-button-remove, .imupl-file-item.loading .imupl-button-edit, .imupl-file-item.loading .imupl-button-rotate-cw, .imupl-file-item.loading .imupl-button-rotate-ccw {
            display: none;
        }

        .imupl-file-item .imupl-button-remove {
            left: 0;
            top: 0;
            border-bottom-right-radius: 10px;
            padding-right: 10px;
        }

        .imupl-file-item .imupl-button-edit {
            right: 0;
            top: 0;
            border-bottom-left-radius: 10px;
            padding-left: 10px;
        }

        .imupl-file-item .imupl-button-rotate-cw {
            bottom: 0;
            left: 30px;
            border-top-right-radius: 10px;
            padding-left: 10px;
        }

        .imupl-file-item .imupl-button-rotate-ccw {
            bottom: 0;
            left: 0;
            padding-left: 10px;
            border-top-right-radius: 5px;
        }

        @media (max-width: 400px) {
            .imupl-file-item {
                width: 150px;
                height: 150px;
            }
        }

        .imupl-fileinput {
            width: 1px;
            height: 1px;
            opacity: 0;
            position: fixed;
            left: -10px;
        }

        .imupl-dragdrop-hover {
            z-index: 9999;
            position: fixed;
            top: 25px;
            left: 25px;
            right: 25px;
            bottom: 25px;
            border: 5px rgba(0, 150, 0, 0.5) dashed;
            border-radius: 25px;
            background: rgba(0, 150, 0, 0.1);
            display: none;
            pointer-events: none;
        }

        .imupl-dragdrop-hover.active {
            display: initial;
        }

        .imupl-edit-overlay {
            display: flex;
            justify-content: center;
            align-items: center;
            opacity: 0;
            pointer-events: none;
            background: rgba(0, 0, 0, 0.5);
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            z-index: 999;
            transition: 0.2s opacity;
        }

        .imupl-edit-overlay .thumbnail {
            transform: translateY(-100%);
            transition: 0.5s transform ease-in-out;
        }

        .imupl-edit-overlay .thumbnail .img {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
        }

        .imupl-edit-overlay.active {
            opacity: 1;
            pointer-events: initial;
        }

        .imupl-edit-overlay.active .thumbnail {
            transform: translateY(0);
        }

        .imupl-crop-wrapper {
            position: relative;
            margin-bottom: 15px;
        }

        .imupl-cropper {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            border: 2px solid rgba(0, 0, 0, 0.5);
        }

        .imupl-cropper-start, .imupl-cropper-end {
            position: absolute;
            width: 20px;
            height: 20px;
            cursor: nwse-resize;
        }

        .imupl-cropper-start {
            top: -2px;
            left: -2px;
            border-top: 4px solid black;
            border-left: 4px solid black;
        }

        .imupl-cropper-end {
            bottom: -2px;
            right: -2px;
            border-bottom: 4px solid black;
            border-right: 4px solid black;
        }


    </style>
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 400px; /* The height is 400 pixels */
            width: 100%; /* The width is the width of the web page */
        }
    </style>
@endsection
@section('extra_js')
    <script>
        var imuplFilesMax = 6;
        var imuplMaxDimension = 1200;
        var imuplJpgQuality = 0.75;
        var imuplMaxEditorSize = 0.8;

        var imuplFilesCurrent = 0;
        var imuplOffset = 1;
        var imuplFiles = [];
        var imuplEditorLoaded = 0;
        var imuplCropperDragging = 0;
        var imuplEditorRatio = 1;

        var $imuplFilesList = $('.imupl-files-list'),
            $imuplFilesCurrent = $('.imupl-files-current'),
            $imuplFilesMax = $('.imupl-files-max'),
            $imuplEditorImg = $('.imupl-edit-overlay .thumbnail .img'),
            $imuplEditorOverlay = $('.imupl-edit-overlay'),
            $imuplCropWrapper = $('.imupl-crop-wrapper'),
            $imuplCropper = $('.imupl-cropper'),
            $imuplCropperStart = $('.imupl-cropper-start'),
            $imuplCropperEnd = $('.imupl-cropper-end');

        $imuplFilesList.sortable();

        function imuplUpdateFileCount() {
            $imuplFilesCurrent.html(imuplFilesCurrent);
            $imuplFilesMax.html(imuplFilesMax);
        }

        imuplUpdateFileCount();

        function imuplRender(offset) {
            var img = imuplFiles[offset];
            var thisElement = $('div[data-imupl-offset="' + offset + '"]');

            var cropWidth = img.cropEndX - img.cropStartX;
            var cropHeight = img.cropEndY - img.cropStartY;
            var finalWidth = 0;
            var finalHeight = 0;
            if (cropWidth <= imuplMaxDimension && cropHeight <= imuplMaxDimension) {
                finalWidth = cropWidth;
                finalHeight = cropHeight;
            } else if (cropWidth > cropHeight) {
                finalWidth = imuplMaxDimension;
                finalHeight = cropHeight * (imuplMaxDimension / cropWidth);
            } else {
                finalHeight = imuplMaxDimension;
                finalWidth = cropWidth * (imuplMaxDimension / cropHeight);
            }

            var canvas = document.createElement('canvas');
            canvas.width = finalWidth;
            canvas.height = finalHeight;
            var context = canvas.getContext('2d');

            context.drawImage(img.imgObj, img.cropStartX, img.cropStartY, cropWidth, cropHeight, 0, 0, finalWidth, finalHeight);
            img.resultData = canvas.toDataURL('image/jpeg', imuplJpgQuality);

            thisElement.css('background-image', 'url(' + img.resultData + ')');
            $('input', thisElement).val(img.resultData);
            thisElement.removeClass('loading');
        }

        function imuplRotateCW(offset) {
            var img = imuplFiles[offset];
            var newWidth = img.origHeight,
                newHeight = img.origWidth;
            var oldCropStartX = img.cropStartX,
                oldCropEndX = img.cropEndX,
                oldCropStartY = img.cropStartY,
                oldCropEndY = img.cropEndY;

            img.cropStartX = img.origHeight - oldCropEndY;
            img.cropStartY = oldCropStartX;
            img.cropEndX = img.origHeight - oldCropStartY;
            img.cropEndY = oldCropEndX;
            img.origHeight = newHeight;
            img.origWidth = newWidth;

            var canvas = document.createElement('canvas');
            canvas.width = newWidth;
            canvas.height = newHeight;
            var context = canvas.getContext('2d');

            context.save();
            context.translate(newWidth / 2, newHeight / 2);
            context.rotate(90 * Math.PI / 180);
            context.drawImage(img.imgObj, -(newHeight / 2), -(newWidth / 2));
            context.restore();

            img.rawData = canvas.toDataURL();
            var imgObj = new Image;
            imgObj.onload = function () {
                img.imgObj = imgObj;
                imuplRender(offset);
            };
            imgObj.src = img.rawData;
        }

        function imuplRotateCCW(offset) {
            var img = imuplFiles[offset];
            var newWidth = img.origHeight,
                newHeight = img.origWidth;
            var oldCropStartX = img.cropStartX,
                oldCropEndX = img.cropEndX,
                oldCropStartY = img.cropStartY,
                oldCropEndY = img.cropEndY;

            img.cropStartX = oldCropStartY;
            img.cropStartY = img.origWidth - oldCropEndX;
            img.cropEndX = oldCropEndY;
            img.cropEndY = img.origWidth - oldCropStartX;
            img.origHeight = newHeight;
            img.origWidth = newWidth;

            var canvas = document.createElement('canvas');
            canvas.width = newWidth;
            canvas.height = newHeight;
            var context = canvas.getContext('2d');

            context.save();
            context.translate(newWidth / 2, newHeight / 2);
            context.rotate(-90 * Math.PI / 180);
            context.drawImage(img.imgObj, -(newHeight / 2), -(newWidth / 2));
            context.restore();

            img.rawData = canvas.toDataURL();
            var imgObj = new Image;
            imgObj.onload = function () {
                img.imgObj = imgObj;
                imuplRender(offset);
            };
            imgObj.src = img.rawData;
        }


        function imuplCloseEditor() {
            $imuplEditorOverlay.removeClass('active');
            var img = imuplFiles[imuplEditorLoaded];
            img.cropStartX = parseInt($imuplCropper.css('left')) / imuplEditorRatio;
            img.cropStartY = parseInt($imuplCropper.css('top')) / imuplEditorRatio;
            img.cropEndX = ($imuplCropWrapper.width() - parseInt($imuplCropper.css('right'))) / imuplEditorRatio;
            img.cropEndY = ($imuplCropWrapper.height() - parseInt($imuplCropper.css('bottom'))) / imuplEditorRatio;
            console.log(img);
            setTimeout(function () {
                imuplRender(imuplEditorLoaded);
                imuplEditorLoaded = 0;
            }, 1);
        }

        function imuplOpenEditor(offset) {
            var img = imuplFiles[offset];
            imuplEditorLoaded = offset;
            imuplCropperDragging = 0;

            $imuplEditorImg.attr('style', '');
            $imuplEditorImg.css('background-image', 'url(' + img.rawData + ')');

            var imgWidth = img.origWidth,
                imgHeight = img.origHeight,
                ratio = 1;
            if (imgWidth > $(window).width() * imuplMaxEditorSize) {
                ratio *= $(window).width() * imuplMaxEditorSize / imgWidth;
            }
            if (imgHeight > $(window).height() * imuplMaxEditorSize) {
                ratio *= $(window).height() * imuplMaxEditorSize / imgHeight;
            }
            imgWidth = img.origWidth * ratio;
            imgHeight = img.origHeight * ratio;

            $imuplEditorImg.css('width', imgWidth);
            $imuplEditorImg.css('height', imgHeight);

            $imuplCropper.css('left', img.cropStartX * ratio).css('top', img.cropStartY * ratio);
            $imuplCropper.css('right', imgWidth - img.cropEndX * ratio).css('bottom', imgHeight - img.cropEndY * ratio);

            imuplEditorRatio = ratio;
            $imuplEditorOverlay.addClass('active');
        }

        function imuplAddFile(f) {
            if (imuplFilesCurrent >= imuplFilesMax || f.type.indexOf("image") !== 0) {
                return;
            }
            var thisOffset = imuplOffset++;
            var thisElement = $('<div class="imupl-file-item loading" data-imupl-offset="' + thisOffset + '"><div class="imupl-button-remove"><i class="fas fa-trash-alt"></i></div><div class="imupl-button-edit"><i class="fa fa-crop"></i></div><div class="imupl-button-rotate-ccw"><i class="fas fa-sync"></i></div><input type="hidden" name="imupl_payload[]" value="" /></div>');
            var reader = new FileReader();
            reader.onload = function (e) {
                var img = new Image;
                img.onload = function () {
                    var newImage = {
                        rawData: e.target.result,
                        resultData: e.target.result,
                        imgObj: img,
                        origWidth: img.width,
                        origHeight: img.height,
                        cropStartX: 0,
                        cropStartY: 0,
                        cropEndX: img.width,
                        cropEndY: img.height
                    };
                    imuplFiles[thisOffset] = newImage;
                    imuplRender(thisOffset);
                };
                img.src = e.target.result;
            }
            reader.readAsDataURL(f);
            thisElement.appendTo($imuplFilesList);
            $('.imupl-button-remove', thisElement).click(function () {
                var o = $(this).parent().attr('data-imupl-offset');
                delete imuplFiles[o];
                $(this).parent().remove();
                imuplFilesCurrent--;
                imuplUpdateFileCount();
            });
            $('.imupl-button-rotate-cw', thisElement).click(function () {
                var o = $(this).parent().attr('data-imupl-offset');
                $('div[data-imupl-offset="' + o + '"]').addClass('loading').css('background-image', '');
                setTimeout(function () {
                    imuplRotateCW(o);
                }, 1);
            });
            $('.imupl-button-rotate-ccw', thisElement).click(function () {
                var o = $(this).parent().attr('data-imupl-offset');
                $('div[data-imupl-offset="' + o + '"]').addClass('loading').css('background-image', '');
                setTimeout(function () {
                    imuplRotateCCW(o);
                }, 1);
            });
            $('.imupl-button-edit', thisElement).click(function () {
                var o = $(this).parent().attr('data-imupl-offset');
                $('div[data-imupl-offset="' + o + '"]').addClass('loading').css('background-image', '');
                imuplOpenEditor(o);
            });
            imuplFilesCurrent++;
            imuplUpdateFileCount();
        }

        $('.imupl-button-choose').click(function (e) {
            e.preventDefault();
            $('.imupl-fileinput').trigger('click');
        });

        $('.imupl-fileinput').on('change', function (e) {
            e.preventDefault();
            e.stopPropagation();
            var files = e.target.files;
            for (var i = 0, f; f = files[i]; i++) {
                imuplAddFile(f);
            }
        });

        $('body').on('drop', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (imuplFilesCurrent >= imuplFilesMax || imuplEditorLoaded != 0) {
                return;
            }
            $('.imupl-dragdrop-hover').removeClass('active');
            var files = e.originalEvent.dataTransfer.files;
            for (var i = 0, f; f = files[i]; i++) {
                imuplAddFile(f);
            }
        });

        function imuplMoveCropper(e) {
            if (!imuplCropperDragging) {
                return;
            }
            e.preventDefault();
            if (imuplCropperDragging == 1) {
                var posX = e.pageX - $imuplCropWrapper.offset().left;
                var posY = e.pageY - $imuplCropWrapper.offset().top;
                posX = Math.max(0, posX);
                posY = Math.max(0, posY);
                posX = Math.min(posX, $imuplCropWrapper.width() - parseInt($imuplCropper.css('right')) - 40);
                posY = Math.min(posY, $imuplCropWrapper.height() - parseInt($imuplCropper.css('bottom')) - 40);
                $imuplCropper.css('left', posX);
                $imuplCropper.css('top', posY);
            }
            if (imuplCropperDragging == 2) {
                var posX = e.pageX - $imuplCropWrapper.offset().left;
                var posY = e.pageY - $imuplCropWrapper.offset().top;
                posX = Math.min($imuplCropWrapper.width(), posX);
                posY = Math.min($imuplCropWrapper.height(), posY);
                posX = Math.max(posX, parseInt($imuplCropper.css('left')) + 40);
                posY = Math.max(posY, parseInt($imuplCropper.css('top')) + 40);
                posX = $imuplCropWrapper.width() - posX;
                posY = $imuplCropWrapper.height() - posY;
                $imuplCropper.css('right', posX);
                $imuplCropper.css('bottom', posY);
            }
        }

        $('body').mousemove(imuplMoveCropper);

        $imuplCropperStart.mousedown(function (e) {
            e.preventDefault();
            imuplCropperDragging = 1;
        });

        $imuplCropperEnd.mousedown(function (e) {
            e.preventDefault();
            imuplCropperDragging = 2;
        });

        $('body').mouseup(function () {
            imuplCropperDragging = 0;
        });

        $('.imupl-button-edit-save').click(imuplCloseEditor);

        $('body').on('dragover', function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (imuplFilesCurrent >= imuplFilesMax || imuplEditorLoaded != 0) {
                return;
            }
            $('.imupl-dragdrop-hover').addClass('active');
        });

        $('body').on('dragleave', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('.imupl-dragdrop-hover').removeClass('active');
        });
    </script>

    <script>
        function initMap() {
            var myLatlng = new google.maps.LatLng(24.7246242, 46.8197233);
            var mapOptions = {
                zoom: 11,
                center: myLatlng
            }

            var geocoder = new google.maps.Geocoder;
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            infoWindow = new google.maps.InfoWindow;
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        animation: google.maps.Animation.DROP,
                        icon: "{{asset('assets/site/images/mylocationpin.png')}}"

                    });

                    infoWindow.setPosition(pos);
                    infoWindow.setContent('Location found.');
                    infoWindow.open(map);
                    map.setCenter(pos);
                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                    'Error: The Geolocation service failed.' :
                    'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            }

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                animation: google.maps.Animation.DROP
            });

            // To add the marker to the map, call setMap();
            marker.setMap(map);

            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(event.latLng);
            });

            function placeMarker(location) {
                marker.setPosition(location);
                console.log(location)
                geocodeLatLng(geocoder, map, location);
                latLngLocation = location.toString().slice(1, -1); // remove ()
                $('#location').val(latLngLocation);
            }

            function geocodeLatLng(geocoder, map, latlng) {
                geocoder.geocode({'location': latlng}, function (results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            address = results[0].formatted_address;

                            $('#pac-input').val(address);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
            }
        }

    </script>
    <script>
        //start up load photo
        $(function () {
            var container1 = $(".container1"),
                inputFile = $("#file1"),
                img,
                btn,
                txt = "اسم الحقل",
                txtAfter = "تغير الصوره";

            if (!container1.find("#upload1").length) {
                container1
                    .find(".input1")
                    .append('<input type="button" value="' + txt + '" id="upload1">');
                btn = $("#upload1");
                container1.prepend(
                    '<img src="" class="This_WWidth hidden1" alt="Uploaded file" id="uploadImg1" >'
                );
                img = $("#uploadImg1");
            }

            btn.on("click", function () {
                img.animate({opacity: 0}, 300);
                inputFile.click();
            });

            inputFile.on("change", function (e) {
                container1.find("label1").html(inputFile.val());

                var i = 0;
                for (i; i < e.originalEvent.srcElement.files.length; i++) {
                    var file = e.originalEvent.srcElement.files[i],
                        reader = new FileReader();

                    reader.onloadend = function () {
                        img.attr("src", reader.result).animate({opacity: 1}, 700);
                    };
                    reader.readAsDataURL(file);
                    img.removeClass("hidden1");
                }

                btn.val(txtAfter);
            });
        });

        //2
        $(function () {
            var container2 = $(".container2"),
                inputFile = $("#file2"),
                img,
                btn,
                txt = "اسم الحقل",
                txtAfter = "تغير الصوره";

            if (!container2.find("#upload2").length) {
                container2
                    .find(".input2")
                    .append('<input type="button" value="' + txt + '" id="upload2">');
                btn = $("#upload2");
                container2.prepend(
                    '<img src="" class="This_WWidth hidden2" alt="Uploaded file" id="uploadImg2">'
                );
                img = $("#uploadImg2");
            }

            btn.on("click", function () {
                img.animate({opacity: 0}, 300);
                inputFile.click();
            });

            inputFile.on("change", function (e) {
                container2.find("label2").html(inputFile.val());

                var i = 0;
                for (i; i < e.originalEvent.srcElement.files.length; i++) {
                    var file = e.originalEvent.srcElement.files[i],
                        reader = new FileReader();

                    reader.onloadend = function () {
                        img.attr("src", reader.result).animate({opacity: 1}, 700);
                    };
                    reader.readAsDataURL(file);
                    img.removeClass("hidden2");
                }

                btn.val(txtAfter);
            });
        });


        //3


        $(function () {
            var container3 = $(".container3"),
                inputFile = $("#file3"),
                img,
                btn,
                txt = "اسم الحقل",
                txtAfter = "تغير الصوره";

            if (!container3.find("#upload3").length) {
                container3
                    .find(".input3")
                    .append('<input type="button" value="' + txt + '" id="upload3">');
                btn = $("#upload3");
                container3.prepend(
                    '<img src="" class="This_WWidth hidden3" alt="Uploaded file" id="uploadImg3" >'
                );
                img = $("#uploadImg3");
            }

            btn.on("click", function () {
                img.animate({opacity: 0}, 300);
                inputFile.click();
            });

            inputFile.on("change", function (e) {
                container3.find("label3").html(inputFile.val());

                var i = 0;
                for (i; i < e.originalEvent.srcElement.files.length; i++) {
                    var file = e.originalEvent.srcElement.files[i],
                        reader = new FileReader();

                    reader.onloadend = function () {
                        img.attr("src", reader.result).animate({opacity: 1}, 700);
                    };
                    reader.readAsDataURL(file);
                    img.removeClass("hidden3");
                }

                btn.val(txtAfter);
            });
        });


        //4

        $(function () {
            var container4 = $(".container4"),
                inputFile = $("#file4"),
                img,
                btn,
                txt = "اسم الحقل",
                txtAfter = "تغير الصوره";

            if (!container4.find("#upload4").length) {
                container4
                    .find(".input4")
                    .append('<input type="button" value="' + txt + '" id="upload4">');
                btn = $("#upload4");
                container4.prepend(
                    '<img src="" class="This_WWidth hidden4" alt="Uploaded file" id="uploadImg4" >'
                );
                img = $("#uploadImg4");
            }

            btn.on("click", function () {
                img.animate({opacity: 0}, 300);
                inputFile.click();
            });

            inputFile.on("change", function (e) {
                container4.find("label4").html(inputFile.val());

                var i = 0;
                for (i; i < e.originalEvent.srcElement.files.length; i++) {
                    var file = e.originalEvent.srcElement.files[i],
                        reader = new FileReader();

                    reader.onloadend = function () {
                        img.attr("src", reader.result).animate({opacity: 1}, 700);
                    };
                    reader.readAsDataURL(file);
                    img.removeClass("hidden4");
                }

                btn.val(txtAfter);
            });
        });


        //5

        $(function () {
            var container5 = $(".container5"),
                inputFile = $("#file5"),
                img,
                btn,
                txt = "اسم الحقل",
                txtAfter = "تغير الصوره";

            if (!container5.find("#upload5").length) {
                container5
                    .find(".input5")
                    .append('<input type="button" value="' + txt + '" id="upload5">');
                btn = $("#upload5");
                container5.prepend(
                    '<img src="" class="This_WWidth hidden5" alt="Uploaded file" id="uploadImg5" >'
                );
                img = $("#uploadImg5");
            }

            btn.on("click", function () {
                img.animate({opacity: 0}, 300);
                inputFile.click();
            });

            inputFile.on("change", function (e) {
                container5.find("label5").html(inputFile.val());

                var i = 0;
                for (i; i < e.originalEvent.srcElement.files.length; i++) {
                    var file = e.originalEvent.srcElement.files[i],
                        reader = new FileReader();

                    reader.onloadend = function () {
                        img.attr("src", reader.result).animate({opacity: 1}, 700);
                    };
                    reader.readAsDataURL(file);
                    img.removeClass("hidden5");
                }

                btn.val(txtAfter);
            });
        });


        //6

        $(function () {
            var container6 = $(".container6"),
                inputFile = $("#file6"),
                img,
                btn,
                txt = "اسم الحقل",
                txtAfter = "تغير الصوره";

            if (!container6.find("#upload6").length) {
                container6
                    .find(".input6")
                    .append('<input type="button" value="' + txt + '" id="upload6">');
                btn = $("#upload6");
                container6.prepend(
                    '<img src="" class="This_WWidth hidden6" alt="Uploaded file" id="uploadImg6">'
                );
                img = $("#uploadImg6");
            }

            btn.on("click", function () {
                img.animate({opacity: 0}, 300);
                inputFile.click();
            });

            inputFile.on("change", function (e) {
                container6.find("label6").html(inputFile.val());

                var i = 0;
                for (i; i < e.originalEvent.srcElement.files.length; i++) {
                    var file = e.originalEvent.srcElement.files[i],
                        reader = new FileReader();

                    reader.onloadend = function () {
                        img.attr("src", reader.result).animate({opacity: 1}, 700);
                    };
                    reader.readAsDataURL(file);
                    img.removeClass("hidden6");
                }

                btn.val(txtAfter);
            });
        });
    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvZ8TXE8hvtoapELAaHIjb9NC78KOq9Xo&callback=initMap">
    </script>

    <script>

        {{--$(document).ready(function () {--}}
        {{--    $('.main_category_id').on('change', function () {--}}
        {{--        var main_category_id = $('.main_category_id').val();--}}
        {{--        $.post("{{url("/getCategories")}}",--}}
        {{--            {--}}
        {{--                category_id: main_category_id,--}}
        {{--                _token: "{{csrf_token()}}"--}}
        {{--            },--}}
        {{--            function (data, status) {--}}

        {{--                if (data.status == true) {--}}
        {{--                    $('.category_id_list_2').html('');--}}
        {{--                    if (data.status == true) {--}}
        {{--                        $(".category_id_list_2").prepend('' +--}}
        {{--                            '<option  value="0"> اختر قسم فرعي</option>');--}}
        {{--                    }--}}
        {{--                    $.each(data, function () {--}}
        {{--                        $.each(this, function (index, item) {--}}
        {{--                            console.log(item);--}}
        {{--                            $(".category_id_list_2").prepend('' +--}}
        {{--                                '<option name=""  value="' + item.id + '">' + item.name + '</option>');--}}
        {{--                        });--}}
        {{--                    });--}}
        {{--                } else {--}}
        {{--                    Swal.fire({--}}
        {{--                        icon: 'info',--}}
        {{--                        text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '--}}
        {{--                    })--}}
        {{--                    $('.category_id_list_2').html('');--}}
        {{--                    $(".category_id_list_2").prepend('' +--}}
        {{--                        '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');--}}
        {{--                    $('.category_id_list_3').html('');--}}
        {{--                    $(".category_id_list_3").prepend('' +--}}
        {{--                        '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');--}}
        {{--                    $('.category_id_list_4').html('');--}}
        {{--                    $(".category_id_list_4").prepend('' +--}}
        {{--                        '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');--}}
        {{--                }--}}
        {{--            });--}}
        {{--    });--}}
        {{--    $('.category_id_2').on('change', function () {--}}
        {{--        var category_id = $('.category_id_2').val();--}}
        {{--        $.post("{{url("/getCategories")}}",--}}
        {{--            {--}}
        {{--                category_id: category_id,--}}
        {{--                _token: "{{csrf_token()}}"--}}
        {{--            },--}}
        {{--            function (data, status) {--}}
        {{--                if (data.status == true) {--}}
        {{--                    console.log(data)--}}
        {{--                    $('.category_id_list_3').html('');--}}
        {{--                    if (data.status == true) {--}}
        {{--                        $(".category_id_list_3").prepend('' +--}}
        {{--                            '<option name=""  class="basicOption"  value="0"> اختر قسم فرعي</option>');--}}
        {{--                    }--}}
        {{--                    $.each(data, function () {--}}
        {{--                        $.each(this, function (index, item) {--}}
        {{--                            $(".category_id_list_3").prepend('' +--}}
        {{--                                '<option name=""  value="' + item.id + '">' + item.name + '</option>');--}}
        {{--                        });--}}
        {{--                    });--}}
        {{--                } else {--}}
        {{--                    $('.category_id_list_3').html('');--}}
        {{--                    $(".category_id_list_3").prepend('' +--}}
        {{--                        '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');--}}
        {{--                    $('.category_id_list_4').html('');--}}
        {{--                    $(".category_id_list_4").prepend('' +--}}
        {{--                        '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');--}}

        {{--                }--}}
        {{--            });--}}
        {{--    });--}}
        {{--    $('.category_id_3').on('change', function () {--}}
        {{--        var category_id = $('.category_id_3').val();--}}
        {{--        $.post("{{url("/getCategories")}}",--}}
        {{--            {--}}
        {{--                category_id: category_id,--}}
        {{--                _token: "{{csrf_token()}}"--}}
        {{--            },--}}
        {{--            function (data, status) {--}}
        {{--                if (data.status == true) {--}}
        {{--                    console.log(data)--}}
        {{--                    $('.category_id_list_4').html('');--}}
        {{--                    if (data.status == true) {--}}
        {{--                        $(".category_id_list_4").prepend('' +--}}
        {{--                            '<option name=""  class="basicOption"  value="0"> اختر قسم فرعي</option>');--}}
        {{--                    }--}}
        {{--                    $.each(data, function () {--}}
        {{--                        $.each(this, function (index, item) {--}}
        {{--                            $(".category_id_list_4").prepend('' +--}}
        {{--                                '<option name=""  value="' + item.id + '">' + item.name + '</option>');--}}
        {{--                        });--}}
        {{--                    });--}}
        {{--                } else {--}}
        {{--                    $('.category_id_list_4').html('');--}}
        {{--                    $(".category_id_list_4").prepend('' +--}}
        {{--                        '<option name="category_id"  value="0">لا توجد اقسام فرعيه </option>');--}}
        {{--                }--}}
        {{--            });--}}
        {{--    });--}}
        {{--    $('.category_id_4').on('change', function () {--}}
        {{--        var category_id = $('.category_id_4').val();--}}
        {{--        $.post("{{url("/getCategories")}}",--}}
        {{--            {--}}
        {{--                category_id: category_id,--}}
        {{--                _token: "{{csrf_token()}}"--}}
        {{--            },--}}
        {{--            function (data, status) {--}}
        {{--                if (data.status == true) {--}}
        {{--                    console.log(data)--}}
        {{--                    $('.category_id_4').html('');--}}
        {{--                    if (data.status == true) {--}}
        {{--                        $(".category_id_4").prepend('' +--}}
        {{--                            '<option name="category_id"  class="basicOption"  value="0"> اختر قسم فرعي</option>');--}}
        {{--                    }--}}
        {{--                    $.each(data, function () {--}}
        {{--                        $.each(this, function (index, item) {--}}
        {{--                            $(".category_id_4").prepend('' +--}}
        {{--                                '<option name="category_id"  value="' + item.id + '">' + item.name + '</option>');--}}
        {{--                        });--}}
        {{--                    });--}}
        {{--                } else {--}}
        {{--                    $('.basicOption').addClass('hide');--}}
        {{--                    Swal.fire({--}}
        {{--                        icon: 'info',--}}
        {{--                        text: 'لا توجد اقسام فرعي متفرعه من هذا القسم  '--}}
        {{--                    })--}}
        {{--                }--}}
        {{--            });--}}
        {{--    });--}}

        {{--});--}}


    </script>

    {{--    <script>--}}
    {{--        $(".country_id").on('change', function () {--}}
    {{--            $(".regoin_id").empty();--}}
    {{--            $('.loader').fadeIn();--}}
    {{--            $.post("{{url("get_regoins")}}",--}}
    {{--                {--}}
    {{--                    country_id: $(this).val(),--}}
    {{--                    _token: "{{csrf_token()}}"--}}
    {{--                },--}}
    {{--                function (data, status) {--}}
    {{--                    $.each(data, function () {--}}
    {{--                        $.each(this, function (index, item) {--}}
    {{--                            $("#regoin_id").prepend('' +--}}
    {{--                                '<option selected value="' + item.id + '">' + item.name + '</option>');--}}
    {{--                        });--}}
    {{--                    });--}}
    {{--                    $('.loader').fadeOut();--}}
    {{--                });--}}
    {{--        });--}}
    {{--    </script>--}}
    {{--    <script>--}}
    {{--        $(".regoin_id").on('change', function () {--}}
    {{--            $(".distract_id").empty();--}}
    {{--            $('.loader').fadeIn();--}}
    {{--            $.post("{{url("get_distracts")}}",--}}
    {{--                {--}}
    {{--                    regoin_id: $(this).val(),--}}
    {{--                    _token: "{{csrf_token()}}"--}}
    {{--                },--}}
    {{--                function (data, status) {--}}
    {{--                    $.each(data, function () {--}}
    {{--                        $.each(this, function (index, item) {--}}
    {{--                            $("#distract_id").prepend('' +--}}
    {{--                                '<option selected value="' + item.id + '">' + item.name + '</option>');--}}
    {{--                        });--}}
    {{--                    });--}}
    {{--                    $('.loader').fadeOut();--}}
    {{--                });--}}
    {{--        });--}}
    {{--    </script>--}}
    {{--    <script>--}}
    {{--        $(".distract_id").on('change', function () {--}}
    {{--            $(".city_id").empty();--}}
    {{--            $('.loader').fadeIn();--}}
    {{--            $.post("{{url("get_cities")}}",--}}
    {{--                {--}}
    {{--                    distract_id: $(this).val(),--}}
    {{--                    _token: "{{csrf_token()}}"--}}
    {{--                },--}}
    {{--                function (data, status) {--}}
    {{--                    $.each(data, function () {--}}
    {{--                        $.each(this, function (index, item) {--}}
    {{--                            $("#city_id").prepend('' +--}}
    {{--                                '<option selected value="' + item.id + '">' + item.name + '</option>');--}}
    {{--                        });--}}
    {{--                    });--}}
    {{--                    $('.loader').fadeOut();--}}
    {{--                });--}}
    {{--        });--}}
    {{--    </script>--}}
@endsection


