<!DOCTYPE html>
<html lang='en' class=''>
<head>
    <script
        src="https://static.codepen.io/assets/editor/live/console_runner-35ce412171641ff1bf5aafef664326622ab85fdc72688d6861505d88ff7e036f.js"></script>
    <script
        src="https://static.codepen.io/assets/editor/live/css_reload-5619dc0905a68b2e6298901de54f73cefe4e079f65a75406858d92924b4938bf.js"></script>
    <meta charset='UTF-8'>
    <meta name="robots" content="noindex">
    <link rel="shortcut icon" type="image/x-icon"
          href="https://static.codepen.io/assets/favicon/favicon-aec34940fbc1a6e787974dcd360f2c6b63348d4b1f4e06c77743096d55480f33.ico"/>
    <link rel="mask-icon" type=""
          href="https://static.codepen.io/assets/favicon/logo-pin-8f3771b1072e3c38bd662872f6b673a722f4b3ca2421637d5596661b4e2132cc.svg"
          color="#111"/>
    <link rel="canonical" href="https://codepen.io/ThomasDaubenton/pen/QMqaBN"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <style class="cp-pen-styles">
        body {
            padding: 5%;
            background-color: #F5F5F5;
        }

        .container {
            padding: 0;
            background-color: #FFF;
            box-shadow: 0 10px 20px rgba(0, 0, 0, 0.19), 0 6px 6px rgba(0, 0, 0, 0.23);
            height: 800px;
            width: 1200px;
        }


        .discussions .search .searchbar input {
            margin-left: 15px;
            height: 38px;
            width: 100%;
            border: none;
            font-family: 'Montserrat', sans-serif;;
        }

        .discussions .search .searchbar *::-webkit-input-placeholder {
            color: #E0E0E0;
        }

        .discussions .search .searchbar input *:-moz-placeholder {
            color: #E0E0E0;
        }

        .discussions .search .searchbar input *::-moz-placeholder {
            color: #E0E0E0;
        }

        .discussions .search .searchbar input *:-ms-input-placeholder {
            color: #E0E0E0;
        }

        .discussions .message-active {
            width: 98.5%;
            height: 90px;
            background-color: #FFF;
            border-bottom: solid 1px #E0E0E0;
        }

        .discussions .discussion .photo {
            margin-left: 20px;
            display: block;
            width: 45px;
            height: 45px;
            background: #E6E7ED;
            -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }

        .online {
            position: relative;
            top: 30px;
            left: 35px;
            width: 13px;
            height: 13px;
            background-color: #8BC34A;
            border-radius: 13px;
            border: 3px solid #FAFAFA;
        }

        .desc-contact {
            height: 43px;
            width: 50%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .discussions .discussion .name {
            margin: 0 0 0 20px;
            font-family: 'Montserrat', sans-serif;
            font-size: 11pt;
            color: #515151;
        }

        .discussions .discussion .message {
            margin: 6px 0 0 20px;
            font-family: 'Montserrat', sans-serif;
            font-size: 9pt;
            color: #515151;
        }

        .timer {
            margin-left: 15%;
            font-family: 'Open Sans', sans-serif;
            font-size: 11px;
            padding: 3px 8px;
            color: #BBB;
            background-color: #FFF;
            border: 1px solid #E5E5E5;
            border-radius: 15px;
        }

        .chat {
            width: calc(100%);
        }

        .header-chat {
            background-color: #FFF;
            height: 90px;
            box-shadow: 0px 3px 2px rgba(0, 0, 0, 0.100);
            display: flex;
            align-items: center;
            text-align: center;
        }

        .chat .header-chat .icon {
            margin-left: 30px;
            color: #515151;
            font-size: 14pt;
        }

        .chat .header-chat .name {
            margin: 0 0 0 20px;
            text-transform: uppercase;
            font-family: 'Montserrat', sans-serif;
            font-size: 13pt;
            color: #515151;
        }

        .chat .header-chat .right {
            position: absolute;
            right: 40px;
        }

        .chat .messages-chat {
            padding: 25px 35px;
            overflow: auto;
            height: 633px;
        }

        .chat .messages-chat .message {
            display: flex;
            align-items: center;
            margin-bottom: 8px;
        }

        .chat .messages-chat .message .photo {
            display: block;
            width: 45px;
            height: 45px;
            background: #E6E7ED;
            -moz-border-radius: 50px;
            -webkit-border-radius: 50px;
            background-position: center;
            background-size: cover;
            background-repeat: no-repeat;
        }

        .chat .messages-chat .text {
            margin: 0 35px;
            background-color: #f6f6f6;
            padding: 15px;
            border-radius: 12px;
        }

        .text-only {
            margin-left: 45px;
        }

        .time {
            font-size: 10px;
            color: lightgrey;
            margin-bottom: 10px;
            margin-left: 85px;
        }

        .response-time {
            float: right;
            margin-right: 40px !important;
        }

        .response {
            float: right;
            margin-right: 0px !important;
            margin-left: auto; /* flexbox alignment rule */
        }

        .response .text {
            background-color: #e3effd !important;
        }

        .footer-chat {
            width: calc(100%);
            height: 80px;
            display: flex;
            align-items: center;
            position: absolute;
            bottom: 0;
            background-color: transparent;
            border-top: 2px solid #EEE;

        }

        .chat .footer-chat .icon {
            margin-left: 30px;
            color: #C0C0C0;
            font-size: 14pt;
        }

        .chat .footer-chat .send {
            color: #fff;
            background-color: #4f6ebd;
            position: absolute;
            right: 50px;
            padding: 12px 12px 12px 12px;
            border-radius: 50px;
            font-size: 14pt;
        }

        .chat .footer-chat .name {
            margin: 0 0 0 20px;
            text-transform: uppercase;
            font-family: 'Montserrat', sans-serif;
            font-size: 13pt;
            color: #515151;
        }

        .chat .footer-chat .right {
            position: absolute;
            right: 40px;
            float: right;
        }

        .write-message {
            border: none !important;
            width: 90%;
            height: 50px;
            margin-left: 20px;
            padding: 10px;
            direction: rtl;
        }

        .footer-chat *::-webkit-input-placeholder {
            color: #C0C0C0;
            font-size: 13pt;
        }

        .footer-chat input *:-moz-placeholder {
            color: #C0C0C0;
            font-size: 13pt;
        }

        .footer-chat input *::-moz-placeholder {
            color: #C0C0C0;
            font-size: 13pt;
            margin-left: 5px;
        }

        .footer-chat input *:-ms-input-placeholder {
            color: #C0C0C0;
            font-size: 13pt;
        }

        .clickable {
            cursor: pointer;
        }


        .header-chat {
            position: relative;
        }

        .btn-info {
            position: absolute;
            right: 2%;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="row">
        <section class="chat">
            <div class="header-chat">
                {{--                <i class="icon fa fa-envelope" aria-hidden="true"></i>--}}
                {{--                <p class="name">رسائل الدردشة</p>--}}
                <br>
                <a class="btn btn-info" style="text-align: center !important;" href="{{url("/")}}"> الرجوع الي
                    الرئيسية </a>
            </div>
            <div class="clear"></div>
            <div class="messages-chat">
                {{--    Admin  Start --}}
                <div class="sender">

                </div>
                {{--    Admin  End --}}

                {{--    User  Start --}}
                <div class="receiver">
                    {{--                    <div class="message ">--}}
                    {{--                        <div class="response">--}}
                    {{--                            <p class="text">متي يمكن ان نتقابل ؟</p>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                </div>
                {{--    User  End --}}

            </div>
            <div class="footer-chat">
                <i class="icon send fa fa-paper-plane-o clickable" aria-hidden="true"></i>
                <input type="text" class="write-message" placeholder="اكتب رسالتك هنا ..."></input>
            </div>
        </section>
    </div>
</div>
<style>
    h1, h2, h3, h4, h5, h6,
    .h1, .h2, .h3, .h4, .h5, .h6, input, span, a, p, span, .an-quote {
        font-family: 'Cairo', sans-serif;
    }

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase.js"></script>
<script>

    var config = {
        apiKey: "AIzaSyA9Ktpr0NZaCi29VBU_YVg-MCEu8GcbQAg",
        authDomain: "vtyakoot.firebaseapp.com",
        databaseURL: "https://vtyakoot.firebaseio.com",
        projectId: "vtyakoot",
        messagingSenderId: "659761609768"
    };


    firebase.initializeApp(config);

    var messaging = firebase.messaging();

    messaging.requestPermission().then(function () {
        // console.log('Notification permission granted.');
    }).catch(function (err) {
        // console.log('Unable to get permission to notify.', err);
    });

    messaging.getToken().then(function (currentToken) {

        if (currentToken) {
            @auth
            $.get("{{url('/')}}" + "/set_fire_base/" + currentToken, function (data, status) {
                // console.log(data);
            });
            @endauth
            // console.log(currentToken);
        } else {
            console.log('No Instance ID token available. Request permission to generate one.');
        }

    }).catch(function (err) {
        console.log('An error occurred while retrieving token. ', err);
    });

    messaging.onMessage(function (payload) {
        $(document).scrollTop($(document).height());
        $('.show-right-side .notiTitle').append(payload.data.body);
    });

</script>
<script>

    $(document).ready(function () {

        // Get a reference to the database service
        var database = firebase.database();
        $(".send").click(function () {
            var message = $(".write-message").val();
            var chat_id = "{{$chat->id}}";
            var user_id = "{{Auth::id()}}";
            var time = "{{time()}}";
            $.post("{{url("sendMessage")}}",
                {
                    _token: "{{csrf_token()}}",
                    message: message,
                    chat_id: chat_id,
                    user_id: user_id,
                    time: time,
                },
                function (data, status) {
                    $(".write-message").val("");
                    var audio = new Audio('https://proxy.notificationsounds.com/wake-up-tones/the-little-dwarf-498/download/file-sounds-792-the-little-dwarf.mp3');
                    audio.play();
                    animation();
                });
        }); // End Click

        var chat_id = "{{$chat->id}}";
        messagesRefs = "false";
        database = firebase.database();
        messagesRef = database.ref("chats/{{$chat->id}}");
        messagesRef.once('value', function (snapshot) {
            messagesRefs = "true";
        });
        messagesRef.on('child_added', function (snapshot) {
            messagesRefs = "true";
            key = snapshot.key;
            value = snapshot.val();
            if (messagesRefs == "true") {
                if (value.send_by == "{{Auth::id()}}") {
                    response = "response";
                    $(".sender").append($(
                        '<div class="receiver">' +
                        '<div class="message ">' +
                        '<div class="' + response + '">' +
                        '<p class="text">' + value.message + '</p>' +
                        '</div>' +
                        '</div>' +
                        '</div>'
                        )
                    );
                } else {
                    response = "";
                    $(".sender").append($(
                        '<div class="message">' +
                        '<div class="photo" style="background-image: url({{asset("assets/site/img/profile/profile.png")}});">' +
                        '<div class="online"></div>' +
                        '</div>' +
                        '<p class="text messageVal"> ' + value.message + '</p>' +
                        '</div>'
                        )
                    );
                }
                // var audio = new Audio('https://proxy.notificationsounds.com/notification-sounds/got-it-done-613/download/file-sounds-1155-got-it-done.mp3');
                // audio.play();
                // animation();
            }

        });

    });


</script>
</body>
</html>


