@extends('.site.layout.container')
@section('title'," الاشتراك فى الدورة")

@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h8 class="title font_baner"> الاشتراك في دورة ({{$course->name}}) </h8>
                </div>
            </div>
        </div>
    </section>



    <!--====== Page Banner Ends ======-->
    <!--====== Event Start ======-->

    <section class="event-area">
        <div class="container">

            <div class="tab-content event-tab-items wow fadeInUpBig" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="tab-pane fade show active" id="courses">
                    <div class="row rtl-direction">
                        <div class="col-lg-12 col-sm-6">
                            <div class=" text-center mt-30">
                                <img src="{{$course->image}}" alt="Icon">
                                <br>
                                <br>
                                <br>
                                <h5 class="sub-title">مميزات الدورة</h5>
                                <br>
                                {!! $course->features !!}


                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--====== Event Ends ======-->
    <!--====== Event Start ======-->

    <section class="event-area">
        <div class="container">

            <div class="tab-content event-tab-items wow fadeInUpBig" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="tab-pane fade show active" id="courses">
                    <div class="row rtl-direction">
                        <div class="col-lg-4 col-sm-6">
                            <div class="single-event text-center mt-30">
                                <img src="{{asset("/assets/site")}}/images/icon/calendar1.png" alt="Icon">
                                <br>
                                <br>
                                <span class="date"> التاريخ  <strong>{{$course->date}} </strong></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="single-event text-center mt-30">
                                <img src="{{asset("/assets/site")}}/images/icon/wall-clock.png" alt="Icon">
                                <br>
                                <br>
                                <span class="date">المدة <strong>{{$course->duration}}</strong></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6">
                            <div class="single-event text-center mt-30">
                                <img src="{{asset("/assets/site")}}/images/icon/pricing.png" alt="Icon">
                                <br>
                                <br>
                                <span class="date">السعر<br></br> <strong>{{$course->price}} ريال سعودى</strong></span>
                            </div>
                        </div>
@if($course->is_special==1)
                        <div class="col-lg-12 col-sm-6">
                            <div class="single-event text-center mt-30">
                                <img src="{{asset("/assets/site")}}/images/icon/pricing.png" alt="Icon">
                                <br>
                                <br>
                                <span class="date">عروض الدورة<br></br>  {!! $course->offer !!} </span>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--====== Event Ends ======-->
    <!--====== Login Start ======-->

    <section class="login-register">
        <div class="container">
            <div class="row justify-content-center text-right">
                <div class="col-lg-6">
                    <div class="login-register-content">
                        <h4 class="title">سجل الان </h4>
                        @if (\Session::has('error'))
                            <div class="alert alert-danger top-alert alert-icon-left mb-0 fade-message" role="alert">
                                <span style="display: inline" class="alert-icon"><i class="fa fa-ban"></i></span>
                                <p style="display: inline" class="mb-0">{{ \Session::get('error') }}</p>
                            </div>
                        @endif
                        <div class="login-register-form">
                            <form action="{{url("/sponsored_advertisement")}}"
                                  enctype="multipart/form-data" method="post" >
                                @csrf
                                <input type="hidden" value="{{$item->id}}" name="user_id">
                                <input type="hidden" value="{{$course->id}}" name="course_id">

                                <div class="single-form ">
                                    <label>الاسم *</label>
                                    <input type="text" name="name" class="form-control"  placeholder="الاسم " required>
                                </div>
                                    <div class="single-form ">
                                        <label> رقم الجوال*</label>
                                        <input type="text" name="mobile" class="form-control"  placeholder="رقم الجوال" required>

                                    </div>

                                <div class="single-form ">
                                    <label>البريد الالكتروني *</label>
                                    <input type="email" name="email" class="form-control"  placeholder="البريد الالكتروني" required >
                                </div>



                                <div class="single-form">
                                    <button class="main-btn btn-block" type="submit">سجل </button>
                                </div>
                                <span class="line"></span>

                                <div class="single-form">
                                   <a class="main-btnw btn-block  " target="_blank" href="https://api.whatsapp.com/send?phone=+966{{$setting->whatsapp}}">
                                          للاستفسار والتسجيل عبر الوتساب
                                        </a>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Login Ends ======-->





@endsection
@section('extra_css')

@endsection

@section('extra_js')
    <script language="JavaScript" type="text/javascript" >
        $(function() {

            $('#colorselector').change(function() {
                $('.colors').hide();
                $('#' + $(this).val()).show();
            });
        });

    </script>
@endsection




