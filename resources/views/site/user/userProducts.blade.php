@extends('.site.layout.container')
@section('title',"منتجاتاك - سوق سماكو")

@section('content')
    <div id="titlebar" class="gradient margin-bottom-0"
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2>اعلانات </h2>
                <span>( {{$user->name}} ) </span>
            </div>
        </div>
    </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12" style="direction: rtl">
                <div class="utf_dashboard_list_box margin-top-15">
                    <h4><i class="sl sl-icon-list"></i> قائمه الاعلانات</h4>
                    <ul>
                        @if(count($items) > 0)
                            @foreach ($items as $product)
                                <li>
                                    <div class="utf_list_box_listing_item">
                                        <div class="utf_list_box_listing_item-img"><a
                                                href="{{url("/productDetails/$product->id")}}">
                                                <img style="height: 240px !important;"
                                                     src="{{$product->serv_one_image}}" alt=""></a></div>
                                        <div class="utf_list_box_listing_item_content">
                                            <div class="inner">
                                                <a href="{{url("/productDetails/$product->id")}}">
                                                    <p style="color: black;font-weight: bold">{{$product->name}}</p>
                                                </a>
                                                <span><i class="im im-icon-Hotel"></i> {{$product->dash_category_name}}</span>
                                                <span><i
                                                        class="sl sl-icon-location"></i> {{$product->dash_city_name}}</span>
                                                <span><i
                                                        class="sl sl-icon-phone"></i> {{$product->dash_user_mobile}}</span>
                                                <p>{{$product->description}}</p>
                                                <span><i class="sl sl-icon-clock"></i> {{$product->created_at}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    @if($is_my_products)
                                        <div class="buttons-to-right">
                                            <a href="{{url("/productEdit/$product->id")}}" class="button gray"><i
                                                    class="sl sl-icon-pencil"></i> تعديل</a>
                                            <form class="deleteForm" method="post" action="{{url("/productDelete")}}"
                                                  style="display: inline">
                                                <input class="deleteId" type="hidden" name="product_id"
                                                       value="{{$product->id}}">
                                                @csrf
                                            </form>
                                            <button type="submit" class="button gray" id="myBtn"><i
                                                    class="sl sl-icon-trash"></i> {{trans('language.delete')}}</button>
                                        </div>
                                    @endif
                                    <div id="myModal" class="modal">
                                        <!-- Modal content -->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <span class="close">&times;</span>
                                                <div class="swal2-icon swal2-error swal2-icon-show"
                                                     style="display: flex;"><span class="swal2-x-mark">
                                                    <span class="swal2-x-mark-line-left"></span>
                                                    <span class="swal2-x-mark-line-right"></span>
                                                  </span>
                                                </div>
                                            </div>
                                            <div class="modal-body">
                                                <p>تأكيد</p>
                                                <p>هل تم بيع سلعتك من خلال سماكو؟</p>
                                            </div>
                                            <div class="modal-footer"
                                                 style="display: flex;justify-content:space-around; font-size:20px;  ">
                                                <a href="{{url("/productSold/$product->id")}}" class="btn"
                                                   style="background-color: #4CAF50">نعم </a>
                                                <a href="{{url("/productDelete/$product->id")}}" class="btn"
                                                   style="background-color: #f44336">لا </a>
                                                <a href="#" class="btn dismiss"
                                                   style="background-color: #6c757d">الغاء </a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <div class="col-md-12">
                                <h3>لا يوجد اعلانات مضافه الي الان </h3>
                            </div>
                        @endif
                        @if(count($items) > 0 )
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="utf_pagination_container_part margin-top-20 margin-bottom-70">
                                            <nav class="pagination">
                                                <ul>
                                                    {{$items->appends(request()->input())->links()}}
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="utf_pagination_container_part margin-top-30 margin-bottom-30">
                </div>
            </div>
        </div>

    </div>


@endsection
@section('extra_css')
    <style>

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 14% !important; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            text-align: center;
            border-radius: 4px;
            padding: 0;
            border: 1px solid #888;
            width: 36%;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        /* Add Animation */
        @-webkit-keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }
            to {
                top: 0;
                opacity: 1
            }
        }

        @keyframes animatetop {
            from {
                top: -300px;
                opacity: 0
            }
            to {
                top: 0;
                opacity: 1
            }
        }

        /* The Close Button */
        .close {
            color: red;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #000;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-header {
            padding: 2px 16px;
            border-radius: 3px !important;

            color: white;
        }

        .modal-body {
            padding: 2px 16px;
            color: black
        }

        .modal-footer {
            padding: 31px 21px;

            color: white;
        }

        .modal-footer a {
            text-decoration: none;
            color: white;
            padding: 10px;

        }

        a:hover {
            opacity: 0.8;
        }

        .btn {
            border-bottom: 0px !important;
        }

        .svg-inline--fa {
            height: 4rem !important;
        }

        .svg-inline--fa {
            width: 4rem !important;
            margin: 10px 0px 10px 0px;
        }
    </style>

@endsection
@section('extra_js')
    <script>

        var modal = document.getElementById("myModal");

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];
        var dis = document.getElementsByClassName("dismiss")[0];

        // When the user clicks the button, open the modal
        btn.onclick = function () {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }
        dis.onclick = function () {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it

        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
@endsection

