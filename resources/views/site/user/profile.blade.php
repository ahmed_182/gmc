@extends('.site.layout.container')
@section('title',($user->name))
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title"> تفاصيل الحساب</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Teacher Details Start ======-->

    <section class="teacher-details">
        <div class="container">
            <div class="row teachers-row justify-content-center rtl-direction">
                <div class="col-lg-5 col-md-6 col-sm-8 teachers-col">
                    <div class="single-teacher-details mt-50 text-center">

                        <div class="teacher-image">
                            <a href="#">
                                <img src="{{$user->dash_image}}" alt="teacher">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 teachers-col text-right">
                    <div class="teacher-details-content mt-45">
                        <h4 class="teacher-name">{{$user->name}}</h4>
                        <p >{{$user->email}}</p>
                        <p >{{$user->mobile}}</p>

                    </div>
                </div>
            </div>

           <div class="teacher-details-tab">
                <ul class="nav nav-justified" role="tablist">
                    <li class="nav-item"><a data-toggle="tab" href="#achievements " role="tab">الدورات التى تم الاشتراك بها </a></li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane fade" id="achievements" role="tabpanel">
                        <div class="row">
                            @foreach(\App\Order::where("trainee_id",$user->id)->where('paid',1)->orderBy("id","ASC")->get() as $course)
                                <div class="col-lg-4 col-sm-6">
                                    <div class="single-content-tab">
                                        <h4 class="title">{{$course->dash_course}}</h4>
                                        <p>{{$course->dash_breif}}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!--====== Teacher Details Ends ======-->


@endsection
@section('extra_css')
    {{--   <style>
           .footer_sticky_part {
               padding-top: 100px !important;
           }
       </style>--}}
@endsection

