@extends('.site.layout.container')
@section('content')
    <div class="container">
        <div id="dashboard" style="direction: rtl">
            <a href="#" class="utf_dashboard_nav_responsive"><i class="fa fa-reorder"></i> Dashboard Sidebar Menu</a>
            <div class="utf_dashboard_content">
                <div class="col-lg-12 col-md-12">
                    <div class="utf_dashboard_list_box margin-top-0">
                        <h4><i class="sl sl-icon-star"></i>جميع التقييمات</h4>
                        <ul>
                            @foreach($reviews as $review)
                                <li>
                                    <div class="comments utf_listing_reviews dashboard_review_item">
                                        <ul>
                                            <li>
                                                <div class="avatar"><img
                                                        src="{{$review->serv_user_image_one}}"
                                                        alt=""></div>
                                                <div class="utf_comment_content">
                                                    <div class="utf_arrow_comment"></div>
                                                    <div
                                                        class="utf_by_comment">{{$review->serv_user_name_one}}
                                                        <div class="utf_by_comment-listing rightSide"></div>
                                                        <span class="date">  {{$review->time}}</span>
                                                    </div>
                                                    <p>{{$review->review_description}}</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            @endforeach
                            <nav class="pagination" style="margin-bottom: 10px">
                                <ul>
                                    {{$reviews->links()}}
                                </ul>
                            </nav>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

