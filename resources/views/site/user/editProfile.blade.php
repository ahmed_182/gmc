@extends('.site.layout.container')
@section('title',"تعديل البيانات - سوق سماكو")

@section('content')
    <div class="container">
        <div id="dashboard" style="direction: rtl">
            <a href="#" class="utf_dashboard_nav_responsive"><i class="fa fa-reorder"></i> Dashboard Sidebar Menu</a>
            <div class="utf_dashboard_content ">
                <div id="titlebar" class="dashboard_gradient">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>{{Auth::user()->user_name}}</h2>
                            <nav id="breadcrumbs">
                                <ul>
                                    <li>{{trans('language.profile')}}</li>
                                    <li><a href="{{url('/')}}">{{trans('language.home')}}</a></li>
                                </ul>
                                @auth
                                    <a href="{{url("/siteLogout")}}" class="button border " style="margin-top: 20px">
                                        <i class="sl sl-icon-logout "></i>{{trans('language.logout')}}
                                    </a>
                                @endauth
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2 col-md-6">
                        <a href="{{url("/followers/".Auth::user()->id)}}" style="color: #fff;">
                            <div class="utf_dashboard_stat  " style="color: #fff;background-color: #6A2755">
                                <div class="utf_dashboard_stat_content">
                                    <h4>{{Auth::user()->followers_count}}</h4>
                                    <span>المتابعين</span>
                                    <a class="forMore"
                                       href="{{url("/followers/".Auth::user()->id)}}">{{trans('language.details')}}</a>
                                </div>
                                <div class="utf_dashboard_stat_icon"><i class="im im-icon-Business-Mens"></i></div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-6">
                        <a href="{{url("/following/".Auth::user()->id)}}" style="color: #fff;">
                            <div class="utf_dashboard_stat  " style="color: #fff;background-color: #A21F4F">
                                <div class="utf_dashboard_stat_content">
                                    <h4>{{Auth::user()->following_count}}</h4>
                                    <span>المتابعون</span>
                                    <a class="forMore"
                                       href="{{url("/following/".Auth::user()->id)}}">{{trans('language.details')}}</a>
                                </div>
                                <div class="utf_dashboard_stat_icon"><i class="im im-icon-Add-UserStar"></i></div>
                            </div>
                        </a>
                    </div>


                    <div class="col-lg-2 col-md-6">
                        <div class="utf_dashboard_stat " style="color: #fff;background-color: #D2204A">
                            <div class="utf_dashboard_stat_content">
                                <h4>{{Auth::user()->my_sold_products_count}}</h4>
                                <span>عدد المبيعات</span>
                            </div>
                            <div class="utf_dashboard_stat_icon"><i class="im im-icon-Money-Bag"></i></div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-6">
                        <div class="utf_dashboard_stat " style="color: #fff;background-color: #FF6C4A">
                            <div class="utf_dashboard_stat_content">
                                <h4>{{Auth::user()->my_all_products_count}}</h4>
                                <span>اجمالي الاعلانات</span>
                            </div>
                            <div class="utf_dashboard_stat_icon"><i class="im im-icon-Align-JustifyRight"></i></div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-6">
                        <a href="{{url("/userProducts/".Auth::user()->id)}}" style="color: #fff;">
                            <div class="utf_dashboard_stat " style="color: #fff;background-color: #ec8b38">
                                <div class="utf_dashboard_stat_content">
                                    <h4>{{Auth::user()->my_current_products_count}}</h4>
                                    <span>الاعلانات الحاليه</span>
                                    <a class="forMore"
                                       href="{{url("/userProducts/".Auth::user()->id)}}">{{trans('language.details')}}</a>
                                </div>
                                <div class="utf_dashboard_stat_icon"><i class="im im-icon-Diploma"></i></div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-6">
                        <div class="utf_dashboard_stat " style="color: #fff;background-color: #b74b0ccc">
                            <div class="utf_dashboard_stat_content">
                                <h4>%{{Auth::user()->rate_percetange}}</h4>
                                <span>التقيم العام</span>
                            </div>
                            <div class="utf_dashboard_stat_icon"><i class="im im-icon-Star"></i></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="utf_dashboard_list_box margin-top-0">
                            <h4 class="gray"><i class="sl sl-icon-user"></i> البيانات الشخصيه <span> ( رقم العميل : #{{Auth::user()->id}} ) </span>
                            </h4>
                            <div class="utf_dashboard_list_box-static">

                                <form action="{{ url('/updateProfile') }}" method="post"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="container1 edit-profile-photo">
                                        <label class="label1" for="input1">اضف صوره</label>

                                        <div class="input1 ">
                                            <input name="image" id="file1" type="file"/>
                                        </div>
                                    </div>


                                <!-- <div class="edit-profile-photo">
                                        <img style="height: 150px; width: 150px;"
                                             src="{{Auth::user()->serv_image}}" alt="">
                                        <div class="change-photo-btn">
                                            <div class="photoUpload"><span><i class="sl sl-icon-cloud-upload "></i> اضافه صوره</span>
                                                <input type="file" class="upload">
                                            </div>
                                        </div>
                                    </div> -->


                                    <div class="my-profile" style="float: right;direction: rtl">
                                        <div class="row with-forms">
                                            <div class="col-md-6 float-right">
                                                <label>البريد الاليكتروني</label>
                                                <input type="text" class="input-text" placeholder="البريد الالكيتروني"
                                                       value="{{Auth::user()->email}}" name="email">
                                            </div>
                                            <div class="col-md-6">
                                                <label>رقم الجوال</label>
                                                <input type="text" class="input-text" placeholder="رقم الجوال"
                                                       value="{{Auth::user()->mobile}}" name="mobile">
                                            </div>
                                            <div class="col-md-6">
                                                <label>الاسم </label>
                                                <input type="text" class="input-text" placeholder="الاسم بالكامل"
                                                       value="{{Auth::user()->name}}" name="name">
                                            </div>

                                            <div class="col-md-6">
                                                <label>اسم المستخدم </label>
                                                <input type="text" class="input-text" placeholder="اسم المستخدم"
                                                       value="{{Auth::user()->user_name}}" name="user_name">
                                            </div>
                                            <div class="col-md-12">
                                                <label> اخترالدوله : </label>
                                                <p class="utf_row_form utf_form_wide_block">
                                                    <select style="height: 60px;direction: rtl" class="country_id"
                                                            name="country_id">
                                                        <option value="0">اخترالدوله</option>
                                                        @foreach(\App\Country::all() as $country_obj)
                                                            <option
                                                                @if($country_obj->id == Auth::user()->country_id ) selected
                                                                @endif value="{{$country_obj->id}}">{{$country_obj->dash_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label> المنطقه : </label>
                                                <p class="utf_row_form utf_form_wide_block">
                                                    <select id="regoin_id" style="height: 60px;direction: rtl"
                                                            class="regoin_id" name="region_id">
                                                        <option value="0">اختر المنطقه</option>
                                                        @foreach(\App\Regoin::all() as $regoin_obj)
                                                            <option
                                                                @if($regoin_obj->id == Auth::user()->region_id ) selected
                                                                @endif value="{{$regoin_obj->id}}">{{$regoin_obj->dash_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <label> المدينه : </label>
                                                <p class="utf_row_form utf_form_wide_block">
                                                    <select id="distract_id" style="height: 60px ;direction: rtl"
                                                            class="distract_id"
                                                            name="city_id">
                                                        <option value="0">اختر المدينه</option>
                                                        @foreach(\App\Distrct::all() as $city_obj)
                                                            <option
                                                                @if($city_obj->id == Auth::user()->city_id ) selected
                                                                @endif value="{{$city_obj->id}}">{{$city_obj->dash_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </p>
                                            </div>





                                            <div class="col-md-12">
                                                <label> اختر حاله ظهور رقم الجوال : </label>
                                                <p class="utf_row_form utf_form_wide_block">
                                                    <select style="height: 60px;direction: rtl" class="hide_mobile"
                                                            name="hide_mobile">
                                                        <option @if(Auth::user()->hide_mobile == 1 ) selected
                                                                @endif value="1">
                                                            ظهور
                                                        </option>
                                                        <option @if(Auth::user()->hide_mobile == 0 ) selected
                                                                @endif  value="0">اخفاء
                                                        </option>
                                                    </select>
                                                </p>
                                            </div>


                                            <div class="col-md-12">
                                                <label>نبذه عنك .</label>
                                                <textarea name="bio" cols="30"
                                                          rows="10">{{Auth::user()->bio}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="button preview btn_center_item margin-top-15">تحديث
                                        البيانات
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('extra_js')
    <script>
        $(function () {
            var container1 = $(".container1"),
                inputFile = $("#file1"),
                img,
                btn,
                txt = " اضف صورة",
                txtAfter = "تغير الصوره";

            if (!container1.find("#uploadD").length) {
                container1
                    .find(".input1")
                    .append('<input type="button" class="upload" value="' + txt + '" id="uploadD">');
                btn = $("#uploadD");
                container1.prepend(
                    '<img src="{{Auth::user()->serv_image}}" class="edit_WIDth" alt="Uploaded file" id="uploadImg1" width="300">'
                );
                img = $("#uploadImg1");
            }

            btn.on("click", function () {
                img.animate({opacity: 0}, 300);
                inputFile.click();
            });

            inputFile.on("change", function (e) {
                container1.find("label1").html(inputFile.val());

                var i = 0;
                for (i; i < e.originalEvent.srcElement.files.length; i++) {
                    var file = e.originalEvent.srcElement.files[i],
                        reader = new FileReader();

                    reader.onloadend = function () {
                        img.attr("src", reader.result).animate({opacity: 1}, 700);
                    };
                    reader.readAsDataURL(file);
                    img.removeClass("hidden1");
                }

                btn.val(txtAfter);
            });
        });
    </script>
@endsection
