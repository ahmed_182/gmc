@extends('.site.layout.container')
@section('title',"اشعاراتك - سوق سماكو")

@section('content')
    <div class="container">
        <div class="an-title-container center ">
            <h1 class="menu_AllNoti header1">الاشعارات</h1>
        </div> <!-- end title container -->
    </div> <!-- end cotnainer -->
    <!-- start  notification -->
    <div class="container">
        <a href="{{url("delete_all_notification")}}" class="button gray" style=""><i
                class="sl sl-icon-trash"></i>حذف جميع الاشعارات</a>
        <div class="AllNoti">

            <ul class="menu_AllNoti">
                @if (count($items) > 0 )
                    @foreach($items as $item)
                        <div class="menu_AllNoti">
                            <a href="{{url("delete_notification/$item->id")}}" class="button gray" style="float: left"><i
                                    class="sl sl-icon-trash"></i> {{trans('language.delete')}}</a>

                           <li> <i class="sl"></i>{{$item->title}} </li>
                            <div class="inner-div">
                            <a class="notif_site" href="{{url("showNotification/$item->id")}}"   @if($item->viewed == 0 ) style="background: #38a6a6"   @endif>


                            <li> <i class="sl sl-icon-arrow-left-circle"></i> {{$item->body}}

                            </a>
                            <li class="timeing"> <i class="sl sl-icon-clock"></i> {{$item->notification_relation->getTime()}}<li>
                            </div>
                            <br>
                            <hr  style="font-size: 1px; border-top: 1px solid #ccc !important;">



                        </div>
                    @endforeach
                        <div class="row" style="width: 100%">
                            <div class="col-md-12">
                                <div class="utf_pagination_container_part margin-top-20 margin-bottom-70">
                                    <nav class="pagination">

                                        {{$items->links()}}

                                    </nav>
                                </div>
                            </div>
                        </div>

                @else
                            <h2>لا توجد اشعارك لك حتي الان .</h2>
                        @endif

            </ul>
        </div>
    </div>

  <br>
    <br>
    <br>

<br>
    <br>

    <!-- end notification -->
@section("extra_css")
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 400px; /* The height is 400 pixels */
            width: 100%; /* The width is the width of the web page */
        }

        /* start notification style */
        .AllNoti{
            margin-top:20px;
            padding:5px 10px;
        }
        .menu_AllNoti{
            text-align: right;
        }
        .menu_AllNoti li{
            margin-top:23px;

        }
        .menu_AllNoti li i{
            float:right;
            color:#ff2222;
            padding:0 10px;
        }
.menu_AllNoti li:first-child{

    font-size: large;
    font-weight: 900;

}
        .timeing{
            font-weight: 900;
        }
        .inner-div a:hover{
            background-color: initial !important;
            color: #ff2222;
        }
        .header1{
            text-align: center;
            margin-bottom: 36px;

        }
        .AllNoti{
            background-color: #eee;
            color: black !important;
        }
        .gray:hover{

            background-color: #6d7a83 !important;
        }

       </style>
@endsection
@endsection
