@extends('.site.layout.container')

@section('content')
    <div class="container">
        <div id="dashboard" style="direction: rtl">
            <a href="#" class="utf_dashboard_nav_responsive"><i class="fa fa-reorder"></i> Dashboard Sidebar Menu</a>
            <div class="utf_dashboard_content">


                <div class="row">
                    <div class="col-lg-2 col-md-6">
                        <a href="{{url("/followers/".$user->id)}}" style="color: #fff;">
                            <div class="utf_dashboard_stat color-1">
                                <div class="utf_dashboard_stat_content">
                                    <h4>{{$user->followers_count}}</h4>
                                    <span>المتابعين</span>
                                    <a class="forMore"
                                       href="{{url("/followers/".$user->id)}}">{{trans('language.details')}}</a>
                                </div>
                                <div class="utf_dashboard_stat_icon"><i class="im im-icon-Map2"></i></div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-6">
                        <a href="{{url("/following/".$user->id)}}" style="color: #fff;">
                            <div class="utf_dashboard_stat color-2">
                                <div class="utf_dashboard_stat_content">
                                    <h4>{{$user->following_count}}</h4>
                                    <span>المتابعون</span>
                                    <a class="forMore"
                                       href="{{url("/following/".$user->id)}}">{{trans('language.details')}}</a>
                                </div>
                                <div class="utf_dashboard_stat_icon"><i class="im im-icon-Add-UserStar"></i></div>
                            </div>
                        </a>
                    </div>


                    <div class="col-lg-2 col-md-6">
                        <div class="utf_dashboard_stat color-5">
                            <div class="utf_dashboard_stat_content">
                                <h4>{{$user->my_sold_products_count}}</h4>
                                <span>عدد المبيعات</span>
                            </div>
                            <div class="utf_dashboard_stat_icon"><i class="im im-icon-Eye-Visible"></i></div>
                        </div>
                    </div>

                    <div class="col-lg-2 col-md-6">

                        <div class="utf_dashboard_stat color-3">
                            <div class="utf_dashboard_stat_content">
                                <h4>{{$user->my_all_products_count}}</h4>
                                <span>اجمالي الاعلانات</span>
                            </div>
                            <div class="utf_dashboard_stat_icon"><i class="im im-icon-Align-JustifyRight"></i></div>
                        </div>

                    </div>

                    <div class="col-lg-2 col-md-6">
                        <a href="{{url("/userProducts/".$user->id)}}" style="color: #fff;">
                            <div class="utf_dashboard_stat color-4">
                                <div class="utf_dashboard_stat_content">
                                    <h4>{{$user->my_current_products_count}}</h4>
                                    <span>الاعلانات الحاليه</span>
                                    <a class="forMore"
                                       href="{{url("/userProducts/".$user->id)}}">{{trans('language.details')}}</a>
                                </div>
                                <div class="utf_dashboard_stat_icon"><i class="im im-icon-Diploma"></i></div>
                            </div>
                        </a>
                    </div>

                    <div class="col-lg-2 col-md-6">
                        <div class="utf_dashboard_stat color-6">
                            <div class="utf_dashboard_stat_content">
                                <h4>%{{$user->rate_percetange}}</h4>
{{--
                                {{$item->dash_rate}}%
--}}
                                <span>التقيم العام</span>
                            </div>
                            <div class="utf_dashboard_stat_icon"><i class="im im-icon-Star"></i></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="utf_dashboard_list_box margin-top-0">
                            <h4 class="gray"><i class="sl sl-icon-user"></i> البيانات الشخصيه <span> ( رقم العميل : #{{$user->id}} ) </span>
                            </h4>
                            <div class="utf_dashboard_list_box-static" style="padding: 0px">

                                <div class="edit-profile-photo">

                                    <div class="row">

                                        <div class="col-lg-6" style="padding-top: 50px">

                                            <div>
                                                @if($user->hide_mobile == 0)
                                                    <a href="whatsapp://send?phone=+966{{$user->mobile}}"
                                                       class="width_but button border margin-top-20">
                                                        تواصل بالواتس اب
                                                        <i style="margin-right: 5px" class="fa fa-phone"></i>
                                                    </a>
                                                @endif
                                            </div>

                                            <div>
                                                <a href="{{url("/add-user-review/$user->id")}}"
                                                   data-toggle="modal" data-target="#exampleModalCenter"
                                                   class="width_but button border margin-top-20">
                                                    اضافه تقييم
                                                    <i style="margin-right: 5px" class="fa fa-plus"></i>
                                                </a>
                                            </div>

                                            <div>
                                                <a href="{{url("/chat/$user->id")}}"
                                                   class="width_but button border margin-top-20"> رسائل الخاصة
                                                    <i style="margin-right: 5px" class="fa fa-mail-bulk"></i>
                                                </a>
                                            </div>

                                        </div>
                                        <div class="col-lg-6" style="padding-top: 20px">
                                            <img style="height: 150px; width: 150px;"
                                                 src="{{$user->image}}" alt="">
                                            <h2>{{$user->name}}</h2>
                                            <div>
                                                @auth
                                                    @if(Auth::user()->followThisUser($user->id) == true)
                                                        <a href="{{url("/delete_follow/$user->id")}}"
                                                           class="width_but button   margin-top-20"> الغاء متابعه
                                                            الحساب</a>
                                                    @else
                                                        <a href="{{url("/add_follow/$user->id")}}"
                                                           class="width_but button   margin-top-20">متابعه
                                                            الحساب</a>
                                                    @endif

                                                @endauth
                                            </div>
                                        </div>
                                    </div>


                                </div>

                                <div class="col-lg-12 col-md-12">
                                    <br>
                                    <p><i class="sl sl-icon-user color_ico"> : </i> عضو {{$user->time}}</p>
                                    <p><i class="sl sl-icon-eye color_ico"> : </i> اخر ظهور {{$user->online_at_time}}
                                    </p>
                                </div>

                                <div class="my-profile" style="float: right;direction: rtl">
                                    <div class="row with-forms">
                                        <div class="col-md-6 float-right">
                                            <label>البريد الاليكتروني </label>
                                            <p style="margin-right: 5px"><i class=" color_ico fa fa-envelope"></i>
                                                : {{$user->dash_email}}</p>
                                        </div>
                                        <div class="col-md-6">
                                            <label>رقم الجوال</label>
                                            @if($user->hide_mobile == 1)
                                                <p>رقم الجوال غير متاح</p>
                                            @else
                                                <p style="margin-right: 5px"><i class=" color_ico fa fa-phone"></i>
                                                    : {{$user->mobile}}</p>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            <label>الاسم </label>
                                            <p style="margin-right: 5px"><i class=" color_ico fa fa-user"></i>
                                                : {{$user->name}}
                                            </p>
                                        </div>
                                        <div class="col-md-6">
                                            <label>اسم المستخدم </label>
                                            <p style="margin-right: 5px"><i class=" color_ico fa fa-user"></i>
                                                : {{$user->user_name}}
                                            </p>
                                        </div>

                                        <div class="col-md-12">
                                            <label>العنوان </label>
                                            <p style="margin-right: 5px"><i class="color_ico  fa fa-map-pin"></i>
                                                : {{$user->address}}</p>

                                        </div>
                                        <div class="col-md-12">
                                            <label>نبذه عنك .</label>
                                            <p style="margin-right: 5px"><i
                                                    class="color_ico  fa fa-arrow-circle-down"></i>
                                                : {{$user->bio}}</p>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="col-lg-12 col-md-12">
                                <div class="utf_dashboard_list_box margin-top-0">
                                    <h4><i class="sl sl-icon-star"></i> تقيم العملاء</h4>
                                    <ul>
                                        @foreach($reviews as $review)
                                            <li>
                                                <div class="comments utf_listing_reviews dashboard_review_item">
                                                    <ul>
                                                        <li>
                                                            <div class="avatar"><img
                                                                    src="{{$review->serv_user_image_one}}"
                                                                    alt=""></div>
                                                            <div class="utf_comment_content">
                                                                <div class="utf_arrow_comment"></div>
                                                                <div
                                                                    class="utf_by_comment">{{$review->serv_user_name_one}}
                                                                    <div class="utf_by_comment-listing rightSide"></div>
                                                                    <span class="date">  {{$review->time}}</span>
                                                                </div>
                                                                <p>{{$review->review_description}}</p>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                    @if(count($reviews) > \App\ModulesConst\Paginate::value)
                                        <div class="col-md-12 centered_content"><a
                                                href="{{url("/user_reviwes/$user->id")}}"
                                                class="button border margin-top-20">عرض المزيد</a></div>
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                                <br>
                                <br>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection
@section('extra_css')
    <style>
        .color_ico {
            color: #ff2222;
        }

        .width_but {
            width: 200px;
        }

        .img_wh {
            width: 30px !important;
            height: 30px;
            border: none !important;
        }
    </style>


@endsection
