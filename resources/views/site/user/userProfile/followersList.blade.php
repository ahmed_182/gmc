@extends('.site.layout.container')
@section('content')

    <div id="titlebar" class="gradient margin-bottom-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>قائمه المتابعيين</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <div class="utf_box_widget margin-top-35 margin-buttom-35 col-md-12" style="direction: rtl">
                <h3><i class="sl sl-icon-book-open"></i> الاشخاص الذين قاموا بمتابعتك  </h3>
                <ul class="utf_widget_tabs">
                    @if(count($followers) > 0)
                        @foreach ($followers as $follower)
                    <li>
                        <div class="utf_widget_content">
                            <div class="utf_widget_thum"><a href="{{url("/user_profile/$follower->serv_user_id_one")}}"><img
                                        src="{{$follower->serv_user_image_one}}" alt=""></a></div>
                            <div class="utf_widget_text">
                                <h5><a href="{{url("/user_profile/$follower->serv_user_id_one")}}">{{$follower->serv_user_name_one}}</a></h5>
                                <span><i class="fas fa-clock"></i> {{$follower->time}}</span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </li>

                        @endforeach
                    @else
                        <h3>لم تقم احد بمتابعتك  الي الان </h3>
                    @endif

                </ul>
            </div>


        </div>

        <br>
        <br>
    </div>





@endsection
