@extends('.site.layout.container')
@section('content')

    <div id="titlebar" class="gradient margin-bottom-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>قائمه المتابعون</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <div class="utf_box_widget margin-top-35 margin-buttom-35 col-md-12" style="direction: rtl">
                <h3><i class="sl sl-icon-book-open"></i> الاشخاص التي تقوم بمتابعتهم </h3>
                <ul class="utf_widget_tabs">
                    @if(count($followings) > 0)
                        @foreach ($followings as $following)
                            <li class="list-users">
                                <div class="utf_widget_content">
                                    <div class="utf_widget_thum"><a href="{{url("/user_profile/$following->serv_user_id_two")}}"><img
                                                src="{{$following->serv_user_image_two}}" alt=""></a></div>
                                    <div class="utf_widget_text">
                                        <h5><a href="{{url("/user_profile/$following->serv_user_id_two")}}">{{$following->serv_user_name_two}}</a></h5>
                                        <span><i class="fas fa-clock"></i> {{$following->time}}</span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div>
                                    @auth
                                        @if(Auth::user()->followThisUser($following->serv_user_id_two) == true)
                                            <a href="{{url("/delete_follow/$following->serv_user_id_two")}}"
                                               class="width_but button   margin-top-20"> الغاء متابعه
                                                الحساب</a>
                                        @else

                                        @endif

                                    @endauth
                                </div>
                        </li>

                        @endforeach
                    @else
                        <h3>لم تقم بمتابعة احد الي الان </h3>
                    @endif

                    <li>

                    </li>

                </ul>
            </div>


        </div>

        <br>
        <br>
    </div>





@endsection
@section('extra_css')
    <style>
      .list-users{

          display: flex;
          flex-wrap: wrap;
          justify-content: space-between;
      }
    </style>


@endsection

