@extends('.site.layout.container')
@section('content')
    @if($errors->all())
        @foreach ($errors->all() as $error)
            <h4 class="text-center">{{ $error }}</h4>
        @endforeach
    @endif
    <section class="fullwidth_block  padding-top-75 padding-bottom-70" data-background-color=""
             style="background: rgb(249, 249, 249);">

        <div class="container">
            <div class="row ">
                <div class="col-md-12">
                    <h3 class="headline_part centered margin-bottom-45">
                        اضف تقييم
                    </h3>
                </div>
            </div>

            <div class="col-md-12" style="direction: rtl">
                <section id="contact">
                    @if(Session::has('reviewed_within_day_ago'))
                        <h4 class="text-danger">{{trans("language.reviewed_within_day_ago")}}</h4>
                    @endif
                    <h4>
                        <label class="box">
                            اقسم بالله
                            {{Auth::user()->name}}
                            أنني اشتريت منتجاً او خدمه من
                            {{$user->name}}
                            والمعلومات المقدمة صحيحه ودقيقة واتحمل كل المسؤوليه عنها .
                            <input type="checkbox" name="okk"/><span class="checkmark"></span></label>
                    </h4>
                    <form id=" " method="post" action="{{url("/store-user-review")}}"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <div class="row">

                            <div class="col-md-5">
                                <h4 class="takkim_head">
                                    هل تم شراء منتج او خدمة من {{$user->name}}
                                </h4>
                                <div class="takkim_ch">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="radio">
                                                <input type="radio" id="no2" value="2"
                                                       name="sale_product"/>
                                                <label id="n2" for="no2"></label>
                                                <span> لا </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="radio">
                                                <input type="radio" id="yes2" value="1"
                                                       name="sale_product"/>
                                                <label id="y2" for="yes2"></label>
                                                <span> نعم </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="Takkim_border"></div>
                            </div>

                            <div class="col-md-5">
                                <h4 class="takkim_head">
                                    هل توصي بالتعامل مع {{$user->name}}
                                </h4>
                                <div class="takkim_ch">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="radio">
                                                <input type="radio" id="no1" value="3"
                                                       name="review_type"/>
                                                <label id="n1" for="no1"></label>
                                                <span> لا </span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="radio">
                                                <input type="radio" id="yes1" value="4"
                                                       name="review_type"/>
                                                <label id="y1" for="yes1"></label>
                                                <span> نعم </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <br><br>
                                <input class="TAKK_product Tkkim_hide" name="service_description" type="text"
                                       placeholder="اكتب الخدمة او المنتج التي اشتريتها من البائع">
                            </div>

                            <div class="col-md-12">
                                <textarea class="TAKK_product Tkkim_hide" name="review_description" cols="40" rows="2"
                                          placeholder="اكتب تجربتك للبائعين الاخرين ..."
                                ></textarea>
                            </div>

                            <div class="col-md-12">
                                <div class="takkim_radio">
                                    <div class="takkim_face">
                                        <i class="dislike fa fa-smile-beam"></i>
                                        <span>كم نسبة تقييمك لهذا البائع</span>
                                        <i class="like fa fa-frown"></i>
                                    </div>
                                    <div class="takkim_radio_cont">
                                        <div class="row">
                                            <div class="col-md-1">

                                            </div>

                                            <div class="col-md-2">
                                                <div class="radio">
                                                    <input type="radio" id="rate5" value="100" name="rate"/>
                                                    <label for="rate5"></label>
                                                    <span> 100% </span>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="radio">
                                                                <input type="radio" id="rate4" value="75"
                                                                       name="rate"/>
                                                                <label for="rate4"></label>
                                                                <span> 75% </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="radio">
                                                                <input type="radio" id="rate3" value="50"
                                                                       name="rate"/>
                                                                <label for="rate3"></label>
                                                                <span> 50% </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="radio">
                                                                <input type="radio" id="rate2" value="25"
                                                                       name="rate"/>
                                                                <label for="rate2"></label>
                                                                <span> 25% </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="radio">
                                                                <input type="radio" id="rate1" value="0"
                                                                       name="rate"/>
                                                                <label for="rate1"></label>
                                                                <span> 0% </span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-1">
                                            </div>


                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <input type="submit" class="takkim_but submit button" value="اضافة تقييم">
                    </form>
                </section>
            </div>
        </div>
    </section>
@endsection



