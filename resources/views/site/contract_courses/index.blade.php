@extends('.site.layout.container')
@section('title','الدورات التعاقدية')
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title">الدورات التعاقدية </h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Login Start ======-->

    <section class="login-register">
        <div class="container">
            <div class="row justify-content-center text-right">
                <div class="col-lg-6">
                    <div class="login-register-content">
                        <h4 class="title">يرجى تعبئة النموذج التالي لتحميل دليل الدورات التدريبية</h4>

                        <div class="login-register-form">
                            <form action="{{url("/make_contract_courses")}}"
                                  enctype="multipart/form-data" method="post">
                                @csrf
                                <div class="single-form">
                                    <label>الاسم بالكامل*</label>
                                    <input type="text" name="name" class="form-control"  placeholder="الاسم بالكامل" required>
                                </div>
                                <div class="single-form">
                                    <label> اسم الجهة *</label>
                                    <input type="text" name="entity" class="form-control"  placeholder="اسم الجهة" required>
                                </div>
                                <div class="single-form">
                                    <label> رقم الجوال*</label>
                                    <input type="text" name="mobile" class="form-control"  placeholder="رقم الجوال" required>
                                </div>
                                <div class="single-form">
                                    <label>البريد الالكتروني *</label>
                                    <input type="email" name="email" class="form-control"  placeholder="البريد الالكتروني" required>
                                </div>
                                <div class="single-form">
                                    <label>المدينة  *</label>
                                    <input  type="text" name="city" class="form-control"  placeholder="المدينة" required>
                                </div>
                                <div class="single-form">
                                    <label>اسم الدورة (اختياري) </label>
                                    <input type="text" name="course" class="form-control"  placeholder="اسم الدورة" >
                                </div>

                                <div class="single-form">
                                    <button class="main-btn btn-block" type="submit">تأكيد</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Login Ends ======-->


@endsection
@section('extra_css')
   {{-- <style>
        .footer_sticky_part {
            padding-top: 100px !important;
        }
    </style>--}}
@endsection

