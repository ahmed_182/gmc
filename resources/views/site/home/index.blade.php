@extends('.site.layout.container')
@section('title',trans('language.home_page'))
@section('content')

    <!--====== Slider Start ======-->

    <section class="slider-area slider-active">
        @foreach(\App\Slider::orderBy("id","ASC")->get() as $slider)

        <div class="single-slider d-flex align-items-center bg_cover" style="background-image: url({{$slider->image}});">
            <div class="container">
                <div class="slider-content">
                    <h2 class="title" data-animation="fadeInLeft" data-delay="0.2s">{{$slider->name_ar}}</h2>
                    <ul class="slider-btn">
                        <li><a data-animation="fadeInUp" data-delay="0.6s" class="main-btn main-btn-2" href="{{url("/courses")}}">مشاهدة الدورات</a></li>
                        <li><a data-animation="fadeInUp" data-delay="1s" class="main-btn" href="{{url("/courses")}}"> المزيد</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
    </section>

    <!--====== Slider Ends ======-->



    <!--====== Campus Visit Start ======-->


    <!--====== Event Start ======-->

    <section class="event-area">
        <div class="container">
            <div class="event-title-tab-menu text-center">
                <div class="event-title mt-40 ">
                    <h2 class="title" >اهم ما يميزنا</h2>
                </div>
            </div>
            <div class="tab-content event-tab-items wow fadeInUpBig" data-wow-duration="1s" data-wow-delay="0.2s">
                <div class="tab-pane fade show active" id="courses">
                    <div class="row rtl-direction">
                        <div class="col-lg-3 col-sm-6">
                            <div class="single-event text-center mt-30">
                                <img src="{{asset("/assets/site")}}/images/icon/icon-2-1.png" alt="Icon">
                                <span class="date">الدورات</span>
                                <h4 class="event-title"><a href="{{url("/courses")}}">توفير برامج للشركات والجهات التي تلبي حاجاتها وطموحاتها</a></h4>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="single-event text-center mt-30">
                                <img src="{{asset("/assets/site")}}//images/icon/icon-2-4.png" alt="Icon">                                <span class="date">المدربين</span>
                                <h4 class="event-title"><a href="{{url("/courses")}}">من أفضل المدربين على مستوى المملكة والخليج والوطن العربي</a></h4>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="single-event text-center mt-30">
                                <img src="{{asset("/assets/site")}}//images/icon/icon-2-2.png" alt="Icon">                                <span class="date">الإستشارات</span>
                                <h4 class="event-title"><a href="{{url("/courses")}}">استشارات متنوعة في كافة المجالات التدريبية لحجز المواعيد</a></h4>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-6">
                            <div class="single-event text-center mt-30">
                                <img src="{{asset("/assets/site")}}//images/icon/icon-2-5.png" alt="Icon">                                <span class="date">الدورات تعاقدية</span>
                                <h4 class="event-title"><a href="{{url("/contract_courses")}}">نقدم البرامج التدريبية للمنظمات وفق الاحتياجات التدريبية</a></h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!--====== Event Ends ======-->


    <!--====== Blog Start ======-->

    <section class="blog-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-9">
                    <div class="section-title-2 text-center">
                        <h2 class="title">الدورات العامه </h2>
                        <span class="line"></span>
                        <p>قسم الدورات العامه حيث يمكنك ان تجد كل الدوارات التي تبحث عنها</p>
                    </div>
                </div>
            </div>
            <div class="blog-wrapper">
                <div class="row blog-active">
                    @foreach (\App\Course::where("end_date", '>=',Carbon\Carbon::now()->format('Y-m-d'))->orderBy("id","desc")->take(6)->get() as $course)
                    <div class="col-lg-4 flo-right">
                        <div class="single-blog mt-30">
                            <div class="blog-image">
                                <a href="{{url("/courseDetails/$course->id")}}">
                                    <img src="{{$course->image}}" alt="blog">
                                </a>
                            </div>
                            <div class="blog-content">
                                <h4 class="blog-title text-right" ><a href="{{url("/courseDetails/$course->id")}}">{{$course->name}}
                                    </a></h4>
                                <a href="{{url("/courseDetails/$course->id")}}" class="more"><i class="fal fa-chevron-left"></i>المزيد </a>
                            </div>
                        </div>
                    </div>
                        @endforeach
                </div>
            </div>
        </div>
    </section>

    <!--====== Blog Ends ======-->



@endsection
@section('extra_css')
   {{-- <style>
        .footer_sticky_part {
            padding-top: 100px !important;
        }
    </style>--}}
@endsection

