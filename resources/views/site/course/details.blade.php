@extends('.site.layout.container')
@section('title',($item->name))
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title">تفاصيل الدورة</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Courses Details Start ======-->

    <section class="courses-details">
        <div class="container">
            <div class="row flex-row-reverse rtl-direction">
                <div class="col-lg-9">
                    <div class="courses-details-content mt-50 text-right">
                        <img src="{{$item->image}}" width="100%" height="508px" alt="">

                        <h2 class="title">{{$item->name}}</h2>

                        <p>{{$item->breif}}</p>

                        <h5 class="sub-title">المحاور</h5>
                        {!! $item->description !!}
                    </div>

                    <div class="courses-details-tab">
                        <ul class="nav nav-justified" role="tablist">
                            <li class="nav-item"><a class="active" data-toggle="tab" href="#benefit" role="tab">مميزات الدورة</a></li>
                            <li class="nav-item"><a data-toggle="tab" href="#teachers" role="tab">المدرب</a></li>
<!--                            <li class="nav-item"><a data-toggle="tab" href="#reviews" role="tab">الاراء</a></li>
                            -->
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="benefit" role="tabpanel">
                                <div class="benefit-content">
                                    {!! $item->features !!}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="teachers" role="tabpanel">
                                <div class="courses-teachers">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                            <div class="single-teacher mt-30 text-center">
                                                <div class="teacher-social">

                                                </div>
                                                <div class="teacher-image">
                                                    <a href="{{url("/trainer-details/$item->trainer_id")}}">
                                                        <img src="{{@$item->trainer->dash_image}}" width="270px" height="310px" alt="teacher">
                                                    </a>
                                                </div>
                                                <div class="teacher-content">
                                                    <h4 class="name text-right">{{@$item->trainer->dash_name}}
                                                        </h4>
                                                    <br>
                                                    <p class="text-right">{{@$item->trainer->dash_currentPosition}}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="reviews" role="tabpanel">
                                <div class="courses-reviews">
                                    <div class="review-wrapper">
                                        <div class="review-star">
                                            <div class="single-review">
                                                <span class="label">Stars 5</span>
                                                <div class="review-bar">
                                                    <div class="bar-inner" style="width: 100%;"></div>
                                                </div>
                                                <span class="value">35</span>
                                            </div>
                                            <div class="single-review">
                                                <span class="label">Stars 4</span>
                                                <div class="review-bar">
                                                    <div class="bar-inner" style="width: 80%;"></div>
                                                </div>
                                                <span class="value">10</span>
                                            </div>
                                            <div class="single-review">
                                                <span class="label">Stars 3</span>
                                                <div class="review-bar">
                                                    <div class="bar-inner" style="width: 55%;"></div>
                                                </div>
                                                <span class="value">8</span>
                                            </div>
                                            <div class="single-review">
                                                <span class="label">Stars 2</span>
                                                <div class="review-bar">
                                                    <div class="bar-inner" style="width: 0;"></div>
                                                </div>
                                                <span class="value">0</span>
                                            </div>
                                            <div class="single-review">
                                                <span class="label">Stars 1</span>
                                                <div class="review-bar">
                                                    <div class="bar-inner" style="width: 0;"></div>
                                                </div>
                                                <span class="value">0</span>
                                            </div>
                                        </div>
                                        <div class="review-point">
                                            <span>4.9</span>
                                        </div>
                                    </div>

                                    <div class="review-form">
                                        <div class="review-rating">
                                            <h5 class="title">أكتب تعلقيك</h5>

                                            <ul id='stars'>
                                                <li class='star' title='Poor' data-value='1'>
                                                    <i class='fas fa-star'></i>
                                                </li>
                                                <li class='star' title='Fair' data-value='2'>
                                                    <i class='fas fa-star'></i>
                                                </li>
                                                <li class='star' title='Good' data-value='3'>
                                                    <i class='fas fa-star'></i>
                                                </li>
                                                <li class='star' title='Excellent' data-value='4'>
                                                    <i class='fas fa-star'></i>
                                                </li>
                                                <li class='star' title='WOW!!!' data-value='5'>
                                                    <i class='fas fa-star'></i>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="review-form-wrapper">
                                            <form action="#">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="single-form">
                                                            <input type="text" placeholder="الاسم بالكامل">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="single-form">
                                                            <input type="email" placeholder="البريد الالكتروني">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="single-form">
                                                            <input type="text" placeholder="رقم الجوال">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="single-form">
                                                            <input type="text" placeholder="العنوان">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="single-form">
                                                            <textarea placeholder="أكتب تعلقيك هنا"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="single-form">
                                                            <button class="main-btn"> تأكيد</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                   
                </div>

                <div class="col-lg-3">
                    <div class="courses-sidebar pt-20">
                        <div class="courses-features mt-30">
                            <div class="sidebar-title">
                                <h4 class="title">تفاصيل الدورة</h4>
                            </div>
                            <ul class="courses-features-items">
                                <li>السعر <strong>{{$item->price}} ريال سعودى</strong></li>
                                <li>المدة <strong>{{$item->duration}}</strong></li>
                                <li>التاريخ  <strong>{{$item->date}} </strong></li>
                            </ul>
                           <!-- <div class="sidebar-btn">
                                <a class="main-btn" href="#">اشترك في الدورة</a>
                            </div>-->
                        </div>

                        <div class="courses-sidebar-banner mt-30">
<!--                            <a href="#">
                                <img src="assets/images/01.png" width="270px" height="310px" alt="">
                            </a>-->
                            <iframe width="270px" height="310px" src="{{$item->link_youtube}}">
                            </iframe>
                        </div>
                    </div>
                </div>
                 <div class="review-form-wrapper courses-details-content">
                        @if($archive==false)
                        <h5 class="title text-right  sub-title">بادر بالتسجيل الآن
                        </h5>
                        <form action="{{url("/register-course")}}"
                              enctype="multipart/form-data" method="post">
                            @csrf
                            <input type="hidden" value="{{$item->id}}" name="course_id">
                            <div class="row">
                                <div class="col-md-6">

                                    <div class="single-form">
                                        <input type="text" name="name" class="form-control"  placeholder="الاسم بالكامل" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-form">
                                        <input type="email" name="email" class="form-control"  placeholder="البريد الالكتروني" required>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="single-form">
                                        <input type="text" name="mobile" class="form-control"  placeholder="رقم الجوال" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="single-form ">
                                        <select name="country_id" class="select1" >
                                            @foreach(\App\Country::get() as $country)

                                                <option value="{{$country->id}}" >{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="single-form">
                                        <button class="main-btn" type="submit"> تأكيد</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        @endif

                    </div>


            </div>
        </div>
    </section>

    <!--====== Courses Details Ends ======-->


@endsection
@section('extra_css')


{{--  <style>

      .select{
          height:50px;
          overflow:scroll;
          overflow-x: hidden;
      }
    </style>--}}
@endsection

