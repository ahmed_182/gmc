@extends('.site.layout.container')
@section('title','الدورات العامة')
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg " style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title"> الدورات العامة</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <section class="blog-page">
        <div class="container">
            <div class="row">
                @foreach ($items as $course)
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog mt-30">
                        <div class="blog-image">
                            <a href="{{url("/courseDetails/$course->id")}}">
                                <img src="{{$course->image}}" alt="blog">
                            </a>
                        </div>
                        <div class="blog-content">
                            <h4 class="blog-title"><a href="{{url("/courseDetails/$course->id")}}">{{$course->name}}</a></h4>
                            <a href="{{url("/courseDetails/$course->id")}}" class="more"><i class="fal fa-chevron-left"></i> المزيد </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <ul class="pagination-items text-center">
                {{$items->links()}}
            </ul>
        </div>
    </section>
    <!--====== Counter Start ======-->

    <div class="counter-area-2">
        <div class="container">
            <div class="counter-wrapper-2 " style="background-image: url({{asset("/assets/site")}}/images/counter-bg-2.jpg);">
                <div class="row">
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30">
                            <span class="counter-count"><span class="count">3652</span> +</span>
                            <p>الطلاب</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30">
                            <span class="counter-count"><span class="count">105</span> +</span>
                            <p>المدرسين</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30">
                            <span class="counter-count"><span class="count">120</span> +</span>
                            <p>الدورات</p>
                        </div>
                    </div>
                    <div class="col-sm-3 col-6 counter-col">
                        <div class="single-counter mt-30">
                            <span class="counter-count"><span class="count">30</span> +</span>
                            <p>الجوائز</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====== Counter Ends ======-->


@endsection
@section('extra_css')
 {{--   <style>
        .footer_sticky_part {
            padding-top: 100px !important;
        }
    </style>--}}
@endsection

