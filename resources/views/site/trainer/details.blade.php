@extends('.site.layout.container')
@section('title',($item->name))
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title"> تفاصيل المدرب</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Teacher Details Start ======-->

    <section class="teacher-details">
        <div class="container">
            <div class="row teachers-row justify-content-center rtl-direction">
                <div class="col-lg-5 col-md-6 col-sm-8 teachers-col">
                    <div class="single-teacher-details mt-50 text-center">
<!--                        <div class="teacher-social">
                            <ul class="social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                            </ul>
                        </div>-->
                        <div class="teacher-image">
                            <a href="#">
                                <img src="{{$item->dash_image}}" alt="teacher">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 teachers-col text-right">
                    <div class="teacher-details-content mt-45">
                        <h4 class="teacher-name">{{$item->dash_name}}</h4>
                        <span class="designation">{{$item->dash_currentPosition}}</span>

                        <br>
                        <br>
                        {!! $item->about !!}

                    </div>
                </div>
            </div>

            <div class="teacher-details-tab">
                <ul class="nav nav-justified" role="tablist">
                    <li class="nav-item"><a class="active" data-toggle="tab" href="#experience" role="tab">الخبرات</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#educational" role="tab">المؤهل العلمى</a></li>
                    <li class="nav-item"><a data-toggle="tab" href="#achievements " role="tab">الدورات المقدمة </a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="experience" role="tabpanel">
                        <div class="row">
                            <br>
                            {!! $item->experience !!}
                        </div>
                    </div>
                    <div class="tab-pane fade" id="educational" role="tabpanel">
                        <div class="row">
                            <br>
                            {!! $item->qualification !!}
                        </div>
                    </div>
                    <div class="tab-pane fade" id="achievements" role="tabpanel">
                        <div class="row">
                            @foreach(\App\Course::where("end_date","<",Carbon\Carbon::now()->format('Y-m-d'))->where("trainer_id",$item->id)->orderBy("date","ASC")->get() as $course)
                            <div class="col-lg-4 col-sm-6">
                                <div class="single-content-tab">
                                    <h4 class="title">{{$course->name}}</h4>
                                    <p>{{$course->breif}}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <!--====== Teacher Details Ends ======-->


@endsection
@section('extra_css')
 {{--   <style>
        .footer_sticky_part {
            padding-top: 100px !important;
        }
    </style>--}}
@endsection

