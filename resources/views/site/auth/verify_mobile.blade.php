@extends('.site.layout.container')
@section('title','التحقق من الحساب')

@section('content')
    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg " style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title">التحقق من الحساب</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->


    <!--====== Login Start ======-->

    <section class="login-register">
        <div class="container">
            <div class="row justify-content-center text-right">
                <div class="col-lg-6">
                    <div class="login-register-content">
                        <h4 class="mb-4">الرجاء التحقق من البريد الالكترونى</h4>
                        <p>لقد تم ارسال رسالة تحقق الى بريدك الالكترونى</p>

                        <div class="login-register-form">
                            <form class="login-form"action="{{url('/verifyaccountWithemail')}}" method="post" enctype="multipart/form-data" >

                                @csrf
                               {{-- <div class="single-form">
                                    <label>البريد الالكتروني</label>
                                    <input type="email" class="form-control" placeholder="{{trans('language.email')}}" name="email" required="">
                                </div>--}}
                                <div class="single-form">
                                    <label>كود التفعيل</label>
                                    <input type="text" class="form-control" placeholder="كود التفعيل" name="passCode" required="">
                                </div>
                                <div class="text-center">
                                    <div id="countdown_div" class="text-center">
                                        ( <span style="color: #ff2222;display: inline" id="countdown"> 60 </span> )
                                    </div>

                                </div>
                                <div class="single-form">
                                    <button class="main-btn btn-block">تأكيد</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Login Ends ======-->




@endsection
@section("extra_js")
    <script>
        $('#confirmMobileCodeForm').on('submit', function (e) {
            e.preventDefault();
            var code = $("#code").val();
            code = toEnNumber(code);
            $("#code").val(code);
            this.submit();
        });
    </script>
    <script type="text/javascript">
        $('#proceed').delay(60000).show(0);
        var seconds = document.getElementById("countdown").textContent;
        var countdown = setInterval(function () {
            seconds--;
            document.getElementById("countdown").textContent = seconds;
            if (seconds <= 0) clearInterval(countdown);
            if (seconds <= 0) {
                $('#countdown_div').addClass('hide');
            }
        }, 1000);
    </script>
@endsection
