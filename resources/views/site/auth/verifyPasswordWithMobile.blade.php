@extends('.site.layout.container')
@section('content')
    <div class="container">
        <div class="row">
            <h1 class="input-text rightSide mobileVal">ادخل الكود المرسل على هاتفك</h1>
            <form id="verifyPasswordWithMobile" class="an-form" autocomplete="off" role="form" method="POST"
                  action="{{ url('verifyPasswordWithMobile') }}">
                <input type="hidden" name="mobile" value="{{$mobile}}">
                @csrf
                @if ($errors->any())
                    <div class="" style="text-align: right; color: #F0758A ; font-weight: bold;">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

{{--                @if(Session::has('alert'))--}}
{{--                    <div class="" style="text-align: right; color: #F0758A ; font-weight: bold;">--}}
{{--                        <ul>--}}
{{--                            <li class="">{{ Session::get('alert') }}</li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                @endif--}}

                <div class="row">
                    {{--  <div class="col-sm-12">
                          <span style="text-align: right;float: right;margin-bottom: 8px; color: #F0758A ; font-weight: bold;" class="userIdError">   </span>
                          <input name="mobile" type="number" class="input-text rightSide mobileVal"
                                 oninvalid="this.setCustomValidity('لابد من ادخال رقم الجوال')" required
                                 oninput="setCustomValidity('')"
                                 placeholder="{{trans('web.mobile')}}" autocomplete="off">
                      </div>--}}
                    <div class="col-sm-12">
                        <input id="code" type="number" name="passCode" class="input-text rightSide mobileVal"
                               oninvalid="this.setCustomValidity('لابد من ادخال كود التحقق')" required
                               oninput="setCustomValidity('')"
                               placeholder="كود التفعيل" autocomplete="off">
                    </div>
                    <div class="col-sm-12">
                        <a href="{{url("/login_site")}}">
                            <p style="text-align: right">الرجوع الي تسجيل الدخول ؟</p>
                        </a>
                    </div>
                </div>
                <div class="text-center">
                    <div id="countdown_div">
                        ( <span style="color: #ff2222;display: inline" id="countdown"> 60 </span> )
                    </div>
                    <a id="proceed" href="#."
                       class="button border   "
                       style="display: none;width: 100%">
                        <i class="sl sl-icon-reload "></i>
                        إعادة ارسال الكود
                    </a>
                </div>
                <input type="submit" class="an-btn an-btn-default large-padding btn-lg" value="التاكد من كود التحقق ">
            </form>
        </div>
    </div>
@stop
@section('extra_css')
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
@stop
@section('extra_js')
    <script>
        $("#proceed").click(function () {
            location.reload();
        });
        $('#proceed').delay(60000).show(0);
        var seconds = document.getElementById("countdown").textContent;
        var countdown = setInterval(function () {
            seconds--;
            document.getElementById("countdown").textContent = seconds;
            if (seconds <= 0) clearInterval(countdown);
            if (seconds <= 0) {
                $('#countdown_div').addClass('hide');
            }
        }, 1000);


    </script>
    <script>
        userId = $("#userId");
        userId.on('keypress', function () {
            filter2 = /^1/;
            if ($(this).val().length > 0) {
                if (filter2.test($(this).val())) {
                    $(this).css('border', '1px solid green');
                    $('.userIdError').text("")
                } else {
                    $(this).css('border', '1px solid red');
                    $('.userIdError').text("- لابد ان يبداء رقم الهويه ب ( 1 ).");
                    $(this).tooltip().mouseover();
                    return false;
                }
            } else {
                $(this).tooltip('disable');
                return true;
            }
            if ($(this).val().length > 9) {
                return false;
            } else {
                return true;
            }
        });


    </script>

    <script>
        document.getElementById('userId').addEventListener('keydown', function (e) {
            if (e.which === 38 || e.which === 40) {
                e.preventDefault();
            }
        });


    </script>
@stop
