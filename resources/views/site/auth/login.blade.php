@extends('.site.layout.container')
@section('title',"تسجيل الدخول - سوق سماكو")

@section('content')
    <div class="container">
        <br>
        <div class="row">
            <div class="col-md-2"></div>

            <div id="dialog_signin_part" class="zoom-anim-dialog col-md-8 col-sm-12 "
                 style="min-width: auto !important;">
                <div class="small_dialog_header">
                    <h3 class="text-center">سوق سماكو</h3>
                </div>
                <div class="utf_signin_form style_one col-md-12">
                    <ul class="utf_tabs_nav">
                        <li class=""><a href="#tab1">تسجيل الدخول</a></li>
                        <li><a href="#tab2 ">انشاء حساب</a></li>
                    </ul>
                    <div class="tab_container alt">
                        <div class="tab_content" id="tab1" style="text-align: right">
                            <form method="post" class="login">
                                <span style="color: #ff2222;font-weight: bold"
                                      class="login_noError  alert-danger"></span>


                                <p class="utf_row_form utf_form_wide_block">

                                    <select style="height: 60px;direction: rtl" class="country_id" name="country_id">
                                        {{-- <option
                                             value="0">{{trans('language.select')}} {{trans('language.country')}}</option>--}}
                                        @foreach(\App\Country::get() as $country)
                                            <option value="{{$country->id}}">{{$country->dash_name}} ({{$country->code}}
                                                )
                                            </option>
                                        @endforeach

                                    </select>
                                </p>

                                <p class="utf_row_form utf_form_wide_block">
                                    <label for="username">
                                        <input type="text" class="input-text rightSide mobileVal" name="mobile"
                                               id="username"
                                               placeholder="رقم الجوال">
                                    </label>
                                </p>
                                <p class="utf_row_form utf_form_wide_block">
                                    <label for="password">
                                        <input class="input-text rightSide passwordVal" type="password" name="password"
                                               id="password"
                                               placeholder="كلمه المرور">
                                    </label>
                                </p>
                                <div class="utf_row_form utf_form_wide_block form_forgot_part"><span
                                        class="lost_password  rightSide"> <a
                                            href="{{url("/forgetPassword")}}">هل نسيت كلمه المرور؟</a> </span>
                                </div>
                                <div class="utf_row_form">
                                    <input type="button" class=" loginBtn button border margin-top-5" name="login"
                                           value="تسجيل الدخول">
                                </div>
                            </form>
                        </div>

                        <div class="tab_content" id="tab2" style="display:none;">
                            <form method="post" class="register">
                                <input type="hidden" class="location" id="location" name="location"
                                       value=" 24.7246242,46.8197233">
                                <input type="hidden" class="address" id="address" name="address" value="">
                                <span style="color: #ff2222;font-weight: bold;float: right"
                                      class="register_noError alert-danger"></span>


                                <p class="utf_row_form utf_form_wide_block">
                                    <label for="username2">
                                        <input type="text" class="input-text rightSide Valname" name="name"
                                               id="username2"
                                               value=""
                                               placeholder="الاسم بالكامل">
                                    </label>
                                </p>

                                <p class="utf_row_form utf_form_wide_block">
                                    <label for="username4">
                                        <input type="text" class="input-text rightSide Valuser_name" name="user_name"
                                               id="username4"
                                               value=""
                                               placeholder="اسم المستخدم ">
                                    </label>
                                </p>


                                {{-- <p class="utf_row_form utf_form_wide_block">

                                     <select style="height: 60px;direction: rtl" class="Valcountry_id" name="country_id">
                                         <option
                                             value="0">{{trans('language.select')}} {{trans('language.country')}}</option>
                                         @foreach(\App\Country::get() as $country)
                                             <option value="{{$country->id}}">{{$country->dash_name}} ({{$country->code}})
                                             </option>
                                         @endforeach

                                     </select>
                                 </p>--}}
                                <p class="utf_row_form utf_form_wide_block">
                                    <label for="mobile2">
                                        <input type="text" class="input-text rightSide Valmobile" name="mobile"
                                               id="mobile2"
                                               value=""
                                               placeholder="رقم الجوال">
                                    </label>
                                </p>
                                <p class="utf_row_form utf_form_wide_block">
                                    <select style="height: 60px;direction: rtl" class="country_id" name="country_id">

                                        <option value="0">اخترالدوله</option>

                                        @foreach(\App\Country::all() as $country)
                                            <option value="{{$country->id}}">{{$country->dash_name}}</option>
                                        @endforeach

                                    </select>
                                </p>

                                <p class="utf_row_form utf_form_wide_block">
                                    <select id="regoin_id" style="height: 60px;direction: rtl" class="regoin_id"
                                            name="regoin_id">
                                        <option value="0">اختر المنطقه</option>
                                    </select>
                                </p>

                                <p class="utf_row_form utf_form_wide_block">
                                    <select id="distract_id" style="height: 60px ;direction: rtl" class="distract_id"
                                            name="city_id">
                                        <option value="0">اختر المدينه</option>
                                    </select>
                                </p>

                                {{--  <p class="utf_row_form utf_form_wide_block">
                                      <select id="city_id" style="height: 60px;direction: rtl" class="city_id" name="city_id">
                                          <option value="0">اختر الحي</option>
                                      </select>
                                  </p>--}}

                                <p class="utf_row_form utf_form_wide_block">
                                    <label for="password1">
                                        <input class="input-text rightSide Valpassword" type="password" name="password"
                                               id="password1"
                                               placeholder="كلمه المرور">
                                    </label>
                                </p>
                                <p class="utf_row_form utf_form_wide_block">
                                    <label for="password2">
                                        <input class="input-text rightSide Valpassword2" type="password"
                                               name="password2"
                                               id="password2"
                                               placeholder="تأكيد كلمه المرور">
                                    </label>
                                </p>
                                <div class="text-right" style="display: flex;flex-flow: row-reverse;">
                                    <input type="checkbox" id="term" name="terms" class="checkbox_check" value="1"
                                           style="width: auto">
                                    <div class="inner" style="    margin-top: 9px; padding-right: 4px;">
                                        <label for="terms" style="display: inline"> أوافق على</label>
                                        <a href="{{url("/usingService")}}" target="_blank" style="color:#ff2222">شروط
                                            الاستخدام</a>
                                    </div>
                                </div>
                                <!--The div element for the map -->
                                {{-- <label
                                     style="text-align: right;margin-bottom: 5px">{{trans('language.select_map_location')}}</label>
                                 <div id="map"></div>--}}


                                <input type="button" class=" registerBtn button border fw margin-top-10" name="register"
                                       value="انشاء حساب">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>

    </div>





@endsection
@section("extra_css")

    <style>
        #main_wrapper {
            overflow: hidden;
        }

        #dialog_signin_part {
            max-width: 741px !important;
        }

        @media (max-width: 767px) {

            .small_dialog_header {
                right: auto !important;
                left: -30px !important;
            }
        }

        @media (max-width: 767px) {
            #dialog_signin_part, #small-dialog {
                max-width: 97vw !important;
            }
        }

        /* Set the size of the div element that contains the map */
        #map {
            height: 400px; /* The height is 400 pixels */
            width: 100%; /* The width is the width of the web page */
        }

        /* start notification style */
        .AllNoti {
            margin-top: 20px;
            padding: 5px 10px;
        }

        .menu_AllNoti {
            text-align: right;
        }

        .menu_AllNoti li {
            margin-top: 10px;
        }

        .menu_AllNoti li i {
            float: right;
            color: #ff2222;
            padding: 0 10px;
        }

        /* end notification style */

        /* start reset pass style */

        .mt-20 {
            margin-top: 20px;
        }

        .cus_wid {
            width: 50%;
        }

        /* end reset pass style */

    </style>
@endsection

@section("extra_js")
    <script>
        $(".loginBtn").click(function () {
            var mobile = $('.mobileVal').val();
            mobile = toEnNumber(mobile);
            var password = $('.passwordVal').val();
            var country_id = $('.country_id').val();
            $.ajax({
                type: 'POST',
                url: '{{url("siteLoginForm")}}',
                data: {
                    _token: "{{csrf_token()}}",
                    country_id: country_id,
                    mobile: mobile,
                    password: password
                },
                success: function (data, status) {
                    if (data.success == false) {
                        $('.login_noError').text(data.message)
                    } else {
                        if (data.status_ == 1) {
                            window.location.reload();
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'خطأ',
                                text: data.message
                            })
                            $('.login_noError').text(data.message)
                        }
                    }
                }
            });
        });
    </script>

    <script>
        $(".registerBtn").click(function () {
            var name = $('.Valname').val();
            var user_name = $('.Valuser_name').val();
            var mobile = $('.Valmobile').val();
            mobile = toEnNumber(mobile);
            var password = $('.Valpassword').val();
            var country_id = $('.country_id').val();
            var city_id = $('.distract_id').val();
            // var location = $('.location').val();
            var address = $('.address').val();

            //or
            // if ( $(this).prop("checked") ){}
            //or
            // if ( $(this).is(":checked") ){}
            if ($('input.checkbox_check').prop('checked')) {

                $.ajax({
                    type: 'POST',
                    url: '{{url("siteRegisterForm")}}',
                    data: {
                        _token: "{{csrf_token()}}",
                        // location: location,
                        address: address,
                        name: name,
                        user_name: user_name,
                        mobile: mobile,
                        password: password,
                        country_id: country_id,
                        city_id: city_id
                    },
                    success: function (data, status) {
                        if (data.success == false) {
                            $('.register_noError').text(data.message)
                        } else {
                            if (data.status_ == 1) {
                                window.location.reload();
                            } else {

                                $('.register_noError').text(data.message)
                            }
                        }
                    }
                });
            }

        });
    </script>
    <script>
        function initMap() {
            var myLatlng = new google.maps.LatLng(24.7246242, 46.8197233);
            var mapOptions = {
                zoom: 9,
                center: myLatlng
            }
            var geocoder = new google.maps.Geocoder;
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            infoWindow = new google.maps.InfoWindow;
            // Try HTML5 geolocation.
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    var marker = new google.maps.Marker({
                        position: pos,
                        map: map,
                        animation: google.maps.Animation.DROP,
                        icon: "{{asset('assets/site/images/mylocationpin.png')}}"
                    });

                    infoWindow.setPosition(pos);
                    infoWindow.setContent('Location found.');
                    infoWindow.open(map);
                    map.setCenter(pos);
                }, function () {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                handleLocationError(false, infoWindow, map.getCenter());
            }

            function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                infoWindow.setPosition(pos);
                infoWindow.setContent(browserHasGeolocation ?
                    'Error: The Geolocation service failed.' :
                    'Error: Your browser doesn\'t support geolocation.');
                infoWindow.open(map);
            }


            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                animation: google.maps.Animation.DROP
            });

            // To add the marker to the map, call setMap();
            marker.setMap(map);

            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(event.latLng);
            });

            function placeMarker(location) {
                marker.setPosition(location);
                console.log(location)
                geocodeLatLng(geocoder, map, location);
                latLngLocation = location.toString().slice(1, -1); // remove ()
                $('#location').val(latLngLocation);
            }

            function geocodeLatLng(geocoder, map, latlng) {
                geocoder.geocode({'location': latlng}, function (results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            address = results[0].formatted_address;
                            $('#address').val(address);
                            $('#pac-input').val(address);
                        } else {

                        }
                    } else {

                    }
                });
            }
        }

    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9Ktpr0NZaCi29VBU_YVg-MCEu8GcbQAg&callback=initMap">
    </script>
@endsection



