@extends('.site.layout.container')
@section('title',"تأكيد الاشتراك فى الدورة")

@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h8  class="title">تأكيد الاشتراك في دورة ({{$course->name}}) </h8>
                </div>
            </div>
        </div>
    </section>



    <!--====== Page Banner Ends ======-->

    <!--====== Login Start ======-->

    <section class="login-register">
        <div class="container">
            <div class="row justify-content-center text-right">
                <div class="col-lg-6">
                    <div class="login-register-content">
                        <h4 class="title"> استكمال الاشتراك في الدورة</h4>
                        @if (\Session::has('error'))
                            <div class="alert alert-danger top-alert alert-icon-left mb-0 fade-message" role="alert">
                                <span style="display: inline" class="alert-icon"><i class="fa fa-ban"></i></span>
                                <p style="display: inline" class="mb-0">{{ \Session::get('error') }}</p>
                            </div>
                        @endif
                        <div class="login-register-form">
                            <form action="{{url("/confirm-register")}}"
                                  enctype="multipart/form-data" method="post" >
                                @csrf
                                <input type="hidden" value="{{$item->id}}" name="user_id">
                                <input type="hidden" value="{{$course->id}}" name="course_id">

                                <div class="single-form ">
                                    <label>الاسم الرباعي*</label>
                                    <input type="text" name="name" class="form-control"  placeholder="الاسم الرباعي" required>
                                </div>
                                <div class="single-form">

                                    <label>الدولة*</label>
                                    <select name="country_id">
                                        @foreach(\App\Country::get() as $country)

                                            <option class="options"  value="{{$country->id}}" >{{$country->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                    <div class="single-form ">
                                        <label> رقم الجوال*</label>
                                        <input type="text" name="mobile" class="form-control"  placeholder="رقم الجوال" required>

                                    </div>

                                <div class="single-form ">
                                    <label>البريد الالكتروني *</label>
                                    <input type="email" name="email" class="form-control"  placeholder="البريد الالكتروني" required >
                                </div>

                                <div class="single-form">

                                    <label>المؤهل الدراسي*</label>
                                    <select name="qualification_id" >
                                            <option class="options"  value=" بكالوريوس" > بكالوريوس</option>
                                            <option class="options"  value=" ماجستير" > ماجستير</option>
                                            <option class="options"  value=" دكتوراه" > دكتوراه</option>
                                            <option class="options"  value=" دبلوم عالي" > دبلوم عالي</option>
                                            <option class="options"  value=" ثانوية عامة" > ثانوية عامة</option>
                                            <option class="options"  value="1" >أخرى</option>

                                    </select>

                                    <input type="text" name="other" class="form-control"  placeholder="المؤهل الغير موجود"   >


                                </div>
                                <div class="single-form ">
                                    <label>رقم الهوية *</label>
                                    <input type="text" name="identity" class="form-control"  placeholder="رقم الهوية"   oninvalid="this.setCustomValidity('رقم الهوية لا يقل او يزيد عن 10 أرقام')"    oninput="setCustomValidity('')" required>
<!--                                    <span style="color: #c21d17;">رقم الهوية لا يقل او يزيد عن 10 أرقام</span>
                                    -->

                                </div>

                                <div class="single-form">
                                    <div class="checkbox">
                                        <input type="checkbox" id="remember" oninvalid="this.setCustomValidity('من فضلك قم بتأكيد البيانات المسجلة')"    oninput="setCustomValidity('')" required >
                                        <label for="remember"><span></span>التأكيد على البيانات المسجلة أعلاه بحيث سيتم استخدامها للشهادة الخاصة بالدورة</label>
                                    </div>
                                </div>

                                <div class="single-form">
                                    <button class="main-btn btn-block" type="submit">تأكيد </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Login Ends ======-->





@endsection
@section('extra_css')

@endsection

@section('extra_js')
    <script language="JavaScript" type="text/javascript" >
        $(function() {

            $('#colorselector').change(function() {
                $('.colors').hide();
                $('#' + $(this).val()).show();
            });
        });

    </script>
@endsection




