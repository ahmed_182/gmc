@extends('.site.layout.container')
@section('title',"نسيت كبمة السر - سوق سماكو")
@section('content')
    <div class="container">
        <div class="row">
            <br>
            <br>
            <div class="col-md-12">
                <h3 class="text-center">من فضلك ادخل رقم الجوال</h3>
            </div>
            @if(session("alert"))
                <div class="col-md-12">
                    <h3 class="text-danger text-right">{{session("alert")}}</h3>
                </div>
            @endif
            <form class="an-form" autocomplete="off" role="form" method="POST"
                  action="{{ url('forgetPassword') }}">
                {{ csrf_field() }}
                <div class="col-md-12">
                    <p class="utf_row_form utf_form_wide_block mt-20">
                        <label for="username">
                            <input type="text" class="input-text rightSide mobileVal" name="mobile"
                                   oninvalid="this.setCustomValidity('لابد من ادخال رقم الجوال')" required
                                   oninput="setCustomValidity('')"
                                   placeholder="{{trans('web.mobile')}}" autocomplete="off">
                        </label>
                    </p>
                </div>

                <div class="col-md-12">
                    <div class="utf_row_form text-center">
                        <input type="submit" class="cus_wid button border margin-top-5" name="send"
                               value="ارسال ">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <br>
@stop

@section('extra_css')
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
@stop
@section('extra_js')
    <script>
        userId = $("#userId");
        userId.on('keypress', function () {

            filter2 = /^1/;

            if ($(this).val().length > 0) {
                if (filter2.test($(this).val())) {
                    $(this).css('border', '1px solid green');
                    $('.userIdError').text("")
                } else {
                    $(this).css('border', '1px solid red');
                    $('.userIdError').text("- لابد ان يبداء رقم الهويه ب ( 1 ).");
                    $(this).tooltip().mouseover();
                    return false;
                }
            } else {
                $(this).tooltip('disable');
                return true;
            }
            if ($(this).val().length == 10) {
                return false;
            } else {
                return true;
            }
        });
    </script>
    <script>
        document.getElementById('userId').addEventListener('keydown', function (e) {
            if (e.which === 38 || e.which === 40) {
                e.preventDefault();
            }
        });
    </script>
@stop
