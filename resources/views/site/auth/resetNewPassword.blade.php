@extends('.site.layout.container')
@section('title',"اعادة تعيين كلمة السر - سوق سماكو")

@section('content')
    <br>
    <br>
    <div class="container">
        <div class="row">
                <h1 class="input-text rightSide mobileVal">تغيير كلمة المرور</h1>
                <form class="an-form" autocomplete="off" role="form" method="POST"
                      action="{{ url('resetnewpassword') }}">
                    {{ csrf_field() }}
                    @if ($errors->any())
                        <div class="" style="text-align: right; color: #F0758A ; font-weight: bold;">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li class="">{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif



                @if(Session::has('alert'))
                        <div class="" style="text-align: right; color: #F0758A ; font-weight: bold;">
                            <ul>
                                <li class="">{{ Session::get('alert') }}</li>
                            </ul>
                        </div>

                    @endif

                    <div class="row">

                        <div class="col-sm-12">
                            <input type="password" name="password" class="input-text rightSide mobileVal"
                                   placeholder="كلمة المرور" autocomplete="off">
                        </div>
                        <div class="col-sm-12">
                            <input type="password" name="r_password"class="input-text rightSide mobileVal"
                                   placeholder="اعادة كلمة المرور" autocomplete="off">
                        </div>
                        <div class="col-sm-12">
                            <a href="{{url("/login_site")}}">
                                <p style="text-align: right">الرجوع الي تسجيل الدخول ؟</p>
                            </a>
                        </div>
                    </div>

                    <input type="submit"
                            class="an-btn an-btn-default large-padding btn-lg" value="ارسال" >
                </form>
            </div>
        </div>

@stop

@section('extra_css')
    <style>
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>
@stop
@section('extra_js')



    <script>
        userId = $("#userId");
        userId.on('keypress', function () {

            filter2 = /^1/;

            if ($(this).val().length > 0) {
                if (filter2.test($(this).val())) {
                    $(this).css('border', '1px solid green');
                    $('.userIdError').text("")
                } else {
                    $(this).css('border', '1px solid red');
                    $('.userIdError').text("لابد ان يبداء الرقم ب 05 في حاله رقم الهاتف او 1 في حاله رقم الهويه .");
                    $(this).tooltip().mouseover();
                    return false;
                }
            } else {
                $(this).tooltip('disable');
                return true;
            }
            if ($(this).val().length == 10) {
                return false;
            } else {
                return true;
            }
        });
    </script>

    <script>
        document.getElementById('userId').addEventListener('keydown', function (e) {
            if (e.which === 38 || e.which === 40) {
                e.preventDefault();
            }
        });
    </script>



@stop
