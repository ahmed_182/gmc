@extends('.site.layout.container')
@section('title',"سياسة الخصوصية")
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg bg_cover" style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title">سياسة الخصوصية</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Courses Details Start ======-->

    <section class="courses-details" style="padding-top: 0px;">
        <div class="container">
            <div class="row flex-row-reverse rtl-direction">
                <div class="col-lg-12">

                    <div class="courses-details-tab">
                        <ul class="nav nav-justified" role="tablist">
                            <li class="nav-item"><a  href="{{url("/e_learning")}}" role="tab">سياسة التعليم الالكتروني</a></li>
                            <li class="nav-item"><a class=" @if (str_contains(url()->full(), '/privacy')) active @endif"  href="{{url("/privacy")}}" role="tab">سياسة الخصوصية</a></li>
                            <li class="nav-item"><a   href="{{url("/integrity")}}" role="tab">سياسة النزاهة الأكاديمية</a></li>
                            <li class="nav-item"><a  href="{{url("/property_rights")}}" role="tab">الالتزام بحقوق الملكية الفكرية</a></li>
                            <li class="nav-item"><a  href="{{url("/cheat")}}" role="tab">سياسة الغش</a></li>

                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="terms" role="tabpanel">
                                <div class="benefit-content">
                                    {!! $item->privacy !!}
                                </div>
                            </div>

                        </div>
                    </div>


                </div>



            </div>
        </div>
    </section>

    <!--====== Courses Details Ends ======-->


@endsection
@section('extra_css')


    {{--  <style>

          .select{
              height:50px;
              overflow:scroll;
              overflow-x: hidden;
          }
        </style>--}}
@endsection

