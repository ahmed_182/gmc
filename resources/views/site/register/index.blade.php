@extends('.site.layout.container')
@section('title','إنشاء حساب جديد')
@section('content')

    <!--====== Page Banner Start ======-->

    <section class="page-banner">
        <div class="page-banner-bg " style="background-image: url({{asset("/assets/site")}}/images/page-banner.jpg);">
            <div class="container">
                <div class="banner-content text-center">
                    <h2 class="title"> إنشاء حساب جديد</h2>
                </div>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->


    <!--====== Login Start ======-->

    <section class="login-register">
        <div class="container">
            <div class="row justify-content-center text-right">
                <div class="col-lg-6">
                    <div class="login-register-content">
                        <h4 class="title">إنشاء حساب جديد</h4>

                        <div class="login-register-form">
                            <form method="post"  action="{{url("/siteRegisterForm")}}" enctype="multipart/form-data" >

                                @csrf
                                <div class="single-form">
                                    <label>{{trans('language.name')}}</label>
                                    <input type="text" class="form-control" placeholder="{{trans('language.name')}}" name="name" required="">
                                </div>

                                <div class="single-form">
                                    <label>البريد الالكتروني *</label>
                                    <input type="email" class="form-control" placeholder="{{trans('language.email')}}" name="email" required="">
                                </div>
                                <div class="single-form ">
                                    <label> رقم الجوال*</label>
                                    <input type="text" name="mobile" class="form-control"  placeholder="رقم الجوال" required>

                                </div>

                                <div class="single-form">

                                    <label>نوع مالك الحساب*</label>
                                    <select name="user_type_id" >
                                        <option class="options"  value="3" > مدرب</option>
                                        <option class="options"  value="2" > متدرب</option>

                                    </select>


                                </div>

                                <div class="single-form">
                                    <label>كلمة المرور</label>
                                    <input type="password" class="form-control" placeholder="{{trans('language.password')}}" name="password" required="">
                                </div>
                                <div class="single-form">
                                    <label>تأكيد كلمة المرور</label>
                                    <input type="password" class="form-control" placeholder="{{trans('language.confirm-password')}}" name="confirm-password" required="">
                                </div>
                                <div class="single-form">
                                    <div class="checkbox">
                                        <input type="checkbox" id="remember" required>
                                        <label for="remember"><span></span>موافقه على الشروط والأحكام</label>
                                    </div>
                                </div>
                                <div class="single-form">
                                    <button class="main-btn btn-block">إنشاء حساب</button>
                                </div>

                                <div class="single-form">
                                    <label>لديك حساب بالفعل ؟</label>
                                    <a href="{{url("/login")}}" class="main-btn main-btn-2 btn-block">تسجيل الدخول</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Login Ends ======-->


@endsection
@section('extra_css')
    {{--   <style>
           .footer_sticky_part {
               padding-top: 100px !important;
           }
       </style>--}}
@endsection

